import { useState } from "react";
import "./App.css";

function App() {
  const initialForm = {
    lt: {
      word: "",
      description: "",
      examples: [["", ""]],
    },
    it: {
      word: "",
      description: "",
      examples: [["", ""]],
    },
  };
  const backEndUrl = "http://localhost:5000/api/lt/post";

  const [formData, setFormData] = useState(initialForm);

  console.log(formData);

  function handleLtChange(event) {
    const { name, value } = event.target;
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        lt: {
          ...prevFormData.lt,
          [name]: value,
        },
      };
    });
  }
  function handleLtExamplesChange(event) {
    const { name, value } = event.target;
    const index = name.split("-")[1];
    const index2 = name.split("-")[2];

    setFormData((prevFormData) => {
      const newFormData = { ...prevFormData };
      newFormData.lt.examples[index][index2] = value;
      return newFormData;
    });
  }

  function addExamplesRow(lang) {
    setFormData((prevFormData) => {
      const newFormData = {
        ...prevFormData,
        [lang]: {
          ...prevFormData[lang],
          examples: [...prevFormData[lang].examples],
        },
      };
      newFormData[lang].examples.push(["", ""]);
      return newFormData;
    });
  }

  function removeExamplesRow(lang) {
    setFormData((prevFormData) => {
      const newFormData = {
        ...prevFormData,
        [lang]: {
          ...prevFormData[lang],
          examples: [...prevFormData[lang].examples],
        },
      };
      newFormData[lang].examples.pop();
      return newFormData;
    });
  }

  function handleItChange(event) {
    const { name, value } = event.target;
    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        it: {
          ...prevFormData.it,
          [name]: value,
        },
      };
    });
  }
  function handleItExamplesChange(event) {
    const { name, value } = event.target;
    const index = name.split("-")[1];
    const index2 = name.split("-")[2];

    setFormData((prevFormData) => {
      const newFormData = { ...prevFormData };
      newFormData.it.examples[index][index2] = value;
      return newFormData;
    });
  }

  async function handleSubmit(event) {
    event.preventDefault();
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData),
    };
    try {
      let res = await fetch(backEndUrl, requestOptions);
      if (!res.ok) {
        alert("We have problems with the connection :(");
        throw new Error(res.statusText);
      }
      const data = await res.json();
      console.log(data);
      setFormData(initialForm);
    } catch (err) {
      console.error("Error:", err);
    }
  }

  return (
    <div className="App">
      <h1>Translation App DB Form</h1>
      <form onSubmit={handleSubmit} className="form">
        <fieldset className="fieldset">
          <legend className="legend">Lithuanian language</legend>
          <h4>Word</h4>
          <input
            type="text"
            onChange={handleLtChange}
            name="word"
            value={formData.lt.word}
          />
          <h4>Description</h4>
          <input
            type="text"
            onChange={handleLtChange}
            name="description"
            value={formData.lt.description}
          />
          <h4>Examples</h4>
          {formData.lt.examples.map((__, index) => {
            return (
              <div className="examples-row" key={index}>
                <input
                  type="text"
                  onChange={handleLtExamplesChange}
                  name={`examples-${index}-0`}
                  value={formData.lt.examples[index][0]}
                />
                <input
                  type="text"
                  onChange={handleLtExamplesChange}
                  name={`examples-${index}-1`}
                  value={formData.lt.examples[index][1]}
                />
              </div>
            );
          })}

          <button
            type="button"
            className="examples-button"
            onClick={() => addExamplesRow("lt")}
          >
            Add one more line
          </button>
          <button
            type="button"
            className="examples-button"
            onClick={() => removeExamplesRow("lt")}
          >
            Remove last line
          </button>
        </fieldset>
        <fieldset className="fieldset">
          <legend className="legend">Italian language</legend>
          <h4>Word</h4>
          <input
            type="text"
            onChange={handleItChange}
            name="word"
            value={formData.it.word}
          />
          <h4>Description</h4>
          <input
            type="text"
            onChange={handleItChange}
            name="description"
            value={formData.it.description}
          />
          <h4>Examples</h4>
          {formData.it.examples.map((__, index) => {
            return (
              <div className="examples-row" key={index}>
                <input
                  type="text"
                  onChange={handleItExamplesChange}
                  name={`examples-${index}-0`}
                  value={formData.it.examples[index][0]}
                />
                <input
                  type="text"
                  onChange={handleItExamplesChange}
                  name={`examples-${index}-1`}
                  value={formData.it.examples[index][1]}
                />
              </div>
            );
          })}

          <button
            type="button"
            className="examples-button"
            onClick={() => addExamplesRow("it")}
          >
            Add one more line
          </button>
          <button
            type="button"
            className="examples-button"
            onClick={() => removeExamplesRow("it")}
          >
            Remove last line
          </button>
        </fieldset>
        <button type="submit" className="submit-btn">
          SUBMIT
        </button>
      </form>
    </div>
  );
}

export default App;
