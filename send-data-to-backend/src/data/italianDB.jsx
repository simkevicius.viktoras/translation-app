export const italianDB = `
**forfettârio, -a** *bdv* fiksúoto dýdþio.

**fórfora** *dkt m* pléiskanos; ***avere la f.*** tur¸ti pléiskanø.

**fòrgia** *dkt m* kãlvës þa¤zdras.

**forgiâre** *vksm* (*fòr-*) **1.** (nu)kãlti; (nu)­kãl­d­inti; **2.** *fig* (su)formúoti.

**forgiatùra** *dkt m* kalôba.

**foriéro, -a** *bdv* (*di qcs*) pranaðãujantis (*kà*).

**fórm|a** *dkt m* **1.** fòrma, pavídalas; (*aspetto*) íðvaizda; ***senza f.*** befòrmis *agg*; ***dare f.*** formúoti; sute¤kti fòrmà; (*plasmare*) sulipdôti; ***prênde­re f.*** ágãuti pavídalà / fòrmà; formúotis; ¨ ***a f.*** (*di qcs*) (*ko*) pavídalo, (*ko*) fòrmos; ***di f. quadrata*** kvadråto fòrmos, kvadråtinis *agg*; ***sotto f.*** (*di qcs*) ka¤p (*kas*); *t.p. þymi bûdà*; **2.** (*ppr. dgs* ***~e***) k¿no fòrmos, figûrâ *sg*; **3.** (*tipo*) fòrma; r¿ðis -ies; (*modo*) b¾das; ***f. di go­verno*** sãntvarka, vaµdymo fòrma; ***f. grave di una malattia*** rimtâ ligõs fòrma; ***f. mentale*** galvósena; ***in f. privata*** privatùs *agg*; *biol* ***f. di vita*** bûtôbë; *mus* ***f. sonata*** sonåtos fòrma; **4*.*** *(espressione*) fòrma; ***f. e contenuto*** tu­rinýs i» fòrma; **5.** (*stampo e sim.*) (kepímo / liejímo *ir pan.*) fòrma; *fig* ***una f. di parmigiano*** parmidþåno ritinýs; **6.** (*condizione fisica*) fòrma; (fízinis) pajëgùmas; ***êssere*** ***in** **f.*** b¿ti gerõs fòrmos; ***non êssere in f., êssere fuori f.*** b¿ti prastõs / blogõs fòrmos; ¨ *kaip* *bdv nkt* ***peso f.*** idealùs svõris; **7.** (*formalitâ*) formalùmai *pl*; etikêto taisýklës *pl*; ***per (la) f.*** formalia¤; dºl akiÿ; ¨ ***salvare la f.*** laikôtis manda­gù­mo; **8.** *gram* fòrma; ***f. attiva (passiva)*** (ne)veikiamóji r¿ðis; ***f. di cortesia*** mandagùmo fòrma; **9.** *gram* (*variante*) fòrma; þõdis.

**formagg(i)éra** *dkt m* s¿rinë (*indas sûriui*).

**formaggìno** *dkt v* sûrìlis; ***f.*** ***fuso*** lôdytas sûrìlis.

**formâggio** *dkt v* s¿ris; ***f. con i buchi*** a­k­ô­tas s¿ris; ***f. caprino / di capra*** oþkõs s¿ris; ***f. olandese (svìzzero)*** olãndiðkasis (ðveicåriðka­sis) s¿ris; ***f. stagionato*** brandíntas s¿ris.

**formâl|e** *bdv* **1.** formalùs; oficialùs; ***protesta f.*** oficialùs protêstas; ***dei rap­por­ti ~i*** formålûs sãnty­kiai; ***êssere molto f.*** smùlkmeniðkai lai­kôtis fòrmos; **2.** (*esteriore*) iðorínis.

**formalìsmo** *dkt v* formalízmas.

**formalit|â** *dkt m nkt* formalùmas; proced¾ros *pl*; ***~a doganali*** mu¤tinës formalùmai; ***sbri­ga­re al­cu­ne ~â*** su­tvarkôti / atlíkti kelís formalumùs; ¨ ***ê solo una f.*** èiâ tík formalùmas.

**formalizzârsi** *vksm* **1.** (*indignarsi*) pasipíkinti, píktintis; **2.** (*far complimenti*) kùklintis.

**formalménte** *prv* formalia¤.

**formâ|re** *vksm* (*fór-*) **1.** (su)formúoti, su­da­rôti; ***f. una fila*** rikiúotis (ñ e¤læ); ***f. una frase*** sudarôti såkiná; ***f. il go­ver­no*** formúoti / sudarôti vy­riausôbæ; ***il consìglio ê ~to da sei membri*** tarôba susídeda íð ðeðiÿ nariÿ, tarôbà sudåro ðeðí naria¤; **2.** (*creare*) sukùrti; (*fondare*) ákùrti; **3.** (*modellare*) (nu)lipdôti; (*dar forma a qcs*) sute¤kti (*kam*) fòrmà; **4.** *fig* (su)­formúoti; (*educare*) (ið)ugdôti; (*addestrare*) apmókyti; ***f. il carâtte­re*** formúoti charåkte­rá; ***f. il personale*** apmókyti personålà.

**> form|ârsi** *sngr* formúotis, susiformúoti, susidarôti, darôtis; (*comparire*) atsirâsti; (*su una superficie*) d¸tis, uþsid¸ti; (*dei frutti*) mêgztis;  ***si ê ~ata la cros­ta sul­la fe­ri­ta*** ðåðas a¹t þaizdõs uþsid¸jo, þaizdâ uþsitrãukë; ***sull'âcqua si ~a il ghiâccio*** a¹t vand­e¹s dìdasi lìdas; ***nel terreno si ê ~ato un avval­lamento*** þìmë á­dùbo; ***sull'intònaco si ê ~ata la muffa*** apipe­líjo ti¹kas.

**formâto 1.** *dkt v* formåtas; dýdis; ***f. tascâbile*** kiðenínis *agg*; ***fotografia f. têssera*** dokume¹tinë núotrauka; ¨ ***f. famìglia*** ðeimôninis *agg* (*apie porcijà, pakuotæ*); **2.** *bdv* (*maturo e sim.*) subréndæs; **3.** *bdv, dlv*: ***êssere f.*** (*da qc*) b¿ti sudarôtam (*ið ko*), susi­d¸ti (*ið ko*); *t.p.* Þ **formâre**.

**formattâre** *vksm inf* **1.** (*dischi*) þénklinti (*diskus*); **2.** (*testo e sim.*) (su)form(at)úoti.

**formattazióne** *dkt m inf* **1.** (*di testo e sim.*) formuõtë; (*l'azione*) (su)form(at)åvimas; **2.** (*di dischi*) þénkli­nimas.

**formazióne** *dkt m* **1.** formåvimas(is)*,* su(si)då­rymas; (*comparsa*) atsiradímas; ***la f. di un ascesso*** p¿linio atsiradímas; ***la f. della persona­litâ*** asmenôbës formåvimasis; *ling* ***f. delle parole*** þõdþiø darôba; ***êssere in (via di) f.*** darôtis, formúotis; **2.** (*creazione*) kûrímas; **3.** *fig* (*educazione*) ùgdymas; (*addestramento*) apmó­kymas; parengímas; instruktåþas; ***corso di f.*** parengia­mâ­sis kùrsas; ***f. culturale*** iðsimóks­li­ni­mas; ***la f. dei giòvani*** jauní­mo mókymas; **4.** *anat, geol* formåcija, darinýs; **5.** *mil* rikiuõtë; ið(si)d¸stymas; ¨ ***in f.*** rikiuõ­tëje; iðsid¸stæ *pl*; **6.** *sport* sudëtís; (*schieramento*) (þaid¸jø) ið(si)d¸sty­mas.

**form|ìca** *dkt m* **1.** *zool* skruzdëlº, skruzdº; ***labo­rioso come una f.*** darbðtùs ka¤p bítë; **2.** *fam*: ***ho le ~che a un piede*** (mãn) nuti»po kója.

**formicâio** *dkt v* skruzdëlônas.

**formichiére** *dkt v zool* skruzd¸da.

**formicolâre** *vksm* (*-m**ì**-*) **1.** [A] (*brulicare*) knibþd¸ti; **2.** [E] (*di parti del corpo*) ti»pti (*apie kûno dalá*).

**formicolìo** *dkt v* **1.** (*brulichio*) knibþd¸­jimas; **2.** (*intorpidimento*) nutirpímas; tirpu­lýs; (*doloroso*) dilgs¸ji­mas.

**formidâbile** *bdv* **1.** (*potente*) galíngas; **2.** (*ecce­zionale*) nuostabùs; áspûdíngas; fantåstiðkas; ***memòria f.*** nuostabí atmintís.

**formìna** *dkt m dimin* formìlë.

**formós|o, -a** *bdv* apvalùs (*apie moters kûno formas*); ***ragazza ~a*** merginâ sù apvalùmais.

**fòrmula** *dkt m* **1.** fòrmulë; þõdþiai *pl*; ***f. augura­le*** link¸jimas; ***f. mâgi­ca*** mågið­ka fòrmulë; ***f. di giuramento*** prîesaikos þõdþiai; ***f. di saluto*** svéikinimosi þõdþiai; *dir **assòlvere con f. piena*** vísiðkai iðtéisinti; **2.** *chim, fis, mat* fòrmulë; ***f. chì­mi­ca*** chêminë fòrmulë; **3.** *fig* (*insieme di criteri e sim.*) fòrma; (*metodo*) metòdas; ***la f. di un programma televisivo*** televízijos laidõs fòrma; **4.** (*motto*) ð¾kis; **5.:** *sport* ***F. uno, F. 1*** Fòrmulë-1.

**formulâre** *vksm* (*fòr-*) **1.** (su)formulúoti; (*esprimere*) iðréikðti; ***f. un augùrio*** palink¸ti, pasakôti lin­k¸jimà; *dir* ***f. un capo d'impu­ta­zione*** patéikti kãltinimà; **2.** (*inventare e sim.*) (su)kùrti; sugalvóti; ***f. un'ipòte­si*** sukùrti hi­potêzæ.

**formulazióne** *dkt m* formuluõtë, apibrëþímas; (*il formulare*) formulåvimas.

**fornâce** *dkt m* krósnis -ies; (*fabbrica*) plýtø gamyklâ.

**fornâio**, **-a** *dkt v/m* kep¸jas -a; duonkepýs -º.

**fornêll|o** *dkt v* **1.** (*t.p. dgs* ***~i***) virýklë; viryklºlë; (*bru­ciatore*) (virýklës) degíklis; ***f. a cherosene*** prímusas; ***f. a gas*** dùjinë virýklë; ***f. elêttrico*** ka¤tvietë, elektrínë plytìlë; ***méttere la péntola sul f.*** uþka¤sti púodà; ¨ ***stare tra i ~i*** sùktis põ virtùvæ; **2.: *il f. della pipa*** pôpkës galvùtë; **3.** (*di stufa e sim.*) pakurâ.

**fornicâre** *vksm* [A] (*fór-*) paleistuvãuti; svetimãuti.

**forn|ìre** *vksm* (*-**ì**sc-*) **1.** (*procurare*) (pa)tiìkti; (*dare*) dúo­ti; (pa)te¤kti; (*rifornire*) (*qc di qcs*) apr¾pinti (*kà kuo*); ***f. un consìglio*** dúoti pata­rímà; ***f. energia elêttrica*** tiìkti elêktros enêr­gijà; ***f. informazioni*** te¤kti infor­måcijà; ***f. un servìzio*** tiìkti påslaugà; ***le pécore ~ìscono lana*** åvys dúoda vílnà; **2.** (*presentare*) pate¤k­ti; ***f. prove schiaccianti*** pate¤k­ti negi¹­èijamus áródymus.

**> fornìrsi** *sngr* (*di qcs*) apsir¾pinti (*kuo*); (*essere cliente*) b¿ti (*ko*) klientù, nuõlat pi»kti (*tam ti­kroje parduotuvëje*).

**fornitóre,** **-trìce** *dkt v/m* tiek¸jas -a.

**fornitùra** *dkt m* tiekímas.

**fórno** *dkt v* **1.** krósnis -ies *femm*; (*di cucina*) órkaitë; ***f. a legna*** mãlkinë krósnis; ***f. a microonde*** mikro­bangÿ krosnìlë; ***al*** ***f.*** kêptas órkaitëje; ***prodotto da f.*** kepinýs; ***méttere in f.*** paðãuti ñ krósná; ád¸ti ñ órkai­tæ; **2.** (*panificio*) kepyklâ; **3.** *tecn* krósnis -ies *femm*; ***f. crematòrio*** krematò­riu­mas; **4.** *fig* (*luogo cal­do*) pirtís -iìs *femm* (*karðta vieta*), keptùvë.

**fóro I** *dkt v* skylº, skylùtë; (*apertura*) angâ.

**fòro II** *dkt v* **1.** *stor* fòrumas; **2.** *dir* te¤smas.

**fórse** *prv* **1.** gãl; galb¿t; ***f. ver­so se­ra verrâ a piò­ve­re*** ñ våka­rà gãl pradºs lôti; **2.** (*in domande*) a»gi; nêgi; benê; (*per caso*) ka»tais; ***sei f. cie­co?*** a»gi tù åklas?; ***hai f. paura?*** nêgi bija¤?; **3.** *kaip* *dkt v*: ¨ ***méttere in f.*** kélti abejõniø; ve»sti suabejóti; ***la riunione ê in f.*** neãiðku, a» pósëdis ávýks; ***sono in f. se...*** nesù tíkras, a»...

**forsennâto, -a** *dkt v/m* pasiùtæs; pamíðæs.

**fòrt|e** **1.** *bdv* stiprùs (*ir prk*); (*potente*) galíngas; pajëgùs; ***carâttere f.*** stiprùs charåkteris; ***un colpo f.*** stiprùs sm¾gis; *fig* ***un f. bevitore*** díde­lis gëríkas; *fig* ***f. in matemâtica*** gera¤ iðmånantis matemåtikà; ¨ ***f.*** (*di qcs*) ásitíkinæs (*kuo*); ***piatto f.*** pagrindínis påtiekalas; *fig* akce¹tas; ***il sesso f.*** stiprióji lytís; ***maniere ~i*** grîeþtos prîemo­nës; ***dare man f.*** (*a qcn*) pare§ti (*kà*); ***farsi f.*** (*di qcs*) pasinaudóti (*kuo*); ***ê più f. di me*** ne­galiù susilaikôti; *kaip jst* ***f.!, che f.!*** jëgâ!; **2.** *bdv* (*resistente*) tvi»tas, stiprùs; patvarùs; ***colla f.*** stíprûs klija¤; ***economia f.*** tvirtâ ekonòmi­ka; ***una stoffa f.*** patvarùs audinýs; **3.** *bdv* (*in­tenso*) stiprùs; smarkùs; ***caffê f.*** stiprí kavâ; ***dolore f.*** stiprùs ska÷smas; ***luce f.*** ryðkí ðvie­sâ; ***~i pêrdite*** didelí núosto­liai; ***rumore f.*** garsùs triùkðmas; ***salsa f.*** aðt­rùs pådaþas; ***~i sospetti*** su¹kûs átarímai; ***un f. vento*** smarkùs / stiprùs v¸jas; ¨ ***parole ~i*** ðiu»­kðtûs þõdþiai; **4.** *bdv* (*grande*) dídelis; (*importante*) svarbùs; ***una f. somma (di denaro)*** dídelë sumâ; ***tâglia f.*** dídelis dýdis; (*di qcn*) drûtuõlis; **5.** *prv* (*con forza*) stipria¤; sma»kiai; ***abbracciare f.*** stip­ria¤ apkabínti; ***piòvere f.*** sma»kiai lôti; **6.** *prv* (*saldamente*) tvirta¤; ***tenersi f.*** tvirta¤ laikôtis; **7.** *prv* (*velocemente*) gre¤tai; ¨ ***andare f.*** b¿ti laba¤ populia­riãm; b¿ti a¹t bangõs; **8.** *prv* (*non piano*) ga»siai; ***parlare f.*** ga»siai kalb¸ti; ¨ ***puoi dirlo f.!*** galí nepåsakoti!; i» dãr ka¤p!; **9.** *prv* (*posposto: assai*) laba¤; ***dubitare f.*** laba¤ abejóti; ***brutto f.*** ganâ (*labai*) negraþùs; **10.** *dkt v nkt* stiprióji pùsë; ***non ê il mio f.*** ta¤ nê mâno sritís; nelaba¤ mãn sìkasi (*tam tikroje srityje*); **11.** *dkt v* (*fortezza e sim.*) fòrtas; **12.** *dkt v/m* (*persona forte*) stipruõlis -ë.

**forteménte** *prv* (*molto*) laba¤; *t.p.* Þ **fòrte 9.**

**fortézza** *dkt m* **1.** (*roccaforte*) tvirtóvë; **2.** (*d'ani­mo*) tvirtùmas.

**fortificâre** *vksm* (*-t**ì**-*) **1.** (su)stíprinti; (su)­tvírtinti; (uþ)gr¿dinti; **2.** *mil* átvírtinti.

**fortificazióne** *dkt m* átvírtinimas; fortifikåcija.

**fortilìzio** (*dgs* -zi) nedídelë tvirtóvë.

**fortìno** *dkt v* nedídelis fòrtas; redùtas.

**fortùito,** **-a** *bdv* atsitiktínis; ***un incontro f.*** atsitiktínis susitikímas.

**fortùna** *dkt m* **1.** (*la dea bendata*) Fortûnâ; ***la f. ci arride*** Fortûnâ mùms ðýpsosi; ¨ ***tentare la f.*** mëgínti lãimæ; *kaip jst* ***buona f.!*** sëkmºs!; gerõs  klotiìs!, **2.** (*buona sorte*) sëkmº; lãi­më; (*successo*) pasisekímas; ***colpo di f.*** netik¸­ta sëkmº; ***hanno avuto f.*** jîems pasísekë; ***la f. ê dalla mia*** mãn sìkasi; ¨ ***per*** ***f., f. che...*** lãi­mei... ; (visâ) lãimë (, kâd...); ***cercar f.*** ieðkóti lãimës; ***fare f.*** pralõbti; ***non ho la f. di conó­scerlo*** dëjâ, jõ nepaþïstu; netur¸jau lãimës sù juõ susipaþínti; *flk* ***cias­cu­­no ê artêfi­ce del­la pròpria f.*** kiek­vîenas þmo­gùs sâvo lãimës kãlvis; *kaip jst* ***che f.!*** nâ, i» sìkasi!; **3.** tu»tai *pl*; ¨ ***costare una f.*** kainúoti milijonùs; **4.:** ¨ ***atterrâggio di f.*** pri­verstínis nutûpímas; ***con mezzi di f.*** iðsive»èiant (*nesant reikiamø priemoniø*); ka¤p nórs; ***fare una riparazione di f.*** ka¤p nórs aptaisôti.

**fortunâle** *dkt v* ðtòrmas, smarkí audrâ j¿roje.

**fortunataménte** *prv* lãimei; visâ lãimë; ***f. per te*** tâvo lãimei / lãimë.

**fortunâto,** **-a** *bdv* **1.** (*di qcn*) tùrintis lãimës; laimíngas; ***sei*** ***stato** **f.*** tãu pasísekë; **2.** (*di qcs*) (*fausto; propizio*) laimíngas;  ***il mio nùmero f.*** laimíngas ska¤èius; **3.** (*di qcs*) (*che riesce bene*) sëkmíngas; ***tentativo f.*** sëkmíngas ba¹dymas.

**fortunóso, -a** *bdv* nus¸tas nelaimíngø atisiti­kímø; pérmainingas.

**forùncolo** *dkt v med* furùnkulas, ðùnvotë.

**fòrz|a** *dkt m* **1.** jëgâ; stiprôbë; (*vigore*) pajëgù­m­as; (*intensitâ*) stiprùmas; (*potenza*) galiâ; ***f. fìsica*** fízinë jëgâ; ***f. di volontâ*** vålios galiâ; ***f. della natura*** stíchija, ga¤valas; ***la f. dell'abitù­dine*** ïproèio jëgâ; ***la f. di un'esplosione*** sprogímo jëgâ; ***con tutte le pròprie ~e, a tutta f.*** íð visÿ jëgÿ; visomís íðgalëmis; ***êssere senza ~e*** netur¸ti jë­gÿ; ***ricórrere alla f.*** griìbtis jëgõs; ***riméttersi in ~e*** atgãuti jëgâs; ***usare la f.*** naudóti jºgà; *mil* ***êssere in f.*** (*a qc*) b¿ti paskirtãm (*á kà*); *dir* ***per câuse di f. maggiore*** dºl nenugalimõs jëgõs aplinkôbiø; ¨ ***a f.*** (*di (far) qcs*) *þymi priemonæ*; nuõlat (*kà darant*); ***con f.*** stipria¤; ***a f., di f., con la f.*** jëgâ, pe» jºgà; pe» prîevartà; (*contro voglia*) nenóromis; *dir* ***in f.*** (*di qcs*) pagaµ (*kà*); rìmiantis (*kuo*); ***per amore o per f.*** nórom nenórom; ***per f. (di cose)*** þínoma; (*non sorprende*) nenuostabù; (*inevitabilmente*) neiðvéngiamai; ***fare f.*** stipria¤ spãusti (/ trãukti *ir pan.*); *flk* ***l'unione fa la f.*** vienôbë -- stiprôbë; *kaip jst* ***bella f.!, per f.!*** dídelis èíâ dãiktas!; *kaip jst* ***che f.!*** jëgâ!; *kaip jst* ***f. Inter!*** pirmýn, Ínterai!; **2.** (*t.p.* ***f. d'ânimo***) (dvåsios) stiprôbë; (*coraggio*) dràsâ; (*tenacia*) atkaklùmas; tvirtùmas; ***con f.*** tvirta¤; ***contare sulle pròprie ~e*** kliãutis sâvo jëgomís; ¨ ***fare f.*** (*a qcn*) padrçsinti (*kà*); ***farsi f.*** pasidrçsinti; *kaip jst* ***f.!*** nâgi!; dràsia÷!; **3.** *econ*: ***f. lavoro*** dãrbo jëgâ; ***~e produttive*** gamôbinës jºgos; **4.** *fis*: ***f. centrìfuga*** iðcentrínë jëgâ; ***f. di gravitâ*** su¹kis, su¹kio jëgâ; ***f. di inerzia*** inêrcijos jëgâ; ***f. motrice*** våromoji jëgâ; **5.:** *dgs **~e*** påjëgos; ***~e*** ***armate*** ginklúotosios påjëgos; ***~e dell'òrdine*** (*t.p. vns* ***la f. pùbblica***) polícija (*policininkai ir karabinieriai*); ***~e polìtiche*** polítinës jºgos.

**forzâre** *vksm* (*fòr-*) **1.** iðlãuþti; jëgâ atidarôti; ***f. una porta (la serratura)*** iðlãuþti durís (spý­nà); **2.** (*sforzare*) pe» jºgà sùkti (/ kíðti / trãukti / ve»sti *ir pan.*); (*accelerare*) spa»tinti; ***f. il motore*** spãusti varíklá; ***f. un tappo*** kíðti ka§ðtá pe» jºgà; *fig **f. gli eventi*** forsúoti ïvykius; *fig* ***f. il senso di un discorso*** iðkre¤pti þõdþiø pråsmæ; *fig* ***f. i tempi*** paspa»tinti te§pà; **3.** *mil* forsúoti; (*sfondare*) pralãuþti; ***f. un assédio*** prasilãuþti / prasimùðti íð apsuptiìs; **4.** (*obbligare*) (*a far qcs*) (pri)ve»sti (*kà daryti*); spãusti; ***f. il fìglio a studiare*** spãusti va¤kà mókytis; *fig* ***f. la pròpria natura*** e¤ti priìð prígimtá.

**forzât|o,** **-a** **1.** *bdv* priverstínis; ***lavori*** ***~i*** priverèiamîeji darba¤; kåtorga; ***sosta ~a*** priverstínis susto­­­jímas; **2.** *bdv* (*innaturale*) dirbtínis, nenatûralùs; ***sorriso f.*** priverstínë / dirbtínë ðýpsena; **3.** *bdv fig* prítemptas; **4.** *dkt v* kåtorgininkas.

**forziére** *dkt v* skryniâ (*vertybëms*).

**forzóso, -a** *bdv dir, econ* priverstínis.

**forzùto, -a** *bdv* jëgíngas; stiprùs.

**foschìa** *dkt m* ¿kana, r¾kana.

**fó|sco,** **-a** *bdv* **1.** gûdùs; tamsùs; (*di cielo e sim.*) niûrùs; ¨ ***dipíngere a tinte ~sche*** nupiìðti li¾dnà va¤­zdà; **2.** *fig* niûrùs, tamsùs; ***pensieri ~schi*** ta§sios mi¹tys.

**fosfâto** *dkt v chim* fosfåtas.

**fosforescénte** *bdv* ðvýtintis (*dël fosforescenci­jos*); ðviì­èiantis tamsojê; fosforínis.

**fòsforo** *dkt v chim* fòsforas.

**fòss|a** *dkt m* **1.** duobº; (*fosso*) griovýs; ***la f. dei leoni*** li¿tø duobº; ***f. dell'orchestra*** orkêstro duobº; *anat **~e nasali*** nósies ïdubos; *tecn* ***f. biològica*** núotekø biològinio vålymo árenginýs; **2.** (*tomba*) kåpas; ***f. comune*** be¹dras kåpas; ***colmare una f.*** supílti kåpà; ***scavare la f.*** (ið)kâsti duõbæ (*ir prk*); ¨ ***ave­re un pie­de nel­la f., êssere con un piede nella f.*** b¿ti vîena kója karstê; *flk* ***del senno di poi son piene le ~e*** kiekvîenas gudrùs põ la¤ko; **3.** *geol, geogr* (*t.p. **fossa oceânica***) povandenínis lovýs, povandenínë ïduba.

**fossâto** *dkt v* (*anche di castello*) griovýs (*t.p. ap­link pilá*).

**fossétta** *dkt m* duobùtë, duobùkë.

**fòssile 1.** *bdv* iðkastínis, fosílinis; ***carbone f.*** ak­me¹s a¹glys *pl*; ***combustìbile f.*** iðkastínis kùras; **2.** *bdv fig* prieðtvanínis; **3.** *dkt v* (*t.p. **reperto f.***) fosílija, (suak­men¸jusi) íðkasena; **4.** *dkt v fig* (*di qcn*) prieðtva­níniø pa­þiûrÿ þmogùs.

**fòsso** *dkt v* griovýs, griovìlis; ***fi­ni­re in un f.*** (*con l'auto e sim.*) áva­þiúoti ñ griõvá.

**fòto** *dkt m nkt* Þ **fotografìa 1.**

**foto-** *prieðdëlis:* *pirmasis sudurtiniø þodþiø dëmuo, rodantis sàsajà su ðviesa arba su fotografija, pvz.,* fotoservìzio, fotostâtico, fototeca *ir t. t..*

**fotocâmera** *dkt m* (*t.p. **f. digitale***) skaitmenínis fotoaparåtas.

**fotocêllula** *dkt m tecn* fotoeleme¹tas.

**fotocòpia** *dkt m* kòpija (*ir prk*), fotokòpija; ***fare una f.*** (*di qcs*) padarôti (*ko*) kòpijà.

**fotocopiâre** *vksm* (*-cò-*) (*qcs*) kopijúoti (*kà*), padarôti (*ko*) kòpijà / kòpijas.

**fotocopiatrìce** *dkt m* kopijåvimo aparåtas.

**fotofinish** [-&fini^] *dkt v nkt* fotofíniðas; ¨ ***al f.*** pa­skutínæ akímirkà.

**fotogénico,** **-a** *bdv* fotogêniðkas.

**fotografâre** *vksm* (*-tò-*) **(**nu)fotografúo­ti; padarôti (*kam*) núotraukà / núotraukas.

**fotografìa** *dkt m* **1.** núotrauka, fotogråfija; ***fare una f.*** padarôti núotraukà; ***la f. ê (venuta) mossa*** núotrauka iðºjo neryðkí; susilîejo va¤zdas; **2.** (*tecnica*) foto­gråfija; ***direttore della f.*** operåtorius statôto­jas; **3.** *fig* (*rappresentazione di qcs*) pavéiks­las; **4.** *fig* (*di qcn*) kòpija.

**fotogrâfic|o,** **-a** *bdv* fo­togråfinis; fotogråfijos; fotogråfiðkas; ***filtro f.*** fo­togråfinis fíltras; ***mâcchina*** ***~a*** fotoaparåtas; ***mostra ~a*** fotogråfijos parodâ; ***servìzio f.*** fotoreportåþas; *fig* ***memòria ~a*** fotogråfiðkai tikslí atmintís, fotogråfiðka atmintís.

**fotògrafo,** **-a** *dkt v/m* fotogråfas -ë.

**fotogrâmma** *dkt v* (*di pellicola*) kådras.

**fotomodêllo, -a** *dkt v/m* fotomòdelis *masc*.

**fotomontâggio** *dkt v* fotomontåþas.

**fotosìntesi** *dkt m biol* fotosi¹tezë.

**fóttere** *vksm volg* **1.** *tr* písti; ¨ ***vai a farti f.!*** uþsi­písk!; **2.** *intr* [A] krùðtis, pístis; **3.** *tr fig fam* ap­mãuti; **4.** *tr* *fig fam* (*rubare*) nuknísti, nudþiãuti.

**fottùto, -a** *bdv volg* **1.** (*dannato*) sùpistas; **2.** (*finito*) (pra)þùvæs; ***sono f.*** mãn gålas.

**foulard** [fu&l2_] *dkt v nkt* skarìlë, skepetãitë; ka­klåskarë.

**fra I** *prlk* **1.** (*di luogo, anche fig*) (*qcs*) ta»p (*ko*) (*ir prk*); ***fra i boschi*** ta»p miðkÿ; *fig* ***la pace fra i pòpoli*** taikâ ta»p tautÿ; **2.** (*indicando distanza, intervallo*) ta»p (*ko*); (*dopo*) põ (*ko*), ùþ; ***fra le due e le tre*** ta»p antrõs i» treèiõs valandõs; ***fra*** ***un** **minuto*** põ / ùþ minùtës; **3.** (*di relazione*) ta»p (*ko*); ***fra*** *(**di)** **noi*** ta»p m¿sø; ***chi fra*** ***di** **voi?*** kâs íð j¿sø?; **4.** (*partitivo*) íð (*ko*); ta»p; ***fra tutti il migliore*** (íð) visÿ geriãusias; **5.** (*con valore cumulativo*): ***fra una cosa e l'altra ho perso la mattinata*** pe» kelís dãrbus su­gaiða÷ vísà rôtà; *t.p.* Þ **tra**.

**fra II** *dkt v nkt* brólis (*apie vienuolá*).

**frac** [frak] *dkt v nkt* fråkas.

**fracassâre** *vksm* (su)dauþôti, sukúlti; sulãuþyti; suknìþinti; ***f. una finestra*** iðda÷þti lãngà; ***f. la mâcchi­na*** sudauþôti maðínà; ***f. la testa a qcn*** suknìþinti kãm gãlvà.

    **> fracassârsi** *sngr* **1.** sudùþti; **2.** (*qcs*) susilãu­þyti.

**fracâsso** *dkt v* **1.** triùkðmas; bildes­ýs; tre¹ks­m­as; **2.** *fam* galôbë; ***un f. di soldi*** krûvâ pinigÿ.

**frâdicio, -a** *bdv* paþliùgæs; pér­mir­kæs, pérðlapæs; ***ba­gnato f.*** kiaura¤ pérmirkæs; ***ubriaco f.*** gírtas ka¤p p¸das.

**frâgile** *bdv* **1.** trapùs (*ir prk*); duþùs; ***êssere molto f.*** gre¤tai / lengva¤ d¾þti; **2.** *fig* (*delicato*) glìþ­nas, trapùs; (*debole*) siµpnas.

**fragilitâ** *dkt m* **1.** trapùmas (*ir prk*); duþùmas; **2.** *fig* (*delicatezza*) gleþnùmas, trapùmas; (*debo­lezza*) silpnù­m­as; ***la f. del­la natura umana*** þmoga÷s prigimtiìs trapùmas.

**frâgol|a** *dkt m* bråðkë; ***gelato alla f.*** bråðkiniai leda¤; ***marmellata di ~e*** bråð­kiø uogiìnë; *bot* ***f.*** ***di** **bosco*** þémuogë.

**fragóre** *dkt v* gria÷smas; tre¹ksmas; dundesýs; ***il f. del tuono*** perk¿no gria÷smas.

**fragorós|o, -a** *bdv* griausmíngas, audríngas; ***u­na cascata ~a*** ðniõkðèiantis krioklýs.

**fragrânte** *bdv* kvapnùs; (*solo del cibo*) gardþia¤ kvìpiantis, aromatíngas.

**fragrânza** *dkt m* kvapnùmas; (*aroma*) aromåtas.

**frainté|ndere*** *vksm* nê ta¤p suprâsti, klaidíngai su­prâsti; ***sono stato ~so*** manê nê ta¤p supråto; ***non ~ndermi*** nesuprâsk man³s klaidíngai.

**frammentârio,** **-a** *bdv* fragme¹tiðkas; nevísas.

**framménto** *dkt v* **1.** núolauþa; (*scheggia*) skevél­dra; (*coccio*) ðùkë; **2.** *fig* fragme¹tas.

**frammìsto, -a** *bdv* (*a qcs*) sumaiðôtas (*su kuo*).

**frâna** *dkt m* **1.** griûtís -iìs; núoðliauþa; **2.** *fig* (*di qcn*) nevýkëlis -ë, nìtikða *com*.

**franâre** *vksm* [E] (su)gri¿ti; (*all'interno*) ágri¿ti.

**francaménte** *prv* **1.** atvira¤; **2.** atvira¤ kaµbant.

**francescâno, -a** **1.** *bdv* pranciðkõnø; **2.** *dkt v/m* pranciðkõnas -ë.

**francése** **1.** *bdv* Prancûzíjos, pranc¾zø, pranc¾­zið­kas; íð Prancûzíjos; *stor **Rivoluzione f.*** Prancûzíjos revoliù­ci­ja; **2.** *dkt v/m* pranc¾zas -ë; **3.** *dkt v* pran­c¾zø kal­bâ; ***in f.*** pranc¾ziðkai.

**franchézza** *dkt m* atvirùmas; nuoðirdùmas.

**frân|co I,** **-a** *bdv* **1.** nuoðirdùs, åt­vi­ras; ¨ ***farla ~ca*** iðsisùkti; **2.** *comm* neapmu¤­tinamas; ***f. di dogana / di porto*** bemu¤tis *agg*; *kaip prv* frãnko; ***porto f.*** laisvâsis úostas; **3.:** *mil* ***f. tiratore*** snãiperis; *fig polit* pérsimetëlis -ë (*kas slaptai balsuoja prieð savo partijos nurodymus*).

**frânco II** *dkt v* (*la valuta*) frãnkas; ***f. svìzzero*** Ðveicå­rijos frãnkas.

**francobóll|o** *dkt v* påðto þénklas; ***f. da un êuro*** vîeno e÷ro vertºs påðto þénklas; ***collezionare ~i*** ri¹kti påðto þénklus; ***in******collare un f., attaccare un f.*** prikli­júoti påðto þénklà.

**frangénte** *dkt v* **1.** l¿þtanti bangâ; **2.** *fig* keblí padëtís -iìs.

**frangétta** *dkt m* kirpèiùkai *pl*.

**frângia** *dkt m* **1.** kuta¤ *pl*; **2.** (*di capelli*) ki»pèiai *pl*; ***portare la f.*** neðióti kirpèiùs; **3.** *polit* fråkcija; gru­pìlë.

**frangiflùtti** *dkt v nkt* bangólauþis.

**franóso, -a** *bdv*  nepatvarùs (*apie uolienà, þemës sluok­sná*); slankùs.

**frantòio** *dkt v* (*l'edificio*) aliìjaus spau­dyklâ; (*la macchina*) alývuogiø spaustùvas.

**frantumâre** *vksm* (*-tù-*) (su)dauþôti, su­kùlti; ið­dau­þôti; ***f. una brocca*** su­kùlti àsõtá; *fig* ***f. i sogni di qcn*** sugriãuti kienõ svajonês.

**> frantumârsi** *sngr* (su)dùþti (ñ ðukês); (*sbri­ciolar­si*) (su)­byr¸ti.

**frantùmi** *dkt v dgs* ðùkës, ðipulia¤; ***in f.*** sudùþæs -usi; ***mandare in f.*** iðdauþôti, iðda÷þti; sudau­þôti ñ ðukês / ñ ðípulius (*ir prk*); ***il vaso ê in f.*** vazâ sudùþusi / sudauþôta.

**frappê** *dkt v nkt* (pîeno i» lìdo) kokte¤lis.

**frappórre*** *vksm* *fig*: ***f. ostâcoli*** su­da­rôti kli¿èiø.

> **frappórsi** *sngr* ásikíðti; ***f. tra due litiganti*** ásikíðti ta»p dviejÿ besigi¹èijanèiø.

**frasârio** *dkt v* **1.** (profêsinë) kalbâ; **2.** (*manualetto*) pasikalb¸jimø knygìlë.

**frâsca** *dkt m* (lapúota) ðakâ, lapínë; ¨ ***saltare di pa­lo in f.*** ðokin¸ti nuõ vienõs têmos priì kitõs.

**frâse** *dkt m* **1.** sakinýs; fråzë; *gram* ***f. sémplice*** vientisínis sakinýs; **2.** (*locuzione*) pósakis; ¨ ***f. fatta*** sustabar¸jæs pósakis, ðta§pas; **3.** *mus* (*t.p.* ***f. musicale***) fråzë.

**fraseologìa** *dkt m* frazeològija.

**frâssino** *dkt v bot* úosis.

**frastagliâto, -a** *bdv* dantôtas, raiþôtas.

**frastornâre** *vksm* (*-stór-*) apku»tinti, pritre¹kti.

**frastornâto, -a** *bdv* apspa¹gæs -usi.

**frastuòno** *dkt v* klegesýs; ga÷smas.

**frâte** *dkt v* vienuõlis; ***f. domenicano*** dominikõ­nas; ***f. francescano*** pranciðkõnas; ***farsi f.*** ástó­ti ñ vienuolônà.

**fratellânza** *dkt m* brolôbë.

**fratellâstro** *dkt v* ïbrolis.

**fratêll|o** *dkt v* **1.** brólis (*ir prk*); ***mio f.*** mâno bró­lis; ***f. maggiore (minore)*** vyrêsnis (jaunêsnis) brólis; ***il f.*** ***maggiore (minore)*** vyresnýsis (jau­nesnýsis) brólis; ***~i gemelli*** bróliai dvynia¤; *med* ***~i siamesi*** Siåmo dvynia¤; ***hai ~i o so­relle?*** a» turí bróliø a» seserÿ?; ***Nino e Ada sono ~i*** Nínas i» Adâ yrâ brólis i» sesuõ; **2.** *rel* (*t.p.* ***f. in Cristo***) brólis (Krístuje).

**fraternitâ** *dkt m* Þ **fratellânza**.

**fraternizzâre** *vksm* [A] broliãutis; (*farsi amico*) susibièiuliãuti. 

**fratêrno,** **-a** *bdv* bróliðkas (*ir prk*); ***amore f.*** bró­liðka méilë; ***legame f.*** brolýstës ryðýs.

**fratricìda** *dkt v/m* brolþudýs -º.

**frâtta** *dkt m* tankmº, tankumônas.

**frattâglie** *dkt m dgs* pla÷èke­pe­niai.

**frattânto** *prv* tuõ metù, tuõ tãrpu.

**frattémpo** *dkt v*: ¨ ***nel** **f.*** tuõ tãrpu; pe» t± la¤kà; ***tu chiami l'a­scensore e nel f. io chiudo la porta*** âð uþra­kínsiu, õ tù iðkviìsk líftà; kõl âð rakinù, tù iðkviìsk líftà.

**frâtto, -a** *bdv mat*: ***sei f. due*** ðeðí padalínti íð dviejÿ; ***venti d. dieci*** dvídeðimt padalínta íð dìðimt.

**frattùra** *dkt m* **1.** *med* l¾þis; lûþímas; ***f. esposta (mùltipla)*** åtviras (dau­gínis) l¾þis; **2.** *fig* skilímas; l¾þis; ***c'ê stata una f. all'interno del partito*** pãrtija suskílo.

**fratturâre** *vksm* (*-tù-*) *med* sulãuþyti.

**> fratturârsi** *sngr med* **1.** *intr* (su)l¾þti; **2.** *tr* su­si­lãuþyti.

**fraudolént|o, -a** *bdv* **1.** (*di qcs*) apgavíkiðkas; *dir* ***ban­­carotta ~a*** tôèinis bankròtas; **2.** (*di qcn*) sùktas.

**frazionâre** *vksm* (*-zió-*) (su)skírstyti, (pa)­dalôti; (*un terreno*) (ið)parceliúoti.

**frazióne** *dkt m* **1.** *mat* trùpmena; ***f. decimale*** de­ðimta¤në trùpmena; **2.** (*parte*) dalís -iìs; dalìlë; ***una f. dell'elettorato*** elektoråto dalís; ***per una f. di secondo*** akímirkà; **3.** (*centro abitato*) „fråkcija" (*Italijoje: gyvenvietë, neturinti savivaldos*); @ seniûnijâ; **4.** *polit* fråkcija; grupuõtë; **5.** *sport* estafêtës etåpas.

**freâtico, -a** *bdv geol* gruntínis.

**fréc|cia** *dkt m* **1.** strëlº; ***scoccare una f***. paléisti strº­læ; **2.** (*segno grafico*) rodýklë; ***fare una f.*** nupiìðti rodýklæ; **3.** (*cartello stradale*) (kìlio) rodýklë; **4.** (*di auto*) pósûkio lem­pù­të; ***méttere la f.*** ájùngti pósûkio lempùtæ; **5.:** *mil **~ce tricolori*** „trispaµvës strºlës" (*Italijos karinës aeronautikos akroba­tinis pa­da­linys*).

**frecciâta** *dkt m* irònijos strëlº; kandí pastabâ.

**freddaménte** *prv fig* ðalta¤.

**freddâre** *vksm* (*fréd-*) **1.** atðãldyti (*ir prk*); atvësínti; **2.** (*uccidere*) ðaltakra÷jiðkai nuþudôti; **3.** *fig* (*con lo sguardo*) pérverti akimís.

**> freddârsi** *sngr* (at)ðãlti (*ir prk*).

**freddézza** *dkt m* **1.** ðaltùmas; **2.** *fig* ðaltùmas; a­bejin­gùmas; ***con f.*** ðalta¤; **3.** *fig* (*sangue freddo*) ðalta­kraujiðkùmas.

**frédd|o I, -a** **1.** *bdv* ðãltas; ***il mese più f.*** ðalèiãusias m¸nuo; ***piatti ~i*** ðaltí patiekala¤; *stor **guerra ~a*** ðaltâsis kåras; ***diven­tare f.*** (at)ðãlti; ***ê f.*** ðãlta; ***hai le mani ~e*** tâvo ðãltos ra¹kos; ¨ ***colori ~i, tinte ~e*** ðãltos spaµvos; ***dóccia ~a*** ðãltas dùðas; ***a sân­gue f.*** (*spietatamente*) ðaltakra÷jiðkai; *kaip prv* ***sudo f.*** píla ðãltas pråkaitas; **2.** *bdv sport* dãr neapðílæs; ¨ ***a f.*** dãr nesp¸jus apðílti; **3.** *bdv fig* ðãltas; (*arido*) beja÷smis; (*indifferente*) abejíngas; ***un'acco­glienza ~a*** ðãltas priëmímas; ***sguardo f.*** ðãltas þviµgsnis.

**frédd|o II** *dkt v* **1.** ðaµtis; (*gelo*) spéigas; ***i primi ~i*** pirmîeji ðaµèiai; ***che f.!*** ka¤p ðãlta!; ***dor­mire al f.*** miegóti ðaµtyje; ***prêndere*** ***f.*** suðãlti; pérsiðaldyti; ***tremare dal f.*** dreb¸ti íð ðaµèio; ***fa*** ***f.*** ðãlta; *fam* ***fa*** ***un f. cane*** pasiùtusiai ðãlta; ***fa f. fuori?*** a» ðãlta laukê?; ***ho f., sento f.*** mãn ðãlta; ¨ ***a f.*** ðaµtai (*ir prk*); *fig* (*all'istante*) nepasiruõðus; ***non mi fa né caldo né f.*** mãn ne¤ ðiµta, ne¤ ðãlta; ***mi viene f. solo a pensarci*** ma­nê ðiu»pina vîen pagalvójus; **2.** *fig* ðaµtis, ðaltí sãntykiai *pl*.

**freddolóso, -a** *bdv* (laba¤) jautrùs ðaµèiui; ðaltmirýs.

**freddùra** *dkt m* sçmojis (*nebûtinai vykæs*).

**freezer** [&fridzer] *dkt v nkt* refriþeråtorius; ðãl­dymo kåmera; (*congelatore*) ðaldíklis.

**fre|gâre** *vksm* (*fré-*) **1.** (pa)trínti; (*per pulire*) (nu)­ðve¤­sti; ***f. un fiammìfero*** br¸þti / bra÷kti degtùkà; **2.** *fam* (*ingan­nare*) apmãuti; ***non mi ~ghi!*** neap­mãusi!; **3.** *fam* (*rubare*) (nu)knia÷kti, nuðviµpti.

**> freg|ârsene** *ávdþ fam*: ***non me ne ~a nien­te di nien­te*** mãn vískas nusispjãuti; ***non me ne ~a nien­te di lei*** nusispjãuti mãn ñ j±; ***e chi se ne ~a?*** kãm r¾pi?; ***tu ~atene dei suoi consi­gli!*** spjãuk tù ñ jõ patarimùs!

  **>** **fregârsi** *sngr* **1.:** ***f. le ma­ni*** patrínti rankâs (*ir prk*); ***f. gli occhi*** trínti akís; **2.** *fam* pirðtùs nu­dêgti (*apsigauti*).

**fregâta I** *dkt m mar* fregatâ.

**fregâta II** *dkt m* **1.: *dare una f.*** (*a qcs*) patrínti (*kà*); paðve¤sti; **2.** *fam* Þ **fregatùra**.

**fregatùra** *dkt m fam*: ***dare una f.*** apmãuti; ***hai preso una f.*** tavê apmóvë; ¨ *kaip jst* ***che f.!*** ka¤p apmóvë!; sùkèiai!

**fré|gio** *dkt v* **1.** dekoratývinis åpvadas; papuoðímas; **2.** *archit* frízas.

**freménte** *bdv* vírpantis, drìbantis.

**frémere** *vksm* [A] (*fré-*) virp¸ti, dreb¸ti; ***f. di gelosia*** nesitvérti pavydù; ***f. di râbbia*** virp¸ti pykèiù;  ***f. di sdegno*** dreb¸ti íð pasipíktinimo.

**frémito** *dkt v* virpulýs, jaudulýs; virpesýs.

**fren|âre** *vksm* (*fré-*) **1.** *intr* [A] (su)­­stab­dôti; pristabdôti; ***f. bruscamente*** staigâ ­­stab­dôti; ***la mâcchina non ~a*** maðínos stabdþia¤ bloga¤ ve¤kia; *fig* ***il ministro ~a sulla possibilitâ di ridurre le tasse*** minístras nêtiki, kâd galimôbë sumåþinti mókes­èius realí; **2.** *tr* (su)­sta­b­dôti (*ir prk*); (*rallentare*) pristab­dôti (*ir prk*); *fig* ***f. l'inflazio­ne*** pristabdôti infliåcijà; **3.** *tr* *fig* (*tratte­ne­re*) sulaikôti; (*contenere*) (nu)slopín­ti; ***f. le lâ­crime*** sulaikôti åðaras; ***f. la língua*** suvaldôti lieþùvá; ***f. le passioni*** slopínti aistrâs.

**> frenârsi** *sngr* tvãrdytis, susitvãrdyti; susilai­kôti.

**frenât|a** *dkt m* (pri)ståbdymas; ***spâzio di f.*** stå­bdy­mo kìlias; ***evitare brusche ~e*** nestabdôti sta¤giai; ***fare una f.*** paspãusti ståbdá.

**frenesìa** *dkt m* **1.** (*pazzia*) ásiutímas; **2.** (*smania*) aistrâ; månija; (*al gioco*) azãrtas.

**frenéti|co,** **-a** *bdv* pad¾kæs; (*febbri­le*) ka»ðtligiðkas; (*entusiastico*) entuziastíng­as; ***applâusi ~ci*** entuzia­stíngi plojímai.

**frén|o** *dkt v* **1.** stabdýs; (*il pedale*) stabdþiÿ pe­då­­las; ***f. a mano*** ra¹kinis stabdýs; **2.** (*del cavallo*) þçslai *pl*; ***mòrdere il f.*** þçslus kramtôti; *fig* nerímti; ¨ ***senza f., senza ~i*** bê sa¤ko; nesuvãldomas *agg*; ***tenere a f.*** paþabóti, sulaikôti.

**frequentâre** *vksm* (*-quén-*) lankôti(s); ***f. gli amici*** ma­tôtis sù drauga¤s; ***f. un bar*** nuõlat lankôtis barê; ***f.*** ***la** **scuola*** lankôti mokýklà, mókytis mokýkloje.

    **> frequentârsi** *sngr* bendrãuti, matôtis.

**frequentâto,** **-a** *bdv* la¹komas; pílnas þmoniÿ; ***mal f.*** átartinÿ típø la¹komas.

**frequént|e** *bdv* dåþnas; ***piogge ~i*** daþní lîetûs; ***ê (una cosa) abbastanza f.*** ta¤ pasitãiko ganâ daþna¤; ¨ ***di f.*** daþna¤.

**frequenteménte** *prv* daþna¤.

**frequénza** *dkt m* **1.** daþnùmas; ¨ ***con f.*** daþna¤; **2.** (*partecipazione*) lankomùmas; la¹kymas; ***la f. alle urne*** rink¸jø aktyvùmas; ***òbbligo di f.*** privålomas la¹kymas; ***c'ê grande f. dei turisti sulle spiagge*** paplûdimiuosê didelí poilsiãu­tojø srauta¤; **3.** *fis, med* dåþnis; *fis* ***alta (bassa) f.*** (aukðtâsis) þemâsis dåþnis; *med **f. del polso*** pùlso dåþnis.

**frésa** *dkt v tecn* frezâ; (*disco*) pjovímo dískas.

**fresâre** *vksm* (*fré-*) frezúoti.

**fresatóre** *dkt v* frezúotojas.

**fresatrìce** *dkt m tecn* (elektrínë) frezâ.

**freschézza** *dkt m* **1.** ðvieþùmas; ***la f. dei prodotti*** prodùktø ðvieþùmas; **2.** (*dell'aria sim.*) vësù­mas; vësâ, vësumâ; **3.** *fig* (*floridezza*) skaistù­mas; (*vitalitâ*) gai­vùmas.

**frés|co I,** **-a** *bdv* **1.** vësùs; (*rinfrescante*) gaivùs; ***âlito f.*** gaivùs kvåpas; ***âria ~sca*** grônas / gaivùs óras; ***notti ~che*** vºsios nåktys; ***stanza ~ca*** vësùs kam­barýs; ¨ *fam **stai f.!*** nesulãuksi!; **2.** (*di cibi e sim.*) ðviìþias; ***fiori ~chi*** k± tík nuskíntos gºlës; ***for­mâggio (latte) f.*** ðviìþias s¿ris (pîenas); ***frutta ~ca*** ðvieþí va¤siai *pl*; ***pesce f.*** ðviìþios þùvys *pl*; **3.** (*riposato*) pails¸jæs, þvalùs; (*florido*) skaistùs; ***un colorito f.*** skaistí véido óda; ***truppe ~che*** na÷jos påjëgos; ¨ ***f. come una rosa*** vísiðkai þvalùs; ***a mente ~ca*** blaiviù protù; **4.:** ***notìzie ~che*** naujãusios þínios, naujîenos; ***ricordo f.*** dãr gôvas pri-siminí­mas; ***sposi ~chi*** jaunavedþia¤; ***vernice ~ca*** dãr neiðdþi¿væ daþa¤ *pl*; ¨ ***di f.*** nesenia¤; nauja¤; ðvie­þia¤; **5.:** ***f. di lâurea (di studi)*** k± tík ba¤gæs univer­sitêtà (mókslus); ***f. di nòmina*** k± tík íðrinktas.

**frésco II** *dkt v* vësumâ, vësâ; ***il f. della sera*** vå­karo vësâ; ***fa*** ***f.*** vësù; *fam* ***fa freschino*** vësóka; ***conservare al f., tenere in f.*** laikôti vësiojê viì­toje; ***godersi il f.*** m¸gautis vºsuma; ¨ ***con il f., col f.*** ka¤ vësù; ***sbâttere al f.*** ákíðti ñ cýpæ.

**frescùra** *dkt m* vësâ.

**frésia** *dkt m bot* frêzija.

**frétta** *dkt m* skub¸jimas, skubâ; ***senza f.*** bê sku­bõs; nêskubant; ***aver*** ***f.*** skub¸ti; ***fare f.*** (*a qcn*) (pa)skù­binti (*kà*); ***non c'ê f.*** nërâ kõ skub¸ti; ***fai in f.!*** paskub¸k!; ¨ ***di f., in*** ***f.*** skubia¤; pa­skubomís, pasku­bõm; ***in f. e fùria*** pasiùtusiai skubia¤; ***per la f., nella f.*** pe» skùbà; ***che f.!*** kãm ta¤p skub¸ti?

**frettolosaménte** *prv* skubótai.

**frettolós|o,** **-a** *bdv* **1.** (*di qcs*) skubótas; ***conclusioni ~e*** skubótos íðvados; ***saluto f.*** gre¤tas atsisvéikinimas; **2.** (*di qcn*) skùbantis; ***êssere troppo f.*** pe» da÷g skub¸ti.

**friâbile** *bdv* trapùs; (*del terreno*) birùs.

**friggere*** *vksm* **1.** *tr* (ið)kêpti (*riebaluose*); (pa)kì­pin­ti; ¨ ***andare a farsi f.*** niìkais iðe¤ti; ***vai a farti f.!*** e¤k velnióp!; **2.** *intr* [A] (ið)kêpti; (*sfri­golare*) èi»k­ðti, spírgti; **3.** *intr* [A]: *fig* ***f. di râb­bia*** kunkuliúoti pykèiù.

**friggitrìce** *dkt m* gruzdintùvë.

**frigiditâ** *dkt m* frigidiðùmas, lytínis ðaltùmas.

**frìgido, -a** *bdv* frigídiðkas.

**frignâre** *vksm* [A] bliãuti; verkðlénti, zýzti; zirzénti.

**frignâta** *dkt m* zyzímas.

**frignìo** *dkt v* zirzìnimas, zirzesýs.

**frigobar** [-&bar] *dkt v nkt* båras (*spintelë gërimamas*); måþas ðaldytùvas (*ppr. gërimams laikyti*).

**frìgo** *dkt v nkt* Þ **frigorìfero**.

**frigorìfer|o, -a** **1.** *dkt v* ðaldytùvas; ***conservare in f.*** laikôti ðaldytuvê; **2.** *bdv*: ***borsa ~a*** ðãltkrepðis; ***cella ~a*** ðãldymo kåmera; ***vagone f.*** refriþeråtorius.

**fringuéllo** *dkt v zool* kikílis.

**frinìre** *vksm* [A] (*-**ì**sc-*) svi»pti.

**frittâta** *dkt m* kiauðiniìnë; omlêtas; ¨ ***rivoltare / ri­girare la f.*** iðve»sti ñ kítà pùsæ; (*mutare opinione*) iðve»sti kãilá; (*sostenere l'assurdo*) árodin¸ti, kãd júo­da yrâ bãlta; ***la f. ê fatta*** þalâ ja÷ padarôta.

**frittêlla** *dkt m* blýnas, skli¹dis; blynìlis.

**frìtt|o,** **-a** **1.** *bdv, dlv* kìptas (*riebaluose*), íðkep­t­as; ***patate ~e*** grùzdintos bùlvës; ***patatine ~e*** bùlviø traðkùèiai; ¨ ***âria ~a*** tuðèiaþodþiåvi­mas; **2.** *bdv fig* þùvæs; ***siamo ~i*** mìs þùvæ; **3.** *dkt v gastr* kìptas påtiekalas; ***f. misto (di pe­sce)*** keptÿ j¿ros gërôbiø rinkinýs; ***f. di zuc­chine*** kìptos cukínijos.

**frittùra** *dkt m* **1.** kepímas; **2.** Þ **frìtto 3.**

**friulâno, -a** **1.** *bdv* Friùlio; friuliìèiø; íð Friùlio; **2.** *dkt v/m* friuliìtis -ë; **3.** *dkt v* friuliìèiø ðnektâ.

**frivolézza** *dkt m* lengvabûdiðkùmas.

**frìvol|o,** **-a** *bdv* lengvab¾diðkas; nerímtas, le¹­gvas; ***discorsi ~i*** nerímtos kaµbos; ***un tipo*** ***f.*** lengvab¾­dis.

**frizionâre** *vksm* (*-zió-*) (pa)trínti; (*qcs con qcs*) átrínti (*kà kuo*).  

**frizióne** *dkt m* **1.** (pa)trynímas; átrynímas; **2.** *tecn* sãnkaba; (*il pedale*) sãnkabos pedålas / pami­nâ; **3.** *fis* trintís -iìs; **4.** *fig* trintís -iìs, nesutarímas.

**frizzânte** *bdv* **1.** (*dell'aria e sim.*) ga¤viai ðãltas; **2.** *fig* (*vivace*) trôkðtantis enêrgija; pad¾kæs; **3.** (*gasa­to*) gazúotas; (*che fa la schiuma*) putójantis; ***âcqua f.*** gazúotas vanduõ.

**frìzzo** *dkt v* kandùs sçmojis; (*di scherno*) paðaipâ.

**fròcio** *dkt v volg* píderas, hòmikas.

**frodâre** *vksm* (*frò-*) apgãuti; (*più volte*) apgaudin¸ti; átrãukti ñ afêrà.

**fròde** *dkt m* sukèiåvimas; apgãule; aferâ; ***con la f.*** apgãule; *dir* ***f. in commêrcio*** prekôbinis sukèiåvimas; *dir* ***f. fiscale*** mokestínis sukèiå­vimas.

**fròdo** *dkt v*: ***cacciatore di f.*** brakoniìrius; ***câccia (pe­sca) di f.*** brakonieriåvimas.

**frollâre** *vksm* (*frò-*) (su)mínkðtinti (*ppr. mësà*).

**fròll|o, -a** *bdv*: ***pasta ~a*** smëlínë teðlâ.

**frónd|a** *dkt m* (lapúota) ðakâ, lapínë; *dgs* ***le ~e*** låpai, lapijâ *sg*; lajâ *sg*.

**frondóso, -a** *bdv* lapúotas; skarótas.

**frontâle** *bdv* **1.** prieðakínis, prîekinis; frontalùs; íð prîekio; ***attacco f.*** frontalí atakâ; ***parte f.*** prîekinë dalís; ***vista f.*** va¤zdas íð prîekio; ***(scontro) f.*** susi­dûrímas kaktómuða; **2.:** *anat* ***osso f.*** kaktíkaulis.

**frontalìno** *dkt v* (*di autoradio*) automagnetòlos sky­dìlis, automobílinës magnetòlos sky­dìlis.

**frónte 1.** *dkt m* kaktâ; ***corrugare la f.*** sura÷kti kåktà; ¨ ***di** **f.*** prîeðais; (*a qc*) priìð (*kà*), prîeðais; *kaip bdv* prîeðais ìsantis, prîeðingas; ***scrit­to / stam­pa­to in f.*** a¹t kaktõs uþ­raðôta; ***trovarsi di f.*** (*a qcs*) atsidùrti (*kur*); susidùrti (*su kuo*); **2.** *dkt v mil* fròntas; ***al f., sul f.*** frònte; ¨ ***far f.*** (*a qcs*) dorótis (*su kuo*), su­sidoróti; ***far f. agli impegni*** (á)vôkdyti ásipareigó­jimus; ***far f. alle spese*** tur¸ti íðlaidø.

**fronteggiâre** *vksm* (*-tég-*) **1.** prîeðintis (*kam*), pasi­prîeðinti; dr±siai sutíkti (*kà*); *fig* ***f. delle difficoltâ*** dorótis sù sunkùmais; **2.** (*essere di fronte*) (*qc*) b¿ti (/ stov¸ti) prîeðais (*kà*).

**frontespìzio** *dkt v* antraðtínis / titulínis pùslapis.

**frontiér|a** *dkt m* **1.** pasîenis; (*confine*) sîena; ***f. di stato*** valstôbës sîena; ***pas­sare / varcare la f.*** péreiti / ki»sti sîenà; **2.** *fig* (*ppr. dgs **~e***) ríbos.

**frontóne** *dkt v arch* frontònas.

**frónzol|o** *dkt v* (*ppr. dgs **~i***) nereikalínga puoð­me­nâ, niekùtis; *fig* ***senza (troppi) ~i*** tiìsiai ðviìsiai.

**fròtt|a** *dkt m* bûrýs; puµkas; ¨ ***in f., a ~e*** bûria¤s.

**fròttol|a** *dkt m* påsaka; pråmanas; ***(rac)contare ~e*** påsakoti neb¿tus dalykùs.

**frugâle** *bdv* kuklùs; bê prabangõs; pâprastas; ***pasto f.*** kuklùs / pâprastas vaµgis.

**frugâre** *vksm* **1.** *intr* [A] ra÷stis; narðôti; ***f. negli archivi*** ra÷stis põ archyvùs; **2.** *tr* apieðkóti; iðkratôti; ***f. i passeggeri*** apieðkóti keleiviùs; ***f. (in) tutta la casa*** iðkratôti vísà nåmà.

**fruìbile** *bdv* (*da parte di qc*) (*kieno*) naudójamas; gålimas naudóti; ***bene f. da tutti*** tu»tas, kuriuõ visí gåli naudótis.

**fru|ìre** *vksm* [A] (*-**ì**sc-*) (*di qcs*) (gal¸ti) nau­dótis (*kuo*); tur¸ti (*teisæ, pajamas ir pan.*); ***f. di una pensione*** gãuti / tur¸ti pe¹sijà; ***gli studenti ~íscono di sconti*** stude¹tams tãikomos núolaidos.

**frullâre** *vksm* **1.** *tr* (ið)plâkti; **2.** *intr* [A] plasnóti; **3.** *intr* [A] *fig*: ***f. per la testa*** kirb¸ti galvojê.

**frullâto** *dkt v* (pîeno i» va¤siø) kokte¤lis.

**frullatóre** *dkt v* plakíklis.

**frùllo** *dkt v* tãnkus sparnÿ plazdìnimas.

**fruménto** *dkt v* kvieèia¤ *pl*; *bot* kvietýs; ***farina di f.*** kvietíniai míltai *pl*.

**frusciâre** *vksm* [A] (*frù-*) ðlam¸ti; èeþ¸ti; zirz¸ti.

**fruscìo** *dkt v* ðlamesýs; ðlam¸jimas; èeþ¸jimas.

**frùsta** *dkt m* botågas; rôkðtë; ***schioc­ca­re la f.*** pliauk­ð¸ti botagù.

**frustâre** *vksm* (ið)plâkti (sù botagù), (ið)pliìkti (sù rôkðte); èãiþyti, kapóti, lùpti.

**frustât|a** *dkt m* ki»tis (sù) botagù (/ rôkðte), botågo (/ rôkðtës) ki»tis; *dgs* ***~e*** plakímas *sg*.

**frustìno** *dkt v* ðma¤kðtis, ri§bas.

**frùsto, -a** *bdv* nudëv¸tas, nuvãlkiotas (*ir prk*).

**frustrâto, -a 1.** *bdv* (laba¤) nusivôlæs; âpimtas fru­s­tråcijos, frustrúotas; **2.** *dkt v/m* nusivôlëlis -ë.

**frustrazióne** *dkt m* gilùs nusivylímas; fru­s­tråcija.

**frùtta** *dkt m* va¤siai *pl*; ***f. secca*** dþiovínti va¤siai; (*noci e sim.*) rîeðutai *pl*; ***macedònia di f.*** va¤siø salõ­tos; ¨ ***êssere alla f.*** b¿ti prisiba¤gusiam.

**fruttâre** *vksm* **1.** *intr* [A] vêsti vaisiùs / va¤siø (*ir prk*), der¸ti; dúoti de»liø; **2.** *intr* [A] (*dare un utile*) dúoti peµno; atsipi»kti; **3.** *tr* dúoti (*der­liø, pelno, rezultatà*); pelnôti; ***f. qualche milione*** atnêðti kelís milijo­nùs pajamÿ; ***f. il 20******%*** dúoti 20 proc. peµno; *fig **f. elogi*** pelnôti pagyrímà; *fig* ***f. l'invìdia di molti*** sukélti da÷gelio pavýdà.

**fruttéto** *dkt v* va¤smedþiø sõdas.

**frutticoltóre, -trìce** *dkt v/m* va¤siø augíntojas -a, va¤­sininkas -ë; sõdininkas -ë.

**fruttìfero, -a** *bdv* vaisíngas (*ir prk*).

**fruttivéndolo,** **-a** *dkt v/m* va¤siø i» darþóviø pardav¸­jas -a; darþóvininkas -ë.

**frùtt|o** *dkt v* **1.** va¤sius (*ir prk*); ***~i ma­tu­ri (acer­bi)*** prinókæ (þalí) va¤siai; ***âlbero da f.*** va¤sinis mìdis, va¤smedis; ***dare ~i, portare f.*** vêsti vaisiùs; *fig* dúoti va¤siø / rezultåtø; *fig* ***i ~i del pròprio lavoro*** sâvo dãrbo va¤siai; ¨ ***il f. proibito*** úþdraustas va¤­sius; ***~i*** ***di** **bosco*** úogos; ***~i*** ***di** **mare*** j¿ros gërôbës; *fig **méttere a f.*** (*qcs*) sëk­míngai naudótis (*kuo*); *t.p.* Þ **frùtta**; **2.** *fig* (*conseguenza, prodotto*) padarinýs; va¤sius; ***f. del­la fan­ta­sia*** vaizduõtës pa­da­rinýs; ***il f. di una buona edu­cazione*** gìro ãuklëjimo va¤sius; **3.** *fig*: ***dare buoni ~i*** (*rendere*) dúoti peµno.

**fruttòsio** *dkt v chim* fruktòzë, va¤siø cùkrus.

**fruttuóso,** **-a** *bdv* **1.** *fig* vaisíngas; naðùs; **2.** *fig* (*econo­mi­ca­mente*) pelníngas.

**fu** *bdv nkt* veliónis; ***Ivo Rossi fu Gino*** Ivo Rossi, veliónio Gino sûnùs.

**fucilâre** *vksm* (*-c**ì**-*) (su)ðãudyti.

**fucilâta** *dkt m* ðãutuvo ð¾vis.

**fucilazióne** *dkt m* (su)ðãudymas; ***condannare al­la f.*** nute¤sti suðãudyti.

**fucìle** *dkt v* ðãutuvas; ***f. automâtico*** automåtinis ðãutuvas; ***f. a pallettoni*** ðratínis ðãutuvas; ***f. subâcqueo*** povandenínis ðãutuvas.

**fucìna** *dkt m* **1.** þa¤zdras; (*officina*) kãlvë; **2.** *fig* kãlvë, formåvimosi térpë.

**fucinatùra** *dkt m* kalôba; (*l'azione*) kalímas.

**fùco** *dkt v* trånas.

**fùcsia** **1.** *dkt m nkt* *bot* fùksija; **2.** *bdv nkt* ra÷s­vas sù violêtiniu åtspalviu, fùksijos spalvâ..

**fùga** *dkt m* **1.** (pa)bëgímas (*ir prk*); ***f. dal cârcere*** pabë­gímas íð kal¸jimo; *fig* ***f. dalla realtâ*** pa­bëgí­mas nuõ tikróvës; ***darsi alla f.*** pasiléisti b¸gti; ***méttere in f.*** prive»sti b¸gti, vôti; **2.** *fig* (*perdita*) núotëkis; nutek¸jimas; ***f. di cervelli (di notìzie)*** smege­nÿ (informåcijos) nutek¸­ji­mas; ***f. di gas*** dùjø núotëkis; *econ* ***f. di capitali*** kapitålo bëgímas; kapitålo iðveþímas; ***c'ê sta­ta u­na f. di gas*** nu­te­­k¸jo dùjos; **3.** *mus* fugâ; **4.** *sport* spùrtas.

**fugâce** *bdv* trumpala¤kis; gre¤tai prab¸gantis.

**fugâre** *vksm* (ið)sklaidôti; iðblaðkôti; *fig* ***f. i dubbi*** iðsklaidô­ti abejonês.

**fuggévole** *bdv* gre¤tai prae¤nantis, trumpala¤kis; la¤­kinas.

**fuggiâsco, -a** *dkt v/m* bëglýs -º.

**fuggifùggi** *dkt v nkt* (*t.p.* ***f.*** ***generale***) påniðkas bëgí­mas.

**fuggìre** *vksm* **1.** *intr* [E] (pa)b¸gti; (pa)sprùkti, iðtr¿kti; ***f. di casa (di prigione)*** pab¸gti íð na­mÿ (íð kal¸jimo); ***f. all'êstero (davanti al ne­mico)*** pab¸gti ñ ùþsiená (nuõ prîeðo); **2.** *intr* [E] (*del tempo*) (pra)­b¸gti; **3.** *tr* (*evitare*) (*qcs*) véngti (*ko*); ***f. ogni re­sponsabilitâ*** véngti bêt kokiõs atsakomôbës.

**fuggitìvo**, **-a** *dkt v/m* bëglýs -ë.

**fùlcro** *dkt v* atramõs tåðkas (*ir prk*).

**fùlgido, -a** *bdv* **1.** skaistùs, spi¹dintis; **2.** *fig* ryðkùs.

**fulgóre** *dkt v* åkinamas spindesýs; spind¸jimas.

**fulìggine** *dkt m* súodþiai *pl*; ***sporco di f.*** súodinas.

**fulmin|âre** *vksm* (*fùl-*) **1.** *tr* (nu)tre¹kti; ***ê stato ~to dalla corrente*** jñ nùtrenkë elektrâ; *fig* ***f.*** (*qcn*) ***con gli occhi*** pérsmeigti (*kà*) þvilg­s­niù; akimís þaibúoti; **2.** *fig* (*uccidere*) paki»­sti; ***l'ha ~to un infarto*** jñ paki»to infãrktas; **3.:** *intr* [E, A] (su)þaibúoti, paþaibúoti.

**> fulminârsi** *sngr* (*di lampadina*) pérdegti.

**fùlmine** *dkt v* þa¤bas; ***come un f.*** þa¤biðkai; ka¤p þa¤­bas; ¨ ***f. a ciel sereno*** perk¿nas íð giìdro danga÷s;  ***colpo di f.*** méilë íð pírmo þviµgsnio.

**fulmìneo,** **-a** *bdv* þa¤biðkas.

**fùlvo, -a** *bdv* gelsva¤ rùdas; rùsvas.

**fumaiòlo** *dkt v* kåminas (*ppr. laivo, fabriko*).

**fum|âre** *vksm* **1.** *tr* (pa)rûkôti; ***sméttere di f.*** mês­ti rûkôti; ***f. la pipa*** rûkôti pôpkæ; ***f. una siga­retta*** (su)rûkôti cigarêtæ; parûkôti; ***lei ~a?*** j¾s r¾kote?; ¨ ***vietato f.*** r¿kyti dra÷dþiama; **2.** *intr* [A] r¾kti; smiµkti; (*emettere vapore*) ga­rúoti; ***la minestra ~a*** sriubâ garúoja.

**fumâ|rio, -a** *bdv*: ***canna ~ria*** d¿mtraukis, kåminas (*vamzdis*).

**fumâta** *dkt m* **1.** (*vienu kartu*) iðleidþiamí d¿mai *pl*; d¿mas; ¨ ***f. nera*** juodí d¿mai (*iðleidþiami nepavy­kus iðrinkti popieþiaus; t.p. prk, þymi nepasisekimà*); **2.: *fare / farsi una f.*** parûkôti.

**fumatór|e,** **-trìce** *dkt v/m* rûkålius -ë; rûkôtojas -a; *kaip* *bdv nkt* ***(per) non ~i*** ner¾kantiems.

**fumettìsta** *dkt v/m* kòmiksø kûr¸jas -a / pieð¸jas -a.

**fumétt|o** *dkt v* (*ppr. dgs* ***~i***) kòmiksas.

**fùm|o** *dkt v* **1.** d¿mai *pl*; ***odore di f.*** d¾mø kvå­pas; ***il f. esce dal camino*** d¿mai r¾ksta íð kå­mino; ***puz­zo di f.*** smírdu d¾mais, nuõ man³s d¿­mais nìða; ¨ ***andare in f.*** þlùgti; subli¿kðti; ***mandare in f.*** (su)þlugdôti; ***véndere f.*** p¾sti míglà ñ akís; *flk* ***non c'ê f. sen­za ar­ros­to*** nërâ d¿mø bê ug­niìs; **2.** (*il fumare*) r¾kymas; ***tabacco da f.*** r¾komasis tabåkas; ***il f. fa male alla salute*** r¾kymas ke¹kia sveikåtai; ¨ ***f. passivo*** pasyvùs r¾kymas; **3.** *fig*: ***i ~i dell'alcol*** apsvaigímas, svaigulýs; ***i ~i dell'ira*** prõto apte­mímas (supýkus); **4.** *fam* þolº (*haðiðas ir pan.*), þolôtë.

**fumògeno** *dkt v* d¿minis fåkelas.

**fumóso, -a** *bdv* **1.** pílnas d¿mø; prirûkôtas; dûmúo­tas; **2.** *fig* mi­glótas.

**funâmbolo** *dkt v* lôno akrobåtas.

**fùne** *dkt m* vi»vë; lônas, tròsas; ***tiro alla f.*** vi»vës traukímas.

**fùnebr|e** *bdv* **1.** gedulíngas; lãidotuviø; ***corona f.*** lãidotuviø vainíkas; ***mârcia f.*** gedulíngas mãrðas; ***pompe ~i*** (*la ditta*) lãidojimo biùras; ***véglia f.*** ðe»­menys *pl*; **2.** (*lugubre*) graudùs; gûdùs.

**funerâl|e** *dkt v* (*t.p. dgs **~i***) lãidotuvës *pl*; ¨ ***da f.*** niûrùs; lãidotuviðkas; ***f. di Stato*** valstôbinës lãidotuvës; ***êssere un f., avere una fâccia da f.*** b¿ti ka¤p pe» lãidotuves; b¿ti ka¤p þìmæ pardåvus.

**funerâri|o, -a** *bdv* a¹tkapio; kåpo; lãidojimo; ***iscri­zione ~a*** a¹tkapio ùþraðas; ***urna ~a*** lãidojimo ùrna.

**funêreo, -a** *bdv* gedulíngas; gûdùs.

**funestâre** *vksm* (*-nê-*) aptémdyti; (*solo qcn*) sute¤kti ska÷smo (*kam*).

**funêsto, -a** *bdv* **1.** (*rovinoso*) praþûtíngas; **2.** (*luttuoso*) gedulíngas; niûrùs; (*doloroso*) sielvartíngas.

**fungâia** *dkt m* grybåvietë.

**fùng|ere*** *vksm* [A] **1.** (*da qcn*) e¤ti (*kito þmogaus*) påreigas; pavadúoti (*kà*); **2.** (*da qcs*) b¿ti var­tó­jamam (*vietoj ko kito*), b¿ti naudójamam; (*anda­re bene*) tíkti; ***la cucina ~e anche da sala da pranzo*** virtùvë ti¹ka i» ka¤p vãlgomasis; ***questa parola ~e da soggetto*** ðís þõdis e¤na veiksniù; **3.** (*sostituire*) atstóti; (*in un incarico*) (*da qc*) pavadúoti (*kà*).

**fùn|go** *dkt v* **1.** grýbas; ***f. com­mestìbi­le (ve­le­no­so)*** vãlgo­ma­s (nuodíngas) grýbas; ***ri­sotto coi /ai ~ghi*** rýþiai *pl* / rizòtas sù grýbais; ***andare*** ***a** **/ per** **~ghi, cercare** **~ghi*** grybãuti; ¨ ***a f.*** grýbo pavídalo; ***spùnta­no co­me ~ghi*** dôgsta ka¤p grýbai põ lie­ta÷s; **2.:** *fig* ***f. atòmico*** atòminis grýbas; **3.** *med* (ódos) grybìlis.

**funicolâre** *dkt m* funikuliìrius.

**funivìa** *dkt m* (kalnÿ) (lônø) kéltuvas.

**funzionâle** *bdv* **1.** fùnkcinis; *med* ***disturbo f.*** fùn­kcinis sutrikímas; **2.** (*pratico*) funk­cionalùs; pråk­tiðkas; (*comodo*) patogùs.

**funzionalitâ** *dkt m nkt* **1.** funk­cionalùmas; praktiðkù­mas; (*comoditâ*) patogùmas; **2.** Þ **funzióne 1.**

**funzionaménto** *dkt v* veikímas; funkcionåvi­m­as; *tecn* eigâ; ***difetto di f.*** veikímo sutrikímas (/ defêktas); ***spiegare il f. di qcs*** paãiðkinti, ka¤p kâs ve¤kia.

**funzion|âre** *vksm* [A] (*-zió-*) **1.** (*tinkamai*) ve¤kti; fun­kcionúoti; ***f. a elettricitâ*** naudóti e­lêktrà; ***non ~a*** neve¤kia, sugì­do; **2.** (*dare risultati*) dúoti va¤siø; b¿ti naudíngam; ***con me le lâcrime non ~ano*** åðaromis man³s nepave¤ksi.

**funzionârio,** **-a** *dkt v/m* valdini¹kas -ë, valstôbës tar­nãutojas -a; (*pubblio ufficiale*) pareig¾nas -ë; ***f. di banca*** bãnko tarnãutojas.

**funzión|e** *dkt m* **1.** fùnkcija; (*finalitâ*) paskirtís -iìs; ***~i vi­ta­li*** gyvôbi­nës fùnkcijos; ***svòlgere una f.*** atlíkti fùnkcijà; **2.** (*attivitâ*) veiklâ; veikímas; (*ruolo*) vaidmuõ; ¨ ***in f.*** (*in qualitâ di*) ka¤p; ***entrare in f.*** suve¤kti; ásijùngti; ***êssere in f.*** ve¤kti; (*di qcs*) priklausôti (*nuo ko*); ***méttere in f.*** ájùngti; **3.** (*rito sacro*) påmaldos *pl*; **4.** (*mansione*) (*ppr. dgs **~i***) påreigos *pl*; ***eser­ci­ta­re le ~i di pre­si­den­te*** e¤ti pre­zi­de¹to påreigas; ¨ ***facente f.*** laikina¤ e¤nantis påreigas; **5.** *mat* fùnkcija.

**fuò|co** *dkt v* **1.** ugnís -iìs *femm*; (*fiamma*) liepsnâ; ***~chi*** ***d'artifìcio, ~chi*** ***artificiali*** fejervêr­kai; ***dare*** ***f., appiccare il f.*** (*a qc*) padêgti (*kà*); ***accéndere (spégnere) il f.*** uþdêgti ((uþ)ge­sínti) ùgná; ¨ ***di f.*** ugníngas *agg*; ***f. fâtuo*** þaltvý­kslë; ***f. di pâglia*** trum­pala¤kë sensåcija; atsitiktínis laim¸jimas; ***Vìgile del F.*** ga¤srininkas -ë; ugniagesýs; ***a f. lento (vivo)*** a¹t silpnõs (stipriõs) ugniìs; ***andare a f.*** (su)­dêgti; ***prêndere*** ***f.*** uþsidêgti; ***métterci la mano sul f.*** dúoti ra¹kà nuki»sti; ***gettare òlio sul f., soffiare sul f.*** pílti aliìjø ñ ùgná; ***giocare / scherzare col f.*** þa¤sti sù ugnimí; ***al f.!*** ugnís!; *kaip jst* ***f. f.*** (*nei giochi*) ðiµta, ðilèia÷; **2.** (*fornello*) degíklis; ka¤tvietë; ***méttere l'âcqua sul f.*** (uþ)ka¤sti vãndená; ***méttere la péntola sul f.*** (uþ)d¸ti púodà a¹t ugniìs; **3.** (*focolare*) þidinýs; ***accén­dere il f.*** (su)kùrti ùgná; **4.** *fig* (*ardore*) ugnís -iìs *femm*; uþsidegímas; **5.** *geom, fis* þidinýs; **6.:** *fot*: ***a f.*** sufokusúotas *agg*; ***méttere a f.*** (su)fokusúoti; **7.** *mil* ugnís -iìs *femm*; ***sotto il f. nemico*** prîeðo ugnyjê; ***f. incrociato*** kryþmínë ugnís; ***f. di sbarramento*** ugniìs rúoþas; ***cessate il f.*** ugniìs nutraukímas; *fig* paliãubos *pl*; ***scontro a f.*** susiðãudymas; ¨ ***f. amico*** dra÷giðka kulkâ; ***arma da f.*** ðaunamâsis gi¹klas; ***bocca da f.*** pab¾klas; ***aprire il f.,*** ***fare f.*** ðãuti; paléisti ùgná; (*su qc*) apðãudyti (*kà*); ***êssere tra due ~chi*** ta»p dviejÿ ugniÿ; ***rispòndere al f.*** atsiðãudyti; *kaip jst* ***f.!*** ugnís!

**fuorché** **1.** *prlk* iðskôrus (*kà*); ***tutti f. lui*** visí iðskôrus jñ; **2.** *jngt* tík nê; ***sono pronto a tutto, f. scusarmi*** âð vískam pasiruõðæs, tík neatsipraðôsiu.

**fuòri** **1.** *prlk* (*qcs, da qcs*) ùþ (*ko*); ***f.*** ***cittâ*** ùþ miìsto, ùþmiestyje; (*moto a luogo*) ñ ùþmiestá; ***f. Milano*** ùþ Milåno; ***aspettare f. dalla porta*** lãukti ùþ dùrø; ***guardare f. dalla finestra*** þiûr¸ti prõ lãngà; ***méttere qcs f. posto*** pad¸ti k± nê ñ viìtà; *sport* ***f. casa*** íðvykoje, sveèiuosê, nê namiì; ¨ ***al di f.*** (*di qcn*) iðskôrus (*kà*); ***f. (dai giochi)*** ùþ bòrto; ***f. corso*** nebegaliójantis *agg*; ***f. luogo*** (nê laikù i») nê viìtoje; ***f. mano*** nuoðalùs *agg*; nuoðalia¤; ***f.*** ***moda*** ne(be)ma­díngas *agg*; pasìnæs *agg*; *fam* ***f. porta*** ùþmiestyje; ***f. di sé*** nesåvas *agg*; ***f. tempo*** nê ñ tåktà; ***f. di testa*** patråkæs *agg*, iðsikrãustæs *agg* ís galvõs; ***f. uso, f. servìzio*** sugìdæs *agg*; ***dire f. dai denti*** iðr¸þti, iðdróþti; ***non métte­re pie­de f. di ca­sa*** neiðkélti nº kójos íð namÿ; **2.** *prv* laukê; (*moto a luogo*) ñ la÷kà; la÷k; ***da f.*** íð la÷ko; ***andare*** ***fuori*** iðe¤ti, e¤ti ñ la÷kà; ***buttare f.*** iðmêsti; ***f. fa freddo*** ðãlta laukê; *sport* ***tirare f.*** mùðti ñ ùþribá; ¨ ***di f.*** íð íðorës; iðorínis *agg*, la÷ko; (*di gente*) nê viìtinis *agg*; ***in f.*** ñ íðoræ; (*in avanti*) ñ prîeká; ***tagliato f.*** ùþ bòrto (*iðmestas, neátrauktas*); ***far f.*** (*qcn*) nugalåbyti (*kà*); ***veni­re f., saltare f.*** (*diventare noto*) iðaiðk¸ti; iðkílti aikðtºn; (*comparire*) atsirâsti; ***tirare f.*** iðtrãuk­ti; iði§ti; ***venire f., uscire f.*** iðlñsti; iðbrísti (*ir prk*); ***ê f. perìcolo*** gyvôbei nebêgrìsia pavõjus; ***f. i soldi!*** atidúok pínigus!; *kaip jst* ***f. (di qui)!*** la÷k (íð èiâ)!; e¤k íð èiâ!; *kaip dkt*: ***il di f.*** íðorë; **3.** *prv* (*non a casa*): ***cenare f.*** vakarieniãuti nê namiì (/ restoranê *ir pan.*); ***stare f. a lungo*** ilga¤ nepare¤ti; **4.** *prv* (*non al lavoro*): nê darbê; ***il direttore ê f.*** vadõvas iðvýkæs; **5.** *prv* *fam* (*non in carcere*) lãisvë­je; paléistas *agg*; **6.** *prv fig* (*all'apparenza*) íð íðorës; **7.** *bdv nkt fam* tre¹ktas; ***sei f.?*** a» iðprot¸­jai?; gãl pakvaiða¤?

**fuoriclâsse** *dkt v/m nkt* åsas -ë.

**fuorigiòco** *dkt v nkt sport* núoðalë.

**fuorilégge** **1.** *bdv* *nkt* ùþ áståtymo ribÿ; **2.** *dkt v/m nkt* nusikaµtëlis -ë.

**fuorisérie** *dkt m nkt* nestandãrtinis automobílis (*pra­bangus, pagamintas pagal uþsakymà*).

**fuoristrâda** *dkt v nkt* visure¤gis; dþípas.

**fuor(i)uscìre*** *vksm* [E] iðsipílti; ið­silîeti; (*fluire*) (ið)­tek¸ti; (*erompere*) iðsive»þti, ve»þtis.

**fuor(i)uscìta** *dkt m* ið­siliejímas: (*perdita*) núotëkis.

**fuor(i)uscìto, -a** *dkt v/m* diside¹tas -ë; atskal¾nas -ë.

**fuorviâre** *vksm* (*-v**ì**-*) (su)klaidínti.

**furbacchióne, -a** *dkt v/m* gudrõèius -ë.

**furberìa** *dkt m* **1.** Þ **furbìzia 1.**; **2.** (*inganno*) suktôbë.

**furbìzia** *dkt m* **1.** gudrùmas; ***con f.*** gudria¤; **2.** (*azione furba*) gudrôbë; gudrùs triùkas.

**fùrbo, -a** **1.** *bdv* gudrùs; ***fatti f.!*** nebùk kva¤las!; **2.** *dkt v/m* gudruõlis -ë; ***non fare il f.!*** tík negudrãuk!

**furénte** *bdv* ásiùtæs, át¿þæs.

**furétto** *dkt v zool* (laukínis) ðìðkas.

**furfânte** *dkt v* nevidõnas, nenãudëlis.

**furgoncìno** *dkt v* furgonºlis; pikåpas.

**furgóne** *dkt v* furgònas; ***f. del latte*** pîenvëþis.

**fùr|ia** *dkt m* **1.** ïnirðis; ïtûþis; (á)siutímas; ¨ ***an­dare / montare su tutte le ~ie*** áni»ðti, ásiùsti; **2.** *fig* (*im­peto*) siãutëjimas; ðºlsmas; ïnirðis; ¨ ***a f.*** (*di (far) qcs*) *þymi bûdà, priemonæ*; nuõlat (*kà darant*); **3.** *fig, mit* fùrija; **4.** Þ **frétta**; ***in fretta e f.*** paðºlusiai skubia¤.

**furibóndo,** **-a** *bdv* **1.** (*di qcn*) ásiùtæs, áni»ðæs; **2.** (*di qcs*) audríngas, paðºlæs.

**furióso,** **-a** *bdv* **1.** (*di qcn*) (vísiðkai) ásiùtæs, áði»dæs; ***êssere f.*** ði»sti, siùsti, laba¤ pýkti; ¨ ***pazzo f.*** vísiðkas beprõtis; **2.** (*di qcs*) ánirtíngas; (*del mare, del vento e sim.*) siautulíngas.

**furóre** *dkt v* **1.** ïnirðis; pasiutímas; ***preso dal f.*** ïnirðio âpimtas; ¨ ***a furor di pòpolo*** (*visiems*) audríngai prítariant; ***fare f.*** su­kélti furòrà; **2.** *fig* (*impeto*) ðºlsmas, siau­tulýs; (*slancio*) pólëkis; ***f. giovanile*** jaunåtviðkas ïkarðtis.

**furoreggiâre** *vksm* [A] (*-rég-*) b¿ti a¹t bangõs.

**furtìvo, -a** *bdv* slåptas; vogèiomís dåromas; ***con pas­so f.*** vogèiâ / slaptâ sle¹kant; sºlinant.

**furtivaménte** *prv* vogèiâ, vogèiomís; paslapèio­mís.

**fùrto** *dkt v* vagýstë; (*l'azione*) pavogímas; ***accusare di f.*** apkãltinti vagystê; ***comméttere un f.*** pavõgti; ávôkdyti vagýstæ; ***condannare per f.*** nute¤sti ùþ va­gýstæ; *fig* ***ê un f.!*** èiâ grynâ vagýstë!, ta¤ apiplëðí­mas!

**fùsa** *dkt m dgs*: ***fare le f.*** mu»kti.

**fuscêllo** *dkt v* (*pagliuzza*) ðiãudas; (*ramoscello*) vi»bas.

**fusìbile** *dkt v tecn* (lydùsis) saugíklis; ***ê saltato il f.*** pérdegë saugíklis.

**fusìllo** *dkt v* (*ppr. dgs* ***~i***) spirålinis makarõnas, srai­gtìlis.

**fusióne** *dkt m* **1.** lôdymas(is); (*scioglimento*) (ið)­ti»p­dy­mas; (ið)tirpímas; *fis* ***temperatura di f.*** lô­dy­mosi tem­pera­tûrâ; *fis* ***f. nucleare*** (termo)bran­duo­línë si¹tezë; **2.** (*colata*) liejímas; **3.** *fig* su(si)jungí­mas; su(si)­viìni­ji­mas.

**fùso I** *dkt v* **1.** ve»pstas, ve»pstë; ***drit­to co­me un f.*** ít miìtà praríjæs; **2.:** ***f.*** ***orârio*** la¤ko júosta.

**fùso, -a II** *bdv, dlv* **1.** (ið)lôdytas; (*scioltosi*) susilôdæs; ***formâggio f.*** lôdytas s¿ris; **2.** (*colato*) lietínis; nu­lîetas; **3.** *bdv fam* nuva»gæs, iðsìkæs.

**fusoliéra** *dkt m* fiuzeliåþas, órlaivio liemuõ.

**fustâgno** *dkt v* mùltinas.

**fustigâre** *vksm* (*fù-*) (nu)plâkti (sù) rôkðte.

**fustìno** *dkt v* dëþùtë (*ppr. cilindrinë, biralams ir pan.*).

**fùsto** *dkt v* **1.** kamîenas; liemuõ; *bot* (*caule*) stîebas; virkðèiâ; **2.** (*bidone*) sta­tínë; bidònas; **3.** *archit* fù­stas; (kolònos) liemuõ; **4.** *fam* (*di qcn*) graþuõlis; r¸mas *fam*.

**fùtil|e** *bdv* nereikðmíngas; nerímtas; (*vano*) tùðèias; ***discorsi ~i*** tùðèios kaµbos; ***per ~i motivi*** dºl niì­kø, bê rimtõs prieþastiìs.

**futurìsmo** *dkt v* futurízmas.

**futurìsta 1.** *bdv* futurízmo; futurístinis; futurístø; **2.** *dkt v/m* futurístas -ë.

**futurìstico, -a** *bdv* futurístinis.

**futùr|o, -a** **1.** *bdv* b¿simas; (*del futuro*) ateitiìs; ***~a mamma*** bûsimâ mamâ; ***le generazioni ~e*** b¿si­mosios ka»tos; **2.** *dkt v* ateitís -iìs *femm*; ***pensare al f.*** galvóti apiì åteitá; ***prevedere il f.*** numatôti / sp¸ti åteitá; ¨ ***in*** ***f.*** ateityjê; ***senza f.*** bê ateitiìs; **3.** *dkt v gram* bûsimâsis la¤kas; ***f. anteriore, f. secondo, f. composto*** sudëtínis bûsimâsis la¤kas.
  `;
