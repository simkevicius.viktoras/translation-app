export const lithuanianDB = `
**uþnuõd|yti** *v* , ~ija, ~ijo) avvelenare; ***u. vãndená*** avvelenare / infettare l'âcqua; prk ***u. kãm gyvìnimà*** avvelenare l'esistenza a qcn.

**uþpajam|úoti, ~úoja, ~åvo** *vx* fin segnare a crédito, accreditare.

**uþpakalín|is,** **-ë** *agg* posteriore; di dietro; ***u. áëjímas*** l'entrata sul retro; ***~iai råtai*** le ruote posteriori; lingv ***~ës eilºs baµsiai*** le vocali posteriori.

**ùþpakal|is** *s* **1.** dietro *nkt*; ***~yje*** (di) dietro; ***la¤vo ù.*** la poppa di una nave; ***íð ~io*** da dietro, da tergo; alle spalle; ***pùlti íð ~io*** assalire alle spalle; **2.** (*kûno dalis*) sedere *v*; didietro *nkt*; ***áspírti ñ ~á*** dare / tirare un câlcio nel sedere; ***kraipôti ~á*** sculettare; ¨ ***ñ ~á lñsti*** lustrare le scarpe; leccare il culo *vulg*; ***~iais badôtis*** stare stipati / pigiati.

**uþpakaliùkas** *s* *dimin* sederino; culetto.

**uþpatentúotas, -a** *agg*, *prcp* brevettato.

**uþpatent|úoti, ~úoja, ~åvo** *vx* brevettare.

**uþpelk¸|ti, ~ja, ~jo** *vi* impaludarsi.

**uþpelnô|ti, uþpeµno, uþpeµnë** *vi* ottenere profitti; guadagnare (*ir* prk).

**uþper¸tas, -a** *prcp* : ***u. kiauðínis*** uovo andato a male.

**ùþpernai** *avv* due anni fa.

**ùþpernykðtis,** **-ë** *agg* di due anni fa.

**uþpôkd|yti, ~o, ~ë** *vx*, **uþpýkin|ti, ~a, ~o** *vx* fare arrabbiare; (*erzinti*) irritare.

**uþpýk|ti** *v* , ~sta, ~o) (*ant ko*) arrabbiarsi (*con qcn*); (*labai*) infuriarsi; ***jí ~usi a¹t tav³s*** ê ar­rabbiata con te.

**ùþpilas** *s* **1.** *qualsiasi* salsa, salsina *che si versa su una pietanza*; **2.** (*vaistaþoliø*) infuso, decotto, tisana; ***ramunºliø ù.*** infuso di camomilla; **3.** stat (*þemës*) rinterro.

**ùþpildas** *s* chem riempitivo, filler *v nkt*.

**uþpíld|yti, ~o, ~ë** *vx* (*kà ko/kuo*) riempire (*qcs di qcs*); (*ypaè blankà ir pan*.) compilare; ***u. ankêtà*** compilare / riempire un questionârio.

**uþpílt|as, uþpiltâ** *agg*, *prcp* ðnek ***~os åkys*** occhi *v* lùcidi (*per ubriachezza*).

**uþpílti** *v* (ùþpila, uþpôlë) **1.** versare (*sopra*); (*ypaè netyèia*) rovesciare; **2.** (*þoles*, *kad pritrauktø*) méttere in infusione; **3.** (*uþtvindyti*) inondare, allagare; **4.** (*kà kuo*) ricoprire (*qc di qcs*).

**uþpiltínë** *s* *tipo di* liquore *v* dolce.

**uþpirk¸jas, -a** *s* acquirente *v/m*.

**uþpi»k|ti** *v* (ùþperka, ~o) **1.** (*ko*) fare (una / la) scorta (*di qcs*); **2.:** ***u. miðiâs*** far dire una messa.

**uþpís|ti** *v* (ùþpisa, ~o) vulg rómpere / scassare il cazzo.

**uþpjãu|ti** *v* , ~na, uþpjóvë) **1.** (*kandant nuþudyti*) dilaniare; **2.** (*padaryti rantà*) intaccare.

**uþpjudôti** *v* (uþpjùdo, uþpjùdë) (*ðunimi*) sguinzagliare (*un cane dietro qcn*).

**uþplâkti** *v* (ùþplaka, ùþplakë) **1.** (*botagu*) frustare (*anche a morte*), sferzare; (*rykðte*) fustigare (*anche a morte*), scudisciare; **2.** (*kniedëmis*) rivettare.

**uþpla÷k|ti** *v* , ~ia, ùþplaukë) **1.:** ***u.*** ***a¹t*** ***seklumõs*** incagliarsi / arenarsi su una secca; **2.** ðnek (*uþplûsti*) arrivare a vagonate.

**uþpl|ikôti** *v* , ~íko, ~íkë) (*kà*) versare âcqua bollente (*sopra qcs*); ***u. arbåtà*** méttere il tê in infusione; fare un tê.

**uþplomb|úoti, ~úoja, ~åvo** *vx* **1.** piombare, impiombare; **2.:** ***u. da¹tá*** otturare un dente, fare un'otturazione.

**uþplûdímas** *s* **1.** inondazione *m*; invasione *m*; (*ir* prk); **2.** prk (*jausmø ir pan.*) ondata.

**uþplu§pin|ti, ~a, ~o** *vx* ðnek fare secco.

**uþpl¿|sti** *v* , ~sta, ~do) **1.** (*uþlieti*) sommêrgere; inondare; **2.** prk (*gausiai prieiti*) invâdere; affollare, gremire; ***turístai ~do påplûdimius*** i turisti hanno invaso le spiagge; **3.** prk (*smarkiai apimti*) sopraffare, pervâdere; inondare; ***u. bjauria¤s þõdþiais*** insul­tare.

**uþporôt** *avv* fra / tra tre giorni.

**ùþpraeit|as, -a** *agg* due... fa; ***~ais mìtais*** due anni fa; ***~à savãitæ*** due settimane fa.

**uþpr|aðôti** *v* , ~åðo, ~åðë) **1.** (*pakviesti*) invitare (per tempo); **2.** (*per didelæ kainà*) chiédere troppo; far prezzi troppo alti.

**uþprenumer|úoti, ~úoja, ~åvo** *vx* (*kà kam*) abbonare (*a qcs qcn*).

**uþprogram|úoti, ~úoja, ~åvo** *vx* programmare.

**uþprotest|úoti, ~úoja, ~åvo** *vx* (*kà*) protestare (*contro qc*); teis ***u. te¤smo sprendímà*** impugnare una sentenza; teis ***u. vêkselá*** protestare una cambiale.

**uþprotokol|úoti, ~úoja, ~åvo** *vx* verbali@@are.

**uþpùlti** *v* (uþpúola, uþpúolë) **1.** aggredire, assalire; attaccare; ***prîeðai uþpúolë kråðtà*** i nemici hanno attaccato il paese; ***dù vyríðkiai manê uþpúolë gåtvëje*** due tizi mi hanno aggredito per strada; ***alkaní vilka¤ i» þmõgø uþpúola*** i lupi affamati assâlgono anche l'uomo; **2.** prk (*smarkiai pristoti*) (*kà daryti*) saltare su (*a far qcs*); **3.** (*uþklupti*) sorprêndere, cògliere (di sorpresa); ***mùs naktís uþpúolë kelyjê*** la notte ci sorprese in viâggio; **4.** (*uþkristi*) (*ant ko*) cadere (*sopra (a) qc*, *su qc*), piombare.

**uþpuol|¸jas,** **-a** *s* , **~íkas,** **-ë** *s* aggressore *v*, aggreditrice *m*, assalitore -trice.

**uþpuolímas** *s* aggressione *m* (*l'azione*); assalto.

**ùþpuolis** *s* aggrssione *m*; assalto.

**uþp¾sti** *v* (ùþpuèia, ùþpûtë) (*puèiant uþgesinti*) (*kà*) soffiare (*su qcs*), spégnere con un sóffio; ***u. þvåkæ*** soffiare su / spégnere una candela.

**uþpustôtas, -a** *prcp* sommerso (*dalla neve e sim*.), sepolto (*sotto la neve e sim*.).

**uþpustôti** *v* (uþpùsto, uþpùstë) seppellire (*sotto la neve*, *la sabbia*) (*del vento*), sommêrgere.

**ùþraitas** *s* **1.** (*paraðytos raidës*) svolazzo; grâzia; **2.** archit voluta, rìccio, rìcciolo.

**uþraitôti, uþra¤to, uþra¤të** *vt* piegare su di sé.

**uþraitó|ti, ~ja, ~jo** *vx* rimboccare.

**uþrakín|ti** *v* , ~na, ~o) chiùdere a chiave; serrare; ***u. sâvo ka§bará*** chiùdere a chiave la pròpria stanza; ***u. k± ñ se¤fà*** chiùdere qcs in cassaforte.

**ùþrakt|as** *s* **1.** serratura; ***põ ~u*** sottochiave, sotto chiave; ***centrínis ù.*** chiusura centrali@@ata; ***iðlãuþti ~à*** forzare / scassinare una serratura; **2.** fot otturatore *v*; **3.** (*ðaunamojo ginklo*) sicura (*di un'arma*).

**ùþrað|as** *s* (1, 3^b^) **1.** scritta; (*paminklo*) iscrizione *m*; (*paaiðkinimas po paveikslu*, *ekrane*) didascalia; ***vieðâsis ù.*** insegna; **2.:** *pl* ***~ai*** appunti *v*; note, annotazioni *v*; ***paskaitÿ ~ai*** appunti delle lezioni; ***~ø knygìlë / knygùtë*** agendina, agenda; taccuino.

**uþråðymas** *s* (*testamentu*) teis lâscito; legato.

**uþraðin¸|ti, ~ja, ~jo** *vx* prêndere appunti / note; (*kà*) annotare (*più cose*), appuntare.

**uþraðôti** *v* (uþråðo, uþråðë) **1.** scrìvere (*sopra*); annotare, appuntare; méttere per iscritto; ***u. ådresà a¹t vóko*** scrìvere l'indirizzo su una busta; **2.** (*átraukti á sàraðà*) segnare (*in una lista*), inserire; (*áraðyti*) iscrìvere; ***u. k± pâs gôdytojà*** fissare a qcn un appuntamento dal mêdico; ***u.*** ***skolõn*** segnare a débito, addebitare; **3.** (*kam turtà*) intestare (*a qcn un bene*); (*testamentu*) lasciare in ereditâ; legare *teis*; **4.** (*aparatu*) registrare.

**uþrãug|ti, ~ia, ~ë** *vx* **1.** far fermentare; (*apie teðlà*) far lievitare; **2.** (*pvz*., *darþoves*) méttere sotto sale; **3.** (*odà*) tannare.

**uþra÷k|ti** *v* , ~ia, ùþraukë) **1.** raccomodare (*facendo una piega*); **2.** ðnek (*pabaigti*) (*kà*) méttere / porre fine (*a qcs*); ***u. gi¹èà*** troncare una discussione.

**uþregistr|úoti, ~úoja, ~åvo** *vx* registrare; (*padaryti uþraðus*) (*kà*) annotare (*qcs*), prêndere nota (*di qcs*); ***u. gãutas prekês*** registrare la merce in arrivo; ***~úota apiì pùsðimtá elêktros gedímø*** si sono regi­strati una cinquantina di guasti elêttrici.

**uþrºk|ti** *v* , ~ia, ùþrëkë) **1.** (*ant ko*) rabbuffare (*qcn*); **2.** (*kà*) coprire (*con la voce e sim*.) (*qc*); gridare più forte (*di qcn*).

**ùþrib|is** *s* **1.** sport out *v nkt*; (*futbole*) fallo laterale; (*uþ vartø*) fallo di fondo; ***nukre¤pti kåmuolá ñ ~á*** deviare fuori il pallone; ***ù.!*** fuori!; **2.** (*vieta uþ ribos*) oltreconfine *v nkt*.

**uþriì|sti** *v* , ~èia, ùþrietë) piegare, incurvare; ¨ ***u. nósá*** (*imti didþiuotis*) montarsi la testa; (*bûti nepatenkintam*) arricciare il naso.

**uþrík|ti** *v* (uþri¹ka, ~o) (*ant ko*) cacciare un grido (*a qc*).

**uþrí|sti** *v* (ùþrita, ~to) far rotolare.

**uþríð|ti** *v* (ùþriða, ~o) **1.** allacciare, legare; annodare (*con uno spago e sim*.); **2.:** ***u.*** ***kãm akís*** bendare gli occhi a qcn, bendare qcn; ***u. kãm bùrnà*** tappare la bocca a qcn, imbavagliare qcn; **3.** ðnek (*su kuo*) farla finita (*con qcs*), chiùdere; ***uþriða÷ sù alkoholiù*** ho chiuso con l'alcol.

**uþrítin|ti, ~a, ~o** *vx* rotolare (su sé stesso).

**uþrûkôti** *v* (uþr¾ko, uþr¾kë) accêndersi una sigaretta (/ un sìgaro *e sim*.); (*uþsitraukti*) dare una tirata.

**uþr¿stin|ti, ~a, ~o** *vx* far adirare, muòvere all'ira.

**uþsagstôti** *v* (uþsågsto, uþsågstë) abbottonare; ***u. sagâs*** allacciare i bottoni.

**ùþsakai** *s pl* teis pubblicazioni *m* di matrimònio.

**uþsåkym|as** *s* (*prekiø*) órdine *v*; ordinazione *m*, ordinativo; commissione *m*; (*gaminiø*) commessa; (*bilietø*, *stalo ir pan*.) prenotazione *m*; ***pagaµ ~à*** su ordinazione; (*ypaè drabuþio*) su misura; ***~o*** ***blãnkas*** mòdulo d'ordinazione; ***atlíkti ~à*** evâdere / sbrigare un órdine; ***padúoti ~à*** fare un'ordinazione; ***prii§ti ~à*** accettare una prenotazione.

**uþsakôtas,** **-a** *prcp* ordinato; prenotato.

**uþsakôti** *v* (uþsåko, uþsåkë) ordinare; (*bilietà*, *stalà ir pan*.) prenotare; ***u. pícà ñ namùs*** ordinare una pizza da asporto; ***u.*** ***telefonù*** ***viìtà viìðbutyje*** prenotare per teléfono un posto in albergo.

**uþsakôtinis, -ë** *agg* su ordinazione; su misura.

**uþsakõvas,** **-ë** *s* cliente *v/m* (*che effettua un'ordinazione*); (*darbø ir pan*.) committente *v/m*.

**uþsêgti** *v* (ùþsega, ùþsegë) assicurare (*con un fermaglio e sim*.), fissare, attaccare; (*sagomis*) abbottonare; (*smeigtuku*) appuntare; (*kà uþtrauktuku*) chiùdere (*qcs*) con una (cerniera) lampo, chiùdere la lampo (*di qcs*); (*dirþà*, *vëriná)* allacciare.

**uþsémti, ùþsemia, uþs¸më** *vt* sommêrgere.

**uþs¸|sti** *v* , ~da, ~do) **1.** montare (in sella); ***u. a¹t ãrklio (a¹t dvíraèio)*** montare a cavallo (in bicicletta); **2.** ðnek (*pristoti*) (*ant ko*) stare addosso (*a qcn*); **3.** (*vietà*) occupare (un posto).

**uþs¸|ti, ~ja, ~jo** *vx* (*kà kuo*) seminare (*qcs* *a qcs*).

**uþsiaugín|ti, ~a, ~o** *vx* (*barzdà*, *plaukus*) farsi créscere; (*gyvulius*, *vaikus*) allevare (*per sé*); (*augalus*) coltivare (*per sé*), coltivarsi.

**uþsiba¤g|ti** *v* , ~ia, uþsíbaigë) finire, terminare.

**uþsibarikad|úoti, ~úoja, ~åvo** *vx* barricarsi (*dentro*), asserragliarsi.

**uþsibr¸þ|ti, ~ia, ~ë** *vx* **1.** tracciare (*per sé*), tracciarsi; prk ***u. tíkslà*** prefìggersi uno scopo; **2.:** ***u. degtùkà*** accêndersi un fiammìfero.

**uþsi|b¿ti** *v* , ~b¾va/~b¾na, ~bùvo) trattenersi (*più del previsto*, *troppo*), rimanere a lungo.

**uþsièiãup|ti, ~ia, ~ë** *vx* chiùdere (*la bocca*); ***~k!*** stai zitto *bdv*!, zitto!, zitto, tu!; chiudi il becco!

**uþsidårëlis,** **-ë** *s* persona chiusa / introversa / schiva; asociale *v/m*.

**uþsidårymas** *s* chiusura (*ir* prk).

**uþsi|darôti, ~dåro, ~dårë** *vr* chiùdersi (*ir* prk).

**uþsidìgëlis, -ë** *s* infervorato -a, esaltato -a.

**uþsidegímas** *s* entusiasmo, fervore *v.*

**uþsidêgti** *v* (uþsídega, uþsídegë) **1.** prêndere fuoco; infiammarsi, incendiarsi; ***mokyklâ uþsídegë*** la scuola ha preso fuoco; **2.** (*pvz*., *sau degtukà ar apie degtukà*) accêndersi; ***u. cigarêtæ*** accêndersi una sigaretta; ***uþsídegë þaliâ ðviesâ*** (*ðviesoforo*) ê venuto il verde; **3.** prk infervorarsi, entusiasmarsi; accalorarsi; ***u. aistrâ (pykèiù)*** infiammarsi di passione (di còllera); **4.** prk (*uþsigeisti*) (*kà daryti*) venire una gran vòglia (*di far qcs*); morire dalla vòglia; ***uþsídegiau vaþiúoti ñ Tibêtà*** mi ê venuta una gran vòglia di andare in Tibet.

**uþside¹g|ti** *v* , ~ia, uþsídengë) coprirsi; (*nuo ko*) protêggersi (*da qc*); ***u. véidà ra¹komis*** coprirsi il volto con le mani.

**uþsid¸|ti** *v* (uþsídeda, ~jo) **1.** (*kà sau*) méttersi (*qcs*); ***u. åkinius (kãukæ, ðãlmà)*** méttersi gli occhiali (una mâschera, il casco); **2.** (*apie ledà*, *ðaðà*) formarsi sopra; ricoprire; ***ðåðas a¹t þaizdõs ~jo*** si ê formata la crosta sulla ferita.

**uþsidírb|ti, ~a, ~o** *vx* guadagnare, guadagnarsi; ***u. dúonà*** guadagnarsi il pane.

**uþsiºmæs, -usi** *agg*, *prcp* occupato, impegnato; ***atléisk, ðia¹dien âð laba¤ u.*** scùsami, oggi sono molto impegnato.

**uþsiëmímas** *s* occupazione *m*; attivitâ; (*pamoka*, *paskaita*) lezione *m*.

**uþsieniìtis, -ë** *s* straniero -a; forestiero -a.

**uþsieniìtiðkas, -a** *agg* , **uþsienínis,** **-ë** *agg* straniero, êstero; forestiero.

**ùþsien|is** *s* êstero; ***~yje*** all'êstero; ***~io kalbâ*** lìngua straniera;***~io prekôba*** commêrcio êstero; ***Ùþsienio reikalÿ ministêrija*** Ministero degli affari êsteri; ***iðvýkti ñ ~á*** andare all'êstero.

**uþsigalvójæs, -usi** *prcp* soprappensiero *nkt*, sovrappensiero *nkt*.

**uþsigalvó|ti, ~ja, ~jo** *vx* pêrdersi nei pensieri; êssere soprappensiero *nkt*.

**uþsigardþ|iúoti, ~iúoja, ~iåvo** *vr* finire il pasto con una golositâ.

**uþsigãu|ti** *v* , ~na, uþsigåvo) **1.** (*kà*) farsi male (*a qcs*); **2.** (*ásiþeisti*) offêndersi, avêrsela a male; ***jís ba¤siai uþsigåvo*** si ê offeso a morte.

**uþsige¤|sti** *v* , ~dþia, uþsígeidë) (*kà daryti*) venire una gran vòglia (*di far qcs*); morire dalla vòglia; prêndere la smânia; ***daþna¤ uþsigeidþiù vãlgyti ðokolådo*** spesso mi viene una gran vòglia di mangiare cioccolata, spesso mi prende la smânia di mangiare cioccolata.

**uþsi|gérti** *v* (uþsígeria, ~g¸rë) (*kà kuo*) bersi (*qcs insieme a/con qcs*); (*kuo*) pasteggiare (*con qcs*).

**uþsi|gínti, uþsígina, ~gônë** *vr* negare; smentire.

**uþsigr¿din|ti, ~a, ~o** *vx* temprarsi.

**uþsigla÷|sti** *v* , ~dþia, uþsíglaudë) (*uþ ko*) stare appostato (*dietro qcs*); appostarsi.

**uþsigul¸|ti** *v* (uþsíguli, ~jo) **1.** stare sdraiato (*solit*. *troppo a lungo*); (*lovoje*) stare a letto; **2.** (*bûti nenaudojamam*) giacere (inevaso); rimanere a lungo (fermo); ***susiraðin¸jimo býlos uþsíguli m¸nesiais*** le prâtiche della corrispondenza giâcciono inevase da mesi.

**uþsiimin¸|ti, ~ja, ~jo** *vx* (*kuo*) occuparsi (*di qcs*); (*tam tikru verslu*) êssere attivo (*in un campo*, *in un settore*), operare; Þ **ve»stis 1.**; ***u. nåftos verslù*** operare nel settore petrolìfero.

**uþsii§ti** *v* (uþsíima, uþsíëmë) **1.** (*kuo*) occuparsi (*di qcs*); (*tam tikru verslu*) êssere attivo (*in un campo*, *in un settore*), operare; ***kuõ tù daba» uþsíimi?*** di che cosa ti occupi attualmente?; ***u. tarptautiniù kroviniÿ gabìnimu*** occuparsi di trasporti internazionali; ***u. kontrabãnda*** esercitare il contrabbando; **2.** (*prasidëti*) strìngere rapporti (*con qcn*); avere a che fare (*con qc*); **3.** (*sau vietà*) occuparsi (*un posto e sim*.); **4.:** ***mãlkos ja÷ uþsíëmë*** la legna ha giâ preso.

**uþsikabín|ti, ~a, ~o** *vx* **1.** (*uþ ko*) agganciarsi (*a qcs*), restare impigliato; incocciare (*contro qc*); ¨ ***neturiù ùþ kõ u.*** non so che pesci pigliare; **2.** (*kà ant peèiø ir pan*.) méttersi (*qcs addosso e sim*.); agganciarsi, appêndersi; ***u. a¹t petiìs ðãutuvà*** méttersi il fucile a tracolla.

**uþsikalb¸|ti** *v* (uþsíkalba, ~jo) trattenersi a parlare.

**uþsi|kãrti** *v* (uþsíkaria, ~kórë) ðnek accollarsi.

**uþsikêpti** *v* (uþsíkepa, uþsíkepë) ðnek (*kà daryti*) incaponirsi (*a/di far qcs*).

**uþsikimðímas** *s* **1.** ostruzione *m*; (*kamðatis*) ressa; **2.** med trombosi *m nkt*.

**uþsiki§ð|ti** *v* (uþsíkemða, ~o) **1.** ostruirsi; intasarsi; ***~o va§zdis*** si ê intasato un tubo; **2.** (*kà sau*) tapparsi (*qcs*); ***u. ausís (nósá)*** tapparsi le orécchie (il naso).

**uþsiki»|sti** *v* (uþsíkerta, ~to) **1.** (*apie mechanizmà*, *ginklà*) incepparsi, bloccarsi; **2.** prk (*kalbant*) impappinarsi, imbrogliarsi.

**uþsikirtímas** *s* inceppamento.

**uþsikíð|ti** *v* (uþsíkiða, ~o) ficcarsi (dentro), infilarsi; ***u. ausís*** méttersi i tappi nelle orécchie.

**uþsikl|ausôti** *v* , ~a÷so, ~a÷së) trattenersi ad ascoltare; pêrdersi ad ascoltare.

**uþsikló|ti, ~ja, ~jo** *vx* (*kuo*) coprirsi (*con qcs*); ***ðilta¤ u.*** rimboccarsi ben bene le coperte.

**uþsikniãub|ti, ~ia, ~ë** *vx* **1.** chinare il volto (*tra le mani e sim*.); **2.** (*ant ko*) curvarsi (*su qcs*), chinarsi.

**uþsikós|ëti** *v* , ~i, ~ëjo) avere un attacco di tosse, cominciare a tossire violentemente.

**uþsikrãu|ti** *v* , ~na, uþsikróvë) **1.** (*kà*) prêndere su di sé (*qcs*); caricarsi (*di qcs*); **2.** prk (*kà*) sobbarcarsi (*(a)* *qc*), accollarsi (*qc*), addossarsi.

**uþsikrºsti** *v* (uþsíkreèia, uþsíkrëtë) **1.** (*kuo*) prêndersi (*una malattia*), contrarre; ***u. gripù nuõ kõ*** prêndere l'influenza da qcn; **2.** prk lasciarsi / farsi contagiare, êssere contagiato; ***u. draugÿ entuziazmù*** farsi contagiare dall'entusiasmo degli amici.

**uþsilãuk|ti, ~ia, ~ë** *vx* aspettare in ânsia.

**uþsiléi|sti, ~dþia, ~do** *vr* darsi per vinto.

**uþsile¹k|ti** *v* , ~ia, uþsílenkë) **1.** piegarsi; **2.** ðnek (*mirti*) restarci (*morire*).

**uþsiliepsnó|ti, ~ja, ~jo** *vx* **1.** andare in fiamme, prêndere fuoco; ***medínis nåmas gre¤tai ~jo*** la casa di legno prese fuoco velocemente; **2.** prk avvampare; ***jõs ðirdís ~jo méile*** il suo cuore avvampò d'amore.

**uþsilík|ti** *v* (uþsiliìka, ~o) rimanere, restare; ***neda÷g yrâ mergínø, kuriõs mâno mintysê uþsiliìka*** non sono molte le ragazze che mi rimângono in mente.

**uþsilíp|ti** *v* (uþsílipa, ~o) salire, salìrsene.

**uþsi|manôti** *v* , ~måno, ~månë) (*ko*; *kà daryti*) venire vòglia (*di qcs*; *di far qcs*); avere vòglia; méttersi in testa (*qcs*; *di far qcs*); pensare (bene) (*di far qcs*).

**uþsimaskåvæs, -usi** *prcp* **1.** mascherato; in mâschera; **2.** kar mimeti@@ato; camuffato.

**uþsimask|úoti, ~úoja, ~åvo** *vx* **1.** mascherarsi; camuffarsi; **2.** kar mimeti@@arsi; camuffarsi; **3.** (*dëtis kuo*) spacciarsi (*per qcn*).

**uþsima÷kðlin|ti, ~a, ~o** *vx* (*kepuræ*) calcarsi (*il berretto e sim*.) in testa.

**uþsim|ãuti** *v* , ~ãuna, ~óvë) (*kà sau*) infilarsi (*qcs*), méttersi; ***u. batùs (kélnes, pi»ðtines)*** infilarsi le scarpe (i pantaloni, i guanti).

**uþsimêgzti** *v* (uþsímezga, uþsímezgë) **1.** (*mazgà*) farsi un nodo; **2.** (*ið mezgimo*) farsi a mâglia; **3.** prk (*prasidëti*) nâscere; cominciare; **4.** (*apie augalus*) far frutti, fruttificare.

**uþsimérk|ti, ~ia, ~ë** *vx* chiùdere gli occhi.

**uþsimêsti** *v* (uþsímeta, uþsímetë) **1.** buttarsi addosso; indossare in fretta; **2.** (*dingti*) (andare a) cacciarsi / ficcarsi; **3.** tarm (*kas kà daryti*) prêndere la smânia (*a qcn di far qcs*); **4.** ðnek impasticcarsi.

**uþsimìtæs, -usi** *prcp* ðnek impasticcato -a.

**uþsimiegójæs, -usi** *agg*, *prcp* intontito dal sonno.

**uþsiminímas** *s* accenno; allusione *m*.

**uþsimi¹ti** *v* (uþsímena, uþsíminë) (*apie kà*) accennare (*a qc*); (*duoti uþuominà*) allùdere; (*paminëti*) menzionare (*qcs*), far menzione (*di qcs*); ***nê ka»tà uþsíminë apiì saviþudôbæ*** non una volta aveva accennato al suicìdio.

**uþsimirðímas** *s* oblio (-lii).

**uþsimi»ð|ti** *v* , ~ta, ~o) **1.** dimenticare, dimenticarsi; (*nebegalvoti*) non pensarci più; ***gìria, kâd ~tø*** beve per dimenticare; **'** **2.** ðnek (*bûti uþmirðtam*) andare dimenticato; ***tóks fílmas neuþsimi»ðta*** un film così non si dimêntica.

**uþsimojímas** *s* **1.** slâncio; **2.** prk intento; mira.

**uþsimok¸|ti** *v* (uþsimóka, ~jo) pagare (*per sé*, *la propria parte*), pagarsi.

**uþsimó|ti, ~ja, ~jo** *vx* **1.** levare in alto (*un braccio*, *un'arma*) (*per colpire*); ***u. kalavijù*** brandire una spada; **2.** prk (*kà daryti*) avere intenzione (*di far qcs*), intêndere; (*pasiryþti*) decìdersi (*a far qcs*); ***plaèia¤ u.*** avere grandi mire; ***pe» plaèia¤ u.*** puntare troppo in alto.

**uþsimùðti** *v* (uþsímuða, uþsímuðë) ammazzarsi, trovare la morte.

**uþsinor¸|ti** *v* (uþsinóri, ~jo) (*kà daryti*) venire vòglia (*di far qcs*); avere vòglia (*di far qcs*); ***~jau gérti*** mi ê venuta vòglia di bere.

**uþsioþ|iúoti, ~iúoja, ~iåvo** *vx* impuntarsi *prk*, puntare i piedi.

**uþsipliì|ksti** *v* , ~skia, uþsíplieskë) **1.** prêndere fuoco; divampare; **2.** prk infiammarsi; **3.** prk (*parausti*) avvampare; (*ypaè ið gëdos*) diventare rosso, arrossire.

**uþsiprenumer|úoti, ~úoja, ~åvo** *vx* (*kà*) abbonarsi (*a qcs*), fare l'abbonamento.

**uþsip|ùlti** *v* , ~úola, ~úolë) (*kà*) assalire (*qcn*); dare addosso (*a qcn*).

**uþsirakín|ti, ~a, ~o** *vx* chiùdersi a chiave.

**uþsir|angôti** *v* , ~a¹go, ~a¹gë) arrampicarsi.

**uþsir|aðôti** *v* , ~åðo, ~åðë) (*kà*) prêndere nota (*di qcs*) (*per sé*), annotarsi (*qcs*), appuntarsi; ***u. mâno telefòno nùmerá*** annòtati il mio nùmero di telé­fo­no.

**uþsirãu|ti** *v* , ~na, uþsiróvë) ðnek (*ant ko*) incappare (*in qcn*).

**uþsiriõglin|ti, ~a, ~o** *vx* (*ant ko*) trascinarsi (*su qcs*), salire arrancando.

**uþsiregistr|úoti, ~úoja, ~åvo** *vx* registrarsi; iscrìversi; ***u. dãrbo bírþoje*** iscrìversi all'uffìcio di collocamento; ***u. pâs gôdytojà*** fissare un appuntamento (*per sé*) dal dottore.

**uþsirekomend|úoti, ~úoja, ~åvo** *vx* *rðt* dare (buona) prova di sé.

**uþsiríð|ti** *v* (uþsíriða, ~o) **1.** allacciarsi; annodarsi; ***u. batùs*** allacciarsi le scarpe; ***u. kaklåraiðtá*** annodarsi la cravatta, farsi il nodo alla cravatta; **2.:** ***u.*** ***akís*** bendarsi gli occhi.

**uþsir|ôti** *v* , ~ýja, ~íjo) soffocarsi (inghiottendo).

**uþsirõpð|ti** *v* , ~èia, uþsíropðtë) (*ant ko*) inerpicarsi (*su qcs*), arrampicarsi a fatica.

**uþsir|ûkôti** *v* , ~¾ko, ~¾kë) accêndersi una sigaretta (/ un sìgaro *e sim*.).

**uþsir¿stin|ti, ~a, ~o** *vx* adirarsi, incollerirsi.

**uþsis|agstôti** *v* , ~ågsto, ~ågstë) abbottonarsi.

**uþsi|sakôti** *v* , ~såko, ~såkë) ordinare; (*bilietà*, *stalà ir pan*.) prenotare; (*leidiná*) abbonarsi (*a qcs*); ***u. ala÷s*** ordinare una birra; ***u. ka§bará viìðbutyje (staliùkà restoranê)*** prenotare una câmera in albergo (un tâvolo al ristorante); ***u. „Lietuvõs rôtà"*** abbonarsi al *Lietuvõs rôtas*.

**uþsisëd¸|ti** *v* (uþsis¸di, ~jo) **1.** stare seduto (*troppo a lungo*); trattenersi; ***u. ikí vëlôvos naktiìs*** stare alzato fino a notte fonda; **2.** ðnek (*ant ko*) prêndere di mira (*qc*).

**uþsisêgti** *v* (uþsísega, uþsísegë) **1.** assicurarsi (*qcs con un fermaglio e sim*.), attaccarsi; (*sagomis*) abbottonarsi; (*smeigtuku*) appuntarsi; (*kà uþtrauktuku*) chiùdersi (*qcs*) con una (cerniera) lampo, chiùdersi la lampo (*di qcs*); (*apykaklæ*, *dirþà*, *vëriná)* allacciarsi; **2.** (*apie apykaklæ*, *dirþà*) allacciarsi, chiùdersi; abbottonarsi.

**uþsiskl|³sti** *v* , ~e¹dþia, uþsísklendë) **1.** (*apie duris*) chiùdersi (con il catenâccio); **2.** (*likti viduje*) chiùdersi (*dentro*), rinchiùdersi; **3.** prk chiùdersi, rinchiùdersi; ***u. savyjê*** chiùdersi in sé stesso.

**uþsispôrëlis,** **-ë** *s* testardo -a, cocciuto -a.

**uþsispôræs, -usi** *agg* cocciuto, testardo; ***u. ka¤p åsilas / oþýs*** cocciuto come un mulo.

**uþsispyrímas** *s* ostinazione *m*, cocciutâggine *m*, testardâggine; (*atkaklumas*) tenâcia, caparbietâ.

**uþsisp|írti** *v* (uþsíspiria, ~ôrë) **1.** (*nesutikti*) impuntarsi, piccarsi; (prk *ir apie arklá*, *asilà*) recalcitrare; **2.** (*pasiryþti*) (*kà daryti*) ostinarsi (*a far qcs*), intestardirsi; méttersi in testa (*di far qcs*).

**uþsispoksó|ti** *v* (uþsispõkso, ~jo) ðnek (*á kà*) incantarsi (a guardare) (*davanti a qc*).

**uþsistov¸|ti** *v* (uþsistóvi, ~jo) **1.** stare in piedi troppo; (*apie daiktus*) giacere fermo; **2.** (*apie vandená*) stagnare, ristagnare.

**uþsisvajó|ti, ~ja, ~jo** *vx* pêrdersi in sogni, sognare ad occhi aperti; êssere assorto (in fantasticherie).

**uþsisveè|iúoti, ~iúoja, ~iåvo** *vx* stare òspite troppo a lungo, trattenersi (da òspite).

**uþsiðnek¸|ti** *v* (uþsíðneka, ~jo) trattenersi a parlare, pêrdersi in chiâcchiere.

**uþsitarn|ãuti, ~ãuja, ~åvo** *vx* meritarsi; conquistarsi (*col lavoro e sim*.); ***u. kienõ malónæ*** conquistarsi il favore di qcn.

**uþsit³s|ti** *v* , ~ia, uþsítæsë) protrarsi, prolungarsi; ***derôbos uþsítæsë ikí våsaros*** le trattative si protrâssero fino all'estate.

**uþsitíkrin|ti, ~a, ~o** *vx* assicurarsi, garantirsi; ***u. ðìðtàjà viìtà*** assicurarsi il sesto posto; ***u. deðiniÿjø påramà*** garantirsi il sostegno delle destre.

**uþsitrãuk|ti, ~ia, ~ë** *vx* **1.** tirarsi su; ***u. a¹tklodæ*** tirarsi su la coperta; **2.** (*pasidengti*) coprirsi; (*ledu*) gelarsi; ***þaizdâ ~ë*** si ê formata la crosta sulla ferita; **3.** (*rûkant*) aspirare; **4.** prk (*kà*) incórrere (*in qcs*); attirarsi (*qcs*), attirare su di sé; ***u. ba÷smæ (bºdà)*** incórrere in una punizione (in un guâio); ***u. g¸dà*** coprirsi di disonore; ***u. visÿ neapôkantà*** attirarsi l'òdio di tutti; ***u. kõ nemalónæ*** pêrdere il favore di qc.

**uþsitre¹k|ti** *v* , ~ia, uþsítrenkë) (*apie duris*) chiùdersi con un botto; sbâttere.

**uþsiùnd|yti, ~o, ~ë** *vx* (*ðuná ant ko*) sguinzagliare (*un cane dietro a qc*).

**uþsiù|sti** *v* (uþsiu¹ta, ~to) andare su tutte le fùrie.

**uþsi¿ti** *v* (ùþsiuva, uþsiùvo) **1.** cucire (*sopra*); ***u. lõpà*** cucire una toppa; **2.** (*uþtaisyti*) ricucire, rammendare; ***u. skýlæ*** rammendare un buco; ***u. þa¤zdà*** ricucire una ferita.

**uþsiùtin|ti, ~a, ~o** *vx* mandare su tutte le fùrie.

**uþsive»|sti** *v* , ~èia, uþsívertë) **1.** (*ant savæs*) caricarsi addosso; **2.** (*apie knygà*) chiùdersi; (*apie lapà*) voltarsi; **3.** ðnek rimanerci secco.

**uþsivérti** *v* (uþsíveria, uþsiv¸rë) chiùdersi.

**uþsivêsti** *v* (uþsíveda, uþsívedë) **1.** tirarsi dietro, portare / condurre con sé; **2.** (*apie variklá*) méttersi in moto, partire; **3.** prk ðnek carburare, ingranare.

**uþsiviµk|ti** *v* (uþsívelka, ~o) vestirsi; (*kà*) méttersi (*qcs*), infilarsi; indossare; ***u. ðva»kà*** infilarsi la giacca.

**uþsiþa¤|sti** *v* , ~dþia, uþsíþaidë) giocare troppo a lungo, trattenersi a giocare.

**uþsiþiìb|ti** *v* , ~ia, uþsíþiebë) (*pvz*., *sau lempà*) accêndersi (*qcs*).

**uþsiþiopsó|ti** *v* (uþsiþiõpso, ~jo) **1.** non vedere (per disattenzione); **2.** Þ **uþsiþiûr¸ti**.

**uþsiþiûr¸|ti** *v* (uþsiþi¾ri, ~jo) (*á kà*) incantarsi (*davanti a qcs*).

**ùþsklanda** *s* **1.** (*pieðinys*) vignetta; **2.** (*sklàstis*) chiavistello; **3.** inf (*ekrano*) salvaschermo.

**uþskle¤|sti** *v* , ~dþia, ùþskleidë) (*knygà*) chiùdere; (*puslapá*) voltare.

**uþskl|³sti** *v* , ~e¹dþia, ùþsklendë) (*duris*) serrare; méttere il chiavistello / il catenâccio (*a una porta*); sprangare (*una porta*).

**uþskrí|sti** *v* (ùþskrenda, ~do) **1.** passare in volo; (*uþ ko*) volare dietro (*qc*); **2.** av fare scalo; ***u. degalÿ*** fare scalo per rifornimento.

**uþslºpti** *v* (ùþslepia, ùþslëpë) celare, occultare.

**uþsli¹k|ti** *v* (ùþslenka, ~o) **1.** (*apie naktá ir pan.*) calare; ***~o sùtemos*** sono calate le téne­bre; **2.** (*prisipildyti þemës*) interrarsi.

**uþslopín|ti, ~a, ~o** *vx* *ir* prk soffocare.

**uþsmãug|ti, ~ia, ~ë** *vx* strangolare, strozzare.

**uþsma÷k|ti** *v* , ~ia, ùþsmaukë): ***u. skrôbëlæ a¹t akiÿ*** calare / calarsi il cappello sugli occhi.

**uþsníg|ti** *v* (uþsni¹ga, ~o) (*kà*) nevicare (*su qcs*); ***vískas ùþsnigta*** ê tutto bianco (*per la neve*); ***kìlià ~o*** la strada ê coperta di neve.

**uþsn¿|sti** *v* , ~ta, ~do) assopirsi, appisolarsi; ***~dau priì televízoriaus*** mi sono appisolato davanti al televisore.

**uþsodín|ti, ~a, ~o** *vx* **1.** far sedere; **2.** (*kà augalais*) piantare (*in un posto piante*); (*miðku*) rimboschire, imboschire.

**uþspãrd|yti, ~o, ~ë** *vt* malmenare a calci.

**ùþsparnis** *s* av ipersostentatore *v*, flap *v nkt*.

**uþspãu|sti** *v* , ~dþia, ~dë) **1.** chiùdere premendo; ***u. kãm akís*** chiùdere gli occhi a qcn (*a un morto*); **2.** (*negyvai*) schiacciare (*investendo e sim*.); **3.** (*pvz*., *cigaretæ*, *nuorûkà*) spégnere; **4.:** ***u. a¹tspaudà*** apporre un timbro.

**uþspe¤|sti** *v* , ~èia, ùþspeitë) ðnek confinare; ¨ ***u. kampê / ñ ka§pà*** strìngere all'ângolo, méttere alle corde.

**uþspri¹g|ti** *v* , ~sta, ~o) strozzarsi; (*kas kuo*) andare di traverso (*a qcn qcs*); ***uþspringa÷ kçsniu*** mi ê andato un boccone di traverso.

**ùþstal|ë** *s* posto a tâvola (*propr*. *dietro la tavola*); ***~ës dainâ*** canzone *m* conviviale.

**ùþstat|as** *s* depòsito (cauzionale); (*ið kalëjimo paleisti*) cauzione *m*; (*daiktinis*) pegno; ***ùþ ~à*** dietro / su cauzione; ***iðpi»kti ~à*** disimpegnare (*qcs*), riscattare; ***dúoti ~à, palíkti ~à*** lasciare un depòsito, dare (*qcs*) in pegno.

**uþståtymas** *s* **1.** (*teritorijos*) edificazione *m*; **2.** (*kelio ir pan.*) ostruzione *m*; **3.** (*turto*) cessione *m* in pegno.

**uþst|atôti** *v* , ~åto, ~åtë) **1.** dare in pegno, impegnare; (*nekilnojamàjá turtà*) ipotecare; ***u. påpuoðalus lombãrde*** impegnare i gioielli (al banco / al monte dei pegni); **2.** (*pastatyti*, *kad bûtø kliûtimi*) ostruire; ***neuþstatôkite ávaþiåvimo*** non ostruite l'accesso; **3.** (*pvz*., *plotà*) edificare; **4.:** ***u. la¤krodá*** puntare la svêglia.

**uþstatôtojas, -a** *s* teis debitore -trice ipotecârio -a.

**uþsteµb|ti, ~ia, ùþstelbë** *vt* soffocare (*di piante*).

**uþstó|ti, ~ja, ~jo** *vx* **1.** (*bûti kliûtimi*) ostruire; impedire (*di vedere e sim*.); coprire; ***u. kìlià*** bloccare / ostruire la strada; ***u. kãm ðviìsà*** tògliere / coprire la luce a qcn; ***u. va¤zdà*** ostruire la visuale; **2.** prk (*ginti*) (*kà*) difêndere (*qc*), prêndere le difese (*di qc*); ***mamâ visadâ tavê ~ja*** la mamma prende sempre le tue difese; **3.** (*pvz*., *apie naktá*) calare; ***staigâ ~jo tylâ*** all'improvviso calò il silênzio.

**uþstríg|ti** *v* (uþstri¹ga, ~o) bloccarsi; incepparsi; ***põpierius ~o spausdintuvê*** si ê inceppata la carta nella stampante; ***~o e¤smas dºl avårijos*** il trâffico ê bloccato a câusa di un incidente.

**ùþstûma** *s* paletto; catenâccio.

**uþst|ùmti, ùþstumia, ~¿më** *vt* **1.** spìngere (*su*); **2.** (*sklàsèiu*) incatenacciare.

**uþsùk|ti** *v* (ùþsuka, ~o) **1.** (*iðjungti*) chiùdere; ***u. dùjø èiãupà*** chiùdere il rubinetto del gas; ***u. rådijà*** spêgnere la râdio; **2.** (*sukant átvirtinti*) avvitare; **3.** (*pvz*., *laikrodá*, *þaislà*) caricare; **4.** (*uþeiti*) passare; fare un salto / una scappata; ***u. ñ kavínæ*** fare un salto al bar; ***u. pâs k± (ñ sveèiùs)*** fare una visitina a qcn.

**uþsuktùvas** *s* giravite *v*.

**uþðãldymas** *s* congelamento; (*ðaldiklyje*) surgelamento.

**uþðãldytas,** **-a** *prcp* congelato; (*tik apie maistà*) surgelato; ***u. embriònas*** un embrione congelato.

**uþðãld|yti, ~o, ~ë** *vx* congelare, ghiacciare; (*ðaldiklyje*) surgelare; prk ***u. kãinas*** congelare i prezzi.

**uþðalímas** *s* congelamento.

**uþðãlti** *v* (uþð±la, uþðålo) gelare (*in superficie*), ghiacciare; ghiacciarsi; ***ìþeras uþðålo*** il lago si ê ghiacciato.

**uþðãu|ti** *v* , ~na, uþðóvë) (*duris*) serrare; méttere il chiavistello / il catenâccio (*a una porta*); sprangare (*una porta*); (*sklàstá*) serrare.

**uþðnìkin|ti, ~a, ~o** *vx* (*kà*) méttersi a parlare (*con qcn*), rivòlgere la parola (*a qcn*); attaccare bottone / discorso (*con qcn*).

**uþðók|ti, ~a, ~o** *vx* **1.** (*ant ko*) saltare sopra / su (*qc*); balzare sopra (*(a) qc*); **2.** (*uþsukti*) fare un salto; **3.:** ***u. a¹t pasalõs*** cadere in un agguato.

**ùþðovas** *s* catenâccio.

**uþta¤** *avv* Þ **uþtât**.

**ùþtaisas** *s* kar cârica; ***branduolínis ù.*** testata nucleare.

**uþtaisôti** *v* (uþta¤so, uþta¤së) **1.** (*ginklà*) caricare; **2.** (*angà*) tappare, otturare; (*uþlopyti*) rattoppare; **3.** prk combinare (*un guaio*); ***u.*** ***kãm*** ***va¤kà*** méttere incinta qcn.

**ùþtakis** *s* insenatura; (*tik jûros*) cala.

**uþtampôti** *v* (uþta§po, uþta§pë) strapazzare.

**uþtãmsin|ti, ~a, ~o** *vx* (*uþstoti ðviesà*) (*kà*) tenere in ombra (*qcs*), fare ombra (*a qc*).

**uþtar¸jas,** **-a** *s* intercessore, interceditrice; (*globëjas*) protettore -trice.

**uþtarímas** s intercessione *m*; (*globëjimas*) protezione *m*.

**uþtarnãut|as,** **-a** *prcp* meritato; ***~a bausmº*** casti­go meritato.

**uþtarn|ãuti, ~ãuja, ~åvo** *vx* meritare, meritarsi, conquistarsi (*col lavoro e sim*.).

**uþta»ti** *v* (ùþtaria, ùþtarë) (*kà*) intercédere (*per qc*), intervenire (*in favore di qcs*); ***u. þodìlá ùþ k±, u. k± þodeliù*** méttere una buona parola per qcn.

**uþtât** *avv* **1.** perciò, per questo; **'** **2.** Þ **taèia÷**.

**uþtek¸|ti** *v* (ùþteka, ~jo) sòrgere (*dei corpi celesti*), spuntare.

**uþtºkðti** *v* (ùþteðkia, ùþtëðkë) (*kà kuo/ko*) schizzare (*sopra*) (*qc di qcs*), spruzzare.

**uþtêk|ti** *v* (uþte¹ka, uþtìko) bastare, êssere sufficiente; êsserci abbastanza; ***ðiãm dãrbui uþte¹ka pùsdienio*** per questo lavoro basta me@@a giornata; ***~s viìtos visîems*** ci sarâ abbastanza posto per tutti; ***uþtìko påso kòpijos gãuti leidímui*** ê stata sufficiente una còpia del passaporto per ottenere il permesso; ***uþte¹ka pasakôti*** basta dirlo; ***~s!, uþte¹ka!*** basta così!

**uþtektina¤** *avv* abbastanza, quanto basta; a sufficienza, sufficientemente; ***netùrime u. pinigÿ*** non abbiamo denaro a sufficienza.

**uþtémdymas** *s* oscuramento; *ir* prk offuscamento.

**uþtémd|yti, ~o, ~ë** *vx* **1.** oscurare; *ir* prk offuscare; ottenebrare; ***u. lémpà*** oscurare una lâmpada; **2.** (*dangaus kûnà*) eclissare.

**uþtemímas** *s* **1.** (*dangaus kûno*) eclissi *m nkt*; ***dalínis (vísiðkas) sãulës u.*** eclissi parziale (totale) di sole; **2.** prk offuscamento, ottenebramento.

**uþte§p|ti** *v* , ~ia, ùþtempë) **1.** téndere; **2.** (*uþneðti*) trasportare (a fatica) (*sopra*).

**uþtém|ti** *v* , ~sta, uþtìmo) **1.** oscurarsi, offuscarsi; **2.** prk offuscarsi; ottenebrarsi; ***jãm uþtìmo prõtas*** gli si offuscò / si ottenebrò la mente.

**uþte¹kamai** *avv* quanto basta, abbastanza; a sufficienza; ***turiù u. la¤ko*** ho abbastanza tempo.

**uþte¹kam|as,** **uþtenkamâ** *agg* sufficiente; bastévole; ***sijâ ~o iµgio*** una trave di lunghezza sufficiente.

**uþtêpti** *v* (ùþtepa, ùþtepë) **1.** (*dëti sluoksná*) spalmare, stêndere; ***u. k± daþa¤s*** dare una mano di vernice a qcs; ***u. svîesto a¹t dúonos*** spalmare del burro sul pane, imburrare il pane; **2.** (*skylæ*) otturare, tappare.

**uþterðímas** *s* inquinamento; (*vandens ir pan*.) contaminazione *m*.

**uþte»ð|ti** *v* , ~ia, ùþterðë) inquinare *ir prk*, contaminare; ***fabrika¤ ~ia ùpiø vãndená*** le fâbbriche inquìnano le âcque dei fiumi.

**uþterðtùmas** *s* (grado / tasso di) inquinamento; ***óro u.*** inquinamento dell'âria.

**uþt³s|ti** *v* , ~ia, ùþtæsë) tirare per le lunghe / in lungo; ***u. susirinkímà*** tirare una riunione per le lunghe.

**¾þti** *v* (¾þia, ¾þë) **1.** fare rumore; rumoreggiare; (*apie jûrà*, *vëjà*) mugghiare; muggire; (*apie medþius*, *vëjà*) frusciare, stormire; (*apie vabzdþius*) ron@are; (*apie maðinà*) rombare; ***mãn ausysê ¾þia*** mi ròn@ano le orécchie; **2.** prk (*triukðmauti*) fare baccano / chiasso; (*linksmintis*) fare baldoria / bisbòccia; **3.** (*bartis*) (*ant ko*) rimbrottare (*qcn*).

**ùþtiesalas** *s* telo; (*stalo*) copritâvola; (*lovos*) co­pri­letto, sopraccoperta.

**uþtiìs|ti** *v* , ~ia, ùþtiesë) (*kà kuo*) stêndere (*su qcs qcs*); coprire (*con qcs qcs*); ***u. stålà stãltiesë*** stêndere la tovâglia sul tâvolo.

**uþtíkrinimas** *s* assicurazione *m*, garanzia; ***taikõs u.*** garanzia di pace.

**uþtíkrin|ti, ~a, ~o** *vx* assicurare, garantire; ***u. demokråtinius rinkimùs*** assicurare elezioni democrâtiche; ***u. saugiâs dãrbo sçlygas*** garantire la sicurezza delle condizioni di lavoro.

**uþtíkrintai** *avv* con sicurezza, in modo sicuro.

**uþtíkðti** *v* (uþtýðka, uþtíðko) schizzare.

**uþtík|ti** *v* (uþti¹ka, ~o) **1.** (*kà*) imbâttersi (*in qcn*); (*rasti*) trovare (*qc*); scovare; ***u. låpës p¸dsakus*** scovare le tracce di una volpe; **2.** (*uþklupti*) sorprêndere, cògliere (di sorpresa); ***u. dù ásibróvëlius*** sorprêndere due intrusi; ***uþtika÷ vôrà sù kitâ lóvoje*** ho sorpreso mio marito a letto con un'altra.

**uþti»p|ti, ~sta, ~o** *vi* intorpidirsi; ***mãn ~o ran­kâ*** ho la mano indolenzita.

**uþtrãuk|ti, ~ia, ~ë** *vx* **1.** (*uþmauti*) infilare (*tirando*); **2.** (*uþdengti*) coprire (*tirando*); (*uþuolaidà*) tirare (*una tenda e sim*.); **3.** (*uþneðti*) trasportare (*sopra*); (*uþvilkti*) trascinare (*sopra*); **4.** (*rûkant*) aspirare (una boccata); ***u. pôpkæ*** aspirare una boccata con la pipa; ðnek ***u. dûmìlá*** dare una tirata / un tiro; **5.** (*dainà*) attaccare (a cantare); **6.:** ***u. kílpà (måzgà)*** strìngere un câppio (un nodo); **7.** prk (*kam bëdà*) cacciare nei guai (*qcn*); (*kam gëdà*) disonorare (*qc*); **8.:** *rðt* ***u. påskolà*** eméttere un prêstito; **9.** teis comportare; ***u. ba÷dþiamàjà atsakomôbæ*** comportare responsabilitâ penali.

**uþtrauktùk|as** *s* cerniera lampo; lampo *m nkt*, @ip *m nkt*; ***atsisêgti kélniø ~à*** tirarsi giù la lampo dei pantaloni.

**uþtre¹k|ti** *v* , ~ia, ùþtrenkë) sbâttere; chiùdere sbattendo; ***u. kãm panósëje durís*** sbâttere la porta in fâccia a qcn.

**uþtrínti** *v* (ùþtrina, uþtrônë) **1.** (*glaistu*) stuccare; **2.** prk mascherare; dissimulare.

**uþtró|kðti** *v* , ~kðta, ~ðko) soffocare, soffocarsi; ***u. nuõ d¿mø*** soffocare per il fumo.

**uþtrùk|ti** *v* (uþtru¹ka, ~o) **1.** trattenersi; (*sugaiðti*) pêrdere tempo, indugiare; (*kà daryti*) tardare (*a far qcs*); ***uþtruka÷ pùsvalandá universitetê*** mi sono trattenuto me@@'ora all'universitâ; ***tík neuþtrùkit!*** non ci metta troppo!; **2.** (*tæstis*) durare; (*ilgai*) protrarsi; ***pósëdis ~o trís vålandas*** la seduta ê durata tre ore; ***bylâ ~o kìtverius metùs*** il processo si ê protratto per quattro anni.

**uþtr¿k|ti** *v* , ~sta, ~o) (*apie karvæ*) non dare più latte (*di mucca*), sméttere di dare latte.

**uþtuð|úoti, ~úoja, ~åvo** *vx* **1.** ombreggiare; **2.** prk (*paslëpti*) mascherare (*i difetti e sim*.).

**ùþtvanka** *s* diga; chiusa; (*pylimas*) ârgine *v.*

**ùþtvara** *s* **1.** (*kliûtis*) sbarramento; ***mínø ù.*** campo minato; ***polícininkø ù.*** cordone *v* di polizia; (*kelyje*) posto di blocco; **2.** (*tvora*) recinzione *m*; ***spygliúota ù.*** (recinzione *m* di) filo spinato; reticolato; **3.** sport (*krepðinyje*) blocco; (*tinklinyje*) muro.

**ùþtvaras** *s* barriera; sbarra.

**uþtve¹k|ti** *v* , ~ia, ùþtvenkë) **1.** sbarrare (*solit*. *con una diga e sim*.); **2.** prk ingombrare.

**uþtveriam|âsis, -óji** *prcp* : kar ***~óji ugnís*** fuoco / tiro di sbarramento.

**uþtvërímas** *s* sbarramento.

**uþtv|érti** *v* (ùþtveria, ~¸rë) **1.** recintare, recìngere; **2.** prk (*sudaryti kliûtá*) sbarrare, bloccare; ***keliÿ polícija ~¸rë kìlià*** la polizia stradale ha sbarrato la strada.

**uþtvíndymas** *s* inondazione *m*, allagamento.

**uþtvínd|yti, ~o, ~ë** *vx* **1.** allagare, inondare; *ir* prk sommêrgere; ***li¿tis ~ë gatvês*** la piòggia torrenziale ha allagato le strade; **2.** prk (*apie þmones*) invâdere; ***pirk¸jai ~ë na÷jà prekôbos ce¹trà*** i clienti hanno invaso il nuovo supermercato.

**uþtvín|ti** *v* , ~sta, ~o) allagarsi, êssere inondato.

**'** **uþtvírtin|ti, ~a, ~o** *vx* Þ **patvírtinti**.

**uþùitas,** **uþuitâ** *agg*, *prcp* oppresso, vessato; maltrattato.

**uþúojaut|a** *s* condoglianze *dgs*; (*gailestis*) compassione *m*, pietâ; ***ve»tas ~os*** compassionévole, degno di compassione; ***nuoðirdí u.*** sentite condoglianze; ***paréikðti kãm ~à*** esprìmere le (pròprie) condoglianze a qcn; ***ródyti ~à*** mostrare pietâ.

**uþúolaid|a** *s* tenda; ***uþkabínti (uþtrãukti) ~as*** méttere / attaccare (tirare) le tende; ***lóvos u.*** cortinâggio.

**uþúolank|a** *s* deviazione *m*, giro lungo; ***darôti dídelæ ~à*** fare una lunga deviazione; prk ***sakôti bê ~ø*** dire senza giri di parole / senza me@@i têrmini / senza perìfrasi.

**uþúolankom(is)** *avv* aggirando *vksm*; (*aplink*) attorno; prk ***kalb¸ti u.*** parlare con giri di parole; menare il can per l'âia *flk*.

**uþúomarða** *s com* smemorato -a, dimenticone -a.

**uþúomazg|a** *s* **1.** lit esòrdio (-di); **2.** biol, prk embrione *v*; germe *v*; ¨ ***uþgniãuþti ~oje*** stroncare sul nâscere.

**uþuomazgínis, -ë** *agg* germinale (*ir* prk).

**uþúomin|a** *s* allusione *m*; (*uþsiminimas*) accenno; ***neãiðki u.*** un'oscura allusione; ***padarôti (suprâsti) ~à*** fare (cògliere) un'allusione; ***kalb¸ti ~omis*** parlare per allusioni.

**uþúo|sti** *v* , ~dþia, ~dë) **1.** (*kà*) sentire odore (/ puzza) (*di qc*); (*apie ðuná*) fiutare; ***u. dujâs (kìpsná)*** sentire puzza di gas (odore di arrosto); **2.** prk subodorare, fiutare.

**uþúot** *conj* (*kà padarius*) invece (*di far qcs*); al posto; ***u. pad¸jæs, tík trukda¤*** invece di aiutarmi non fai altro che disturbare; ***u. ºmæsi dãrbo, skùndþiatës*** invece di méttervi a lavorare, vi lamentate.

**uþúovëja** *s* **1.** posto / luogo al riparo (dal vento); (*laive*) sottovento *nkt*; **2.** prk riparo, rifùgio.

**ùþupis** *s* @ona oltre il / un fiume; (*mieste*) quartiere *v* al di lâ del fiume.

**uþùtëkis** *s* geogr cala; bâia.

**uþùþvakar** *avv* tre giorni fa.

**ùþvadas** *s com* (1, 3^b^) (*pavaduotojas*) sostituto -a; (*padëjëjas*) aiutante *v/m*.

**uþvad|úoti, ~úoja, ~åvo** *vx* **1.** (*kà*) sostituire (*qcn*), fare le veci (*di qcn*); supplire (*qcn*); **2.** (*atstoti*) (*kà*) prêndere il posto (*di qc*); (*pakeisti*) rimpiazzare (*qcn*).

**uþvaikô|ti, uþva¤ko, uþva¤kë** *vt* sfiancare.

**ùþvakar** *avv* l'altro ieri, ieri l'altro.

**ùþvakarykðtis, -ë** *agg* dell'altro ieri.

**uþvaldôti** *v* (uþvaµdo, uþvaµdë) (*kà*) impossessarsi (*di qcs*), impadronirsi; (*tik apie jausmus*) sopraffare (*qc*); ***u. svìtimà tu»tà*** impossessarsi di un bene altrui; ***jñ uþvaµdë bãimë*** fu sopraffatto dalla paura.

**uþvãlg|yti, ~o, ~ë** *vx* mangiare (*un po'*); mangiare un boccone, fare uno spuntino.

**ùþvalkalas** *s* fòdera; (*pagalvës*) fêdera; copricuscino; (*pûkinës antklodës*) copripiumone *v*; (*knygos*) sovraccoperta.

**ùþvalkas** *s* Þ **ùþvalkalas**.

**uþvãrst|yti, ~o, ~ë** *vx*: ***u. batùs*** méttere / infilare i lacci alle scarpe.

**uþvaþ|iúoti, ~iúoja, ~iåvo** *vx* **1.** passare (*a piedi*); fare un salto; ***u. pâs k± (ñ sveèiùs)*** andare / passare a trovare qcn; **2.** (*uþ ko*) passare (dietro), spostarsi (dietro); **3.** (*ant ko*) salire; **4.** ðnek (*suduoti*) dare un cazzotto.

**uþve¤s|ti** *v* , ~ia, ùþveisë) allevare; (*tik augalø*) coltivare; (*gerinant veislæ*) selezionare.

**uþverb|úoti, ~úoja, ~åvo** *vx* ingaggiare; reclutare.

**ùþverktas,** **uþverktâ** *agg*, *prcp* (*apie akis*) rosso di pianto (*degli occhi*), arrossato dal pianto.

**ùþverstas,** **uþverstâ** *prcp*, *agg* **1.** (*kuo*) ingombro, pieno (*di qcs*); cârico; ***stålas ù. põpieriais*** un tâvolo ingombro di carte; ***âð ù. dãrbu*** sono cârico / pieno / oberato di lavoro; **2.** (*apie knygà*) chiuso; (*apie kortà*) coperto.

**uþve»|sti** *v* , ~èia, ùþvertë) **1.** (*uþkrauti*) (*kà kuo*) riempire (*qcs di qcs*), ingombrare; ammucchiare (*su qcs qcs*); (*pvz*., *kelià*) ostruire; **2.** prk (*kà kuo*) inondare (*qc di qcs*), sommêrgere; subissare; ***u. pråðymais*** subissare di richieste; ***u. ri¹kà pigiomís prìkëmis*** inondare il mercato di merci a basso prezzo; **3.** (*knygà*) chiùdere; **4.:** ***~tæs gãlvà*** con la testa in su / all'indietro; **5.:** ¨ ***u. kójas*** tirare le cuòia.

**uþvérti** *v* (ùþveria, uþv¸rë) (*uþdaryti*) chiùdere, serrare.

**uþve»þ|ti** *v* , ~ia, ùþverþë) strìngere (forte); med ***u. kraujågyslæ*** allacciare una vena.

**uþvêsti** *v* (ùþveda, ùþvedë) **1.** (*ant ko*) portare (*su qcs*), condurre; **2.** (*uþ ko*) portare (*dietro qcs*), condurre; **3.** *rðt* (*pradëti* *kà*) dare il via (*a qcs*), far partire (*qcs*); ***u. kaµbà*** dare il via a una conversazione; ***u. knýgà*** méttere in cantiere un libro; teis ***u. býlà*** istruire un processo; **4.** (*trumpam nuvesti*) portare (a fare un salto / una vìsita); **5.:** ***u. motòrà*** far partire il motore; ***u. maðínà*** méttere in moto la mâcchina.

**uþvê|þti** *v* (ùþveþa, ùþveþë) **1.** (*pakeliui*) passare a portare; lasciare; ***~ðiu tãu lagamínà*** passerò a portarti la valìgia; ***~þk manê pâs tëvùs*** lâsciami dai miei; **2.** (*á virðø*) trasportare (*sopra/su*); **3.** ðnek (*suduoti*) dare un ceffone; **4.** ðnek (*apie narkotikus*) fare sballare.

**uþvílkin|ti, ~a, ~o** *vx* tirare in lungo; (*nukelti*) rimandare, rinviare; differire *rðt*; ***u. býlà*** tirare in lungo un processo; ***u. iðvykímo diìnà*** rimandare il giorno della partenza.

**uþviµk|ti, ùþvelka, ~o** *vt* **1.** trascinare (sopra); **2.** (*apvilkti*) rivestire; **3.** (*uþtæsti*) protrarre.

**uþvírin|ti, ~a, ~o** *vx* (far) bollire; ***u. pîenà*** bollire il latte.

**uþvi»|sti** *v* , ~sta, ~to) cadere (*sopra*); crollare, abbâttersi (al suolo).

**uþvírti** *v* (uþvérda, ùþvirë) **1.** (cominciare a) bollire; sobbollire; ¨ ***u. kõðæ*** piantare grane; combinare un casino; **2.** prk scoppiare; ***ùþvirë dãrbas*** il lavoro ê partito di gran lena; il lavoro ferve; ***ùþvirë muðtýnës*** ê scoppiata una rissa; ***u. pykèiù*** scoppiare (d'ira).

**uþvís** *avv* in assoluto; di tutti; ***u. ge­riãusia*** la cosa mi­gliore in assolu­to; il mêglio.

**uþvôti** *v* (ùþveja, uþvíjo) **1.** scacciare (*sopra*); **2.** (*siûlus*) ravvòlgere.

**'** **uþviz|úoti, ~úoja, ~åvo** *vx* Þ **vizúoti**.

**uþvóþ|ti, ~ia, ~ë** *vx* **1.** (*uþdengti*) coprire; (*dangèiu*) (*kà*) abbassare il copêrchio (*di qcs*); **2.** ðnek (*suduoti*) tirare un cazzotto.

**uþþéldin|ti, ~a, ~o** *vt* far coprire di vegeta­zio­ne; rinverdire.

**uþþélti** *v* (ùþþelia, uþþ¸lë) (*kuo*) riempirsi (*di* *erba*, *erbacce*).

**uþþe»ti** *v* (ùþþeria, ùþþërë) spârgere; (*uþpilti*) rovesciare (*sopra*); ***u. duõbæ*** colmare una fossa.

**uþþiìb|ti** *v* , ~ia, ùþþiebë) accêndere.
`;
