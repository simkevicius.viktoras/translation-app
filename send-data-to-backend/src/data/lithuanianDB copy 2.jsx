export const lithuanianDB = `
**ùbagas** *s* , **ùbagë** *s* Þ **eµgeta**.

**¿b|auti** *v* , ~auja, ~avo) chiurlare, fare chiù.

**¿bavimas** *s* strido (*dgs* -i *v*, -a *mot*), chiù *v nkt*.

**¿dra** *s* zool lontra.

**ufonãutas** *s* ðnek extraterrestre, ufo *nkt*.

**ugandiìtis,** **-ë** *s* ugandese *v/m*.

**ùgdym|as** *s* **1.** (*auginimas*) crêscita, allevamento; (*auklëjimas*) educazione *m*; ***vaikÿ*** ***~o*** ***programâ*** un programma per l'educazione dei bambini; **2.** (*rengimas*) addestramento, allenamento; formazione *m*; ***kådrø*** ***ù.*** formazione del personale.

**ugdôti** *v* (ùgdo, ùgdë) **1.** (*auginti*) crêscere, allevare; (*auklëti*) educare; ***u.*** ***jaunímà*** educare i giòvani; **2.** (*rengti*) addestrare, formare; **3.** prk (*plëtoti*) sviluppare; (*þadinti*) stimolare, suscitare; ***u.*** ***savígarbà*** sviluppare l'autostima; ***u.*** ***tëvýnës*** ***méilæ*** suscitare l'amore per la pâtria.

**ugdôtinis,** **-ë** *s* allievo -a.

**ugdôtojas,** **-a** *s* educatore -trice.

**¾g|is** *s* **1.** altezza; statura; ***vôras*** ***ãukðto*** ***(måþo*** ***/*** ***þìmo,*** ***vidutínio)*** ***~io*** un uomo di alta (bassa, mêdia) statura; ***pagaµ*** ***~á*** in órdine di altezza; ***jís*** ***mâno*** ***~io*** ê alto come me; **2.** (*dydis*) tâglia.

**¾glis** *s* pollone *v.*

**ugnìlë** *s dimin* fiammella.

**ugniages|ýs** *s* (3^4b^, 3^b^) vìgile *v/m* del fuoco, pompiere (-a); ***~iÿ*** ***komãnda*** squadra di vìgili del fuoco.

**ugniåkuras** *s* **1.** *ir* tech focolare *v*; (*pakûra*) fornello; **2.** (*lauþas*) falò.

**ugniåkuras** *s* **1.** focolare *v*; **2.** (*lauþas*) falò.

**ugniaspaµvis, -ë** *agg* color fuoco *nkt*.

**ugniåvietë** *s* **1.** focolare *v*; **2.** kar postazione *m*.

**ugniåþolë** *s* bot celidònia.

**ugníkalnis** *s* vulcano; ***uþgìsæs*** ***(ve¤kiantis)*** ***u.*** vulcano spento (in attivitâ).

**ugníngai** *avv* con fuoco, con ardore.

**ugníng|as,** **-a** *agg* prk di fuoco, ardente; ***u.*** ***þviµgsnis*** uno sguardo di fuoco; ***~i*** ***þõdþiai*** parole di fuoco / roventi.

**ugnínis,** **-ë** *agg* di fuoco.

**ugn|ís** *s* **1.** fuoco; (*liepsna*) fiamma; ***ùgná*** ***kùrti*** accêndere il fuoco; ***d¸ti*** ***púodà*** ***a¹t*** ***~iìs*** méttere una péntola sul fuoco; ðnek ***dúok*** ***mãn*** ***~iìs*** dammi da accêndere; prk ***méilës*** ***u.*** la fiamma dell'amore; ¨ ***~iìs*** ***kríkðtas*** battésimo / prova del fuoco; ***þa¤sti*** ***sù*** ***~imí*** giocare col fuoco; ***ñ*** ***ùgná*** ***pílti*** ***alývos*** buttare / gettare òlio sul fuoco; **2.** (*þiburys*) luce *m*; **3.** (*viryklëlës*) fornello; **4.** kar fuoco; ***paléisti*** ***ùgná*** fare fuoco; ***prîeðo*** ***~yjê*** sotto il fuoco del nemico; ***kryþmínë*** ***u.*** fuoco incrociato.

**ugniùkas** *s* zool pirâlide *m*; ***miltínis*** ***u.*** tignola della farina; ***vaðkínis*** ***u.*** gallêria.

**ugrofínai** *s pl* istor gli Ugro-fìnnici.

**¿gtelë|ti, ~ja, ~jo** *vi* alzarsi un po' (*crescere*).

**ùi** *intz* u!, uu!; ohi! [oi], ahi! [ai].

**ùiti** *v* (ùja, ùjo) **1.** (*barti*) rampognare; ammonire; **2.** (*varyti*) scacciare.

**¿kana** *s* (banco di) nêbbia, foschia.

**¿kanas,** **ûkanâ** *agg* , **ûkanót|as,** **-a** *agg* nebbioso; ***~a*** ***dienâ*** una giornata nebbiosa; ***û.*** ***dangùs*** un cielo nuvoloso / lìvido.

**¾kas** *s* **1.** nêbbia, nebbiolina; **2.** astr nebulosa.

**¿k|auti** *v* , ~auja, ~avo) **1.** strillare, lanciare grida; **2.** Þ **¿bauti**.

**¿kavimas** *s* chiù *v.*

**¿kèio|ti, ~ja, ~jo** *vx* Þ **¿kauti**.

**ûkímas** *s* suono cupo; (*pvz., vëjo*) ululato.

**¿kininkas,** **-ë** *s* (*fermeris*) fattore -essa; massaro; (*þemës ûkio verslininkas*) agricoltore *v*; coltivatore (diretto); (*þemës savininkas*) proprietârio terriero; ***stambùs*** ***¿.*** un grosso proprietârio terriero; un latifondista.

**ûkinink|ãuti, ~ãuja, ~åvo** *vx* (*bûti fermeriu*) fare il fattore / il massaro; (*verstis þemës ûkiu*) fare l'imprenditore agrìcolo; fare il coltivatore diretto; avere un'a@ienda agrìcola.

**ûkininkåvimas** *s* attivitâ agrìcola.

**¿kin|is,** **-ë** *agg* **1.** (*ekonominis*) econòmico; ***~ë*** ***geróvë*** il benêssere econòmico; ***~ë*** ***veiklâ*** attivitâ econòmica; **2.:** ***¿.*** ***påstatas*** casolare *v*, cascinale *v*; **3.:** ***~ës*** ***prìkës*** prodotti *v* / artìcoli *v* per la casa.

**¿kis** *s* **1.** economia; ***kråðto*** ***¿.*** l'economia di un paese; ***miðkÿ*** ***¿.*** silvicoltura; ***planínis*** ***¿.*** economia pianificata; ***þìmës*** ***¿.*** agricoltura; **2.** þ.û. a@ienda agrìcola / rurale; fattoria, masseria; (*dvaras*) tenuta, fondo agrìcolo, podere *v*; ***kolektývinis*** ***¿.*** kolchoz *v nkt*; **3.:** ***namÿ*** ***¿.*** gestione della casa; economia domêstica.

**ûkískaita** *s* kom autonomia gestionale.

**¿kiðkai** *avv* **1.:** ***¿. gyvénti*** fare vita di campagna; **2.** (*taupiai*) con oculatezza, in modo oculato; (*ekonomiðkai*) economicamente; **3.:** ðnek ***¿. kaµbant*** (per dirla) in soldoni.

**¿kiðkas,** **-a** *agg* **1.:** ***¿. mu¤las*** sapone *v* comune; **2.** (*taupus*) econòmico; oculato; (*tik apie þmogø*) ecònomo; **3.** ðnek (*praktiðkas*) concreto, pragmâtico; ***¿. m±stymas*** ragionamento pragmâtico.

**ûkiðkùmas** *s* senso dell'economia; oculatezza; parsimònia.

**ukraínas,** **-ë** *s* , **ukrainiìtis,** **-ë** *s* ucraino -a.

**ukrainiìtiðkas,** **-a** *agg* ucraino.

**ûksm|º** *s* ombra; ***~ëjê*** all'ombra.

**¾ksminas** *s* bot adoxa.

**ûksm¸tas,** **-a**, **ûksmíng|as,** **-a** *agg* ombroso; ombreggiato; ***~a*** ***al¸ja*** un viale ombreggiato.

**ûksmingùmas** *s* ombrositâ.

**¾kti** *v* (¾kia, ¾kë) (*apie garlaivá*) fischiare; (*apie pelëdà*) chiurlare; (*apie vëjà*) ululare.

**¾ktis, ¾kiasi, ¾kësi** *vr* rannuvolarsi; incupirsi.

**ûkvedôba** *s* gestione *m* (econòmica).

**ûkvedýs,** **-º** *s* amministratore -trice; ecònomo -a.

**ulb¸jimas** *s* cinguettio (-tii).

**ulb¸|ti** *v* (ùlba, ~jo) gorgheggiare; *ir* prk cinguettare.

**uldùkas** *s* : zool ***karvìlis*** ***u.*** colombella.

**ulõnas** *s* istor ulano.

**ultimatyvia¤** *avv* categoricamente, in modo categòrico; ultimativamente.

**ultimatyvùmas** *s* categoricitâ.

**ultimatyvùs,** **-í** *agg* categòrico, ultimativo.

**ultimåtumas** *s* ultimatum *v nkt*.

**ultrag|a»sas** *s* fiz ultrasuono; med ***gôdymas*** ***~arsù*** ultrasuonoterapia.

**ultragarsín|is,** **-ë** *agg* ultrasònico; ***~ë diagnòsti­ka*** diagnòstica ad ultrasuoni.

**ultramarínas** *s* chem oltremare *v nkt*.

**ultraviolêtinis,** **-ë** *agg* fiz ultravioletto.

**ûma¤** *avv* all'improvviso, improvvisamente.

**ûmëdº** *s* bot rùssula, colombina; ***þalsvóji*** ***û.*** colombina verde.

**ûm¸|ti, ~ja, ~jo** *vx* (*apie ligà*) acuti@@arsi.

**¿miai** *avv* (*staiga*) repentinamente.

**ûmínis,** **-ë** *agg* (*apie ligà*) acuto.

**¿min|ti, ~a, ~o** *vx* (*ligà*) acuti@@are.

**ûmùs,** **-í** *agg* **1.** (*apie ligà*) acuto, grave; ***û.*** ***apsinuõdijimas*** una grave intossicazione; **2.** (*apie þmogø*) impulsivo, focoso; violento; (*greit supykstantis*) irritâbile, stizzoso; **3.** (*staigus*) repentino.

**ùncija** *s* òncia (-ce).

**undínë** *s* mit sirena.

**ungurýs** *s* zool anguilla; capitone *v*; ***j¿rinis*** ***u.*** grongo.

**unifik|åcija,** **~åvimas** *s* *s* unificazione *m*.

**unifik|úoti, ~úoja, ~åvo** *vx* unificare.

**unifòrma** *s* uniforme *m*, divisa.

**unifòrminis,** **-ë** *agg* dell'uniforme, della divisa.

**uniformúotas,** **-a** *agg* in uniforme, in divisa.

**ùnija** *s* unione *m*.

**unikalia¤** *avv* in modo ùnico.

**unikalùmas** *s* unicitâ.

**unikalùs,** **-í** *agg* ùnico.

**ùnikumas** *s* ùnicum *v nkt lot*; caso ùnico.

**unis|ònas** *s* muz unìsono; ***dainúoti*** ***~onù*** cantare all'unìsono.

**unítas, -ë** *s* baþn uniate *v/m*.

**unitåzas** *s* water [va-] *v nkt*, tazza (*del water*).

**universalia¤** *avv* universalmente.

**universålin|is,** **-ë** *agg* universale; ***~ë*** ***parduotùvë*** grande maga@@ino.

**universalùmas** *s* universalitâ.

**universal|ùs,** **-í** *agg* universale; ***~í*** ***prîemonë*** rimédio universale; ***universålios*** ***þínios*** sapere enciclopêdico.

**universiadâ** *s* sport universìade *m*.

**universitêt|as** *s* universitâ; ***Vôtauto*** ***Dídþiojo*** ***u.*** Universitâ di Vôtautas (Vitoldo) il Grande; ***ástóti*** ***ñ*** ***~à*** iscrìversi all'universitâ; ***~o*** ***miestìlis*** campus *nkt* universitârio.

**universitêtinis,** **-ë** *agg* universitârio -a; ***u.*** ***iðsilåvinimas*** istruzione universitâria.

**u¹kð|ti** *v* , ~èia, ~të) uggiolare.

**unkðtímas** *s* uggiolio (-lii).

**úodas** *s* zool @an@ara; ***maliårinis ú.*** (@an@ara) anòfele *m*; ***ilgakõjis ú.*** tìpula; @an@arone *v; **ú. kãnda*** la @an@ara punge.

**uodeg|â** *s* coda; ***mosúoti úodega*** scodinzolare; prk ***komêtos u.*** coda / scia di una cometa; ¨ ***nue¤ti ðùniui a¹t ~õs*** andare a ròtoli, finire male; ***prispãusti úodegà*** far vedere i sorci verdi; méttere alle corde; ***úodegà ákíðti*** finire nei guai; ***viµktis ~ojê*** arrancare alle spalle (*rimanere indietro*).

**uodegºlë** *s* **1.** *dimin* codina, codino; **2.** lingv codetta (*sottoscritta*, *come nelle vocali à*, *æ*, *á*, *ø*).

**uodegínis, -ë** *agg* caudale; della coda.

**uodegótas, -a** *agg* caudato; con la coda.

**uodegúotas, -a** *agg* Þ **uodegótas**.

**úog|a** *s* bacca; ***míðko ~os*** frutti *v* di bosco.

**uog|ãuti, ~ãuja, ~åvo** *vx* raccògliere bacche.

**uogãutojas,** **-a** *s* raccoglitore -trice di bacche.

**uogiìnë** *s* marmellata (*solo di bacche*), conserva (di frutta).

**uogienójas** *s* arbusto (di bacche).

**úoksas** *s* **1.** cavitâ; cavo (*di un albero*); **2.** (*ðautuvo*) testa della culatta; (*pabûklo*) culatta.

**uolâ** *s* **1.** ròccia (-ce); masso; (*pajûryje*) scogliera; (*povandeninë ar pan*.) scòglio (-gli); **2.** tarm (*galàstuvas*) cote *m*.

**uõlaskëlë** *s* bot sassìfraga.

**úolektis** (-ies) *sf* , **uolektís** *sf* istor (*ilgio matas*) brâccio (*dgs* brâccia *mot*).

**uol¸tas,** **-a** *agg* roccioso.

**uol¸|ti, ~ja, ~jo** *vi* farsi ròccia; pietrificarsi.

**uõliai** *avv* con impegno, con @elo; diligentemente.

**uolîena** *s* ròccia; geol ***mågminë / erùpcinë u.*** ròc­cia erutti­va.

**uolíngas,** **-a** *agg* roccioso.

**uolùmas** *s* impegno, @elo; diligenza, alacritâ.

**uolùs,** **-í** *agg* diligente, impegnato; solerte, @elante.

**uosônas** *s* frassineto.

**uosínis, -ë** *agg* di frâssino.

**úosis** *s* bot frâssino.

**uosl|º** *s* odorato; (*ypaè gyvûnø*, *prk*) fiuto; olfatto *fiziol*; prk ***netur¸ti ~ºs*** (*kam*) non avere fiuto / naso (*per qcs*).

**uoslùs, -í** *agg* dall'olfatto fine.

**uoståmiestis** *s* porto, cittâ portuale.

**úost|as** *s* porto; ***j¿rø (prekôbos, ùpës) ú.*** porto marìttimo (commerciale, fluviale); ***laisvâsis ú.*** porto franco; ***óro ú.*** aeroporto; ***ápla÷kti ñ ~à*** entrare in porto; ***~o*** ***krovíkas*** scaricatore *v* di porto.

**úosti** *v* (úodþia, úodë) sentire (*un odore*, *una puzza e sim*.); (*apie ðuná*) fiutare.

**úostymas** *s* annusata; fiutata.

**uostin¸|ti, ~ja, ~jo** *vx* fiutare, annusare; ***ðuõ p¸das ~ja*** il cane fiuta le orme.

**uostin¸toja**s, **-a** *s* fiutatore -trice; (*narkotikø*) sniffatore -trice.

**úostininkas, -ë** *s* portuale *v/m*.

**úost|yti, ~o, ~ë** *vx* odorare; annusare; (*ypaè tabakà*) fiutare; (*narkotikus*) sniffare, tirare *ðnek*; ***ú. kokaínà*** sniffare cocaina; ***ú. rõþæ*** odorare una rosa.

**úostytojas,** **-a** *s* Þ **uostin¸tojas**.

**úostomasis, -oji** *prcp fd* : ***ú. tabåkas*** tabacco da fiuto.

**uoðvijâ** *s* casa dei suòceri.

**úoðvis, -ë** *s* suòcero -a.

**'** **úotas** *s* Þ **õtas**.

**¾p|as** *s* **1.** (*nuotaika*) umore *v*; ***netur¸ti ~o k± darôti*** non êssere in vena di far qcs; ¨ *intz **gìro ~o!*** allegria!; **2.:** tarm ***~ais*** a tratti; ***vîenu ûpù*** in un solo momento.

**ùp|ë** *s ir* prk fiume *v*; ***~ës*** ***vagâ*** il letto di un fiume; ***aukðtýn (þemýn)*** ***upê*** a monte (a valle) (*del corso di un fiume*); verso la sorgente (verso la foce); ***~iø laivôba*** navigazione *m* fluviale; prk ***åðarø (kra÷jo) ù.*** un fiume di lâcrime (di sângue).

**upe¤vis,** **-ë** *s* trasportatore -trice fluviale.

**up¸lapis** *s* carta fluviale.

**upìlis** *s dimin* ruscello, torrente *v.*

**upelôtis** *s dimin* , **upeliùkas** *s dimin* rivo, ruscelletto.

**upeli¿kðtis** *s* rigâgnolo, rìvolo.

**up¸takis** *s* zool trota; ***margâsis u.*** trota di torrente; ***vaivorykðtínis u.*** trota arcobaleno / iridea / iridata; trota salmonata.

**up¸tyra** *s* idrografia fluviale, potamologia.

**up¸vardis** *s* idrònimo (*solo di fiume*).

**upônas** *s* bacino fluviale.

**upininkýstë** *s* navigazione *m* fluviale.

**ùpin|is,** **-ë** *agg* fluviale; di fiume; ***ù. la¤vas*** battello fluviale; ***~ë þuvís*** pesce *v* di fiume.

**upókðnis** *s dimin* torrente *v.*

**uragånas** *s* uragano.

**urånas** *s* **1.** chem urânio (-ni); **2.:** *astr*, mit ***U.*** Urano.

**urbanístika** *s* urbanìstica.

**urbanístinis, -ë** *agg* urbanìstico.

**urbanizåcija** *s* urbani@@azione.

**urbanízmas** *s* urbanésimo.

**urdulýs** *s* mulinello, gorgo; risùcchio.

**urdul|iúoti, ~iúoja, ~iåvo** *vx* scórrere vorticosamente; (*grioviais*) ruscellare.

**urºdas** *s* istor **1.** (*dvaro*) massaro; **2.** (*miðko*) capo guardaboschi *v nkt*.

**urgzlýs,** **-º** *s* brontolone -a.

**u»gzti** *v* (u»zgia, u»zgë) **1.** ringhiare; **2.** prk (*apie pilvà*) gorgogliare, borbottare; (*apie maðinà*) rombare.

**ùrm|as** *s* **1.** (*bûrys*) massa; ***ásive»þti*** *~* ***u*** irrómpere in massa; **2.:** ***~o prekôba*** vêndita all'ingrosso; ***~u*** all'ingrosso.

**ùrmininkas,** **-ë** *s* grossista *v/m*.

**ùrminis,** **-ë** *agg* all'ingrosso.

**ùrna** *s* urna.

**urològas, -ë** *s* uròlogo (-gi/-ghi) -a.

**urològija** *s* med urologia.

**urugvajiìtis,** **-ë** *s* uruguaiano -a, uruguâgio -a.

**ùrvas** *s* , **u»vas** *s* caverna, grotta; spelonca; (*lindynë*) tana, covo; covile *v.*

**urvínis, -ë** *agg* delle caverne; ***u. þmogùs*** uomo delle caverne, cavernìcolo.

**urzgesýs** *s* , **urzg¸jimas** *s* rìnghio; (*pilve*) gorgoglio (-glii); borborigmo *med*.

**urzg¸|ti** *v* (ùrzga, ~jo) **1.** ringhiare; **2.** (*apie þmogø*) brontolare, borbottare; **3.** (*apie pilvà*) brontolare, gorgogliare.

**urzgímas** *s* rìnghio.

**¾s|as** (*solit*. *dgs* **¾sai**) *s* **1.** baffo; ***sù ~ais*** con i baffi, baffuto; ¨ ***ñ ~à p¾sti*** dormire della grossa; **2.** (*gyvulio*) vibrissa; (*vabalo*) antenna; (*þuvies*) barbìglio (-gli); ðnek (*troleibuso*) brâccio (*per la presa di corrente di un filobus*); ***bangínio ¾.*** stecca di balena; **3.** bot (*braðkiø*) stolone *v.*

**ûsìlis** *s* bot vitìccio, cirro.

**usnís** *s* bot scardaccione *v*, stoppione *v.*

**ûsõrius** *s* **1.** baffone *v*; **2.** zool barbo.

**ûsúotas,** **-a** *agg* con i baffi, baffuto.

**us¾rinis, -ë** *agg* : zool ***u. ðuõ*** nitterêute *v.*

**utëlº** *s* zool pidòcchio; ***galvínë u.*** pidòcchio (del capo); ***kírkðninë u.*** piâttola, pidòcchio del pube; ***naikínti ùtëles*** spidocchiare.

**utël¸tas,** **-a** *agg* pidocchioso.

**uteníðkis, -ë** *s* abitante *v/m* di Utenâ.

**utilitårinis,** **-ë** *agg* utilitarìstico.

**utilitarístas, -ë** *s* utilitarista *v/m*.

**utilitarízmas** *s* utilitarismo.

**utiliz|åcija, ~åvimas** *s* riciclo, riciclâggio.

**utiliz|úoti, ~úoja, ~åvo** *vx* (*atliekas*) riciclare.

**utòpija** *s* utopia.

**utòpinis,** **-ë** *agg* utopìstico.

**utopístas, -ë** *s* utopista *v/m*.

**utòpiðkas,** **-a** *agg* utopìstico.

**uvertiûrâ** (-i¾ros) *s* muz ouverture *m nkt*.

**uzbêkas,** **-ë** *s* usbeco -a, uzbeco -a.

**uzurpåcija** *s* usurpazione *m*.

**uzurpåtorius,** **-ë** *s* usurpatore -trice.

**uzurpåvimas** *s* usurpazione *m*.

**uzurp|úoti, ~úoja, ~åvo** *vx* usurpare.

**ùþ** *prep* **1.** (*gen*) (*þymint vietà*) dietro (*(a) qc*); (*uþ ribø*) fuori (*(da) qcs*); (*anapus*) al di lâ (*di qcs*), dall'altra parte (*di qcs*); ***ùþ man³s*** dietro di me; ***ùþ dùrø*** fuori dalla porta, dietro la porta; ***ùþ miìsto*** fuori cittâ; prk ***ùþ áståtymo ribÿ*** fuori legge; ***ùþ ìþero*** al di lâ del lago; **2.** (*gen*) (*þymint nuotolá*) a; (*laiko atþvilgiu*) tra, fra; dopo; ***gyvìna ùþ ðeðiÿ kilomêtrø*** vive a sei chilòmetri da qui; ***ate¤k ùþ trijÿ dienÿ*** vieni fra tre giorni; ***pråðë ate¤ti ùþ trijÿ dienÿ*** chiese di tornare dopo tre giorni / tre giorni dopo; **3.** (*gen*) (*reiðkiant kà*, *uþ ko imama*, *laikoma ir pan*.) per; ***i§ti k± ùþ ra¹kos*** prêndere qcn per mano, ***te§pti k± ùþ plaukÿ*** tirare qcn per i capelli; ***laikôti ãrklá ùþ påvadþio*** tenere un cavallo per le brìglie; ***laikôtis ùþ turºklø*** tenersi alla ringhiera; **4.** (*gen*): ***iðtek¸ti ùþ gìro vôro*** sposarsi con / sposare un buon uomo; **5.** (*acc*) (*lyginant*) di; ***âð vyrêsnis ùþ tavê*** sono più grande di te; ***jís daugia÷ gåvo ùþ manê*** lui ha ricevuto più di me; **6.** (*acc*) (*kieno labui*) per; ***pad¸ti gãlvà ùþ tëvýnæ*** dare la vita per la pâtria; ***balsúokite ùþ manê*** votate per me; ***argume¹tai ùþ i» priìð*** i pro e i contro; ***kâs ùþ?*** chi ê favorévole / a favore?; ***da÷gelis bùvo ùþ nutarímà*** la maggioranza era a / in favore della delìbera; **7.** (*acc*) (*þymint vertæ*, *kainà*, *atpildà*) per; ***baudâ ùþ gre¤èio vi»ðijimà*** multa per eccesso di velocitâ; ***bãrti ùþ k±*** rimproverare per qcs; ***mok¸ti ùþ bùtà*** pagare (per) l'appartamento; ***nusipi»kti ùþ dìðimt*** ***lítø*** comprare per / a dieci litas; ***padëkóti ùþ vískà*** ringraziare di tutto; ¨ ðnek ***nº ùþ k±!*** neanche per sogno!; **8.** (*kà*) (*vietoj ko*) per, al posto (*di qc*); (*kaip*) come; ***pasiraðôti ùþ k±*** firmare per qcn; ***dírbti ùþ dù*** lavorare per due; ***âð tavê ùþ dra÷gà laikia÷*** ti consideravo un amico; **9.** (*þymint tikslà*): ***pakélti taurês ùþ sºkmæ*** brindare al successo.

**uþakímas** *s* ostruzione *m* (*l'otturarsi*).

**uþakínis, -ë** *agg* teis: ***u. te¤smo núosprendis*** giu­dìzio in contumâcia.

**uþâkti** *v* (uþa¹ka, uþåko) **1.** otturarsi; **2.** (*pvz*., *apie tvenkiná*) coprirsi di vegetazione.

**ùþan|tis** *s* petto (*di camicia e sim*.); seno; ***~tyje*** nel / in seno; ***íð ~èio*** dal / di seno.

**uþantspaud|úoti, ~úoja, ~åvo** *vx* sigillare.

**uþãrti** *v* (ùþaria, ùþarë) arare (*completamente*).

**uþasfalt|úoti, ~úoja, ~åvo** *vx* asfaltare (*completamente*).

**uþaugín|ti, ~a, ~o** *vx* (*vaikus*) créscere, allevare; (*gyvulius*) allevare; (*augalus*) coltivare; ***vienâ ~o trís dùkteris*** da sola ha cresciuto tre fìglie.

**uþãug|ti, ~a, ~o** *vx* **1.** créscere; ***gimia÷ i» ~au Kaunê*** sono nato e cresciuto a Ka÷nas; **2.** (*uþþelti*) (*kuo*) ricoprirsi (*di qcs*), riempirsi; ***tåkas ~o þolê*** il sentiero si ê riempito di erba.

**¿þ|auti** *v* , ~auja, ~avo) **1.** (*apie girià*) mormorare, frusciare; (*apie vëjà*) mugghiare; **2.** (*triukðmauti*) fare chiasso / cagnara; (*linksmintis*) fare baldòria, gozzovigliare.

**¿þautojas,** **-a** *s* festaiolo -a.

**uþbadôti** *v* (uþbådo, uþbådë) (*peiliu*) accoltellare; (*ragais*) incornare; (*durklu*) pugnalare.

**uþbaigímas** *s* conclusione *m*, compimento; completamento.

**ùþbaigtas, uþbaigtâ** *prcp* concluso, compiuto; portato a têrmine, terminato; ***vieni¹telis ðiõ poêto ù. kûrinýs*** l'ùnica opera compiuta di questo poeta.

**uþba¤g|ti** *v* , ~ia, ùþbaigë) **1.** (*kà*) conclùdere (*qcs*), portare a têrmine, terminare; porre fine (*a qcs*); cómpiere (*qcs*); completare; ***u. derôbas (kaµbà, susirinkímà)*** conclùdere le trattative (un discorso, una riunione); ***u. krízæ*** porre fine a una crisi; ***u. trìèià kùrsà*** terminare il terzo anno di studi; **2.** (*nukamuoti*) sfinire, sfiancare; finire.

**uþbaigtùmas** *s* completezza.

**uþbãltin|ti, ~a, ~o** *vx* (*pvz*., *sriubà*) macchiare di latte (/ di panna) (*una zuppa e sim*.).

**uþbarikad|úoti, ~úoja, ~åvo** *vx* barricare; ***u. durís*** sbarrare una porta.

**uþbarstôti,** **ba»sto, ba»stë** *vt* spârgere.

**uþb¸g|ti, ~a, ~o** *vx* **1.** (*á virðø*) córrere su; salire di corsa; ***~ome ñ 4-àjá a÷kðtà*** salimmo di corsa fino al quarto piano; **2.** (*á prieká*) córrere avanti; ¨ ***u. (krízei, lígai, skandålui) ùþ akiÿ*** prevenire (una crisi, una malattia, uno scândalo); (*pasakojant*) anticipare; **3.** (*uþ ko*) córrere dietro (*qcs*); **4.** (*uþeiti*) passare, fare un salto / una scappata; ***u. ñ krãutuvæ*** fare un salto allo spâccio.

**uþbe»ti** *v* (ùþberia, ùþbërë) spârgere; (*uþpilti*) rovesciare (*sopra*).

**uþbl¸s|ti, ~ta, ~o** *vi* spégnersi (*ir* prk).

**uþbliãu|ti** *v* , ~na, uþblióvë) **1.** méttersi a belare; **2.** menk méttersi a frignare.

**uþblok|úoti, ~úoja, ~åvo** *vx* bloccare; ***u. kredíto kortìlæ*** bloccare una carta di crêdito.

**uþbra÷k|ti** *v* , ~ia, ùþbraukë) **1.** barrare, sbarrare; (*iðbraukti*) cancellare; **2.:** ***u. degtùkà*** accêndere / sfregare un fiammìfero.

**uþbr¸þ|ti, ~ia, ~ë** *vx* **1.** tracciare; ***u. ríbà*** tracciare un confine; **2.:** ***u. degtùkà*** accêndere / sfregare un fiammìfero.

**uþbrínk|ti** *v* , ~sta, ~o) gonfiarsi, ingrossarsi.

**uþbuè|iúoti, ~iúoja, ~iåvo** *vx* coprire di baci, mangiare di baci.

**uþbu»k|ti** *v* , ~sta, ~o) ingrossarsi, gonfiarsi.

**uþbùrtas,** **uþburt|â** *agg*, *prcp* incantato, stregato; ***~â pilís*** un castello incantato; ¨ ***u. råtas*** cìrcolo vizioso.

**uþbùrti** *v* (ùþburia, uþb¿rë) (*kà*) stregare (*qc*); fare un incantésimo (*a/su qc*); *ir* prk incantare (*qc*), affascinare, ammaliare; prk ***visùs uþb¿rë*** ***jõs dainåvimas*** il suo canto ha affascinato / ha incantato tutti.

**uþb¿ti** *v* (uþb¾va/uþb¾na, uþbùvo) trattenersi.

**uþèiãup|ti, ~ia, ~ë** *vx* chiùdere (*la bocca*, *le labbra*); ***u. kãm bùrnà*** chiùdere la bocca a qcn; (*nutildyti*) far tacere.

**uþèiúop|ti, ~ia, ~ë** *vx* tastare, palpare; (*èiupinëjant rasti*) trovare a tastoni.

**uþèiùp|ti** *v* (uþèiu§pa, ~o) (*uþklupti*) cògliere, sorprêndere; ***mùs lietùs ~o*** ci ha colto un acquazzone.

**uþdãilin|ti, ~a, ~o** *vx* **1.** smussare, arrotondare; **2.** prk appianare, smussare; ***u. nesutarímà*** appianare una controvêrsia.

**uþdain|úoti, ~úoja, ~åvo** *vx* méttersi a / cominciare a cantare, attaccare (a cantare); intonare; ***tyliù balsù u.*** attaccare sottovoce.

**ùþdang|a** *s* **1.** (*scenos*) sipârio, tela; velârio; (*uþuolaida*) tenda; ***pakélti ~à*** alzare il sipârio; ***ù. nusiléido*** ê calato il sipârio; **2.** (*priedanga*) copertura, schermo; *ir* prk cortina, velo; kar ***d¿mø ù.*** cortina fumògena / nebbiògena; istor prk ***geleþínë ù.*** la cortina di ferro.

**ùþdangalas** *s* **1.** (*dangtis*) copêrchio; **2.** (*pvz*., *langø*) tenda; tendâggio.

**uþdangstôti** *v* (uþda¹gsto, uþda¹gstë) coprire (*solit*. *più cose*), ricoprire.

**ùþdaras** *s* **1.** (*uþdarymo átaisas*) chiusura; **2.** kul *tipo di* lardo; (*lydyti taukai*) strutto.

**ùþdaras,** **uþdar|â** *agg* **1.** chiuso; ***ùþdaros dùrys*** una porta chiusa; ***ù. kiìmas*** un cortile che non dâ sull'esterno, lingv ***~âsis skiemuõ*** sìllaba chiusa; med ***~ÿ*** ***patalpÿ*** ***bãimë*** claustrofobia; sport ***~osê patalposê*** indoor, al coperto; ¨ ***pósëdis ~omís dùrimis*** seduta a porte chiuse; **2.** prk (*ne vieðas*) privato; ***ù. spektåklis*** uno spettâcolo privato; **3.** prk (*izoliuotas*) appartato; (*uþsidaræs*) riservato, schivo; chiuso.

**uþdarb|iãuti**, ~iãuja, ~iåvo *vx* lavorare (*solit*. *in modo temporaneo*); guadagnarsi il pane; ***daþna¤ stude¹tai výksta u. ñ ùþsiená*** spesso gli studenti vanno a guadagnarsi qualche soldo all'êstero.

**ùþdarb|is** *s* guadagno; (*darbininkø*) paga, salârio; ***papíldomas ù.*** un (guadagno) extra *nkt*; ***dienõs ù.*** la paga giornaliera; ***susigùndyti lengvù ~iu*** farsi allettare da un fâcile guadagno.

**uþdårymas** *s* chiusura; ***atòminës elektrínës (parodõs, teåtro sezòno) u.*** la chiusura di una centrale atòmica (di una mostra, della stagione teatrale).

**uþdarôt|as,** **-a** *agg*, *prcp* chiuso; ***aklina¤ u.*** chiuso ermeticamente; ***parduotùvë ~a*** il negòzio ê chiuso; ***~a*** (*uþraðas*) ***(dºl inventorizåcijos, remòntui)*** chiuso (per inventârio, per lavori).

**uþdarôti** *v* (uþdåro, uþdårë) chiùdere; ***u. durís (mokýklà, pårodà, sçskaità bãnke, sîenà, skliaustùs)*** chiùdere la porta (una scuola, una mostra, un conto in banca, una frontiera, le paréntesi); ***u. kìlià*** sbarrare / chiùdere una strada; ***u. ñ na»và (ñ se¤fà)*** chiùdere in gâbbia (in cassaforte); ***u. ñ kal¸jimà*** (rin)chiùdere in prigione.

**uþdarum|â** *s ir* lingv chiusura; lingv occlusione *m*; lingv ***~õs prîebalsiai*** le occlusive, le esplosive.

**uþdarùmas** *s* prk chiusura; introversione *m*.

**uþdavinônas** *s* eserciziârio.

**uþdavinýs** *s* **1.** (*pavestas darbas*) cómpito; incârico, incombenza; (*tikslas*) obbiettivo; ***atlíkti visùs ùþdavinius*** svòlgere tutti i cómpiti; ***pagrindínis u.*** l'obbiettivo principale; **2.** (*klausimas*, *uþduotis*) problema (-mi) *v*; (*mokykloje ir pan*.) esercìzio (-zi); ***sunkùs geomêtrijos u.*** un diffìcile esercìzio / problema di geometria; ***spr²sti ùþdaviná*** lavorare a un problema, (cercare di) risòlvere un problema; ***iðspr²sti ùþdaviná*** risòlvere un problema.

**uþdegímas** *s* **1.** infiammazione *m*; med flogosi *m nkt*; ***gerklºs u.*** infiammazione alla gola; ***ínkstø u.*** nefrite *m*; ***pla÷èiø u.*** polmonite *m*; **2.** chem ignizione *m*; tech accensione *m*.

**uþdegíminis,** **-ë** *agg* med infiammatòrio.

**uþdêgti** *v* (ùþdega, ùþdegë) **1.** accêndere; (*padegti*) incendiare; ***u. degtùkà (lãuþà, lémpà, ðviìsà, þvåkæ)*** accêndere un fiammìfero (un falò, una lâmpada, la luce, una candela); ***u. sprógmená*** fare scoppiare una bomba; **2.** prk accêndere; infiammare, infervorare; ***u. a¤strà*** accêndere la passione; ***u.*** ***þmónes*** infiammare / entusiasmare la gente.

**uþdëjímas** *s* : ***mókesèiø u.*** tassazione *m*.

**ùþdelst|as, uþdelstâ** *prcp* ritardato; kar ***~o veikímo bòmba*** bomba a scòppio ritardato.

**uþdeµs|ti** *v* , ~ia, ùþdelsë) (*kà daryti*) tardare (*a far qcs*), ritardare; (*non far qcs*) in tempo; ***u. mok¸jimà*** ritardare un pagamento; ***u. gràþínti skõlà*** non saldare un débito in tempo, tardare a saldare un débito.

**uþdelstínis, -ë** *agg* : sport ***u. ðúolis*** caduta lìbera.

**uþde¹g|ti** *v* , ~ia, ùþdengë) **1.** coprire; (*nuo ko*) protêggere (*da qc*); ***u. stålà stãltiese, u. stãltiesæ a¹t stålo*** méttere la tovâglia, coprire il tâvolo con una tovâglia; ***u. púodà*** méttere il copêrchio a una péntola, coprire una péntola; sport ***u. þaid¸jà*** chiùdere un giocatore; **2.** (*uþdëti stogà*) (*kà*) méttere il tetto (*a qcs*), costruire il tetto (*di qcs*).

**uþder¸|ti** *v* (ùþdera, ~jo) dare un buon raccolto; venire su bene, créscere bene; ***ðia¤s mìtais gera¤ ~jo java¤*** quest'anno il raccolto di grano ê stato buono, quest'anno il grano ê venuto su bene.

**uþd¸|ti** *v* (ùþdeda, ~jo) **1.** (*kà ant ko*) méttere (*qc su qcs*), porre; poggiare, posare; ***u. púodà a¹t ugniìs*** méttere una péntola sul fuoco; ***u.*** ***a¹trankius*** (a¹tsnuká, ki»tá, lõpà, perùkà, pléistrà)*** méttere le manette (la museruola, un accento, una toppa, una parrucca, un cerotto); ***u. a¹tspaudà*** apporre un timbro; ***u. kãm åreðtà*** porre qcs sotto sequestro; ***u. plokðtìlæ*** méttere su un disco; **2.** (*pritvirtinti*) fissare, assicurare; **'** **3.** Þ **paskírti**.

**uþdírb|ti, ~a, ~o** *vx* guadagnare; ***gera¤ u.*** guadagnare bene; ***neteis¸tai u.*** guadagnare in nero.

**uþdraudímas** *s* proibizione *m*, divieto.

**uþdra÷|sti** *v* , ~dþia, ùþdraudë) (*kam kà daryti*) proibire (*a qc di far qcs*), vietare; interdire; (*knygà*, *laikraðtá*) sopprìmere; ***u. kãm iðvýkti íð ðaliìs*** proibire a qcn di lasciare il paese; ***jãm bùvo ùþdrausta vairúoti bê akiniÿ*** gli ê stato proibito di guidare senza occhiali.

**uþdróþ|ti, ~ia, ~ë** *vx* ðnek mazzolare, lisciare il pelo; (*pagaliu*, *lazda*) legnare.

**uþdúo|ti** *v* , ~da, ùþdavë) **1.** assegnare; ***u. kãm dãrbà (ùþdaviná)*** assegnare a qcn un lavoro (un esercìzio); ***u. klãusimà*** porre / fare una domanda; ***kâs ~ta?*** che cosa c'ê di cómpito?; **2.** (*uþkirsti*) (*kam kuo*) colpire (*qc con qcs*); ***u. kãm dirþù*** dare una cinghiata a qcn.

**uþduot|ís** *sf* **1.** cómpito, incârico; missione *m*; ***gamôbinë u.*** quota di lavoro; ***atlíkti (gãuti, pavêsti) ùþduotá*** adémpiere (a) (ricévere, assegnare) un incârico; ***iðvýkti sù ~imí*** partire in missione; **2.** (*mokykloje*) cómpito, esercìzio (-zi); (*atlikta*) elaborato; ***namÿ (darbÿ) ùþduotys*** compiti a / per casa.

**uþdùsæs, -usi** *prcp* **1.** soffocato; asfissiato; **2.** (*be kvapo*) senza fiato, in affanno; ansante, affannato.

**uþdusímas** *s* soffocamento; asfissia.

**uþdùsinimas** *s* soffocamento; strangolamento.

**uþdùsin|ti, ~a, ~o** *vx* soffocare, strangolare; asfissiare.

**uþdùs|ti** *v* (uþd¾sta, ~o) **1.** êssere soffocato, soffocare; ***nuõ d¿mø u.*** êssere soffocato dal fumo; **2.** (*neatgauti kvapo*) avere il fiato grosso / il fiatone, farsi venire il fiato in gola; ***b¸gdamas uþdusa÷*** mi ê venuto il fiatone per la corsa.

**ùþeig|a** *s* trattoria, osteria; ***~os*** ***nama¤*** locanda *vns*; ***~os ðeimini¹kas (-ë)*** oste (-essa).

**uþe¤|ti** *v* , ~na, uþºjo) **1.** passare (*a piedi*); fare un salto; ***u. ñ knygônà*** fare un salto in libreria; ***u. pâs k± (ñ sveèiùs)*** andare / passare a trovare qcn; ***uþe¤kite (ñ vídø)!*** si accòmodi!, entri!; ***uþe¤kite vºl põ porõs dienÿ*** ripassi tra un pâio di giorni; ***uþëja÷*** ***skalbiniÿ pai§ti*** sono passato a ritirare i panni; **2.** (*uþ ko*) passare (dietro), spostarsi (dietro); ***sãulë uþºjo ùþ debesÿ*** il sole si ê nascosto dietro le nùvole; ***u. ùþ ka§po*** girare l'ângolo; kar ***u. íð spa»no*** aggirare sull'ala; **3.** (*ant ko*) salire; **4.** prk (*kilti*, *prasidëti*) venire; ***mãn uþºjo ilgesýs*** mi ê venuta nostalgia; ***kâs tãu uþºjo?*** che ti ê preso?; **5.** (*atslinkti*) arrivare (*del tempo*); sopraggiùngere; ***uþºjo naktís*** sopraggiunse la notte; ***uþºjo þiemâ*** ê arrivato l'inverno; ***~na dìbesys*** si sta rannuvolando; **6.** (*rasti*, *aptikti*) rinvenire; **7.** (*uþþelti*) (*kuo*) coprirsi (*di vegetazione*).

**uþëmímas** *s* (*pvz*., *miesto*) occupazione *m*.

**ûþes|ýs** *s* rumorio (-rii), rumore sordo; (*ðnaresys*) fruscio (-scii), mormorio (-rii), sussurro; (*ypaè balsø*) brusio (-sii); (*maðinos*) ron@io (-zii); ***gåtvës û.*** il rumorio della strada; ***gírios û.*** il mormorio del bosco; ***v¸jo û.*** il fruscio del vento; med ***ðirdiìs ~ia¤*** sòffio *vns* al cuore.

**uþ¸|sti** *v* , ~da, ~dë) **1.** mangiare (*un po'*), mangiucchiare; **2.** ðnek méttere in croce, perseguitare.

**uþfiks|úoti, ~úoja, ~åvo** *vx* **1.** registrare; méttere per iscritto; (*á protokolà*) méttere a verbale, verbali@@are; (*vaizdajuostëje*) riprêndere, filmare; **2.** lingv attestare.

**ùþfrontë** *s* kar le retrovie.

**ùþgaid|a** *s* caprìccio; grillo; ***ténkinti kienõ ~as*** assecondare / soddisfare i capricci di qcn, contentare qcn nei suoi capricci.

**uþgaidùs,** **-í** *agg* capriccioso.

**uþgaiðín|ti, ~a, ~o** *vx* trattenere; ***manê ~o*** mi hanno trattenuto, sono stato trattenuto.

**uþga¤ð|ti** *v* , ~ta, ~o) trattenersi; attardarsi; pêrdere tempo; ***ilga¤ neuþga¤ðk!*** non métterci troppo!

**uþgãulë** *s* affronto; insulto.

**uþgaulia¤** *avv* in modo offensivo, offensivamente.

**uþgauliójimas** *s* offese *dgs* contìnue; insulti *v dgs*; ingiùrie *dgs*.

**uþgaulió|ti, ~ja, ~jo** *vx* offêndere (*ripetutamente*), insultare; ingiuriare.

**uþgaulùs,** **-í** *agg* **1.** offensivo, ingiurioso; ***u. tònas*** un tono offensivo; ***uþga÷lûs þõdþiai*** parole ingiuriose; **2.** (*áþeidus*) suscettìbile.

**uþgãu|ti** *v* , ~na, uþgåvo) **1.** (*kà*) colpire (*qc*), far male (*a qc*); **2.** (*áþeisti*) ferire; offêndere; ***u. kienõ jausmùs (savígarbà)*** ferire i sentimenti (l'amor pròprio) di qcn; ***u. k± þodþiù*** offêndere qcn a parole.

**Ùþgavënës** *s pl* martedì grasso *vns*.

**uþgavímas** *s* **1.** colpo; botta; **2.** prk offesa.

**uþgélti** *v* (ùþgelia, uþg¸lë) (*apie ðaltá*) bruciare; ***rankâs uþg¸lë*** mi brùciano le mani dal freddo.

**uþgér|ti** *v* (ùþgeria, uþg¸rë) **1.** (*kà kuo*) bere (*qcs dietro qcs/con qcs*); ***u. tablêtæ vãndeniu*** prêndere una compressa con dell'âcqua; ***~k stiklínæ ala÷s*** bévici dietro un bicchiere di birra; **2.** (*sveikinant*) (*kà*) brindare (*a qcn*).

**uþgesínimas** *s* spegnimento; estinzione *m*.

**uþgesín|ti, ~a, ~o** *vx* spégnere; (*ypaè gaisrà*) estìnguere; ***u. liepsnâs*** spégnere / estìnguere le fiamme; ***u. núorûkà*** spégnere una cicca.

**uþgêsti** *v* (uþg³sta, uþgìso) **1.** spégnersi; (*tik apie gaisrà*) estìnguersi; ***ugnís uþgìso*** il fuoco si ê spento; ***varíklis nuõlat uþg³sta*** il motore si spegne / si pianta di contìnuo; **2.** prk spégnersi, morire.

**uþgôd|yti, ~o, ~ë** *vx* (fare) rimarginare, cicatri@@are; *ir* prk ***u. þa¤zdà*** rimarginare una ferita.

**uþgiedó|ti** *v* (uþgîeda, ~jo) méttersi / cominciare a cantare; attaccare (a cantare).

**uþgijímas** *s* cicatri@@azione *m*.

**uþgím|ti** *v* , ~sta, ùþgimë) nâscere; êssere nato.

**uþgi¹è|yti** *v* , ~ija, ~ijo) contestare.

**uþgínti** *v* (ùþgina, uþgônë) (*kam kà daryti*) proibire (*a qcn di far qcs*), vietare; ***t¸vas va¤kui uþgônë*** ***rûkôti*** il padre ha proibito al bambino di fumare.

**uþgi¹ti** *v* (ùþgena, ùþginë) guidare, condurre; ***ù. gôvulius*** ***a¹t p¿dymo*** guidare il bestiame al maggese.

**uþgi»|sti, ~sta, ~do** *vt* sentire (*venire a sapere*).

**uþgôti** *v* (uþgýja, uþgíjo) cicatrizzarsi; formarsi la crosta; (*tik apie þaizdà*) rimarginarsi; ***alk¿në ja÷ uþgíjo*** sul gòmito si ê giâ formata la crosta..

**uþgyv|énti** *v* , ~ìna, ~ìno) (*uþsidirbti*) guadagnarci, ricavarci; ***trejùs metùs buva÷ sù jâ, õ k± ~ena÷?*** sono stato tre anni con lei, e che cosa ci ho guadagnato?

**uþglaistôti** *v* (uþgla¤sto, uþgla¤stë) **1.** otturare; (*glaistu*) stuccare; ***u. skýlæ*** stuccare / otturare un buco; **2.** prk (*konfliktà*) comporre; (*skandalà*) tacitare; ***u. nesklandùmà*** rimediare a un inconveniente; **3.** kul glassare.

**uþglóst|yti, ~o, ~ë** *vx* **1.** appianare, livellare; **2.** prk addolcire, lenire; **3.** (*paslëpti*) coprire, occultare.

**uþgniãuþ|ti, ~ia, ~ë** *vx* **1.:** ***u. kãm bùrnà*** tappare la bocca a qcn; prk ***u.*** ***kvåpà*** mozzare il fiato; **2.** prk (*nuslopinti*) smorzare; soffocare.

**uþgõþ|ti** *v* , ~ia, ùþgoþë) **1.** far passare in secondo piano; (*apie garsà*) coprire, soffocare; (*kità aktoriø*) méttere in ombra; ***privalùmai ~ia tr¿kumus*** i vantaggi fanno passare in secondo piano i difetti; **2.** (*nustelbti*) soffocare.

**uþgrãuþ|ti, ~ia, ~ë** *vx* **1.** *ir* prk divorare (*solit*. *di insetti*); **2.** (*uþvalgyti*) mangiare (*un po'*).

**uþgriìb|ti** *v* , ~ia, ùþgriebë) **1.** (*suèiupti*) afferrare; **2.** (*uþgrobti*) (*ko*) impossessarsi (*di qcs*); **3.** (*uþèiuopti*) trovare a tentoni.

**uþgr|i¿ti** *v* , ~i¾va/~i¾na, ~iùvo) **1.** rovinare, cédere; crollare; (*kà*) abbâttersi (*su qc*); ***tóks stógas gåli bêt kadâ u.*** un tetto in sìmili condizioni può rovinare da un momento all'altro; **2.** (*parvirsti*) cadere, cascare; ruzzolare; **3.** prk (*kà*) abbâttersi (*su qc*), piombare addosso (*a qc*); ***juõs ~iùvo nelâimë*** gli ê piombata addosso una disgrâzia; **4.** ðnek (*staiga uþeiti*) piombare; fare irruzione; ***~iùvo sveèiÿ*** sono piombati in casa degli òspiti.

**uþgrob¸jas,** **-a** *s* occupante *v/m*; ***lëktùvo u.*** @ dirottatore *v.*

**uþgrobímas** *s* occupazione *m*; ***lëktùvo u.*** @ dirottamento.

**uþgrób|ti, ~ia, ~ë** *vx* (*kà*) impadronirsi (*di qcs*); impossessarsi; (*pvz*., *kraðtà*) occupare; (*valdþià*) usurpare; ***u. lëktùvà*** @ dirottare un aêreo; ***u. miìstà*** occupare una cittâ.

**uþgró|ti, ~ja, ~jo** *vx* méttersi / cominciare a suonare; attaccare (a suonare).

**uþgrùb|ti,** **uþgru§ba, ~o** *vi* intirizzire.

**uþgr¿din|ti, ~a, ~o** *vx* *ir* prk temprare; temperare.

**uþgr¿|sti** *v* , ~da, ~do) **1.** (*uþkiðti*) ficcare, infilare; cacciare; **2.** (*daug prikimðti*) (*ko/kuo*) inzeppare (*di qcs*).

**uþgùiti** *v* (ùþguja, uþgùjo) maltrattare, bistrattare; (*pavaldinius*) vessare, angariare.

**ùþgultas, uþgult|â** *agg*, *prcp* (*apie ausá*) otturato; (*apie nosá*) chiuso; ***mâno nósis ~â*** ho il naso chiuso.

**uþguµti** *v* (ùþgula, ùþgulë) **1.** (*kà*) schiacciare (*qc*) (sotto di sé); pesare (*su qc*); **2.** prk (*prispausti*) (*kà*) opprìmere (*qc*); pesare (*su qc*); ***liûdesýs ùþgulë mãn ðírdá*** la tristezza mi opprime l'ânima; **3.** (*palinkti*) (*kà*) piegarsi (*su qcs*), chinarsi; ***u. írklus*** piegarsi sui remi; ***ska¤to uþgùlæs knýgà*** se ne sta chino sul libro; **4.** (*uþkimðti*) ostruire; otturare; ***mân ùþgulë ausís*** ho le orecchie otturate; ***jãm ùþgulë krûtínæ*** si sente il petto costipato / oppresso.

**uþhipnotiz|úoti, ~úoja, ~åvo** *vx* ipnoti@@are, ridurre in stato ipnòtico.

**ûþímas** *s* **1.** rumore sordo; mormorio (-rii); **2.** (*puotavimas*) baldòria, bisbòccia (-ce).

**ùþimtas,** **uþimt|â** *agg*, *prcp* occupato; ***telefòno línija ~â*** ê occupato, la lìnea ê occupata; ***ði± savãitæ âð laba¤ ù.*** questa settimana sono molto occupato.

**uþi§ti** *v* (ùþima, ùþëmë) **1.** occupare; ***u. da÷g la¤ko*** richiédere molto tempo; ***u. da÷g viìtos*** occupare / prêndere molto spâzio; ***u. miìstà*** occupare / espugnare / conquistare una cittâ; ***u. trìèià viìtà*** occupare il terzo posto, êssere al terzo posto; ***u. viìtà*** occupare un posto; (*jà kam palaikyti*) tenere il posto (*a qcn*); prk ***u. kvåpà*** mozzare il fiato, tògliere il respiro; **2.** (*eiti pareigas*) occupare; ricoprire; ***u. atsakíngà pòstà*** occupare un posto di responsabilitâ; **'** ***u. påreigas*** svòlgere mansioni; **3.** (*tam tikrà padëtá*) assùmere (*una posizione*); méttersi; ***u. gùlimà (s¸dimà) pådëtá*** méttersi sdraiato (seduto); ***u. vadovãujamà pådëtá*** assùmere una posizione di comando; **4.** (*pvz*., *sveèius*) intrattenere (*qcn*).

**uþimtùmas** *s* occupazione *m*.

**uþja÷èiantis,** **-i** *prcp* compassionévole; pietoso.

**uþja÷|sti** *v* , ~èia, ùþjautë) (*kà*) provare compassione (*per qcn*); dispiacersi; compatire, commiserare, compassionare; ***nuoðirdþia¤ uþjauèiù*** mi dispiace sinceramente; (*kam nors mirus*) sentite condoglianze.

**uþjautímas** *s* compassione *m*.

**uþjuõk|ti** *v* , ~ia, ùþjuokë) ridicoli@@are, coprire di ridìcolo.

**ùþjûr|is** *s* oltremare *v nkt*; ***íð ~io*** da oltremare.

**uþkabín|ti, ~a, ~o** *vx* **1.** agganciare; appéndere; **2.** (*pvz*., *langà*, *duris*) chiùdere (*con un gancio*), serrare; **3.** (*kibti prie ko*) dare addosso (*a qcn*); attaccare briga (*con qcn*); **4.** (*uþkliudyti*) urtare; agganciare; (*paliesti*) sfiorare; **'** **5.** prk ðnek (*sudominti*) prêndere, intrigare; **6.:** prk ***u. ðìðtàjà dìðimtá*** entrare nei sessanta.

**ùþkabor|is** *s* angolino, cantùccio; ***iðgriõzti visùs ~ius*** frugare in ogni ângolo.

**uþka¤|sti I** *v* , ~èia, ùþkaitë) méttere sul fuoco; ***u. vãndená*** scaldare l'âcqua, méttere l'âcqua sul fuoco.

**uþka¤|sti II** *v* , ~sta, ~to) **1.** (*suðilti*) scaldarsi, riscaldarsi; **2.** (*parausti*) arrossire, avvampare.

**uþkaiðôti,** **ka¤ðo, ka¤ðë** *vt* tappare (*più buchi*).

**ùþkaitas** *s* tech (*èerpës*) orlo (sporgente); (*lentos*) tenone *v*; addentatura.

**uþka¤tin|ti, ~a, ~o** *vi* cominciare a scottare.

**uþkalb¸jimas** *s* incantésimo, incanto.

**uþkalb¸|ti** *v* (ùþkalba, ~jo) incantare; fare un incantésimo; ¨ ***u. kãm dantís*** abbindolare, turlupinare.

**uþkãlbin|ti, ~a, ~o** *vx* (*kà*) méttersi a parlare (*con qcn*), rivòlgere la parola (*a qcn*); attaccare bottone / discorso (*con qcn*); ***u. nepaþïstamà vôrà*** attaccare bottone con uno sconosciuto.

**uþkalk¸|ti, ~ja, ~jo** *vi* incrostarsi (*di calcare*).

**uþkãlti** *v* (ùþkala, ùþkalë) **1.** inchiodare; (*pvz*., *langà lentomis*) chiùdere con assi / con tâvole; (*kalant padaryti*) costruire (*con chiodi*); **2.:** ðnek ***u. nemaþa¤ pínigo*** fare dei gran soldi.

**ùþkampis** *s* luogo / posto fuori mano.

**uþkampùs, -í** *agg* fuori mano *nkt*.

**uþkamðôti** *v* (uþka§ðo, uþka§ðë) tappare (*più cose*), turare.

**uþkam|úoti, ~úoja, ~åvo** *vx* (*labai*) sfinire, stremare; (*negyvai*) massacrare.

**ùþkanda** *s* Þ **ùþkandis**.

**uþkandºlë** *s dimin* antipastino.

**uþkandínë** *s* snack [znɛk] bar *v nkt*, tâvola calda; bar *v nkt*.

**ùþkandis** *s* spuntino, snack [znɛk] *v nkt*; (*pirmasis patiekalas*) antipasto; (*prie alaus ir pan*.) stuzzichino.

**uþkandþ|iãuti**, ~iãuja, ~iåvo *vx* mangiare un boccone, fare uno spuntino.

**uþkandþiåvimas** *s* spuntino.

**uþkankín|ti, ~a, ~o** *vx* torturare, tormentare; (*negyvai*) suppliziare.

**uþkapó|ti, ~ja, ~jo** *vx* (*uþmuðti*) fare a pezzi, massacrare; (*uþplakti*) sferzare a morte, uccìdere a sferzate; (*snapu*) uccìdere a colpi di becco.

**ùþkarda** *s* **1.** *ir* prk barriera; sbarramento; cordone *v*; ***pasîenio ù.*** posto di frontiera; **2.** inf firewall *v nkt*; **3.** kar presìdio (-di).

**ùþkardas** *s* sbarra (*di una frontiera e sim*.); ***pérvaþos ù.*** le sbarre di un passâggio a livello.

**uþkãrdymas** *s* prevenzione *m*.

**uþkãrd|yti, ~o, ~ë** *vx* **1.** ostacolare; bloccare; **2.** prk prevenire; bloccare.

**uþkar|iãuti**, ~iãuja, ~iåvo *vx* conquistare; ***u. kienõ simpåtijas (svìtimà ðålá)*** conquistare la simpatia di qcn (un paese straniero).

**uþkariãutojas,** **-a** *s* conquistatore -trice.

**uþkariåvimas** *s* conquista.

**ùþkarpa** *s* barbìglio (-gli).

**uþkarpºlë** *s* (*ðrifto þenklo*) grâzia (*nei caratteri di stampa*).

**ùþkarta** *s* intaccatura.

**uþkãrti** *v* (ùþkaria, uþkórë) **1.** appéndere, agganciare; **2.** ðnek menk accollare, appioppare.

**uþkâsti** *v* (ùþkasa, ùþkasë) sotterrare, seppellire; interrare; (*pvz*., *duobæ*) colmare; ¨ ***u. kåro ki»vá*** sotterrare l'ascia di guerra.

**uþkçsti** *v* (uþkãnda, uþkãndo) mangiare qualcosa; fare uno spuntino; mangiucchiare; ¨ ***jãm þådà uþkãndo*** ha perso la lìngua.

**uþkeikímas** *s* sortilêgio, incantésimo.

**uþkéiktas, uþkeiktâ** *agg*, *prcp* stregato, incantato.

**uþkéik|ti, ~ia, ~ë** *vx* stregare, incantare.

**uþke¤s|ti** *v* , ~èia, ùþkeitë) sovrapporre.

**uþkélti** *v* (ùþkelia, uþk¸lë) **1.** poggiare (*sollevando*), sollevare; innalzare; issare; ***u. ma¤ðà a¹t peèiÿ*** poggiare / méttere / caricare un sacco sulle spalle; **2.** (*vartus*) chiùdere (*un cancello e sim*.).

**uþkìpin|ti, ~a, ~o** *vi* cominciare a cuòcere / a scottare mol­to (*del sole*).

**uþker¸|ti** *v* (ùþkeri, ~jo) **1.** incantare, stregare; fare un incantésimo; **2.** prk (*suþavëti*) affascinare, incantare; ammaliare.

**uþkíb|ti** *v* (uþki§ba, ~o) (*uþ ko*) impigliarsi (*a qcs*, *in qcs*); *ir* prk ***u. a¹t kabliùko*** abboccare all'amo.

**uþkiet¸j|æs, -usi** *agg*, *prcp* **1.** prk incallito, impenitente; ***u. nusikaµtëlis*** un criminale recidivo; ***u. rûkålius*** un fumatore incallito / accanito; ***u. viengu¹gis*** uno scâpolo impenitente; **2.:** prk ***~usi ðirdís*** cuore indurito; **3.:** ***~æ viduria¤*** ventre *v* costipato.

**uþkiet¸jimas** *s* : ***viduriÿ u.*** stitichezza, costipazione *m*; med stipsi *m nkt*.

**uþkiet¸|ti, ~ja, ~jo** *vx* **1.** indurirsi; ***(jãm) ~jo viduria¤*** ha l'intestino costipato; **2.** prk incallirsi.

**ùþkilas, uþkilâ** *agg* (*apie vartus*) serrato.

**uþkíl|ti, uþkýla, ~o** *vi* levarsi (in alto), alzarsi.

**uþkímæs,** **-usi** *prcp* roco, rãuco.

**uþkimím|as** *s* raucêdine *m*; ***rºkti lígi ~o*** sgolarsi.

**uþki§ð|ti** *v* (ùþkemða, ~o) **1.** otturare; intasare; (*ypaè kamðèiu*) tappare, turare; ***u. skýlæ*** otturare un buco; ***u. va§zdá*** intasare un tubo; ¨ ***u. kãm bùrnà*** chiùdere la bocca a qcn; **2.** (*praëjimà ir pan*.) ostruire; intasare.

**uþkím|ti** *v* , ~sta, ~o) diventare rauco, arrochirsi.

**uþki»|sti** *v* (ùþkerta, ~to) **1.** sbarrare, bloccare; ***lavinâ ~to kìlià*** una valanga ha sbarrato la strada; *ir* prk ***u.*** ***kãm*** ***kìlià*** tagliare la strada a qcn; contrastare efficacemente qc; (*visam laikui*) porre fine a qcs; ¨ ***ka¤p kirviù ~to*** ha tagliato corto; **2.** (*padaryti rantà*) fare una tacca; intaccare; **3.** (*suduoti*) sferrare (un colpo); (*botagu*) sferzare.

**uþkíð|ti** *v* (ùþkiða, ~o) **1.** infilare, ficcare; **2.** (*uþkimðti*) tappare, otturare; (*ypaè kamðèiu*) turare; ***u. kãm bùrnà*** tappare la bocca a qcn; imbavagliare qcn; **3.** (*uþsklæsti*) serrare; chiùdere con il chiavistello / il catenâccio.

**uþklasín|is,** **-ë** *agg* extrascolâstico; ***~ë veiklâ*** at­ti­vitâ extrascolâstica.

**ùþklausa** *s* inf interrogazione *m*.

**uþklãus|ti, ~ia, ~ë** *vx* (*ko*) chiédere (*a qcn*), informarsi (*presso qc*).

**uþklýdëlis, -ë** *s* nuovo -a venuto -a.

**ùþklija** *s* etichetta.

**uþklij|úoti, ~úoja, ~åvo** *vx* incollare, appiccicare; attaccare con la colla; ***u. etikêtæ (påðto þénklà)*** incollare / attaccare un'etichetta (un francobollo); ***u. vókà*** chiùdere una léttera (*con la colla*), sigillare una léttera.

**uþkli§p|ti** *v* , ~sta, ~o) (*purve*) impantanarsi.

**uþklô|sti** *v* , ~sta, ~do) capitare (*per caso*); prk ***u. ñ sveèiùs*** fare una visitina (inaspettata).

**uþkl|iudôti** *v* , ~iùdo, ~iùdë) (*kà*) urtare (*qc*), incocciare; (*koja*) inciampare (*in qc*); (*vos liesti*) sfiorare; prk ***u. õpø dalýkà*** incappare in una questione spinosa.

**uþkl|i¿ti** *v* , ~i¾va, ~iùvo) **1.** (*uþ ko*) incocciare (*(in) qc*), rimanere preso (*in qcs*); (*koja*) inciampare; (*apie akis*, *þvilgsná*) fissarsi (*su qc*); posarsi; ***tamsojê u. ùþ këdºs*** inciampare al bùio in una sêdia; **2.** prk (*patraukti dëmesá*) (*kam*) richiamare l'attenzione (*di qcn*); **3.** prk (*sukelti prieðiðkumà*) (*kam*) dare fastìdio (*a qc*), creare problemi; non andare bene; (*sukelti nepasitenkinimà*) indispettire (*qc*); (*sukelti átarimø*) insospettire (*qc*); ***niìkam ~i¾va*** non crea problemi a nessuno.

**uþklóst|yti, ~o, ~ë** *vx* coprire, ricoprire.

**uþklõtas** *s* coltre *m*.

**uþkló|ti, ~ja, ~jo** *vx* coprire (*solit*. *una superficie*), ricoprire; ***u. stålà stãltiese*** stêndere una tovâglia (sulla tâvola); ***u. va¤kà a¹tklode*** coprire un bambino con una coperta.

**uþklùp|ti** *v* (uþklu§pa, ~o) **1.** sorprêndere, cògliere (di sorpresa); ***mùs ~o lietùs*** ci ha sorpresi un acquazzone; ***u. prîeðà*** cògliere il nemico di sorpresa; **2.** (*netikëtai pagauti*) beccare, pizzicare; ***u. nusikaltímo viìtoje*** cògliere in flagrante.

**uþkna»k|ti** *v* , ~ia, ùþknarkë) cominciare a russare.

**uþknís|ti** *v* (ùþknisa, ~o) ðnek scocciare, rómpere (le scâtole); rómpere le palle.

**uþkod|úoti, ~úoja, ~åvo** *vx* codificare.

**uþkonserv|úoti, ~úoja, ~åvo** *vx* **1.** preservare, protêggere; **2.** (*padaryti konservus*) conservare sottovuoto, inscatolare.

**uþkóp|ti, ~ia, ~ë** *vx* salire; (*sunkiai*) inerpicarsi; ***u. lãiptais (ñ scênà)*** salire le scale (sul palcoscênico); ***u. a¹t kãlno*** scalare un monte.

**ùþkrat|as** *s* med agente patògeno; (*uþkrëtimas*) infezione *m*; ***~o pérneðëjas*** portatore *v* (sano).

**uþkrãu|ti** *v* , ~na, uþkróvë) **1.** (*kà ant ko*) caricare (*qcs su qcs*); (*kà kuo*) coprire (*qcs di qcs*); ammucchiare (*su qcs qcs*); **2.** prk gravare; (*kà kam*) accollare (*a qc qcs*), appioppare; ***u. mókesèius*** gravare / onerare di tasse; ***u. kãm dãrbà*** accollare a qcn un lavoro.

**ùþkreèiamas,** **uþkreèiam|â** *agg* contagioso, infettivo; ***~â / ~óji*** ***ligâ*** malattia contagiosa.

**ùþkrëstas, uþkrëstâ** *agg* inquinato; contaminato.

**uþkrºsti** *v* (ùþkreèia, ùþkrëtë) **1.** (*liga*) infettare, contagiare; (*kà kuo*) attaccare (*a qcn qcs*); ***u. vísà ðe¤mà*** contagiare tutta la famìglia; ***u. k± tyma¤s*** attaccare il morbillo a qcn; prk ***jí mùs visùs ùþkrëtë sâvo entuziazmù*** con il suo entusiasmo ci ha contagiati tutti; **2.** (*pvz*., *vandená*) inquinare; contaminare.

**uþkrëtímas** *s* **1.** (*liga*) infezione *m*, contâgio; ***kra÷jo u.*** infezione del sângue; sepsi *m*; **2.** (*pvz*., *vandens*) contaminazione *m*.

**uþkri§|sti,** **ùþkre§ta, ~to** *vt* mangiare un boc­co­ne.

**uþkr|ísti** *v* , ~i¹ta/ùþkrenta, ~íto) **1.** (*ant ko*; *uþ ko*) cadere (*su qc*; *dietro (a) qc*); ***varvìklis ~íto mãn a¹t galvõs*** mi ê caduto un ghiacciolo in testa; ***knygâ ~íto ùþ sòfos*** il libro ê caduto dietro al divano; **2.** prk (*iðeiti ið atminties*) uscire di mente; ***påvardës profêsoriui daþna¤ ~i¹ta*** il professore (si) scorda / si diméntica spesso i cognomi, al professore i cognomi êscono spesso di mente; **3.:** prk ***(mãn)*** ***~íto a÷sys*** mi si sono otturate le orécchie; ***~íto þådas*** ha perso la voce.

**ùþkulas** *s* *tipo di* lardo, lardello.

**ùþkulis|iai** *s pl* **1.** retroscena *vns*; **2.** prk retroscena *v vns nkt*; ***atskle¤sti nusikaltímo ~ius*** svelare i retroscena di un delitto; ***þviµgsnis ñ ~us*** uno sguardo dietro le quinte.

**uþkulisín|is,** **-ë** *agg* segreto (*propr*. *"che avviene dietro le quinte"*); ***~ës derôbos*** trattative segrete.

**ùþkuln|is** *s* (*bato*) gambino; (*á bato uþpakalá dedama standi oda*) contrafforte *v*; ¨ ***ðirdís ~iuose*** col cuore in gola; col fiato in gola / sospeso.

**uþkurýs** *s* marito che va a vìvere in casa dei suòceri.

**uþkùrti** *v* (ùþkuria, uþk¿rë) **1.** accêndere; ***u. lãuþà*** accêndere un falò; ***u. pi»tá*** riscaldare la sâuna; ¨ levare il pelo; **2.** ðnek (*uþbëgti*) córrere su di volata.

**uþku»tin|ti, ~a, ~o** *vt* assordare.

**uþkvíp|ti, uþkvi§pa, ~o** *vi* cominciare a sentir­si odore; ***~o kavâ*** si ê sentito odore di caffê.

**ùþlaida** *s* parte *m* sovrapposta.

**ùþlaidas** *s* (*stogo*) sporgenza del tetto; aggetto.

**uþlaikôti** *v* (uþla¤ko, uþla¤kë) trattenere.

**uþlað¸|ti,** **ùþlaða, ~jo** *vi* (s)gocciolare sopra.

**uþlãuþ|ti, ~ia, ~ë** *vx* **1.** (*uþsukti*, *uþlenkti*) piegare (indietro); ***u. kãm rankâs ùþ nùgaros*** tòrcere a qcn le brâccia dietro la schiena; **2.** (*palauþti*) rómpere (*parzialmente*); (*indà*) sbrecc(i)are.

**uþléistas,** **uþleist|â** *prcp* trascurato; ***~â ligâ*** una malattia trascurata.

**uþléi|sti** *v* , ~dþia, ~do) **1.** (*kam kà*) cédere (*a qcn qcs*), lasciare; ***u. viìtà senùtei*** cédere il posto a una vecchietta; **2.** (*apleisti*) trascurare; ***u. lígà*** trascurare una malattia; **3.** (*uþdangà*) abbassare (*una tenda e sim*.), calare; (*langà*) coprire (*una finestra con una tenda*); **4.** (*lentas ir pan*.) sovrapporre; (*èerpëmis*) embricare.

**uþlºkti** *v* (ùþlekia, ùþlëkë) córrere a precipìzio; andare di volata; ***u. lãiptais ñ vi»ðø*** córrere di volata su per le scale.

**uþle¹k|ti** *v* , ~ia, ùþlenkë) piegare; (*rankovës galus*) rimboccare; (*vinies galà*) ribadire, ribâttere; ***u. knýgos låpà*** fare l'orécchia alla pâgina di un libro.

**uþliejímas** *s* inondazione *m*; allagamento.

**uþlîe|ti, ~ja, ~jo** *vx* **1.** versare, spândere; (*ypaè netyèia*) rovesciare; **2.** *ir* prk (*uþtvindyti*) inondare, allagare; sommêrgere; ***ùpë ~jo kãimà*** il fiume ha inondato il villâggio; prk ***u. ðviesâ*** inondare di luce; **3.** (*uþgesinti*) estìnguere.

**uþli¹k|ti** *v* , ~sta, ~o) piegarsi, incurvarsi.

**uþlipdôti** *v* (uþlípdo, uþlípdë) **1.** incollare, appiccicare; **2.** (*uþtaisyti*) tappare; (*silikonu*) siliconare.

**uþlíp|ti** *v* (ùþlipa, ~o) **1.** salire, montare; ***u. a¹t garbºs pakylõs (ñ scênà)*** salire sul pòdio (sul palcoscênico); ***u.*** ***a¹t ãrklio*** montare a cavallo; **2.** (*uþminti*) pestare, calpestare; ***u. kãm a¹t kójos*** pestare un piede a qcn; ¨ ***u.*** ***kãm*** ***a¹t*** ***sprãndo*** rómpere l'ânima a qcn.

**uþlñsti** *v* (ùþlenda, uþli¹do) **1.** (*uþ kà*) méttersi dietro (*(a) qc*), infilarsi dietro; nascòndersi; ***va¤kas uþli¹do ùþ mótinos*** il bambino si nascose dietro la madre; **2.** (*be eilës*) saltare la fila, non rispettare la coda.

**uþlôti** *v* (uþlýja, uþlíjo) (*kà*) piòvere (*su qc*).

**uþliûl|iúoti, ~iúoja, ~iåvo** *vx* ninnare, cullare (*solit*. *cantando*).

**uþlóp|yti, ~o, ~ë** *vx* rattoppare, rappezzare; prk ***u. próperðà ta»p koalícijos nariÿ*** ricucire lo strappo tra i membri della coalizione.

**uþl¿þ|ti** *v* , ~ta, ~o) **1.** (*ne visai nulûþti*) rómpersi (*parzialmente*); scheggiarsi; (*apie indà*) sbrecc(i)arsi; **2.** prk (*nutrûkti*) cessare di botto, interrómpersi bruscamente.

**ùþmaèia** *s* , **uþmaèiâ** *s* mira, disegno; (*pikta*) losco (-schi) intento, trama.

**uþm|aiðôti** *v* , ~a¤ðo, ~a¤ðë) amalgamare; (*duonà*, *molá*, *teðlà*) impastare.

**uþmånymas** *s* **1.** sfìzio; **'** **2.** Þ **sumånymas**.

**uþmarín|ti, ~a, ~o** *vx* intorpidire; (*prieð operuojant*) anesteti@@are.

**ùþmaris** *s* terra oltremarina, oltremarino.

**uþmarðtís** *sf* oblio (-lii); ***nugri§zti ñ ùþmarðtá*** cadere / scivolare nell'oblio, finire nel dimenticatòio.

**uþmarðùmas** *s* scarsa memòria, smemoratezza.

**uþmarðùs,** **-í** *agg* smemorato, di poca memòria.

**¾þmas** *s* (*triukðmas*) baccano, chiasso.

**uþmask|úoti, ~úoja, ~åvo** *vx* **1.** mascherare, camuffare; **2.** (*slëpti*) nascòndere; kar mimeti@@are; ***u. pabûklùs*** mimeti@@are l'artiglieria.

**uþmatôti** *v* (uþmåto, uþmåtë) scòrgere; adocchiare; ¨ ***kîek åkys uþmåto*** @ a pêrdita d'òcchio.

**uþma÷kðlin|ti, ~a, ~o** *vx* (*kepuræ*) calcare (*il berretto e sim*.).

**uþmãu|ti** *v* , ~na, uþmóvë) **1.** infilare; **2.** (*kà sau*) infilarsi (*qcs*), méttersi; ***u. kãukæ*** méttersi una maschera.

**uþmêgzti** *v* (ùþmezga, ùþmezgë) **1.** (*uþriðti*) annodare, allacciare; (*mazgà*) fare un nodo; ***u. si¿lo gålà*** fare un nodo al capo di un filo; **2.** prk (*pradëti*) cominciare, iniziare; ***u.*** ***påþintá*** fare conoscenza; ***u. sãntykius*** strìngere una relazione, allacciare rapporti; ***u. ve»slo ryðiùs*** avviare / allacciare rapporti commerciali; ***u. pókalbá*** cominciare una conversazione.

**uþmérk|ti, ~ia, ~ë** *vx* (*akis*) chiùdere gli occhi; ¨ ***u. akís*** (*mirti*) chiùdere gli occhi (*morire*); (*uþmigti*) addormentarsi; chiùdere òcchio.

**uþme»k|ti** *v* , ~ia, ùþmerkë) méttere a mollo, ammollare.

**uþmêsti, ùþmeta, ùþmetë** *vt* **1.** (*ant virðaus*) get­tare (*sopra*), buttare; ¨ ***u. ãká, u. þviµgsná*** dare un'occhia­ta; **2.** (*uþnerti*): ***u. vi»væ a¹t kåklo*** méttere il câppio al collo; **3.** prk (*palikti*) abbandonare; **4.** (*nukiðti*) cacciare, ficcare.

**uþm¸t|yti, ~o, ~ë** *vx* lanciare (*più cose*; *più volte*), gettare; ***u. akmenimís*** lapidare.

**uþmiegótas, -a** *agg* assonnato, sonnolento; sonnacchioso.

**uþmiegó|ti** *v* (uþmiìga, ~jo) sonnecchiare.

**uþmiestínis, -ë** *agg* extraurbano; suburbano.

**ùþmies|tis** *s* i dintorni di una cittâ, campagna; (*gyvenamoji zona*) hinterland *v nkt*; ***~tyje*** fuori cittâ; ***vaþiúoti ñ ~tá*** andare in campagna / fuori cittâ; ***~èio rezide¹cija*** residenza di campagna.

**uþmigdôti** *v* (uþmígdo, uþmígdë) (far) addormentare, far dormire; prk ***u. sçmonæ*** addormentare le coscienze.

**uþmígæs, -usi** *prcp* addormentato.

**uþmíg|ti** *v* (uþmi¹ga, ~o) addormentarsi, prêndere sonno; ***ilga¤ negal¸jau u.*** a lungo non sono riuscito a prêndere sonno.

**uþmínk|yti, ~o, ~ë** *vx* impastare.

**uþmínti** *v* (ùþmina, uþmônë) pestare, calpestare; (*koja nuspausti*) schiacciare (*col piede*); ***u. kãm a¹t kójos*** pestare un piede a qcn; ***u. tarakõnà*** schiacciare / spiaccicare uno scarafâggio.

**uþmi¹ti** *v* (ùþmena, ùþminë) **1.** (*priminti*) rammentare; **2.:** ***u. mñslæ*** fare / porre un indovinello; prk porre un interrogativo.

**uþmin|úoti, ~úoja, ~åvo** *vx* minare; ***u. frònto rúoþà*** minare la lìnea di un fronte.

**uþmirðímas** *s* dimenticanza.

**uþmi»ð|ti** *v* , ~ta, ~o) dimenticare, scordare; (*kà daryti*) dimenticarsi (*di far qcs*), scordarsi; ***u. påvardæ (raktùs, vargùs)*** dimenticare un cognome (le chiavi, le sofferenze); ***uþmirða÷ pasakôti*** mi sono dimenticato di dirlo; ***~k k± pasakia÷!*** lascia pêrdere!, dimêntica quello che ho detto!

**uþmi»ti, uþmírðta, ùþmirë** *vi* **1.** (*nutirpti*) intorpi­dirsi; **2.** prk pêrdere vitalitâ; venir me­­no.

**ùþmoj|is** *s* **1.** piano d'azione; (*ketinimas*) intento; mira; (*veikimo sritis*) campo d'azione; **2.** (*mastas*) ampiezza, portata; ***ágãuti vís didêsná ~á*** acquisire sempre maggiore ampiezza.

**ùþmokestis** *s* compenso; (*alga*) stipêndio (-di); (*darbininkø*) paga, salârio; ***dãrbo ù.*** retribuzione *m*; teis ***minimalùs dãrbo ù.*** salârio mìnimo garantito.

**uþmok¸|ti** *v* (uþmóka, ~jo) (*uþ kà*; *kam uþ kà*) pagare (*qcs*; *qcn per qcs*); ***u. gryna¤s (kredíto kortelê)*** pagare in contanti (con la carta di crêdito); prk ***mãn ùþ ta¤ ~si!*** me la pagherai!

**uþmokýklin|is, -ë** *agg* parascolâstico; ***~iai uþsië­mímai*** attivitâ parascolâstiche.

**ùþmovas** *s* cappùccio; protezione *m*.

**uþm¿r|yti** *v* , ~ija, ~ijo) murare; ***u. durís*** murare una porta.

**uþmuðímas** *s* uccisione *m*, ammazzamento; soppressione *m*.

**uþmùð|ti** *v* (ùþmuða, ùþmuðë) uccìdere, ammazzare; ***u. þiùrkæ*** ammazzare un ratto; prk ***u. þvilgsniù*** uccìdere con lo sguardo; ¨ ***miìga ka¤p ùþmuðtas*** dorme come un sasso / come un ghiro; ***nórs ~k!*** neanche morto!; (*niekaip*) mi venisse un colpo!

**uþnérti** *v* (ùþneria, uþn¸rë) **1.** allacciare (*un laccio e sim*.); ***u. kílpà a¹t kåklo*** strìngere un câppio al collo; **2.** (*pagauti kilpomis*) prêndere al lâccio, accalappiare.

**uþneðió|ti, ~ja, ~jo** *vx* **1.** (*drabuþius*) tenersi a lungo addosso (*sporcando*); **2.** Þ **uþnêðti**.

**uþnêðti** *v* (ùþneða, ùþneðë) **1.** (*ant ko*) portare (*su qcs*); trasportare; ***la¤và ùþneðë a¹t seklumõs*** la nave si ê incagliata su una secca; **2.** (*uþsukus atneðti*) passare a portare; ***dra÷gas ùþneðë mãn ùþraðus*** un amico ê passato a portarmi gli appunti; **3.** (*kà kuo*) ricoprire (*qcs di qcs*); ***kìlias bùvo ùþneðtas akmenimís i» þemê*** la strada ê stata ricoperta di pietre e terra; **4.** (*maðinà*) derapare (*di auto*); slittare; **5.** (*ligà*) trasméttere (*una malattia*).

**uþneðtínis, -ë** *agg* bot avventìzio (-zi).

**uþník|ti, uþni¹ka, ~o** *vi* **1.** (*ásitraukti á veiklà*) darci dentro; **2.** (*uþpulti*) assalire; aggredire.

**uþníþ|ti** *v* (uþnýþta, ~o) cominciare a prùdere.

**ùþnugar|is** *s* **1.** (posto dietro) le spalle; ***íð ~io*** da dietro le spalle; ***kienõ ~yje*** alle spalle di qcn; ***sãugoti ~á*** guardare le spalle; **2.** prk (*prieglobstis*) protezione *m*, appòggio; ***tur¸ti tvírtà ~á*** avere le spalle coperte; **3.** kar retrovie *dgs*; ***pùlti íð ~io*** assalire le retrovie / alle spalle.

**uþnuõd|yti** *v* , ~ija, ~ijo) avvelenare; ***u. vãndená*** avvelenare / infettare l'âcqua; prk ***u. kãm gyvìnimà*** avvelenare l'esistenza a qcn.

**uþpajam|úoti, ~úoja, ~åvo** *vx* fin segnare a crédito, accreditare.

**uþpakalín|is,** **-ë** *agg* posteriore; di dietro; ***u. áëjímas*** l'entrata sul retro; ***~iai råtai*** le ruote posteriori; lingv ***~ës eilºs baµsiai*** le vocali posteriori.

**ùþpakal|is** *s* **1.** dietro *nkt*; ***~yje*** (di) dietro; ***la¤vo ù.*** la poppa di una nave; ***íð ~io*** da dietro, da tergo; alle spalle; ***pùlti íð ~io*** assalire alle spalle; **2.** (*kûno dalis*) sedere *v*; didietro *nkt*; ***áspírti ñ ~á*** dare / tirare un câlcio nel sedere; ***kraipôti ~á*** sculettare; ¨ ***ñ ~á lñsti*** lustrare le scarpe; leccare il culo *vulg*; ***~iais badôtis*** stare stipati / pigiati.

**uþpakaliùkas** *s* *dimin* sederino; culetto.

**uþpatentúotas, -a** *agg*, *prcp* brevettato.

**uþpatent|úoti, ~úoja, ~åvo** *vx* brevettare.

**uþpelk¸|ti, ~ja, ~jo** *vi* impaludarsi.

**uþpelnô|ti, uþpeµno, uþpeµnë** *vi* ottenere profitti; guadagnare (*ir* prk).

**uþper¸tas, -a** *prcp* : ***u. kiauðínis*** uovo andato a male.

**ùþpernai** *avv* due anni fa.

**ùþpernykðtis,** **-ë** *agg* di due anni fa.

**uþpôkd|yti, ~o, ~ë** *vx*, **uþpýkin|ti, ~a, ~o** *vx* fare arrabbiare; (*erzinti*) irritare.

**uþpýk|ti** *v* , ~sta, ~o) (*ant ko*) arrabbiarsi (*con qcn*); (*labai*) infuriarsi; ***jí ~usi a¹t tav³s*** ê ar­rabbiata con te.

**ùþpilas** *s* **1.** *qualsiasi* salsa, salsina *che si versa su una pietanza*; **2.** (*vaistaþoliø*) infuso, decotto, tisana; ***ramunºliø ù.*** infuso di camomilla; **3.** stat (*þemës*) rinterro.

**ùþpildas** *s* chem riempitivo, filler *v nkt*.

**uþpíld|yti, ~o, ~ë** *vx* (*kà ko/kuo*) riempire (*qcs di qcs*); (*ypaè blankà ir pan*.) compilare; ***u. ankêtà*** compilare / riempire un questionârio.

**uþpílt|as, uþpiltâ** *agg*, *prcp* ðnek ***~os åkys*** occhi *v* lùcidi (*per ubriachezza*).

**uþpílti** *v* (ùþpila, uþpôlë) **1.** versare (*sopra*); (*ypaè netyèia*) rovesciare; **2.** (*þoles*, *kad pritrauktø*) méttere in infusione; **3.** (*uþtvindyti*) inondare, allagare; **4.** (*kà kuo*) ricoprire (*qc di qcs*).

**uþpiltínë** *s* *tipo di* liquore *v* dolce.

**uþpirk¸jas, -a** *s* acquirente *v/m*.

**uþpi»k|ti** *v* (ùþperka, ~o) **1.** (*ko*) fare (una / la) scorta (*di qcs*); **2.:** ***u. miðiâs*** far dire una messa.

**uþpís|ti** *v* (ùþpisa, ~o) vulg rómpere / scassare il cazzo.

**uþpjãu|ti** *v* , ~na, uþpjóvë) **1.** (*kandant nuþudyti*) dilaniare; **2.** (*padaryti rantà*) intaccare.

**uþpjudôti** *v* (uþpjùdo, uþpjùdë) (*ðunimi*) sguinzagliare (*un cane dietro qcn*).

**uþplâkti** *v* (ùþplaka, ùþplakë) **1.** (*botagu*) frustare (*anche a morte*), sferzare; (*rykðte*) fustigare (*anche a morte*), scudisciare; **2.** (*kniedëmis*) rivettare.

**uþpla÷k|ti** *v* , ~ia, ùþplaukë) **1.:** ***u.*** ***a¹t*** ***seklumõs*** incagliarsi / arenarsi su una secca; **2.** ðnek (*uþplûsti*) arrivare a vagonate.

**uþpl|ikôti** *v* , ~íko, ~íkë) (*kà*) versare âcqua bollente (*sopra qcs*); ***u. arbåtà*** méttere il tê in infusione; fare un tê.

**uþplomb|úoti, ~úoja, ~åvo** *vx* **1.** piombare, impiombare; **2.:** ***u. da¹tá*** otturare un dente, fare un'otturazione.

**uþplûdímas** *s* **1.** inondazione *m*; invasione *m*; (*ir* prk); **2.** prk (*jausmø ir pan.*) ondata.

**uþplu§pin|ti, ~a, ~o** *vx* ðnek fare secco.

**uþpl¿|sti** *v* , ~sta, ~do) **1.** (*uþlieti*) sommêrgere; inondare; **2.** prk (*gausiai prieiti*) invâdere; affollare, gremire; ***turístai ~do påplûdimius*** i turisti hanno invaso le spiagge; **3.** prk (*smarkiai apimti*) sopraffare, pervâdere; inondare; ***u. bjauria¤s þõdþiais*** insul­tare.

**uþporôt** *avv* fra / tra tre giorni.

**ùþpraeit|as, -a** *agg* due... fa; ***~ais mìtais*** due anni fa; ***~à savãitæ*** due settimane fa.

**uþpr|aðôti** *v* , ~åðo, ~åðë) **1.** (*pakviesti*) invitare (per tempo); **2.** (*per didelæ kainà*) chiédere troppo; far prezzi troppo alti.

**uþprenumer|úoti, ~úoja, ~åvo** *vx* (*kà kam*) abbonare (*a qcs qcn*).

**uþprogram|úoti, ~úoja, ~åvo** *vx* programmare.

**uþprotest|úoti, ~úoja, ~åvo** *vx* (*kà*) protestare (*contro qc*); teis ***u. te¤smo sprendímà*** impugnare una sentenza; teis ***u. vêkselá*** protestare una cambiale.

**uþprotokol|úoti, ~úoja, ~åvo** *vx* verbali@@are.

**uþpùlti** *v* (uþpúola, uþpúolë) **1.** aggredire, assalire; attaccare; ***prîeðai uþpúolë kråðtà*** i nemici hanno attaccato il paese; ***dù vyríðkiai manê uþpúolë gåtvëje*** due tizi mi hanno aggredito per strada; ***alkaní vilka¤ i» þmõgø uþpúola*** i lupi affamati assâlgono anche l'uomo; **2.** prk (*smarkiai pristoti*) (*kà daryti*) saltare su (*a far qcs*); **3.** (*uþklupti*) sorprêndere, cògliere (di sorpresa); ***mùs naktís uþpúolë kelyjê*** la notte ci sorprese in viâggio; **4.** (*uþkristi*) (*ant ko*) cadere (*sopra (a) qc*, *su qc*), piombare.

**uþpuol|¸jas,** **-a** *s* , **~íkas,** **-ë** *s* aggressore *v*, aggreditrice *m*, assalitore -trice.

**uþpuolímas** *s* aggressione *m* (*l'azione*); assalto.

**ùþpuolis** *s* aggrssione *m*; assalto.

**uþp¾sti** *v* (ùþpuèia, ùþpûtë) (*puèiant uþgesinti*) (*kà*) soffiare (*su qcs*), spégnere con un sóffio; ***u. þvåkæ*** soffiare su / spégnere una candela.

**uþpustôtas, -a** *prcp* sommerso (*dalla neve e sim*.), sepolto (*sotto la neve e sim*.).

**uþpustôti** *v* (uþpùsto, uþpùstë) seppellire (*sotto la neve*, *la sabbia*) (*del vento*), sommêrgere.

**ùþraitas** *s* **1.** (*paraðytos raidës*) svolazzo; grâzia; **2.** archit voluta, rìccio, rìcciolo.

**uþraitôti, uþra¤to, uþra¤të** *vt* piegare su di sé.

**uþraitó|ti, ~ja, ~jo** *vx* rimboccare.

**uþrakín|ti** *v* , ~na, ~o) chiùdere a chiave; serrare; ***u. sâvo ka§bará*** chiùdere a chiave la pròpria stanza; ***u. k± ñ se¤fà*** chiùdere qcs in cassaforte.

**ùþrakt|as** *s* **1.** serratura; ***põ ~u*** sottochiave, sotto chiave; ***centrínis ù.*** chiusura centrali@@ata; ***iðlãuþti ~à*** forzare / scassinare una serratura; **2.** fot otturatore *v*; **3.** (*ðaunamojo ginklo*) sicura (*di un'arma*).

**ùþrað|as** *s* (1, 3^b^) **1.** scritta; (*paminklo*) iscrizione *m*; (*paaiðkinimas po paveikslu*, *ekrane*) didascalia; ***vieðâsis ù.*** insegna; **2.:** *pl* ***~ai*** appunti *v*; note, annotazioni *v*; ***paskaitÿ ~ai*** appunti delle lezioni; ***~ø knygìlë / knygùtë*** agendina, agenda; taccuino.

**uþråðymas** *s* (*testamentu*) teis lâscito; legato.

**uþraðin¸|ti, ~ja, ~jo** *vx* prêndere appunti / note; (*kà*) annotare (*più cose*), appuntare.

**uþraðôti** *v* (uþråðo, uþråðë) **1.** scrìvere (*sopra*); annotare, appuntare; méttere per iscritto; ***u. ådresà a¹t vóko*** scrìvere l'indirizzo su una busta; **2.** (*átraukti á sàraðà*) segnare (*in una lista*), inserire; (*áraðyti*) iscrìvere; ***u. k± pâs gôdytojà*** fissare a qcn un appuntamento dal mêdico; ***u.*** ***skolõn*** segnare a débito, addebitare; **3.** (*kam turtà*) intestare (*a qcn un bene*); (*testamentu*) lasciare in ereditâ; legare *teis*; **4.** (*aparatu*) registrare.

**uþrãug|ti, ~ia, ~ë** *vx* **1.** far fermentare; (*apie teðlà*) far lievitare; **2.** (*pvz*., *darþoves*) méttere sotto sale; **3.** (*odà*) tannare.

**uþra÷k|ti** *v* , ~ia, ùþraukë) **1.** raccomodare (*facendo una piega*); **2.** ðnek (*pabaigti*) (*kà*) méttere / porre fine (*a qcs*); ***u. gi¹èà*** troncare una discussione.

**uþregistr|úoti, ~úoja, ~åvo** *vx* registrare; (*padaryti uþraðus*) (*kà*) annotare (*qcs*), prêndere nota (*di qcs*); ***u. gãutas prekês*** registrare la merce in arrivo; ***~úota apiì pùsðimtá elêktros gedímø*** si sono regi­strati una cinquantina di guasti elêttrici.

**uþrºk|ti** *v* , ~ia, ùþrëkë) **1.** (*ant ko*) rabbuffare (*qcn*); **2.** (*kà*) coprire (*con la voce e sim*.) (*qc*); gridare più forte (*di qcn*).

**ùþrib|is** *s* **1.** sport out *v nkt*; (*futbole*) fallo laterale; (*uþ vartø*) fallo di fondo; ***nukre¤pti kåmuolá ñ ~á*** deviare fuori il pallone; ***ù.!*** fuori!; **2.** (*vieta uþ ribos*) oltreconfine *v nkt*.

**uþriì|sti** *v* , ~èia, ùþrietë) piegare, incurvare; ¨ ***u. nósá*** (*imti didþiuotis*) montarsi la testa; (*bûti nepatenkintam*) arricciare il naso.

**uþrík|ti** *v* (uþri¹ka, ~o) (*ant ko*) cacciare un grido (*a qc*).

**uþrí|sti** *v* (ùþrita, ~to) far rotolare.

**uþríð|ti** *v* (ùþriða, ~o) **1.** allacciare, legare; annodare (*con uno spago e sim*.); **2.:** ***u.*** ***kãm akís*** bendare gli occhi a qcn, bendare qcn; ***u. kãm bùrnà*** tappare la bocca a qcn, imbavagliare qcn; **3.** ðnek (*su kuo*) farla finita (*con qcs*), chiùdere; ***uþriða÷ sù alkoholiù*** ho chiuso con l'alcol.

**uþrítin|ti, ~a, ~o** *vx* rotolare (su sé stesso).

**uþrûkôti** *v* (uþr¾ko, uþr¾kë) accêndersi una sigaretta (/ un sìgaro *e sim*.); (*uþsitraukti*) dare una tirata.

**uþr¿stin|ti, ~a, ~o** *vx* far adirare, muòvere all'ira.

**uþsagstôti** *v* (uþsågsto, uþsågstë) abbottonare; ***u. sagâs*** allacciare i bottoni.

**ùþsakai** *s pl* teis pubblicazioni *m* di matrimònio.

**uþsåkym|as** *s* (*prekiø*) órdine *v*; ordinazione *m*, ordinativo; commissione *m*; (*gaminiø*) commessa; (*bilietø*, *stalo ir pan*.) prenotazione *m*; ***pagaµ ~à*** su ordinazione; (*ypaè drabuþio*) su misura; ***~o*** ***blãnkas*** mòdulo d'ordinazione; ***atlíkti ~à*** evâdere / sbrigare un órdine; ***padúoti ~à*** fare un'ordinazione; ***prii§ti ~à*** accettare una prenotazione.

**uþsakôtas,** **-a** *prcp* ordinato; prenotato.

**uþsakôti** *v* (uþsåko, uþsåkë) ordinare; (*bilietà*, *stalà ir pan*.) prenotare; ***u. pícà ñ namùs*** ordinare una pizza da asporto; ***u.*** ***telefonù*** ***viìtà viìðbutyje*** prenotare per teléfono un posto in albergo.

**uþsakôtinis, -ë** *agg* su ordinazione; su misura.

**uþsakõvas,** **-ë** *s* cliente *v/m* (*che effettua un'ordinazione*); (*darbø ir pan*.) committente *v/m*.

**uþsêgti** *v* (ùþsega, ùþsegë) assicurare (*con un fermaglio e sim*.), fissare, attaccare; (*sagomis*) abbottonare; (*smeigtuku*) appuntare; (*kà uþtrauktuku*) chiùdere (*qcs*) con una (cerniera) lampo, chiùdere la lampo (*di qcs*); (*dirþà*, *vëriná)* allacciare.

**uþsémti, ùþsemia, uþs¸më** *vt* sommêrgere.

**uþs¸|sti** *v* , ~da, ~do) **1.** montare (in sella); ***u. a¹t ãrklio (a¹t dvíraèio)*** montare a cavallo (in bicicletta); **2.** ðnek (*pristoti*) (*ant ko*) stare addosso (*a qcn*); **3.** (*vietà*) occupare (un posto).

**uþs¸|ti, ~ja, ~jo** *vx* (*kà kuo*) seminare (*qcs* *a qcs*).

**uþsiaugín|ti, ~a, ~o** *vx* (*barzdà*, *plaukus*) farsi créscere; (*gyvulius*, *vaikus*) allevare (*per sé*); (*augalus*) coltivare (*per sé*), coltivarsi.

**uþsiba¤g|ti** *v* , ~ia, uþsíbaigë) finire, terminare.

**uþsibarikad|úoti, ~úoja, ~åvo** *vx* barricarsi (*dentro*), asserragliarsi.

**uþsibr¸þ|ti, ~ia, ~ë** *vx* **1.** tracciare (*per sé*), tracciarsi; prk ***u. tíkslà*** prefìggersi uno scopo; **2.:** ***u. degtùkà*** accêndersi un fiammìfero.

**uþsi|b¿ti** *v* , ~b¾va/~b¾na, ~bùvo) trattenersi (*più del previsto*, *troppo*), rimanere a lungo.

**uþsièiãup|ti, ~ia, ~ë** *vx* chiùdere (*la bocca*); ***~k!*** stai zitto *bdv*!, zitto!, zitto, tu!; chiudi il becco!

**uþsidårëlis,** **-ë** *s* persona chiusa / introversa / schiva; asociale *v/m*.

**uþsidårymas** *s* chiusura (*ir* prk).

**uþsi|darôti, ~dåro, ~dårë** *vr* chiùdersi (*ir* prk).

**uþsidìgëlis, -ë** *s* infervorato -a, esaltato -a.

**uþsidegímas** *s* entusiasmo, fervore *v.*

**uþsidêgti** *v* (uþsídega, uþsídegë) **1.** prêndere fuoco; infiammarsi, incendiarsi; ***mokyklâ uþsídegë*** la scuola ha preso fuoco; **2.** (*pvz*., *sau degtukà ar apie degtukà*) accêndersi; ***u. cigarêtæ*** accêndersi una sigaretta; ***uþsídegë þaliâ ðviesâ*** (*ðviesoforo*) ê venuto il verde; **3.** prk infervorarsi, entusiasmarsi; accalorarsi; ***u. aistrâ (pykèiù)*** infiammarsi di passione (di còllera); **4.** prk (*uþsigeisti*) (*kà daryti*) venire una gran vòglia (*di far qcs*); morire dalla vòglia; ***uþsídegiau vaþiúoti ñ Tibêtà*** mi ê venuta una gran vòglia di andare in Tibet.

**uþside¹g|ti** *v* , ~ia, uþsídengë) coprirsi; (*nuo ko*) protêggersi (*da qc*); ***u. véidà ra¹komis*** coprirsi il volto con le mani.

**uþsid¸|ti** *v* (uþsídeda, ~jo) **1.** (*kà sau*) méttersi (*qcs*); ***u. åkinius (kãukæ, ðãlmà)*** méttersi gli occhiali (una mâschera, il casco); **2.** (*apie ledà*, *ðaðà*) formarsi sopra; ricoprire; ***ðåðas a¹t þaizdõs ~jo*** si ê formata la crosta sulla ferita.

**uþsidírb|ti, ~a, ~o** *vx* guadagnare, guadagnarsi; ***u. dúonà*** guadagnarsi il pane.

**uþsiºmæs, -usi** *agg*, *prcp* occupato, impegnato; ***atléisk, ðia¹dien âð laba¤ u.*** scùsami, oggi sono molto impegnato.

**uþsiëmímas** *s* occupazione *m*; attivitâ; (*pamoka*, *paskaita*) lezione *m*.

**uþsieniìtis, -ë** *s* straniero -a; forestiero -a.

**uþsieniìtiðkas, -a** *agg* , **uþsienínis,** **-ë** *agg* straniero, êstero; forestiero.

**ùþsien|is** *s* êstero; ***~yje*** all'êstero; ***~io kalbâ*** lìngua straniera;***~io prekôba*** commêrcio êstero; ***Ùþsienio reikalÿ ministêrija*** Ministero degli affari êsteri; ***iðvýkti ñ ~á*** andare all'êstero.

**uþsigalvójæs, -usi** *prcp* soprappensiero *nkt*, sovrappensiero *nkt*.

**uþsigalvó|ti, ~ja, ~jo** *vx* pêrdersi nei pensieri; êssere soprappensiero *nkt*.

**uþsigardþ|iúoti, ~iúoja, ~iåvo** *vr* finire il pasto con una golositâ.

**uþsigãu|ti** *v* , ~na, uþsigåvo) **1.** (*kà*) farsi male (*a qcs*); **2.** (*ásiþeisti*) offêndersi, avêrsela a male; ***jís ba¤siai uþsigåvo*** si ê offeso a morte.

**uþsige¤|sti** *v* , ~dþia, uþsígeidë) (*kà daryti*) venire una gran vòglia (*di far qcs*); morire dalla vòglia; prêndere la smânia; ***daþna¤ uþsigeidþiù vãlgyti ðokolådo*** spesso mi viene una gran vòglia di mangiare cioccolata, spesso mi prende la smânia di mangiare cioccolata.

**uþsi|gérti** *v* (uþsígeria, ~g¸rë) (*kà kuo*) bersi (*qcs insieme a/con qcs*); (*kuo*) pasteggiare (*con qcs*).

**uþsi|gínti, uþsígina, ~gônë** *vr* negare; smentire.

**uþsigr¿din|ti, ~a, ~o** *vx* temprarsi.

**uþsigla÷|sti** *v* , ~dþia, uþsíglaudë) (*uþ ko*) stare appostato (*dietro qcs*); appostarsi.

**uþsigul¸|ti** *v* (uþsíguli, ~jo) **1.** stare sdraiato (*solit*. *troppo a lungo*); (*lovoje*) stare a letto; **2.** (*bûti nenaudojamam*) giacere (inevaso); rimanere a lungo (fermo); ***susiraðin¸jimo býlos uþsíguli m¸nesiais*** le prâtiche della corrispondenza giâcciono inevase da mesi.

**uþsiimin¸|ti, ~ja, ~jo** *vx* (*kuo*) occuparsi (*di qcs*); (*tam tikru verslu*) êssere attivo (*in un campo*, *in un settore*), operare; Þ **ve»stis 1.**; ***u. nåftos verslù*** operare nel settore petrolìfero.

**uþsii§ti** *v* (uþsíima, uþsíëmë) **1.** (*kuo*) occuparsi (*di qcs*); (*tam tikru verslu*) êssere attivo (*in un campo*, *in un settore*), operare; ***kuõ tù daba» uþsíimi?*** di che cosa ti occupi attualmente?; ***u. tarptautiniù kroviniÿ gabìnimu*** occuparsi di trasporti internazionali; ***u. kontrabãnda*** esercitare il contrabbando; **2.** (*prasidëti*) strìngere rapporti (*con qcn*); avere a che fare (*con qc*); **3.** (*sau vietà*) occuparsi (*un posto e sim*.); **4.:** ***mãlkos ja÷ uþsíëmë*** la legna ha giâ preso.

**uþsikabín|ti, ~a, ~o** *vx* **1.** (*uþ ko*) agganciarsi (*a qcs*), restare impigliato; incocciare (*contro qc*); ¨ ***neturiù ùþ kõ u.*** non so che pesci pigliare; **2.** (*kà ant peèiø ir pan*.) méttersi (*qcs addosso e sim*.); agganciarsi, appêndersi; ***u. a¹t petiìs ðãutuvà*** méttersi il fucile a tracolla.

**uþsikalb¸|ti** *v* (uþsíkalba, ~jo) trattenersi a parlare.

**uþsi|kãrti** *v* (uþsíkaria, ~kórë) ðnek accollarsi.

**uþsikêpti** *v* (uþsíkepa, uþsíkepë) ðnek (*kà daryti*) incaponirsi (*a/di far qcs*).

**uþsikimðímas** *s* **1.** ostruzione *m*; (*kamðatis*) ressa; **2.** med trombosi *m nkt*.

**uþsiki§ð|ti** *v* (uþsíkemða, ~o) **1.** ostruirsi; intasarsi; ***~o va§zdis*** si ê intasato un tubo; **2.** (*kà sau*) tapparsi (*qcs*); ***u. ausís (nósá)*** tapparsi le orécchie (il naso).

**uþsiki»|sti** *v* (uþsíkerta, ~to) **1.** (*apie mechanizmà*, *ginklà*) incepparsi, bloccarsi; **2.** prk (*kalbant*) impappinarsi, imbrogliarsi.

**uþsikirtímas** *s* inceppamento.

**uþsikíð|ti** *v* (uþsíkiða, ~o) ficcarsi (dentro), infilarsi; ***u. ausís*** méttersi i tappi nelle orécchie.

**uþsikl|ausôti** *v* , ~a÷so, ~a÷së) trattenersi ad ascoltare; pêrdersi ad ascoltare.

**uþsikló|ti, ~ja, ~jo** *vx* (*kuo*) coprirsi (*con qcs*); ***ðilta¤ u.*** rimboccarsi ben bene le coperte.

**uþsikniãub|ti, ~ia, ~ë** *vx* **1.** chinare il volto (*tra le mani e sim*.); **2.** (*ant ko*) curvarsi (*su qcs*), chinarsi.

**uþsikós|ëti** *v* , ~i, ~ëjo) avere un attacco di tosse, cominciare a tossire violentemente.

**uþsikrãu|ti** *v* , ~na, uþsikróvë) **1.** (*kà*) prêndere su di sé (*qcs*); caricarsi (*di qcs*); **2.** prk (*kà*) sobbarcarsi (*(a)* *qc*), accollarsi (*qc*), addossarsi.

**uþsikrºsti** *v* (uþsíkreèia, uþsíkrëtë) **1.** (*kuo*) prêndersi (*una malattia*), contrarre; ***u. gripù nuõ kõ*** prêndere l'influenza da qcn; **2.** prk lasciarsi / farsi contagiare, êssere contagiato; ***u. draugÿ entuziazmù*** farsi contagiare dall'entusiasmo degli amici.

**uþsilãuk|ti, ~ia, ~ë** *vx* aspettare in ânsia.

**uþsiléi|sti, ~dþia, ~do** *vr* darsi per vinto.

**uþsile¹k|ti** *v* , ~ia, uþsílenkë) **1.** piegarsi; **2.** ðnek (*mirti*) restarci (*morire*).

**uþsiliepsnó|ti, ~ja, ~jo** *vx* **1.** andare in fiamme, prêndere fuoco; ***medínis nåmas gre¤tai ~jo*** la casa di legno prese fuoco velocemente; **2.** prk avvampare; ***jõs ðirdís ~jo méile*** il suo cuore avvampò d'amore.

**uþsilík|ti** *v* (uþsiliìka, ~o) rimanere, restare; ***neda÷g yrâ mergínø, kuriõs mâno mintysê uþsiliìka*** non sono molte le ragazze che mi rimângono in mente.

**uþsilíp|ti** *v* (uþsílipa, ~o) salire, salìrsene.

**uþsi|manôti** *v* , ~måno, ~månë) (*ko*; *kà daryti*) venire vòglia (*di qcs*; *di far qcs*); avere vòglia; méttersi in testa (*qcs*; *di far qcs*); pensare (bene) (*di far qcs*).

**uþsimaskåvæs, -usi** *prcp* **1.** mascherato; in mâschera; **2.** kar mimeti@@ato; camuffato.

**uþsimask|úoti, ~úoja, ~åvo** *vx* **1.** mascherarsi; camuffarsi; **2.** kar mimeti@@arsi; camuffarsi; **3.** (*dëtis kuo*) spacciarsi (*per qcn*).

**uþsima÷kðlin|ti, ~a, ~o** *vx* (*kepuræ*) calcarsi (*il berretto e sim*.) in testa.

**uþsim|ãuti** *v* , ~ãuna, ~óvë) (*kà sau*) infilarsi (*qcs*), méttersi; ***u. batùs (kélnes, pi»ðtines)*** infilarsi le scarpe (i pantaloni, i guanti).

**uþsimêgzti** *v* (uþsímezga, uþsímezgë) **1.** (*mazgà*) farsi un nodo; **2.** (*ið mezgimo*) farsi a mâglia; **3.** prk (*prasidëti*) nâscere; cominciare; **4.** (*apie augalus*) far frutti, fruttificare.

**uþsimérk|ti, ~ia, ~ë** *vx* chiùdere gli occhi.

**uþsimêsti** *v* (uþsímeta, uþsímetë) **1.** buttarsi addosso; indossare in fretta; **2.** (*dingti*) (andare a) cacciarsi / ficcarsi; **3.** tarm (*kas kà daryti*) prêndere la smânia (*a qcn di far qcs*); **4.** ðnek impasticcarsi.

**uþsimìtæs, -usi** *prcp* ðnek impasticcato -a.

**uþsimiegójæs, -usi** *agg*, *prcp* intontito dal sonno.

**uþsiminímas** *s* accenno; allusione *m*.

**uþsimi¹ti** *v* (uþsímena, uþsíminë) (*apie kà*) accennare (*a qc*); (*duoti uþuominà*) allùdere; (*paminëti*) menzionare (*qcs*), far menzione (*di qcs*); ***nê ka»tà uþsíminë apiì saviþudôbæ*** non una volta aveva accennato al suicìdio.

**uþsimirðímas** *s* oblio (-lii).

**uþsimi»ð|ti** *v* , ~ta, ~o) **1.** dimenticare, dimenticarsi; (*nebegalvoti*) non pensarci più; ***gìria, kâd ~tø*** beve per dimenticare; **'** **2.** ðnek (*bûti uþmirðtam*) andare dimenticato; ***tóks fílmas neuþsimi»ðta*** un film così non si dimêntica.

**uþsimojímas** *s* **1.** slâncio; **2.** prk intento; mira.

**uþsimok¸|ti** *v* (uþsimóka, ~jo) pagare (*per sé*, *la propria parte*), pagarsi.

**uþsimó|ti, ~ja, ~jo** *vx* **1.** levare in alto (*un braccio*, *un'arma*) (*per colpire*); ***u. kalavijù*** brandire una spada; **2.** prk (*kà daryti*) avere intenzione (*di far qcs*), intêndere; (*pasiryþti*) decìdersi (*a far qcs*); ***plaèia¤ u.*** avere grandi mire; ***pe» plaèia¤ u.*** puntare troppo in alto.

**uþsimùðti** *v* (uþsímuða, uþsímuðë) ammazzarsi, trovare la morte.

**uþsinor¸|ti** *v* (uþsinóri, ~jo) (*kà daryti*) venire vòglia (*di far qcs*); avere vòglia (*di far qcs*); ***~jau gérti*** mi ê venuta vòglia di bere.

**uþsioþ|iúoti, ~iúoja, ~iåvo** *vx* impuntarsi *prk*, puntare i piedi.

**uþsipliì|ksti** *v* , ~skia, uþsíplieskë) **1.** prêndere fuoco; divampare; **2.** prk infiammarsi; **3.** prk (*parausti*) avvampare; (*ypaè ið gëdos*) diventare rosso, arrossire.

**uþsiprenumer|úoti, ~úoja, ~åvo** *vx* (*kà*) abbonarsi (*a qcs*), fare l'abbonamento.

**uþsip|ùlti** *v* , ~úola, ~úolë) (*kà*) assalire (*qcn*); dare addosso (*a qcn*).

**uþsirakín|ti, ~a, ~o** *vx* chiùdersi a chiave.

**uþsir|angôti** *v* , ~a¹go, ~a¹gë) arrampicarsi.

**uþsir|aðôti** *v* , ~åðo, ~åðë) (*kà*) prêndere nota (*di qcs*) (*per sé*), annotarsi (*qcs*), appuntarsi; ***u. mâno telefòno nùmerá*** annòtati il mio nùmero di telé­fo­no.

**uþsirãu|ti** *v* , ~na, uþsiróvë) ðnek (*ant ko*) incappare (*in qcn*).

**uþsiriõglin|ti, ~a, ~o** *vx* (*ant ko*) trascinarsi (*su qcs*), salire arrancando.

**uþsiregistr|úoti, ~úoja, ~åvo** *vx* registrarsi; iscrìversi; ***u. dãrbo bírþoje*** iscrìversi all'uffìcio di collocamento; ***u. pâs gôdytojà*** fissare un appuntamento (*per sé*) dal dottore.

**uþsirekomend|úoti, ~úoja, ~åvo** *vx* *rðt* dare (buona) prova di sé.

**uþsiríð|ti** *v* (uþsíriða, ~o) **1.** allacciarsi; annodarsi; ***u. batùs*** allacciarsi le scarpe; ***u. kaklåraiðtá*** annodarsi la cravatta, farsi il nodo alla cravatta; **2.:** ***u.*** ***akís*** bendarsi gli occhi.

**uþsir|ôti** *v* , ~ýja, ~íjo) soffocarsi (inghiottendo).

**uþsirõpð|ti** *v* , ~èia, uþsíropðtë) (*ant ko*) inerpicarsi (*su qcs*), arrampicarsi a fatica.

**uþsir|ûkôti** *v* , ~¾ko, ~¾kë) accêndersi una sigaretta (/ un sìgaro *e sim*.).

**uþsir¿stin|ti, ~a, ~o** *vx* adirarsi, incollerirsi.

**uþsis|agstôti** *v* , ~ågsto, ~ågstë) abbottonarsi.

**uþsi|sakôti** *v* , ~såko, ~såkë) ordinare; (*bilietà*, *stalà ir pan*.) prenotare; (*leidiná*) abbonarsi (*a qcs*); ***u. ala÷s*** ordinare una birra; ***u. ka§bará viìðbutyje (staliùkà restoranê)*** prenotare una câmera in albergo (un tâvolo al ristorante); ***u. „Lietuvõs rôtà"*** abbonarsi al *Lietuvõs rôtas*.

**uþsisëd¸|ti** *v* (uþsis¸di, ~jo) **1.** stare seduto (*troppo a lungo*); trattenersi; ***u. ikí vëlôvos naktiìs*** stare alzato fino a notte fonda; **2.** ðnek (*ant ko*) prêndere di mira (*qc*).

**uþsisêgti** *v* (uþsísega, uþsísegë) **1.** assicurarsi (*qcs con un fermaglio e sim*.), attaccarsi; (*sagomis*) abbottonarsi; (*smeigtuku*) appuntarsi; (*kà uþtrauktuku*) chiùdersi (*qcs*) con una (cerniera) lampo, chiùdersi la lampo (*di qcs*); (*apykaklæ*, *dirþà*, *vëriná)* allacciarsi; **2.** (*apie apykaklæ*, *dirþà*) allacciarsi, chiùdersi; abbottonarsi.

**uþsiskl|³sti** *v* , ~e¹dþia, uþsísklendë) **1.** (*apie duris*) chiùdersi (con il catenâccio); **2.** (*likti viduje*) chiùdersi (*dentro*), rinchiùdersi; **3.** prk chiùdersi, rinchiùdersi; ***u. savyjê*** chiùdersi in sé stesso.

**uþsispôrëlis,** **-ë** *s* testardo -a, cocciuto -a.

**uþsispôræs, -usi** *agg* cocciuto, testardo; ***u. ka¤p åsilas / oþýs*** cocciuto come un mulo.

**uþsispyrímas** *s* ostinazione *m*, cocciutâggine *m*, testardâggine; (*atkaklumas*) tenâcia, caparbietâ.

**uþsisp|írti** *v* (uþsíspiria, ~ôrë) **1.** (*nesutikti*) impuntarsi, piccarsi; (prk *ir apie arklá*, *asilà*) recalcitrare; **2.** (*pasiryþti*) (*kà daryti*) ostinarsi (*a far qcs*), intestardirsi; méttersi in testa (*di far qcs*).

**uþsispoksó|ti** *v* (uþsispõkso, ~jo) ðnek (*á kà*) incantarsi (a guardare) (*davanti a qc*).

**uþsistov¸|ti** *v* (uþsistóvi, ~jo) **1.** stare in piedi troppo; (*apie daiktus*) giacere fermo; **2.** (*apie vandená*) stagnare, ristagnare.

**uþsisvajó|ti, ~ja, ~jo** *vx* pêrdersi in sogni, sognare ad occhi aperti; êssere assorto (in fantasticherie).

**uþsisveè|iúoti, ~iúoja, ~iåvo** *vx* stare òspite troppo a lungo, trattenersi (da òspite).

**uþsiðnek¸|ti** *v* (uþsíðneka, ~jo) trattenersi a parlare, pêrdersi in chiâcchiere.

**uþsitarn|ãuti, ~ãuja, ~åvo** *vx* meritarsi; conquistarsi (*col lavoro e sim*.); ***u. kienõ malónæ*** conquistarsi il favore di qcn.

**uþsit³s|ti** *v* , ~ia, uþsítæsë) protrarsi, prolungarsi; ***derôbos uþsítæsë ikí våsaros*** le trattative si protrâssero fino all'estate.

**uþsitíkrin|ti, ~a, ~o** *vx* assicurarsi, garantirsi; ***u. ðìðtàjà viìtà*** assicurarsi il sesto posto; ***u. deðiniÿjø påramà*** garantirsi il sostegno delle destre.

**uþsitrãuk|ti, ~ia, ~ë** *vx* **1.** tirarsi su; ***u. a¹tklodæ*** tirarsi su la coperta; **2.** (*pasidengti*) coprirsi; (*ledu*) gelarsi; ***þaizdâ ~ë*** si ê formata la crosta sulla ferita; **3.** (*rûkant*) aspirare; **4.** prk (*kà*) incórrere (*in qcs*); attirarsi (*qcs*), attirare su di sé; ***u. ba÷smæ (bºdà)*** incórrere in una punizione (in un guâio); ***u. g¸dà*** coprirsi di disonore; ***u. visÿ neapôkantà*** attirarsi l'òdio di tutti; ***u. kõ nemalónæ*** pêrdere il favore di qc.

**uþsitre¹k|ti** *v* , ~ia, uþsítrenkë) (*apie duris*) chiùdersi con un botto; sbâttere.

**uþsiùnd|yti, ~o, ~ë** *vx* (*ðuná ant ko*) sguinzagliare (*un cane dietro a qc*).

**uþsiù|sti** *v* (uþsiu¹ta, ~to) andare su tutte le fùrie.

**uþsi¿ti** *v* (ùþsiuva, uþsiùvo) **1.** cucire (*sopra*); ***u. lõpà*** cucire una toppa; **2.** (*uþtaisyti*) ricucire, rammendare; ***u. skýlæ*** rammendare un buco; ***u. þa¤zdà*** ricucire una ferita.

**uþsiùtin|ti, ~a, ~o** *vx* mandare su tutte le fùrie.

**uþsive»|sti** *v* , ~èia, uþsívertë) **1.** (*ant savæs*) caricarsi addosso; **2.** (*apie knygà*) chiùdersi; (*apie lapà*) voltarsi; **3.** ðnek rimanerci secco.

**uþsivérti** *v* (uþsíveria, uþsiv¸rë) chiùdersi.

**uþsivêsti** *v* (uþsíveda, uþsívedë) **1.** tirarsi dietro, portare / condurre con sé; **2.** (*apie variklá*) méttersi in moto, partire; **3.** prk ðnek carburare, ingranare.

**uþsiviµk|ti** *v* (uþsívelka, ~o) vestirsi; (*kà*) méttersi (*qcs*), infilarsi; indossare; ***u. ðva»kà*** infilarsi la giacca.

**uþsiþa¤|sti** *v* , ~dþia, uþsíþaidë) giocare troppo a lungo, trattenersi a giocare.

**uþsiþiìb|ti** *v* , ~ia, uþsíþiebë) (*pvz*., *sau lempà*) accêndersi (*qcs*).

**uþsiþiopsó|ti** *v* (uþsiþiõpso, ~jo) **1.** non vedere (per disattenzione); **2.** Þ **uþsiþiûr¸ti**.

**uþsiþiûr¸|ti** *v* (uþsiþi¾ri, ~jo) (*á kà*) incantarsi (*davanti a qcs*).

**ùþsklanda** *s* **1.** (*pieðinys*) vignetta; **2.** (*sklàstis*) chiavistello; **3.** inf (*ekrano*) salvaschermo.

**uþskle¤|sti** *v* , ~dþia, ùþskleidë) (*knygà*) chiùdere; (*puslapá*) voltare.

**uþskl|³sti** *v* , ~e¹dþia, ùþsklendë) (*duris*) serrare; méttere il chiavistello / il catenâccio (*a una porta*); sprangare (*una porta*).

**uþskrí|sti** *v* (ùþskrenda, ~do) **1.** passare in volo; (*uþ ko*) volare dietro (*qc*); **2.** av fare scalo; ***u. degalÿ*** fare scalo per rifornimento.

**uþslºpti** *v* (ùþslepia, ùþslëpë) celare, occultare.

**uþsli¹k|ti** *v* (ùþslenka, ~o) **1.** (*apie naktá ir pan.*) calare; ***~o sùtemos*** sono calate le téne­bre; **2.** (*prisipildyti þemës*) interrarsi.

**uþslopín|ti, ~a, ~o** *vx* *ir* prk soffocare.

**uþsmãug|ti, ~ia, ~ë** *vx* strangolare, strozzare.

**uþsma÷k|ti** *v* , ~ia, ùþsmaukë): ***u. skrôbëlæ a¹t akiÿ*** calare / calarsi il cappello sugli occhi.

**uþsníg|ti** *v* (uþsni¹ga, ~o) (*kà*) nevicare (*su qcs*); ***vískas ùþsnigta*** ê tutto bianco (*per la neve*); ***kìlià ~o*** la strada ê coperta di neve.

**uþsn¿|sti** *v* , ~ta, ~do) assopirsi, appisolarsi; ***~dau priì televízoriaus*** mi sono appisolato davanti al televisore.

**uþsodín|ti, ~a, ~o** *vx* **1.** far sedere; **2.** (*kà augalais*) piantare (*in un posto piante*); (*miðku*) rimboschire, imboschire.

**uþspãrd|yti, ~o, ~ë** *vt* malmenare a calci.

**ùþsparnis** *s* av ipersostentatore *v*, flap *v nkt*.

**uþspãu|sti** *v* , ~dþia, ~dë) **1.** chiùdere premendo; ***u. kãm akís*** chiùdere gli occhi a qcn (*a un morto*); **2.** (*negyvai*) schiacciare (*investendo e sim*.); **3.** (*pvz*., *cigaretæ*, *nuorûkà*) spégnere; **4.:** ***u. a¹tspaudà*** apporre un timbro.

**uþspe¤|sti** *v* , ~èia, ùþspeitë) ðnek confinare; ¨ ***u. kampê / ñ ka§pà*** strìngere all'ângolo, méttere alle corde.

**uþspri¹g|ti** *v* , ~sta, ~o) strozzarsi; (*kas kuo*) andare di traverso (*a qcn qcs*); ***uþspringa÷ kçsniu*** mi ê andato un boccone di traverso.

**ùþstal|ë** *s* posto a tâvola (*propr*. *dietro la tavola*); ***~ës dainâ*** canzone *m* conviviale.

**ùþstat|as** *s* depòsito (cauzionale); (*ið kalëjimo paleisti*) cauzione *m*; (*daiktinis*) pegno; ***ùþ ~à*** dietro / su cauzione; ***iðpi»kti ~à*** disimpegnare (*qcs*), riscattare; ***dúoti ~à, palíkti ~à*** lasciare un depòsito, dare (*qcs*) in pegno.

**uþståtymas** *s* **1.** (*teritorijos*) edificazione *m*; **2.** (*kelio ir pan.*) ostruzione *m*; **3.** (*turto*) cessione *m* in pegno.

**uþst|atôti** *v* , ~åto, ~åtë) **1.** dare in pegno, impegnare; (*nekilnojamàjá turtà*) ipotecare; ***u. påpuoðalus lombãrde*** impegnare i gioielli (al banco / al monte dei pegni); **2.** (*pastatyti*, *kad bûtø kliûtimi*) ostruire; ***neuþstatôkite ávaþiåvimo*** non ostruite l'accesso; **3.** (*pvz*., *plotà*) edificare; **4.:** ***u. la¤krodá*** puntare la svêglia.

**uþstatôtojas, -a** *s* teis debitore -trice ipotecârio -a.

**uþsteµb|ti, ~ia, ùþstelbë** *vt* soffocare (*di piante*).

**uþstó|ti, ~ja, ~jo** *vx* **1.** (*bûti kliûtimi*) ostruire; impedire (*di vedere e sim*.); coprire; ***u. kìlià*** bloccare / ostruire la strada; ***u. kãm ðviìsà*** tògliere / coprire la luce a qcn; ***u. va¤zdà*** ostruire la visuale; **2.** prk (*ginti*) (*kà*) difêndere (*qc*), prêndere le difese (*di qc*); ***mamâ visadâ tavê ~ja*** la mamma prende sempre le tue difese; **3.** (*pvz*., *apie naktá*) calare; ***staigâ ~jo tylâ*** all'improvviso calò il silênzio.

**uþstríg|ti** *v* (uþstri¹ga, ~o) bloccarsi; incepparsi; ***põpierius ~o spausdintuvê*** si ê inceppata la carta nella stampante; ***~o e¤smas dºl avårijos*** il trâffico ê bloccato a câusa di un incidente.

**ùþstûma** *s* paletto; catenâccio.

**uþst|ùmti, ùþstumia, ~¿më** *vt* **1.** spìngere (*su*); **2.** (*sklàsèiu*) incatenacciare.

**uþsùk|ti** *v* (ùþsuka, ~o) **1.** (*iðjungti*) chiùdere; ***u. dùjø èiãupà*** chiùdere il rubinetto del gas; ***u. rådijà*** spêgnere la râdio; **2.** (*sukant átvirtinti*) avvitare; **3.** (*pvz*., *laikrodá*, *þaislà*) caricare; **4.** (*uþeiti*) passare; fare un salto / una scappata; ***u. ñ kavínæ*** fare un salto al bar; ***u. pâs k± (ñ sveèiùs)*** fare una visitina a qcn.

**uþsuktùvas** *s* giravite *v*.

**uþðãldymas** *s* congelamento; (*ðaldiklyje*) surgelamento.

**uþðãldytas,** **-a** *prcp* congelato; (*tik apie maistà*) surgelato; ***u. embriònas*** un embrione congelato.

**uþðãld|yti, ~o, ~ë** *vx* congelare, ghiacciare; (*ðaldiklyje*) surgelare; prk ***u. kãinas*** congelare i prezzi.

**uþðalímas** *s* congelamento.

**uþðãlti** *v* (uþð±la, uþðålo) gelare (*in superficie*), ghiacciare; ghiacciarsi; ***ìþeras uþðålo*** il lago si ê ghiacciato.

**uþðãu|ti** *v* , ~na, uþðóvë) (*duris*) serrare; méttere il chiavistello / il catenâccio (*a una porta*); sprangare (*una porta*); (*sklàstá*) serrare.

**uþðnìkin|ti, ~a, ~o** *vx* (*kà*) méttersi a parlare (*con qcn*), rivòlgere la parola (*a qcn*); attaccare bottone / discorso (*con qcn*).

**uþðók|ti, ~a, ~o** *vx* **1.** (*ant ko*) saltare sopra / su (*qc*); balzare sopra (*(a) qc*); **2.** (*uþsukti*) fare un salto; **3.:** ***u. a¹t pasalõs*** cadere in un agguato.

**ùþðovas** *s* catenâccio.

**uþta¤** *avv* Þ **uþtât**.

**ùþtaisas** *s* kar cârica; ***branduolínis ù.*** testata nucleare.

**uþtaisôti** *v* (uþta¤so, uþta¤së) **1.** (*ginklà*) caricare; **2.** (*angà*) tappare, otturare; (*uþlopyti*) rattoppare; **3.** prk combinare (*un guaio*); ***u.*** ***kãm*** ***va¤kà*** méttere incinta qcn.

**ùþtakis** *s* insenatura; (*tik jûros*) cala.

**uþtampôti** *v* (uþta§po, uþta§pë) strapazzare.

**uþtãmsin|ti, ~a, ~o** *vx* (*uþstoti ðviesà*) (*kà*) tenere in ombra (*qcs*), fare ombra (*a qc*).

**uþtar¸jas,** **-a** *s* intercessore, interceditrice; (*globëjas*) protettore -trice.

**uþtarímas** s intercessione *m*; (*globëjimas*) protezione *m*.

**uþtarnãut|as,** **-a** *prcp* meritato; ***~a bausmº*** casti­go meritato.

**uþtarn|ãuti, ~ãuja, ~åvo** *vx* meritare, meritarsi, conquistarsi (*col lavoro e sim*.).

**uþta»ti** *v* (ùþtaria, ùþtarë) (*kà*) intercédere (*per qc*), intervenire (*in favore di qcs*); ***u. þodìlá ùþ k±, u. k± þodeliù*** méttere una buona parola per qcn.

**uþtât** *avv* **1.** perciò, per questo; **'** **2.** Þ **taèia÷**.

**uþtek¸|ti** *v* (ùþteka, ~jo) sòrgere (*dei corpi celesti*), spuntare.

**uþtºkðti** *v* (ùþteðkia, ùþtëðkë) (*kà kuo/ko*) schizzare (*sopra*) (*qc di qcs*), spruzzare.

**uþtêk|ti** *v* (uþte¹ka, uþtìko) bastare, êssere sufficiente; êsserci abbastanza; ***ðiãm dãrbui uþte¹ka pùsdienio*** per questo lavoro basta me@@a giornata; ***~s viìtos visîems*** ci sarâ abbastanza posto per tutti; ***uþtìko påso kòpijos gãuti leidímui*** ê stata sufficiente una còpia del passaporto per ottenere il permesso; ***uþte¹ka pasakôti*** basta dirlo; ***~s!, uþte¹ka!*** basta così!

**uþtektina¤** *avv* abbastanza, quanto basta; a sufficienza, sufficientemente; ***netùrime u. pinigÿ*** non abbiamo denaro a sufficienza.

**uþtémdymas** *s* oscuramento; *ir* prk offuscamento.

**uþtémd|yti, ~o, ~ë** *vx* **1.** oscurare; *ir* prk offuscare; ottenebrare; ***u. lémpà*** oscurare una lâmpada; **2.** (*dangaus kûnà*) eclissare.

**uþtemímas** *s* **1.** (*dangaus kûno*) eclissi *m nkt*; ***dalínis (vísiðkas) sãulës u.*** eclissi parziale (totale) di sole; **2.** prk offuscamento, ottenebramento.

**uþte§p|ti** *v* , ~ia, ùþtempë) **1.** téndere; **2.** (*uþneðti*) trasportare (a fatica) (*sopra*).

**uþtém|ti** *v* , ~sta, uþtìmo) **1.** oscurarsi, offuscarsi; **2.** prk offuscarsi; ottenebrarsi; ***jãm uþtìmo prõtas*** gli si offuscò / si ottenebrò la mente.

**uþte¹kamai** *avv* quanto basta, abbastanza; a sufficienza; ***turiù u. la¤ko*** ho abbastanza tempo.

**uþte¹kam|as,** **uþtenkamâ** *agg* sufficiente; bastévole; ***sijâ ~o iµgio*** una trave di lunghezza sufficiente.

**uþtêpti** *v* (ùþtepa, ùþtepë) **1.** (*dëti sluoksná*) spalmare, stêndere; ***u. k± daþa¤s*** dare una mano di vernice a qcs; ***u. svîesto a¹t dúonos*** spalmare del burro sul pane, imburrare il pane; **2.** (*skylæ*) otturare, tappare.

**uþterðímas** *s* inquinamento; (*vandens ir pan*.) contaminazione *m*.

**uþte»ð|ti** *v* , ~ia, ùþterðë) inquinare *ir prk*, contaminare; ***fabrika¤ ~ia ùpiø vãndená*** le fâbbriche inquìnano le âcque dei fiumi.

**uþterðtùmas** *s* (grado / tasso di) inquinamento; ***óro u.*** inquinamento dell'âria.

**uþt³s|ti** *v* , ~ia, ùþtæsë) tirare per le lunghe / in lungo; ***u. susirinkímà*** tirare una riunione per le lunghe.

**¾þti** *v* (¾þia, ¾þë) **1.** fare rumore; rumoreggiare; (*apie jûrà*, *vëjà*) mugghiare; muggire; (*apie medþius*, *vëjà*) frusciare, stormire; (*apie vabzdþius*) ron@are; (*apie maðinà*) rombare; ***mãn ausysê ¾þia*** mi ròn@ano le orécchie; **2.** prk (*triukðmauti*) fare baccano / chiasso; (*linksmintis*) fare baldoria / bisbòccia; **3.** (*bartis*) (*ant ko*) rimbrottare (*qcn*).

**ùþtiesalas** *s* telo; (*stalo*) copritâvola; (*lovos*) co­pri­letto, sopraccoperta.

**uþtiìs|ti** *v* , ~ia, ùþtiesë) (*kà kuo*) stêndere (*su qcs qcs*); coprire (*con qcs qcs*); ***u. stålà stãltiesë*** stêndere la tovâglia sul tâvolo.

**uþtíkrinimas** *s* assicurazione *m*, garanzia; ***taikõs u.*** garanzia di pace.

**uþtíkrin|ti, ~a, ~o** *vx* assicurare, garantire; ***u. demokråtinius rinkimùs*** assicurare elezioni democrâtiche; ***u. saugiâs dãrbo sçlygas*** garantire la sicurezza delle condizioni di lavoro.

**uþtíkrintai** *avv* con sicurezza, in modo sicuro.

**uþtíkðti** *v* (uþtýðka, uþtíðko) schizzare.

**uþtík|ti** *v* (uþti¹ka, ~o) **1.** (*kà*) imbâttersi (*in qcn*); (*rasti*) trovare (*qc*); scovare; ***u. låpës p¸dsakus*** scovare le tracce di una volpe; **2.** (*uþklupti*) sorprêndere, cògliere (di sorpresa); ***u. dù ásibróvëlius*** sorprêndere due intrusi; ***uþtika÷ vôrà sù kitâ lóvoje*** ho sorpreso mio marito a letto con un'altra.

**uþti»p|ti, ~sta, ~o** *vi* intorpidirsi; ***mãn ~o ran­kâ*** ho la mano indolenzita.

**uþtrãuk|ti, ~ia, ~ë** *vx* **1.** (*uþmauti*) infilare (*tirando*); **2.** (*uþdengti*) coprire (*tirando*); (*uþuolaidà*) tirare (*una tenda e sim*.); **3.** (*uþneðti*) trasportare (*sopra*); (*uþvilkti*) trascinare (*sopra*); **4.** (*rûkant*) aspirare (una boccata); ***u. pôpkæ*** aspirare una boccata con la pipa; ðnek ***u. dûmìlá*** dare una tirata / un tiro; **5.** (*dainà*) attaccare (a cantare); **6.:** ***u. kílpà (måzgà)*** strìngere un câppio (un nodo); **7.** prk (*kam bëdà*) cacciare nei guai (*qcn*); (*kam gëdà*) disonorare (*qc*); **8.:** *rðt* ***u. påskolà*** eméttere un prêstito; **9.** teis comportare; ***u. ba÷dþiamàjà atsakomôbæ*** comportare responsabilitâ penali.

**uþtrauktùk|as** *s* cerniera lampo; lampo *m nkt*, @ip *m nkt*; ***atsisêgti kélniø ~à*** tirarsi giù la lampo dei pantaloni.

**uþtre¹k|ti** *v* , ~ia, ùþtrenkë) sbâttere; chiùdere sbattendo; ***u. kãm panósëje durís*** sbâttere la porta in fâccia a qcn.

**uþtrínti** *v* (ùþtrina, uþtrônë) **1.** (*glaistu*) stuccare; **2.** prk mascherare; dissimulare.

**uþtró|kðti** *v* , ~kðta, ~ðko) soffocare, soffocarsi; ***u. nuõ d¿mø*** soffocare per il fumo.

**uþtrùk|ti** *v* (uþtru¹ka, ~o) **1.** trattenersi; (*sugaiðti*) pêrdere tempo, indugiare; (*kà daryti*) tardare (*a far qcs*); ***uþtruka÷ pùsvalandá universitetê*** mi sono trattenuto me@@'ora all'universitâ; ***tík neuþtrùkit!*** non ci metta troppo!; **2.** (*tæstis*) durare; (*ilgai*) protrarsi; ***pósëdis ~o trís vålandas*** la seduta ê durata tre ore; ***bylâ ~o kìtverius metùs*** il processo si ê protratto per quattro anni.

**uþtr¿k|ti** *v* , ~sta, ~o) (*apie karvæ*) non dare più latte (*di mucca*), sméttere di dare latte.

**uþtuð|úoti, ~úoja, ~åvo** *vx* **1.** ombreggiare; **2.** prk (*paslëpti*) mascherare (*i difetti e sim*.).

**ùþtvanka** *s* diga; chiusa; (*pylimas*) ârgine *v.*

**ùþtvara** *s* **1.** (*kliûtis*) sbarramento; ***mínø ù.*** campo minato; ***polícininkø ù.*** cordone *v* di polizia; (*kelyje*) posto di blocco; **2.** (*tvora*) recinzione *m*; ***spygliúota ù.*** (recinzione *m* di) filo spinato; reticolato; **3.** sport (*krepðinyje*) blocco; (*tinklinyje*) muro.

**ùþtvaras** *s* barriera; sbarra.

**uþtve¹k|ti** *v* , ~ia, ùþtvenkë) **1.** sbarrare (*solit*. *con una diga e sim*.); **2.** prk ingombrare.

**uþtveriam|âsis, -óji** *prcp* : kar ***~óji ugnís*** fuoco / tiro di sbarramento.

**uþtvërímas** *s* sbarramento.

**uþtv|érti** *v* (ùþtveria, ~¸rë) **1.** recintare, recìngere; **2.** prk (*sudaryti kliûtá*) sbarrare, bloccare; ***keliÿ polícija ~¸rë kìlià*** la polizia stradale ha sbarrato la strada.

**uþtvíndymas** *s* inondazione *m*, allagamento.

**uþtvínd|yti, ~o, ~ë** *vx* **1.** allagare, inondare; *ir* prk sommêrgere; ***li¿tis ~ë gatvês*** la piòggia torrenziale ha allagato le strade; **2.** prk (*apie þmones*) invâdere; ***pirk¸jai ~ë na÷jà prekôbos ce¹trà*** i clienti hanno invaso il nuovo supermercato.

**uþtvín|ti** *v* , ~sta, ~o) allagarsi, êssere inondato.

**'** **uþtvírtin|ti, ~a, ~o** *vx* Þ **patvírtinti**.

**uþùitas,** **uþuitâ** *agg*, *prcp* oppresso, vessato; maltrattato.

**uþúojaut|a** *s* condoglianze *dgs*; (*gailestis*) compassione *m*, pietâ; ***ve»tas ~os*** compassionévole, degno di compassione; ***nuoðirdí u.*** sentite condoglianze; ***paréikðti kãm ~à*** esprìmere le (pròprie) condoglianze a qcn; ***ródyti ~à*** mostrare pietâ.

**uþúolaid|a** *s* tenda; ***uþkabínti (uþtrãukti) ~as*** méttere / attaccare (tirare) le tende; ***lóvos u.*** cortinâggio.

**uþúolank|a** *s* deviazione *m*, giro lungo; ***darôti dídelæ ~à*** fare una lunga deviazione; prk ***sakôti bê ~ø*** dire senza giri di parole / senza me@@i têrmini / senza perìfrasi.

**uþúolankom(is)** *avv* aggirando *vksm*; (*aplink*) attorno; prk ***kalb¸ti u.*** parlare con giri di parole; menare il can per l'âia *flk*.

**uþúomarða** *s com* smemorato -a, dimenticone -a.

**uþúomazg|a** *s* **1.** lit esòrdio (-di); **2.** biol, prk embrione *v*; germe *v*; ¨ ***uþgniãuþti ~oje*** stroncare sul nâscere.

**uþuomazgínis, -ë** *agg* germinale (*ir* prk).

**uþúomin|a** *s* allusione *m*; (*uþsiminimas*) accenno; ***neãiðki u.*** un'oscura allusione; ***padarôti (suprâsti) ~à*** fare (cògliere) un'allusione; ***kalb¸ti ~omis*** parlare per allusioni.

**uþúo|sti** *v* , ~dþia, ~dë) **1.** (*kà*) sentire odore (/ puzza) (*di qc*); (*apie ðuná*) fiutare; ***u. dujâs (kìpsná)*** sentire puzza di gas (odore di arrosto); **2.** prk subodorare, fiutare.

**uþúot** *conj* (*kà padarius*) invece (*di far qcs*); al posto; ***u. pad¸jæs, tík trukda¤*** invece di aiutarmi non fai altro che disturbare; ***u. ºmæsi dãrbo, skùndþiatës*** invece di méttervi a lavorare, vi lamentate.

**uþúovëja** *s* **1.** posto / luogo al riparo (dal vento); (*laive*) sottovento *nkt*; **2.** prk riparo, rifùgio.

**ùþupis** *s* @ona oltre il / un fiume; (*mieste*) quartiere *v* al di lâ del fiume.

**uþùtëkis** *s* geogr cala; bâia.

**uþùþvakar** *avv* tre giorni fa.

**ùþvadas** *s com* (1, 3^b^) (*pavaduotojas*) sostituto -a; (*padëjëjas*) aiutante *v/m*.

**uþvad|úoti, ~úoja, ~åvo** *vx* **1.** (*kà*) sostituire (*qcn*), fare le veci (*di qcn*); supplire (*qcn*); **2.** (*atstoti*) (*kà*) prêndere il posto (*di qc*); (*pakeisti*) rimpiazzare (*qcn*).

**uþvaikô|ti, uþva¤ko, uþva¤kë** *vt* sfiancare.

**ùþvakar** *avv* l'altro ieri, ieri l'altro.

**ùþvakarykðtis, -ë** *agg* dell'altro ieri.

**uþvaldôti** *v* (uþvaµdo, uþvaµdë) (*kà*) impossessarsi (*di qcs*), impadronirsi; (*tik apie jausmus*) sopraffare (*qc*); ***u. svìtimà tu»tà*** impossessarsi di un bene altrui; ***jñ uþvaµdë bãimë*** fu sopraffatto dalla paura.

**uþvãlg|yti, ~o, ~ë** *vx* mangiare (*un po'*); mangiare un boccone, fare uno spuntino.

**ùþvalkalas** *s* fòdera; (*pagalvës*) fêdera; copricuscino; (*pûkinës antklodës*) copripiumone *v*; (*knygos*) sovraccoperta.

**ùþvalkas** *s* Þ **ùþvalkalas**.

**uþvãrst|yti, ~o, ~ë** *vx*: ***u. batùs*** méttere / infilare i lacci alle scarpe.

**uþvaþ|iúoti, ~iúoja, ~iåvo** *vx* **1.** passare (*a piedi*); fare un salto; ***u. pâs k± (ñ sveèiùs)*** andare / passare a trovare qcn; **2.** (*uþ ko*) passare (dietro), spostarsi (dietro); **3.** (*ant ko*) salire; **4.** ðnek (*suduoti*) dare un cazzotto.

**uþve¤s|ti** *v* , ~ia, ùþveisë) allevare; (*tik augalø*) coltivare; (*gerinant veislæ*) selezionare.

**uþverb|úoti, ~úoja, ~åvo** *vx* ingaggiare; reclutare.

**ùþverktas,** **uþverktâ** *agg*, *prcp* (*apie akis*) rosso di pianto (*degli occhi*), arrossato dal pianto.

**ùþverstas,** **uþverstâ** *prcp*, *agg* **1.** (*kuo*) ingombro, pieno (*di qcs*); cârico; ***stålas ù. põpieriais*** un tâvolo ingombro di carte; ***âð ù. dãrbu*** sono cârico / pieno / oberato di lavoro; **2.** (*apie knygà*) chiuso; (*apie kortà*) coperto.

**uþve»|sti** *v* , ~èia, ùþvertë) **1.** (*uþkrauti*) (*kà kuo*) riempire (*qcs di qcs*), ingombrare; ammucchiare (*su qcs qcs*); (*pvz*., *kelià*) ostruire; **2.** prk (*kà kuo*) inondare (*qc di qcs*), sommêrgere; subissare; ***u. pråðymais*** subissare di richieste; ***u. ri¹kà pigiomís prìkëmis*** inondare il mercato di merci a basso prezzo; **3.** (*knygà*) chiùdere; **4.:** ***~tæs gãlvà*** con la testa in su / all'indietro; **5.:** ¨ ***u. kójas*** tirare le cuòia.

**uþvérti** *v* (ùþveria, uþv¸rë) (*uþdaryti*) chiùdere, serrare.

**uþve»þ|ti** *v* , ~ia, ùþverþë) strìngere (forte); med ***u. kraujågyslæ*** allacciare una vena.

**uþvêsti** *v* (ùþveda, ùþvedë) **1.** (*ant ko*) portare (*su qcs*), condurre; **2.** (*uþ ko*) portare (*dietro qcs*), condurre; **3.** *rðt* (*pradëti* *kà*) dare il via (*a qcs*), far partire (*qcs*); ***u. kaµbà*** dare il via a una conversazione; ***u. knýgà*** méttere in cantiere un libro; teis ***u. býlà*** istruire un processo; **4.** (*trumpam nuvesti*) portare (a fare un salto / una vìsita); **5.:** ***u. motòrà*** far partire il motore; ***u. maðínà*** méttere in moto la mâcchina.

**uþvê|þti** *v* (ùþveþa, ùþveþë) **1.** (*pakeliui*) passare a portare; lasciare; ***~ðiu tãu lagamínà*** passerò a portarti la valìgia; ***~þk manê pâs tëvùs*** lâsciami dai miei; **2.** (*á virðø*) trasportare (*sopra/su*); **3.** ðnek (*suduoti*) dare un ceffone; **4.** ðnek (*apie narkotikus*) fare sballare.

**uþvílkin|ti, ~a, ~o** *vx* tirare in lungo; (*nukelti*) rimandare, rinviare; differire *rðt*; ***u. býlà*** tirare in lungo un processo; ***u. iðvykímo diìnà*** rimandare il giorno della partenza.

**uþviµk|ti, ùþvelka, ~o** *vt* **1.** trascinare (sopra); **2.** (*apvilkti*) rivestire; **3.** (*uþtæsti*) protrarre.

**uþvírin|ti, ~a, ~o** *vx* (far) bollire; ***u. pîenà*** bollire il latte.

**uþvi»|sti** *v* , ~sta, ~to) cadere (*sopra*); crollare, abbâttersi (al suolo).

**uþvírti** *v* (uþvérda, ùþvirë) **1.** (cominciare a) bollire; sobbollire; ¨ ***u. kõðæ*** piantare grane; combinare un casino; **2.** prk scoppiare; ***ùþvirë dãrbas*** il lavoro ê partito di gran lena; il lavoro ferve; ***ùþvirë muðtýnës*** ê scoppiata una rissa; ***u. pykèiù*** scoppiare (d'ira).

**uþvís** *avv* in assoluto; di tutti; ***u. ge­riãusia*** la cosa mi­gliore in assolu­to; il mêglio.

**uþvôti** *v* (ùþveja, uþvíjo) **1.** scacciare (*sopra*); **2.** (*siûlus*) ravvòlgere.

**'** **uþviz|úoti, ~úoja, ~åvo** *vx* Þ **vizúoti**.

**uþvóþ|ti, ~ia, ~ë** *vx* **1.** (*uþdengti*) coprire; (*dangèiu*) (*kà*) abbassare il copêrchio (*di qcs*); **2.** ðnek (*suduoti*) tirare un cazzotto.

**uþþéldin|ti, ~a, ~o** *vt* far coprire di vegeta­zio­ne; rinverdire.

**uþþélti** *v* (ùþþelia, uþþ¸lë) (*kuo*) riempirsi (*di* *erba*, *erbacce*).

**uþþe»ti** *v* (ùþþeria, ùþþërë) spârgere; (*uþpilti*) rovesciare (*sopra*); ***u. duõbæ*** colmare una fossa.

**uþþiìb|ti** *v* , ~ia, ùþþiebë) accêndere.
`;
