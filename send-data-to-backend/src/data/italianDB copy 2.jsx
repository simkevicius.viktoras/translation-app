export const italianDB = `
**f** [&0ffe] *dkt v/m nkt* êf; *ðeðtoji italø kalbos abëcë­lës raidë*; ***f minùscola*** maþóji „f"; ***F maiùscola*** didþióji „f"; ***F di / come Firenze*** F ka¤p „Firenze" (*paraidþiui sakant vardà, þodá ir pan.*).

**fa I** *prv* priìð; ***un'ora*** ***fa*** priìð vålandà; ***due mesi fa*** priìð dù m¸nesius; ***poco*** ***fa*** k± tík; ***tanto*** ***tempo** **fa*** laba¤ senia¤.

**fa II** *dkt v nkt mus* fa *inv*.

**fabbisógno** *dkt v* póreikis; ***f. di petròlio*** nåftos póreikis; ***f. giornaliero di câlcio*** kãlcio parõs nòrma; ***soddisfare il f.*** paténkinti póreikius.

**fâbbrica** *dkt m* gamyklâ, fåbrikas; ***f. têssile (di mòbili, di scarpe)*** tekstílës (baµdø, åvalynës) gamyklâ; ***mârchio di f.*** gamýklos mãrkë; ***la­vo­ra­re in f.*** dírbti fab­rikê.

**fabbricâbile** *bdv*: ***terreno f.*** statôbos (paskirtiìs þìmës) sklýpas.

**fabbricânte** *dkt v* gamíntojas; (*possessore di fabbrica*) fabrikãntas; ***f. di chiavi*** råktininkas.

**fabbricâre** *vksm* (*fâb-*) **1.** (pa)gamínti; (pa)darôti; ***f. automòbili*** gamínti automobiliùs; ***f. un tâvolo*** padarôti stålà; **2.** (*costruire*) (pa)statôti; **3.** *fig* (*inventare*) fabrikúoti.

**fabbricâto** *dkt v* statinýs; påstatas; ***f. industriale (rurale)*** pramonínis (¿kinis) påstatas.

**fabbricazióne** *dkt m* gamôba; (*l'azione*) gamí­ni­mas; dårymas; ***difetto di f.*** gamôbos bròkas; ***nùmero di f.*** gamýklinis nùmeris; ***di f. italiana*** itålø gamôbos.

**fâbbro** *dkt v* (*t.p. **f. ferrâio***) kãlvis; (*di serramenti*) råktø / spynÿ méistras.

**faccénd|a** *dkt m* re¤kalas; dalýkas; ***sbrigare una f.*** tvarkôti re¤kalà; ***sbrigare le** **~e domestiche*** e¤ti namÿ apôvokà / ruõðà; ***ê tutta un'altra f.*** ta¤ visãi kítas dalýkas; ***(una) brutta f.!*** prastí põpieriai!, prastí reikala¤!

**faccendiére** *dkt v* vertéiva *com*, kombinåtorius.

**facchìno** *dkt v* neðíkas.

**fâccia** *dkt m* **1.** véidas; ***guardare*** (*qcn*) ***in f.*** þiûr¸ti (*kam*) ñ véidà / ñ akís; ***lavarsi la f.*** nusipra÷sti véidà; ¨ ***f. a f.*** akís ñ åká, priì keturiÿ akiÿ; ***dire le cose in f.*** vískà ñ akís pasakôti; ***non guardare in f. nessuno*** sù niìkuo nesiskaitôti; ***pêrdere la f.*** uþsitrãukti g¸dà, netêkti reputåcijos; ***salvare la f.*** (ið)gélbëti reputåcijà; ***sbâttere la porta in f.*** (*a qc*) uþtre¹kti (*kam*) durís priìð nósá; ***glielo si legge in f.*** tiesióg a¹t jõ kaktõs paraðôta; *volg* ***f. da culo*** ðíknius, ðùnsnukis; *kaip jst* ***alla f.*** (*di qc*)***!*** niìko sãu (*kas*)!; **2.** (*espressione*) véidas, véido íðraiðka; minâ; ***una f. sêria*** rímtas véidas; ¨ ***avere la f. tosta*** b¿ti áþûliãm; nepasidrov¸ti; ***che f. che hai!*** ñ k± tù panaðùs!; neka¤p atródai!; **3.** *fig* (*lato*) pùsë; ¨ ***l'altra f. della medâglia*** kíta medålio pùsë; ***cancellare dalla f. della Terra*** nuðlúoti nuõ þìmës pa­vi»ðiaus; **4.** *fig* (*aspetto*) póþiûris, aspêktas; **5.** *geom* sîena.

**facciâta** *dkt m* **1.** *archit* fasådas; ***la f. nord*** ðiau­rínis fasådas; ***f. rinascimentale*** renesãnsinis fasådas; **2.** (*pagina*) pùs­lapis; **3.** *fig* íðorë (*ppr. apgaulinga*); ¨ ***di f.*** netíkras *agg*, apsimestínis.

**facéto, -a** *bdv* juokãujamas; sàmojíngas; ¨ *kaip dkt v **tra il sêrio e il f.*** pusiãu juoka¤s.

**facézia** *dkt m* sçmojis, sàmojíngas pasåkymas.

**fachìro** *dkt v* fakýras.

**fâcil|e** *bdv* **1.** le¹gvas, nesunkùs; (*non compli­cato*) nesudëtíngas; ***un esercìzio f.*** le¹gvas uþ­davinýs; ***un indirizzo f.*** le¹gvai ásímenamas ådresas; ***un libro f.*** lengva¤ ska¤toma knygâ; ***una f. vittòria*** lengvâ pérgalë; ***f. a dirsi*** le¹gva pasakôti; ***di f. esecuzione*** lengva¤ atliìkamas; le¹gva atlíkti; ***rêndere più f.*** (pa)­le¹gvinti; ***non ê così*** ***f.*** (ta¤) nê ta¤p pâpras­ta; *fig* ***f. preda*** (*di qc*) le¹gvas grõbis (*kam*); ♦ ***f. come bere un bicchier d'âcqua*** viení juoka¤, viení niìkai; **2.** (*incline* *a qcs*): ***ê f. all'ira*** lengva¤ supýksta; ***ê f. al perdono*** lengva¤ atléidþia; **3.** (*di donna*) (*t.p.* ***di ~i costumi***) la¤svo eµgesio; **4.: *ê*** ***f. che...*** panaðù, kâd... ; tikriãusiai... , greièiãusiai... ; ***ê*** ***f.*** (*far qcs*) le¹gva (*kà daryti*), bepíg(u); **5.** *kaip* *prv fam*: ***vìncere f.*** lengva¤ laim¸ti.

**facilitâ** *dkt m* **1.** lengvùmas; (*comprensibilitâ*) su­pran­tamùmas; ***f. di gestione*** le¹gvas vaµdy­mas; **2.** (*capacitâ*) gabùmas; (*sciol­tezza*) sklan­dùmas; ***con f.*** lengva¤; (*in modo sciolto*) skla¹dþiai; ***avere f.*** (*a far qc*) lengva¤ (*kà daryti*).

**facilitâre** *vksm* (*-c**ì**-*) (pa)le¹gvinti; ***f. un còmpito*** pale¹gvinti ùþduotá; ***f. la digestione*** pad¸ti vírðkinti, le¹gvinti vírðkinimà.

**facilitazióne** *dkt m* **1.** lengvatâ; (*l'azione*) pale¹­g­vini­mas; *comm* ***f. di pagamento*** atsiska¤­ty­mas lengvå­ti­nëmis sçlygomis; **2.** *fin* kredíto paslaugâ.

**facilménte** *prv* **1.** lengva¤, nesu¹kiai; (*in modo sciolto*) skla¹dþiai; ***arrabbiarsi f.*** gre¤tai supý­kti; **2.** *fam* (*probabilmente*) grei­èiãusiai.

**facilóne, -a** *dkt v/m* lengvab¾dis -ë.

**facinoróso, -a** *dkt v/m* muðeikâ *com*; riãuðininkas -ë.

**facoltâ** *dkt m nkt* **1.** (su)geb¸jimas; ***f. mentali*** prõtiniai sugeb¸jimai; *dir **f. di intendere e di volere*** pakaltinamùmas; **2.** (*potere*): ***avere la f.*** (*di far qcs*) gal¸ti (*kà daryti*), tur¸ti galimôbæ; (*riuscire*) suge­b¸ti; *dir* tur¸ti téisæ; **3.** (*universitaria*) fakultêtas; ***f. di me­di­ci­na (di leg­ge)*** me­dicínos (téisës) fakultêtas; ***prêside (consìglio) di f.*** fakultêto dekånas (tarôba).

**facoltatìv|o, -a** *bdv* fakultatyvùs; neb¿tinas, neprivålomas; ***matêria ~a*** laisva¤ pasírenka­mas dalýkas.

**facoltóso, -a** *bdv* turtíngas; (*danaroso*) pinigíngas.

**facsìmile** *dkt v nkt* faksimílë.

**factòtum** *dkt v/m nkt* **1.** pasiuntinùkas -ë; **2.** *iron* visÿ galÿ méistras -ë.

**faénza** *dkt m* fajãnsas.

**faggéto** *dkt v* bùkmedþiø giriâ, bukônas.

**fâggio** *dkt v* **1.** *bot* bùkmedis*,* bùkas; **2.:** ***di (legno di) f.*** íð bùko (medîenos).

**fagiâno** *dkt v zool* fazånas.

**fagiolìn|o** *dkt v* (*ppr. dgs* ***~i***) ðparåginë pupìlë.

**fagiòl|o** *dkt v* pupìlë; ***pasta e ~i*** makarõnai sù pupìlëmis; ¨ ***andare a f.*** puikiãusiai tíkti; ***capitare a f.*** pasiródyti (/ pasitâikyti) paèiù laikù.

**fâglia** *dkt m geol* tektòninis l¾þis.

**fagocitâre** *vksm* (*-gò-*) **1.** *biol* fagocitúoti; **2.** *fig* pasi­såvinti.

**fagòtto I** *dkt v* ryðulýs; (*fardello*) neðulýs; ¨ ***far f.*** susi­krãuti ma¹tà.

**fagòtto II** *dkt v mus* fagòtas; ***suonare il f.*** gróti fagotù.

**fâida** *dkt m* va¤das; grupuõèiø kovâ.

**faidate** [-'te]**, fai da te** *nkt* **1.** *dkt v* „pasidarôk pâts" veiklâ (/ darba¤ *dgs*); **2.** *bdv prk* neprofesionalùs, mëg¸jiðkas; ***un falegname f.*** stålius mëg¸jas.

**faìna** *dkt m zool* (namínë) kiãunë.

**falânge** *dkt m anat* (pamatínis) pirðtåkaulis, falãnga.

**falangétta** *dkt m* *anat* (vidurínis) pirðtåkaulis.

**falangìna** *dkt m* *anat* (galínis) pirðtåkaulis.

**falcât|a** *dkt m* spartí e¤sena; ***a grandi ~e*** didelia¤s þi¹gsniais.

**fâlce** *dkt m* pjãutuvas; (*falce fienaia*) daµgis; ***f. e martello*** pjãutuvas i» k¿jis; *fig* ***f. di luna*** mënùlio pjãutuvas.

**falciâre** *vksm* (*fâl-*) **1.** (nu)pjãuti (dalgiù); (*solo* *fieno*) (nu)ðienãuti; **2.** *fig* (*uccidere*) paguldôti (*nu­ðauti*); **3.** *sport* (par)griãuti (*varþovà*); ðienãuti *fam*.

**falciatrìce** *dkt m* pjaunamóji (maðinâ); (*per il fieno*) ðienåpjovë; (*tosaerba*) vejå­pjo­vë.

**fâlco** *dkt v zool* såkalas; ¨ ***òcchio di f.*** såkalo akís -iìs *femm* (*apie gerà regëjimà*).

**falcóne** *dkt v* (medþiõklinis) såkalas.

**falconiére** *dkt v* sakalini¹kas.

**fâld|a** *dkt m* **1.** klõdas; slúoksnis; *geol* ***f.*** ***ìdrica, f. acquìfera*** vandeníngas slúoksnis; ¨ ***nêvica a larghe ~e*** sni¹ga didelia¤s kçsniais; **2.:** *dgs* ***le ~e (di un monte)*** pap¸dë *sg*; **3.** (*di un abito*) skve»nas; (*di cappello*) gra¤þas; atbrailâ; **4.** (*di un tetto*) (stógo) ðla¤tas.

**falegnâme** *dkt v* dailídë *masc,* stålius.

**falegnamerìa** *dkt m* **1.** (*mestiere*) stalýstë, dailidýstë; **2.** (*bottega*) ståliaus / dailídës dirbtùvë.

**faléna** *dkt m* naktínis drugýs.

**fâlla** *dkt m* **1.** iðmuðtâ skylº (*laivo korpuse*), plyðýs; **2.** (*difetto*) brõkas.

**fallâce** *bdv* apgaulíngas.

**fâllico, -a** *bdv* fålinis.

**fallimentâre** *bdv* **1.** katastròfiðkas; **2.** *dir* bankròto; ***procedura f.*** bankròto procedûrâ.

**falliménto** *dkt v* **1.** þlugímas; (*insuccesso*) nepasisekímas, nesëkmº; fiåsko; ***il f. delle trat­ta­tive*** derôbø þlugímas; nesëkmíngos derôbos; ***la cena ê stata un f.*** vakariìnë nenusísekë; ***il film ê stato un f.*** fílmas patôrë fiåsko; **2.** *fig* (*detto di qcn*) nevýkëlis  -ë; **3.** *dir* bankròtas; kråchas; ***sull'orlo del f.*** a¹t bankròto ribõs; ***dichiarare f.*** paskélbti bankròtà; ***fare f.*** (su)­bankrutúoti.

**fallì|re** *vksm* (*-**ì**sc-*) **1.** *intr* [A, E] (su)þlùgti; patírti fiåsko; nepavýkti (*kam*); ***il piano ê ~to*** plånas þlùgo; ***abbiamo ~to*** mùms nepavýko; **2.** *intr* [E] *dir* (su)­bankrutúoti; **3.** *tr*: ***f. il colpo*** nepatãikyti; ***f. il bersâglio*** praðãuti (prõ ðålá).

**fallìto,** **-a 1.** *bdv, dlv* nepavýkæs; nesëkmíngas; þlùgæs; **2.** *bdv, dlv dir* subankrutåvæs; **3.** *dkt v/m dir* subankrutåvæs asmuõ; **4.** *dkt v/m fig* nevýkëlis -ë, netíkëlis -ë.

**fâllo I** *dkt v* **1.** klaidâ; ¨ ***cadere in f.*** suklôsti, ap­sirík­ti; ***cògliere in f.*** nutvérti (*klystant*); sugãuti kalbojê; ***méttere un piede in f.*** paslôsti; **2.** *sport* (*prieð kà*) praþangâ (*su qcn*); ***f. antisportivo (in­vo­lontârio, têcni­co, in­ten­zio­na­le)*** ne­spòr­tinë (netôèi­në, têchni­në, tôèi­në) praþangâ; ***dòp­pio f.*** dvíguba klaidâ; ***f. di mano*** þaidímas rankâ; ***f. laterale*** ùþribis; ***comméttere un f., fare f.*** prasiþe¹gti; paþe¤­sti taisyklês, b¿ti neteisiãm; **3.** (*difetto*) brõkas, defêktas.

**fâllo II** *dkt v* fålas.

**fallóso, -a** *bdv sport* **1.** (*di qcs*) nê pagaµ taisyklês; ***un primo tempo f.*** pirmamê këlinyjê da÷g praþangÿ; **2.** (*di qcn*) daþna¤ prasiþe¹giantis, prasiþengin¸­jantis.

**falò** *dkt v nkt* lãuþas; ***accéndere un f.*** kùrti lãuþà.

**falsaménte** *prv* melagíngai.

**falsâre** *vksm* iðkre¤pti; iðkraipôti; ***f. i fatti*** iðkrai­pôti faktùs.

**falsarìga** *dkt m fig* mòdelis; pavyzdýs; ¨ ***sulla f.*** (*di qc*) (*kieno*) påvyzdþiu.

**falsârio, -a** *dkt v/m* klastótojas -a.

**falsétto** *dkt v mus* falcêtas; ***in f.*** falcetù.

**falsificâre** *vksm* (*-s**ì**-*) (su)klastóti, padírbti; falsifikúoti; ***f. una firma*** padírbti påraðà.

**falsificazióne** *dkt m* **1.** (su)klastójimas, falsifi­kå­cija; **2.** (*falso*) klastõtë, falsifikåtas.

**falsitâ** *dkt m nkt* **1.** (*l'esser falso*) netikrùmas; (*l'esser sbagliato*) klaidingùmas; **2.** (*insinceritâ*) nenuoðirdùmas; (*doppiezza*) dvivei­diðkùmas; **3.** (*bugia*) mìlas; neteisôbë; ***dire f.*** sakôti ne­teisôbæ, (pri)me­lúoti.

**fâls|o,** **-a** **1.** *bdv* netíkras; (*ingannevole*) apgaulíngas; melagíngas; (*errato*) klai­díngas; neteisíngas; (*artificiale*) dirbtínis; ***f. allarme*** netíkras pavõjus; ***fiori ~i*** dirbtínës gºlës; ***notìzie ~e*** neteisíngos þínios; ***un'opinione ~a*** klai­d­ínga núomonë; *dir* ***~a testimonianza*** melagíngi paródymai *pl*; **2.** *bdv* (*contraffatto*) padírbtas, suklastótas; netíkras; falsifikúotas; ***denaro f.*** padirbtí piniga¤ *pl*; ***quadro f.*** netí­kras pavéik­slas; **3.** *bdv* (*insincero*) nenuoðirdùs, dvive¤diðkas; (*finto*) apsimestínis; netíkras; ***~i amici*** neti­krí drauga¤; ***~a modêstia*** apsimestínis kuk­lùmas; **4.** *dkt v* (*ciò che ê falso*) netiesâ, mì­las; ***il vero e il f.*** tiesâ i» mìlas; **5.** *dkt v* (*opera falsa*) klastõtë, falsifikåtas; **6.** *dkt v* *dir* (*t.p.* ***reato di f.***) suklastójimas; ***f. in bilâncio*** apgaulín­gas apskaitõs tva»kymas.

**fâma** *dkt m* ga»sas, garbº; reputåcija; ***di (chia­ra) f.*** (laba¤) garsùs; ***di f. mondiale*** pasãulinio ga»so; ***acquistarsi la fama*** (*di qc*) iðgars¸ti (*kaip kas*); ***lo conosco di f.*** esù apiì jñ gird¸jæs; ***ha la f. di êssere molto coraggioso*** te¤giama, kâd jís laba¤ dràsùs, jís þínomas ka¤p laba¤ dràsùs þmogùs.

**fâme** *dkt m* **1.** aµkis; ***ho f.*** nóriu vãlgyti, nórisi vãlgyti; âð ãlkanas; ***mi ê venuta f.*** iðãlkau; ¨ ***salârio da f.*** varganâ algâ; ***morto di f.*** vísiðkas nùlis (*apie þmogø*); ***ridurre alla f.*** nusku»dinti; ***ho una f. da lupo*** âð ãlkanas ka¤p viµkas; **2.** (*mancanza di cibo*) bådas; ***sciòpero della f.*** bådo stre¤kas; ***morire di f.*** badãuti (*ir prk*); mi»ti íð bådo; **3.** *fig* troðkímas; ***f. di ricchezze (di sapere)*** tu»tø (mókslo) troðkímas.

**famélico, -a** *bdv* ãlkanas; (*affamato*) iðbad¸jæs.

**famigerâto, -a** *bdv* liûdna¤ pagars¸jæs.

**famìglia** *dkt m* **1.** ðeimâ; (*i parenti*) gíminës *pl*; ***una f. benestante (numerosa, pòvera)*** pasítu­rinti (gausí, netu»tinga) ðeimâ; *fig* ***f. di lìngue*** kalbÿ ðeimâ; ¨ ***in f.*** ðeimojê; sù giminëmís; ***stato di f.*** ðeimõs sudëtiìs paþymâ; ***méttere su f.*** susikùrti ðe¤mà; **2.** (*casato*) giminº; ðeimâ; ***la f. reale*** karåliðkoji ðeimâ; **3.** *bot, zool* ðeimâ.

**famigliâre** *bdv* Þ **familiâre**.

**familiâr|e** **1.** *bdv* ðeimôninis; ðeimõs; (*di casa*) namÿ; ***consultòrio f.*** ðeimõs konsultåcija; ***nùcleo f.*** ðeimôna; ***problemi ~i*** ðeimôniniai r¿pesèiai; **2.** *bdv* (*ben noto*) (kaþku») matôtas; (gera¤) þíno­mas; **3.** *bdv* (*amichevole*) dra÷gi­ðkas; **4.** *dkt v/m* namíðkis -ë, ðeimõs narýs -º; ***i ~i*** artimîeji; **5.** *dkt m* (*auto*) heèbêkas.

**familiaritâ** *dkt m* **1.** (*confidenza*) artimùmas; draugiðkùmas; ***prêndersi troppa f.*** familiaria¤ eµgtis; **2.** (*pratica*) nusimånymas; ***acquisire f.*** prasilãuþti; ið­mókti; ***avere f.*** (*con qcs*) nusimanôti (*apie kà*).

**familiarizzâre** *vksm* **1.** *intr* [A] susidraugãuti. **2.** *tr* (*qc a qcs*) (pri)pråtinti (*kà prie ko*).

> **familiarizzârsi** *sngr* (*con qcs*) priprâsti (*prie ko*), áprâsti (*á kà, kà daryti*); (*far conoscenza*) susipaþínti (*su kuo*).

**famóso,** **-a** *bdv* garsùs, þymùs; iðgars¸jæs; ***quadro f.*** þymùs pavéikslas; ***scrittore f.*** garsùs raðôtojas; ***diventare*** ***f.*** iðgars¸ti; ***êssere*** ***f.*** gars¸ti, b¿ti garsiãm.

**fân** *dkt v/m nkt* gerb¸jas -a; mëg¸jas -a.

**fanâl|e** *dkt v* þibi¹tas (*automobilio ir pan.*); ***~i anteriori*** prîekiniai þibi¹tai.

**fanalìno** *dkt v*: ¨ *sport **~ino di coda*** autsãideris.

**fanâtico,** **-a** **1.** *bdv* fanåtiðkas; *fig* ***êssere f.*** (*di qcs*) b¿ti (*ko*) aistruoliù / fanåtiku; **2.** *dkt v/m* fanåtikas -ë; aistruõlis -ë.

**fanatìsmo** *dkt v* fanatízmas.

**fanciùlla** *dkt m* mergìlë.

**fanciullézza** *dkt m* vaikýstë.

**fanciùllo** *dkt v* va¤kas, berniùkas.

**'fancùlo** *jst volg* e¤k ðíkt!

**fandòn|ia** *dkt m* påsaka; ta÷ðkalas; ***non raccon­tare ~ie!*** nesêk påsakø!

**fanfâra** *dkt m* puèiamÿjø orkêstras (*ppr. þygiuojan­tis*), bãnda.

**fanfaronâta** *dkt m* pasip¾tëliðkas elgesýs; (*il lodarsi*) gyrímasis.

**fanfaróne,** **-a** *dkt v/m* pagyr¾nas -ë.

**fanghìglia** *dkt m* skôstas pu»vas; du§blas.

**fân|go** *dkt v* **1.** purva¤, pu»vas; ***sporco di f.*** pu»vinas, purva¤ apsitåðkæs (/ aptaðkôtas *ir pan.*); ***imbrattare di f.*** (ap)drºbti purva¤s; ¨ ***gettare f.*** (*su qc*), ***gettare nel f.*** (*qc*) sù purva¤s (*kà*) (su)maiðôti, (ap)­drabstôti (*kà*) purva¤s; **2.:** *dgs **~ghi (termali)*** (gôdomasis) pu»vas *sg*; ***fare i ~ghi*** gôdytis purvù.

**fangóso, -a** *bdv* pu»vinas; klampùs.

**fangoterapìa** *dkt m* gôdymas purvù.

**fannullóne,** **-a** *dkt v/m* dykin¸tojas -a.

**fanóne** *dkt v zool* bangínio ¾sas.

**fantasciénza** *dkt m* mókslinë fantåstika.

**fantasìa** *dkt m* **1.** vaizduõtë; fantåzija; ***f. fêrvida*** lakí vaizduõtë; ¨ ***di f.*** ið­galvótas *agg*; fantåstinis *agg*; ***lavorare di f.*** fantazúoti; **2.** (*sogno*) fantåzija; ***f. eròtica*** eròtinë fantåzija; **3.** (*varietâ*): ***una f. di colori*** spalvÿ þa¤smas; **4.** *mus* fantåzija; **5.** *kaip bdv nkt* mãrgas; ***cravatta f. a pois*** þirniù­kais iðmãrgintas kaklåraiðtis.

**fantasióso, -a** *bdv* neñprastas; ámantrùs; (*fantastico*) fantåstinis; (*solo di qcn*) lakiõs vaizduõtës.

**fantâsm|a** *dkt v* vaiduõklis; (*spettro*) ðm¸kla; ***un castello abitato dai ~i*** pilís, kuriojê vaidìnasi; *kaip* *bdv nkt* ***cittâ f.*** míræs miìstas, miìstas vaiduõklis.

**fantastic|âre** *vksm* (*-tâ-*) (*(su) qc*) fant­a­zúoti (*apie kà*), svajóti; ***che cosa vai ~ando?*** apiì k± svajóji?

**fantasticherì|a** *dkt m* fantåzija; svajõnës *pl*; ***assorto / perso in ~e*** uþsisvajójæs.

**fantâstico, -a** **1.** *bdv* fantåstiðkas, fan­tåstinis; **2.** *bdv fig* fantåstiðkas, nuostabùs; ¨ *kaip jst* ***f.!*** fantåstika!, nerealù!; **3.** *dkt v nkt* fantåstika.

**fânte** *dkt v* **1.** *mil* p¸stininkas; **2.** (*nelle carte*) va­lêtas, berniùkas.

**fanterìa** *dkt m mil* p¸stininkai *pl*, pëstijâ.

**fantìno** *dkt v* þokºjus, (lenktôniø) jojíkas.

**fantòccio** *dkt v* **1.** lëlº; **2.** *fig* marionêtë; *kaip bdv* ***governo f.*** marionêtinë vyriausôbë.

**fantomâtico, -a** *bdv* paslaptíngas; (*inafferrabile*) nepagãunamas.

**farabùtto** *dkt v* niìkðas; bjaurôbë *com*.

**faraglióne** *dkt v* didþiùlë uolâ j¿roje; „faraljònas".

**faraóna** *dkt m zool* (namínë) patãrðka.

**faraóne** *dkt v stor* faraònas.

**faraòni|co, -a** *bdv* **1.** *stor* faraònø; **2.** *fig* (*sfarzoso*) pra­ðmatnùs; (*grandioso*) didíngas; (*enorme*) miµþinið­kas; ***spese ~che*** miµþinið­kos íðlaidos.

**fârcia** *dkt m gastr* fãrðas; ïdaras.

**farcìre** *vksm* (*-**ì**sc-*) farðirúoti; (*di qcs, con qcs*) ádarôti (*kuo*); priki§ðti (*ko*) (*ir prk*).

**farcìto,** **-a** *bdv, dlv* farðirúotas; (*di qcs, con qcs*) ádarô­tas (*kuo*).

**fardêllo** *dkt v* **1.** ryðulýs; neðulýs; **2.** *fig* (*peso*) naðtâ.

**fâre*** *vksm* **1.** (pa)darôti; (*agire*) ve¤kti; (*eseguire*) atlíkti; ***f. acquisti*** apsipi»kti; ***f. colazione*** pusry­èiãuti; ***f. la dòccia*** mãudytis põ duðù, mãudytis duðê; ***f. un errore*** padarôti kla¤dà; ***f. un esercìzio*** atlíkti pratímà; spr²sti ùþdaviná; ***f. un esperimento*** padarôti / atlíkti ba¹dymà; ***f. ginnâstica*** darôti ma¹kðtà, mankðtíntis; ***f. i lavori di casa*** e¤ti apôvokà; ***f. la mâglia*** mêgzti; ***f. un'osserva­zio­ne*** darôti påstabà; ***f. una passeggiata*** pasivãi­kðèioti; ***f. una rapina*** apipl¸ðti; ***f. rumore*** triukðmãuti; ***f. un rutto*** atsirãugëti; ***f. uno sconto*** pritãikyti núolaidà; ***f. un sondâggio*** atlíkti åpklausà; ***f. un sor­riso*** nusiðypsóti; ***f. tre ore di fila*** atstov¸ti trís vålandas eilëjê; ***fai come ti dico*** darôk, ka¤p tãu saka÷; ***che cosa posso f. per Lei?*** kuõ galiù pasitarnãuti?; ***che fai domani sera?*** k± ve¤ksi rytój vakarê?; ***fai presto!*** pasku­b¸k!; ¨ ***f. benzina*** prisipílti benzíno; ***f. festa*** nedírbti; (*a qc*) ðilta¤ prii§ti; (*divertirsi*) pasilínks­minti; ***f.*** ***il** **letto*** klóti lóvà; ***f. i soldi, f. fortuna*** pralõbti, praturt¸ti; ***f. sport*** spo­rtúoti; ***f.*** ***la** **valìgia*** krãuti(s) lagamínà; ***f.*** ***in** **tempo*** susp¸ti; ***avere a che f.*** (*con qcn*) tur¸ti reikalÿ sù kuõ; (*con qcs*) b¿ti sù kuõ susíju­siam; ***avere da f.*** b¿ti uþsiºmusiam, tur¸ti dãrbo; ***darsi da f.*** i§tis dãrbo; sùktis; ***saperci f.*** nusimanôti; (*con qcn*) mok¸ti apsie¤ti (*su kuo*); ***f. fuori qcn*** nud¸ti k±; ***non fa niente, fa lo stesso*** nesvarbù, niìko; ***non fa che*** (*far qcs*) niìko kíta nedåro, tík... ; ***c'ê poco da f.*** kà èiâ padarôsi; ***ha fatto bene a partire*** gera¤ padårë, kâd iðvýko; ***fâccio da me*** apsie¤siu bê pagãlbos; ***fai pure*** pråðom, darôk ka¤p nóri; ***fa che non sia vero!*** kâd tík ta¤p neb¿tø!, nedúok Diìve!; *fam* ***che cosa teléfoni (pre­noti, mi hai svegliato) a f.?*** kãm / (dºl) kõ ­ska§bini (tãu rezervúoti, pa­þådinai ma­nê)? (*apie nereika­linga*); *fam* ***fate voi!*** jùms spr²sti; *flk* ***chi fa da sé fa per tre*** „kâs dírba vîenas, dåro ùþ trís" (*ne visada kitø pagalba praverèia*); **2.** (*produrre e sim.*) (pa)­ga­mínti, (pa)darôti; (*preparare*) (pa)ruõðti; (*cre­are*) (su)kùrti; ***f. del bene*** darôti gìra; ***f. un buco*** padarôti (/ iðmùðti *ir pan.*) skýlæ; ***f. il pane*** (ið)kêpti dúonà; ***f. la cena*** (pa)ruõðti vakariìnæ; ***f. i cómpiti*** (pa)­darôti namÿ dãrbus; ***f. un fìglio*** prad¸ti va¤kà; (*partorire*) pagimdôti k¾diká; ***f. un film*** (*realizzarlo*) (su)kùrti fílmà; (*recitarvi*) vaidínti fílme; ***f. luce*** (*a qcn*) (pa)ðviìsti (*kam*); (*su qcs*) ãiðkintis (*kà*); ¨ ***f.*** ***piace­re*** (*a qcn*) sute¤kti (*kam*) malonùmo; ***f. schifo*** ðlýkðèiai atródyti; b¿ti ðlykðèiãm; ***che ora fa il tuo orològio?,*** *fam* ***che ore fai?*** kîek valandÿ (ródo tâvo la¤krodis)?; ***due più due fa quattro*** dù i» dù bùs keturí, dù pliùs dù yrâ keturí; *fam* ***f. i 150*** lºkti 150 kilomêtru pe» vålandà greièiù; *fam* ***fanno venti êuro*** bùs dvídeðimt e÷rø; *fam* ***la cittâ fa 50.000 abitanti*** miestê 50.000 gyvéntojø; **3.** *dël savo bendros reikðmës atstoja daugelá vksm, ypaè ðnekamojoje kalboje*: ***f. il biglietto*** (nusi)pi»kti bílietà; ***f. tutta la Sicìlia*** apvaþiúoti vísà Sicílijà; ***f. il Natale in famìglia*** praléisti Kalëdâs sù ðeimâ; ***f. l'autostrada*** (*percorrere*) vaþiúoti autostradâ; ***f. danza*** lankôti ðókiø påmokas; ***f. un film*** (*trasme­ttere*) ródyti fílmà; ***f. l'universitâ*** studijúoti universitetê; ***f. francese*** mókytis pranc¾zø kalbõs; **4.** (*esercitare una professione e sim.*) b¿ti (*kas*); dírbti (*kuo*); ***fâccio*** ***l'ingegnere*** âð inþiniìrius, dírbu inþiniìriumi; ***che fai nella vita?*** k± veikí gyvìnime?; **5.** (*rendere*) (pa)darôti; ***f. felice qcn*** padarôti k± laimíngà; ***f. qcn presidente*** iðri¹kti k± prezidentù; ***f. di qcn un campione*** padarôti íð kõ èempiònà; **6.** (*imitare*) pam¸gdþioti; (*fingersi*) apsimêsti (*kuo*), d¸tis; ***f. lo stùpido*** kvailióti, d¸tis kvailiù; **7.:** ***fare da qc*** (*fungere*) b¿ti (ta»si) (*kas*); ***mi ha fatto da padre*** jís mãn bùvo ka¤p t¸vas; **8.** *fam* (*ritenere*) laikôti (*kuo*); ***non lo facevo così*** ***furbo*** nemania÷, kâd jís tóks gudrùs; **9.** *fam* (*dire*) (pa)sakôti; ta»ti; **10.** *vartojamas su bendratimi kauzatyvine reikðme, arba reiðkiant leidimà*: ***f. entrare*** áléisti; ***f. passare*** praléisti; ***f. uscire*** iðléisti; (*cacciar fuori*) prive»sti iðe¤ti; ***f. vedere*** paródyti; ***f. costruire un ponte*** statôdinti tíltà, liìpti tíltà pastatôti; ***fammi finire!*** léisk mãn paba¤gti!; ***f.*** ***rìdere*** prajuõkinti; ¨ ***chi te lo fa f.?*** kãm tãu tîek va»go?; kâs tavê ve»èia?; **11.** *kaip intr* [A]: ***f. per qcn*** (*esser adatto*) tíkti kãm; **12.** *kaip intr beasm* [A]: ***fa bello*** graþù, graþùs óras; ***fa bùio*** témsta; ***fa caldo*** kãrðta; ***fa freddo*** ðãlta; ***che tempo fa da te?*** kóks óras pâs tavê?; **13.** *kaip intr* [A]: ***f. a meno*** (*di qcs*) apsie¤ti (*be ko*), iðsive»sti; **14.** *kaip intr* [A]: ***come fa di cognome?*** kokiâ jõ pavardº?; ***come fa al / il plurale?*** ka¤p bùs daugískaita?; ***come fa quella canzone?*** ka¤p ska§ba tâ dienâ?; **15.** *kaip intr* [A] (*riuscire*): ***come fai*** (*a far qcs*)***?*** ka¤p tù sùgebi (*kà daryti*)?; (*come te la caverai?*) ka¤p ve»sies?; ***come fâccio*** (*per far qcs, a far qcs*)***?*** ka¤p galiù (*kà daryti*)?, ka¤p mãn...?; ¨ ***ma come si fa!*** ka¤p gí gålima!; **16.** *kaip intr* [A]: ¨ ***f. a botte*** pêðtis, susipêðti; ***f. a palle di neve*** m¸tytis sniìgo gni¿þtëmis; ***f. a testa o croce*** mêsti monêtà; **17.** *kaip dkt v* (*atteggiamento*) manierâ; **18.** *kaip dkt v*: ¨ ***il dolce far niente*** „saldùs niìko neveikímas"; *flk **tra il dire e il f. (c'ê di mezzo il mare)*** le¹gva pasakôti, bêt nê padarôti; **19.** *kaip dkt v*: ***sul far del giorno*** paryèia¤s; ***sul far della notte*** témstant; ***sul far della sera*** ñ våkarà, påvakare.

    **> fârcela** *ávdþ* **1.** (*fare in tempo*) (su)sp¸ti; ***pen­so di f. per le 8*** mana÷, kâd ikí 8 susp¸siu; **2.** (*riuscire*) sugeb¸ti; iðe¤ti (*kam*), pavýkti; ***ce la fai da sola?*** a» sugeb¸si vienâ?; **3.** (*sop­porta­re*) iðtvérti; (*sopravvivere*) iðgyvénti; ¨ ***non ce la fâccio più*** âð nebegaliù.

    **> fârla** *ávdþ* **1.:** ***f. a qcn*** apgãuti k±; ***f. franca*** iðsisùkti; ***f. grossa*** prisidírbti; ***f. da padrone*** ðeimininkãuti; ***f. pagare a qcn*** atsike»ðyti kãm; ***f. finita*** (*uccidersi*) sãu gålà padarôti; *fam* ***falla finita!*** atsi­knísk!; *fam* ***f. lunga*** tampôti gùmà; ¨ ***per f. breve*** trumpa¤ tåriant; **2.****:** *fam* (*defecare*) padarôti kåkø.

**> fârne** *ávdþ*: ¨ ***f. di cotte e di crude, f. di tutti i colori*** da÷g kõðës privírti.

**> fârsela** *ávdþ*: *fam **f. addosso*** prid¸ti ñ kélnes, apsi­dírbti; (*la pip**ì*) prisisisióti; *fig* (*t.p. **f. sotto***) drìbinti kín­kas, pri­d¸ti ñ kélnes.

**> fârsene** *ávdþ* **1.:** ***che me ne fâccio?*** k± mãn sù tuõ darôti?; ***non me ne fâccio niente*** mãn íð tõ jokiõs naudõs, mãn tõ nere¤kia; **2.:** ***non se ne farâ niente*** (íð tõ) niìko neiðe¤s / nebùs; ***non se n'ê fatto niente*** niìko neiðºjo.

    **> fârsi** *sngr* **1.** (*diventare*) tâpti, pasidarôti; ***f. bello*** pasipuõðti, pasigråþinti; ***f.*** ***bùio*** darôtis tamsù, (su)témti; ***f. furbo*** ásigùdrinti; ***f. gior­no*** a÷ðti, ðvísti; ***f. prete*** tâpti kùnigu; ***f.*** ***vécchio*** (pa)sénti; ¨ ***f. da sé*** iðkílti vîen sâvo jëgomís; ***f. in quâttro*** íð kãilio nértis; ***vai a farti frìggere!*** e¤k velnióp!; **2.** *vartojamas su bendratimi kau­zatyvine reikðme*: ***f. capire*** susiðnek¸ti; ***f. créscere i baffi*** augínti ûsùs; ***f. fare una foto*** nusifotografúoti; ***f. ingannare*** léistis apgau­na­mãm; ***f. notare*** iðsiskírti; krísti ñ åká; ***non f. vedere*** nesiródyti; (*stare nascosto*) slºptis; **3.** (*fare qcs per sé*) pasidarôti; ***f. il bagno*** mãudytis; ***f. un'operazione*** iðsioperúoti; ***f. la permanente*** pasidarôti chêminæ ðukúosenà; ***f. un panino*** pasidarôti sumuðtíná; ***f. le trecce*** susipínti kasâs; ¨ ***f. un'idea*** susidarôti núomonæ; ***f. largo*** brãutis, prasibrãuti; **4.** (*procurarsi qcs*): ***f. l'amante*** susirâsti meilùþæ; ***f. un lìvido*** ásitaisôti mëlýnæ; ***f. male*** uþsi­gãuti, susiþe¤sti; ***f. la moto*** ásitaisôti motocí­klà; ***f. un tâglio*** ásipjãuti; ***f. la villa*** nusipi»kti vílà; ***che cosa ti sei fatto al dito?*** k± pi»ðtui pasidare¤?; **5.** *dël savo bendros reikðmës atstoja kai kuriuos vksm, ypaè ðnekamojoje kalboje*: ***f. la barba*** nusiskùsti ba»zdà; ***f. una birra*** iðgérti ala÷s; ***f. un góccio*** iðle¹kti taurìlæ; ***f. una pizza*** (*mangiarla*) suvãlgyti pícà; **7.** (*spostarsi*): ***f. avanti*** prie¤ti, pae¤ti ñ prîeká; *fig* pasisi¿lyti; ***f. da parte*** pasitrãukti (*ir prk*); **8.** *fam* (*drogarsi*) badôtis; (*sniffare*) úostyti; **9.** *volg* (*scopare*) padarôti; **10.** (*scambiarsi reciprocamente qcs*): ***f. le carezze*** glamon¸tis; ***f. complimenti*** apsike¤sti komplime¹tais; **11.:** ***decìdere il da f.*** spr²sti, k± tolia÷ darôti.

**farétra** *dkt m* strëlínë.

**farétto** *dkt v* proþêktorius (*sieninis ðviestuvas*), akce¹­tinis ðviestùvas.

**farfâlla** *dkt m* **1.** drugìlis, plaðtåkë; petelíðkë; *zool* drugýs; ¨ ***cravatta a f.*** petelíðkë, varlíkë; **2.** (*tipo di pasta*) kaspinºlis (*makaronø rûðis*); **3.** *sport* plaukímas peteliðkê.

**farfallìno** *dkt v* varlôtë, petelíðkë.

**farfallóne** *dkt v fig* mergini¹kas, flirtúotojas.

**farfugliâre** *vksm* (*-fù-*) **1.** *tr* (su)vap¸ti, (su)vâpalioti; **2.** *intr* [A] (*balbettare*) môkèioti, mýkti.

**farìna** *dkt m* míltai *pl*; ***f. di frumento*** kvietíniai míltai; ***f. gialla*** kukur¾zø míltai; ***f. "00", f. dòppio zero*** aukðèiãusios r¿ðies míltai; ***f. di grano duro*** kietÿ­jø kvieèiÿ míltai; ¨ ***non ê f. del tuo sacco*** ta¤ nê (vîen) tâvo (ra¹kø) dãrbas, nê pâts  padare¤; *flk* ***la f. del diâvolo va tutta in crusca*** @ kâs võgs, nepralõbs.

**farinâce|o** *dkt v* (*ppr. dgs* ***~i***) krak­molíngas ma¤stas.

**farìnge** *dkt m anat* ryklº.

**faringìte** *dkt m med* faringítas, ryklºs uþde­gí­mas.

**farisêo** *dkt v* *stor, fig* fariziìjus.

**farinós|o, -a** *bdv* miltíngas; *fig* ***neve ~a*** birùs sniìgas.

**farmacêutic|o, -a** *bdv* **1.** (*dei farmaci*) vaistÿ, farmåcijos; ***indùstria ~a*** vaistÿ pråmonë; 2. (*a base di farmaci*) va¤stinis, farmåcinis.

**farmacìa** *dkt m* **1.** (*il negozio*) va¤stinë, vãistinë; ***f. di turno*** bùdinti vãistinë; ¨ ***f. portâtile*** vaistinºlë; **2.** (*la scienza*) farmåcija, vaistininkýstë; ***laureârsi in f.*** gãuti farmåcijos magístro lãipsná.

**farmacìsta** *dkt v/m* vãistininkas -ë, va¤stininkas -ë.

**fârmaco** *dkt v* va¤stas, vãistas, vaista¤ *pl*; ***un f. contro la tosse*** vaista¤ nuõ kósulio; ***un f. antidepressivo*** va¤stas nuõ deprêsijos.

**farneticâre** *vksm* [A] (*-né-*) klejóti; klie­d¸ti (*ir prk*), svãièioti.

**fâr|o** *dkt v* **1.** ðvyturýs; **2.** (*fanale*) þibi¹tas; (*la luce*) ðviìsâ; ***~i*** ***abbaglianti*** tólimosios ðviìs­os; ***~i*** ***anabbaglianti*** a»timosios ðviìsos; ***~i antinébbia*** r¾ko þibi¹tai; ***accéndere (spégne­re) i ~i*** ájùngti (iðjùngti) ðviesâs; **3.** (*proiettore*) proþêktorius; **4.** *fig* kélrodis.

**fârro** *dkt v bot* spêlta; (*i chicchi*) spêltos *pl*; ***f. pìccolo*** vienagr¾dis kvietýs.

**fârsa** *dkt m* fãrsas (*ir prk*).

**fâ|scia** *dkt m* **1.** júosta; (*t.p.* ***fascetta***) juostìlë; **2.:** *dgs* ***~sce*** (*per neonati*) vystykla¤; ***avvòlgere in f.*** (su)vô­styti; **3.** (*benda*) tvãrstis, bíntas; ***f. elâstica*** elåstinis ïtvaras; bandåþas; **3.** (*zona*) júosta; zonâ; (*striscia*) rúoþas; ***f. costiera*** paj¿rio júosta; *sport* ***f. (laterale)*** kråðtas (*futbolo aikðtelës*); **4.** *fig* (*gruppo*) grùpë; (*strato sociale*) slúoks­nis; ***f. di rêddito*** pajamÿ grùpë; ***f. orâria*** vålandos *pl* (*tam tikros paskirties, pvz. laidoms vaikams, prekëms at­­veþti ir t. t.*).

**fasciâre** *vksm* **1.** (ap)tvãrstyti, sutvãrstyti; apríðti tvãrsèiu; (su)bintúoti; ***f. il capo*** aptvãrsty­ti gãlvà; **2.: *f. un neonato*** (su)vôstyti k¾diká; **3.** *fig* (*circondare*) apjúosti.

**fasciatùra** *dkt m* **1.** tvãrstis, tvãrsèiai; ***tògliere la f.*** nui§ti tvãrsèius; **2.** (*il fa­sciare*) (ap)tvãrsty­mas.

**fascìcolo** *dkt v* **1.** sçsiuvinis; (*brochure*) broðiûrâ; **2.** (*di documenti*) bylâ; (*dossier*) dosjº; *dir* ***apri­re un f. contro qc*** iðkélti býlà priìð k±.

**fascìna** *dkt m* þabinýs, faðinâ.

**fâscino** *dkt v* þavesýs; (*l'essere affascinante*) þavùmas; (*attrattiva*) patrauklùmas; ***di grande f.*** þavíngas *agg*; ***esercitare f.*** (*su qc*) (su)þav¸ti (*kà*).

**fâscio** *dkt v* **1.** ryðulýs; plúoðtas; ¨ ***fare di ogni erba un f.*** vískà sumaiðôti (*„suriðti á vienà ryðulá skirtingas þoles"*), mêsti vískà ñ vîenà kåtilà; **2.** *anat, fis* plúo­ðtas, pluoðtìlis; *anat* ***f. muscolare*** rau­me¹s plúo­ðtas; *fis* ***f. di elettroni*** elektrònø plúoðtas; **3.:** *geom* ***f. di rette*** tiesiÿ plúoðtas; **4.** *stor* (*t.p.* ***f. littòrio***) fåscija.

**fascìsmo** *dkt v* faðízmas (*ir prk*).

**fascìsta** **1.** *bdv* faðístinis (*ir prk*); (*del fascismo*) faðízmo; (*dei fascisti*) faðístø; **2.** *dkt v/m* faðístas -ë (*ir prk*).

**fâse** *dkt m* **1.** fåzë; (*stadio*) stådija; (*periodo*) lai­kó­tarpis; ***f. iniziale (finale, transitòria)*** pradínë (baigiamóji, péreinamoji) stådija; ***la f. acuta di una malattia*** ûmí ligõs fåzë; *chim* ***f. gassosa*** garÿ fåzë; *econ* ***f. di ristagno*** stagnå­cijos lai­kótarpis; *tecn* ***f. di aspirazione*** ásiurbí­mo tåktas / fåzë; ¨ ***in f.*** (*di qc*) *rodo, kad kas nors vyksta*; ***il progetto ê in f. di realizzazione*** projêktas ágyvéndinamas; ***êssere fuori f.*** b¿ti bê fåzës *fam*; **2.:** *astr* ***f. lunare*** mënùlio atmainâ / fåzë; **3.** *spec* (*in elettrotecnica*) fåzë.

**fastêllo** *dkt v* ryðulýs; (*di paglia e sim.*) p¸das; (*di legna minuta*) kûlìlis.

**fast food** [fast&fud] *dkt v nkt* gre¤to ma¤sto re­storå­nas.

**fastì|dio** *dkt v* **1.** nejaukùmas; nema­lonùmas; ***dare*** ***f.*** (*disturbare*) trukdô­ti; (*essere d'ostacolo*) kli¿ti; (*non pia­cere*) nepatíkti; (*molestare*) érzinti, ákyr¸ti; ***mi dâ f.*** ***il fumo*** manê e»zina, ka¤ r¾ko; **2.** (*incomodo*) nemalonùmas; nepatogùmas; r¾pestis; ***avere dei ~di*** tur¸ti nemalonùmø; ***creare dei ~di*** (su)kélti r¾pesèiø / nepatogùmø; **3.** (*malessere*) disko­mfòrtas.

**fastidióso,** **-a** *bdv* nemalonùs; (*scomodo*) nepa­togùs; (*molesto*) ákyrùs, érzinantis; (*tedioso*) nuobodùs.

**fâsto** *dkt v* prabangâ; (*fastositâ*) praðmatnùmas.

**fastóso, -a** *bdv* praðmatnùs, prabangùs.

**fasùllo, -a** *bdv* netíkras; (*contraffatto*) padírbtas.

**fâta** *dkt m* f¸ja (*ir prk*).

**fatâle** *bdv* **1.** (*cruciale*) lemtíngas; fatåliðkas; **2.** (*voluto dal fato*) (likímo) lémtas, fatalùs; (*inevitabile*) neiðvéngiamas; ***era f.*** bùvo lémta; **3.** (*funesto*) nelémtas; (*disastroso*) praþûtíngas; (*mortale*) mi»ti­nas; **4.** (*irresistibi­le*) kìrintis; ***donna f.*** lemtínga móteris.

**fatalìsmo** *dkt v* fatalízmas.

**fatalìsta** *dkt v/m* fatalístas -ë.

**fatalitâ** *dkt m nkt* **1.** (*l'essere fatale*) fataliðkùmas; neiðvengiamùmas; **2.** (*evento*) lemtíngas ñvy­kis; ***ê stata una trâgica f.*** ta¤ bùvo nulemtâ nesëkmº; **3.** (*destino*) likímas, lemtís -iìs.

**fatalménte** *prv* **1.** (*inevitabilmente*) neiðvéngiamai, fa­tåliðkai; **2.** (*disgraziatamente*) nelemta¤; (*purtroppo*) nelãimei; dejâ.

**fatalóna** *dkt m* lemtínga móteris -rs.

**fatâto, -a** *bdv* (*incantato*) uþbùrtas; (*magico*) mågiðkas; (*da favola*) påsakiðkas.

**fatìca** *dkt m* **1.** (*stanchezza*) núovargis; **2.** (*sforzo*) va»gas; (*attivitâ*) tri¾sas, júodas dârbas; ***senza troppa f.*** bê didêsnio va»go; ***costare f.*** reika­lãu­ti va»go; ***fare f.*** (*a far qcs*) su¹kiai (*kà dary­ti*); nelaba¤ iðe¤ti (*kam kà daryti*), b¿ti sunkù; ***fâccio f. a capire*** nelaba¤ suprantù, mãn sun­kù suprâsti; ¨ ***a f.*** võs nê võs; ***da f.*** darbínis *agg* (*apie gyvulá*); ***uomo di f.*** juodada»bis; ***f. d'Êrcole*** þýgis.

**faticâre** *vksm* [A] (*-t**ì**-*) **1.** tri¾sti; (*sgobbare*) plùðti, pluð¸ti; **2.** (*avere difficoltâ*) (*a far qcs*) b¿ti sunkù (*kam kà daryti*); ***fatico a prêndere sonno*** mãn sunkù / nele¹gva uþmígti, su¹­kiai uþmingù.

**faticâta** *dkt m* tri¾sas; pråkaito liejímas.

**faticóso,** **-a** *bdv* **1.** (*duro*) vãrginantis; vargíngas; sun­kùs; **2.** (*difficile*) sunkùs.

**fatìdico, -a** *bdv* lemtíngas.

**fatiscénte** *bdv* gri¾vantis; avårinës b¿klës.

**fâto** *dkt v* fåtumas; (*destino*) lemtís -iìs *femm*.

**fâtta** *dkt m*: ***di questa f., di tal f.*** tóks *agg*; tókio plãuko.

**fattâc|cio** *dkt v* **1.** baisùs ïvykis; (*crimine*) nusikaltímas; píktas dãrbas; **2.** *dgs fam*: ***~ci*** reikala¤.

**fattézze** *dkt m dgs* véido brúoþai.

**fattìbile** *dkt v* ámånomas, realùs; ágyvéndinamas.

**fattispécie** *dkt m nkt dir*: ***nella f.*** ðiuõ konkreèiù åtveju.

**fâtt|o I** *dkt v* **1.** fåktas; (*avvenimento*) ïvykis; (*fe­nome­no*) reiðkinýs; ***f. di sângue*** kra÷jo pra­liejímas; ***i ~i del giorno*** dienõs ïvykiai; ¨ ***di f.*** íð esmºs; fåktiðkai; (*effettivo*) fåktinis *agg*; ***ê un dato di f.*** ta¤ fåktas; ***il f. ê che..., f. sta che...*** dalýkas tâs, kâd..., ta¤p yrâ, kâd... ; ***un esperto in f.*** (*di qcs*) (*ko*) þinõvas; ***méttere di fronte al f. compiuto*** nepérspëti; pasakôti / pranêðti ja÷ põ vísko; **2.** (*azione*) dãrbas; ***cògliere sul f.*** suèiùpti nusikaltímo viìtoje; ¨ ***passare alle vie di f.*** i§tis jëgõs; **3.** (*affare*) re¤kalas; ***sono ~i miei*** ta¤ mâno re¤kalas; ¨ ***dire a qcn il f. suo*** pastatôti k± ñ viìtà; ***stârsene per i ~i propri*** (pasi)líkti núoðalyje; ***sa il f. suo*** þíno, k± dåro.

**fâtt|o II, -a 1.** *dlv* padarôtas; pagamíntas; ât­lik­tas; sukùrtas; ***sono f. così*** âð tóks; ¨ ***detto f.*** pasakôta -- padarôta; ***f. a mano*** ra¹kø dârbo; ***f. in casa*** namínis; ***f. a (forma)*** (*di qcs*) (*ko*) pavídalo; ***ben f.!*** ðaunù!; ***ê ~a!*** pavýko!; *t.p.* Þ **fâre**; **2.** *bdv*: ¨ ***venire f.*** atsitíkti, nutíkti; **3.** *bdv*: ***un uomo f.*** suãugæs / subréndæs vôras; ***a notte ~a*** ja÷ nåktá; **4.** *bdv*: ***êssere f.*** (*per qcs*) tíkti (*kam*); b¿ti sutvertãm *fam*; **5.** *bdv, dlv fam* (*dro­gato*) uþsimìtæs; (*fumato*) apsir¾­kæs.

**fattóre I** *dkt v* **1.** fåktorius, veiksnýs; (*circostanza*) aplinkôbë; ***f. emotivo*** emòcinis veiksnýs; ***f. umano*** þmoga÷s veiksnýs; ***f. di rìschio*** rízikos faktorius; *sport* ***f. campo*** namÿ aikðtìlës pranaðùmas; **2.** *mat* daugíklis; ***f. primo*** neda­lùsis daugíklis; **3.** *mat, fis,* (*coefficiente*) koefi­cie¹tas; **4.:** ***il sommo F.*** Sutver¸jas.

**fattóre II, -éssa** *dkt v/m* fêrmeris -ë.

**fattorìa** *dkt m* (*l'azienda*) ¿kis; fêrma; (*i fabbricati*) sodôba; (*casolare*) vîenkiemis.

**fattorìno** *dkt v* **1.** pasiuntinýs; kùrjeris; **2.** (*bigliettaio*) bílietininkas.

**fattucchiéra** *dkt m* þîeþula, rågana.

**fattùra** *dkt m* **1.** (*lavorazione*) pagamínimas; ga­môba; ***di òttima f.*** pu¤kiai (/ da¤liai *ir pan.*) padarôtas (pagamíntas / pasi¿tas *ir pan.*); **2.** *comm* sçskaita-faktûrâ, prekôraðtis; (*conto*) sçskaita; **3.** (*maleficio*) raganýstë; raganåvi­mas; ***fare la f.*** (*a qc*) uþker¸ti (*kà*).

**fatturâ|re** *vksm* (*-tù-*) **1.** *comm* (*emettere fattura e sim.*) iðraðôti sçskaità-fakt¾rà; ***f. una prestazione*** iðraðô­ti paslaugõs sçskaità-fakt¾rà; **2.** *comm* (*registrare*) tur¸ti *tam tikrà* apôvartà; ***la ditta ha ~to venti milioni*** ïmonës apôvarta bùvo dvídeðimt milijõnø.

**fatturâto** *dkt v comm* apôvarta.

**fatturazióne** *dkt m comm* sçskaitos-fakt¾ros iðråðy­mas (/ pateikímas).

**fâtuo, -a** *bdv* **1.** tùðèias; (*superficiale*) pavirðutí­niðkas; **2.****: *fuoco f.*** þaltvýkslë.

**fâuci** *dkt m dgs* **1.** nasra¤, þiótys; **2.** *anat* þiótys.

**fâuna** *dkt m* fãuna, gyvûnijâ.

**fâuno** *dkt v mit* fãunas.

**fâusto, -a** *bdv* þådantis sºkmæ; sëkmíngas.

**fautóre,** **-trìce** *dkt v/m* ðalini¹kas -ë; (*cultore*) púoselëtojas -a.

**fâva** *dkt m bot* pupâ; ¨ *flk* ***prêndere due piccioni con una f.*** vîenu ðû­viù dù kiðkiùs nuðãuti.

**favêlla** *dkt m* kalbâ (*galëjimas kalbëti*); kalb¸jimas.

**favìll|a** *dkt m* kibirkðtís -iìs (*ir prk*); þîeþirba; ***man­dare ~e*** kibirkðèiúoti; ¨ ***fare ~e*** ða÷niai pa­siródyti.

**fâvo** *dkt v* korýs.

**fâvol|a** *dkt m* **1.** påsaka (*ir prk*); pasak¸èia; ***le ~e di Esopo*** Ezòpo pasak¸èios; ***raccontare una f.*** sêkti påsakà; ¨ ***da f.*** påsakiðkas *agg*; **2.** *fig* (*fandonia*) påsaka.

**favolós|o,** **-a** *bdv* **1.** *fig* (*da favola*) påsakiðkas; (*me­ravi­glioso*) nuostabùs; ***un tempo f.*** påsakiðkas óras; ***ricchezze ~e*** påsakiðki tu»tai; **2.** (*delle favole*) påsakø; (*leggendario*) lege¹dinis.

**favóre** *dkt v* **1.** (*servizio*) paslaugâ; patarnåvi­mas; (*beneficio*) malónë; ***chiédere un f.*** (*a qcn*) papraðôti (*ko*) paslaugõs; ***fare*** ***un** **f.*** padarôti påslaugà;¨ ***per*** ***f.*** praða÷; pråðom; ***per f., pâssami un coltello*** praða÷ padúoti mãn pe¤lá; ***per f., si sieda*** praða÷ / pråðom s¸stis; *iron* ***mi fâccia il f.!*** malon¸kite!; **2.** (*benevolenza*) palankùmas; ***guardare con f.*** (*a qcs*) palankia¤ (*á kà*) þiûr¸ti; ***pêrdere il f.*** (*di qcn*) patêkti ñ (*kieno*) nemaló­næ; ***riscuòtere il f. del pùbblico*** sulãukti teigia­mÿ pùblikos atsiliepímø; ¨ ***a f.*** (*benevolo*) palankùs *agg*; ***di f.*** iðskirtínis (*apie kainà, pas*­*laugà*); **3.** (*protezione*) globâ; ***col f. delle ténebre*** naktiìs prîedangoje, naudójantis tamsâ; **4.: *a f., in f.*** (*di qc*) ùþ (*kà*); *sport* ***in f. di qc*** kienõ nãudai; *dir **teste a f.*** gynôbos liùdytojas.

**favoreggiaménto** *dkt v dir* bendrininkåvimas (*padarant nusikalstamà veikà*); ***f. della prostituzione*** sàvadåvimas.

**favorévole** *bdv* **1.** (*vantaggioso*) palankùs; ***prêndere una piega f.*** ágãuti pala¹kià krýptá; **2.** (*di qcn*): ***êssere f. a qcs*** prita»ti kãm.

**favorevolménte** *prv* palankia¤.

**favor|ìre** *vksm* (*-**ì**sc-*) **1.** (*qc*) b¿ti (*kam*) palankiãm; (*aiutare*) pad¸ti (*kam*); **2.** (*appoggiare*) palaikôti; (*promuovere*) (pa)re§ti; ***f. le arti*** re§ti mìnà; **3.** *fig* (*agevolare*) (pa)le¹gvinti; ***f. la digestione*** le¹gvinti vírðkinimà, pad¸ti vír­ðkinti; **4.: *~isca, ~ite*** pråðom; ***~isca i documenti*** pråðom, j¿sø dokumentùs; ***~isca alla cassa*** pråðom, priì kasõs; ***vuole*** ***f.?*** pråðom vaiðíntis.

**favoritìsmo** *dkt v* favoritízmas; protegåvimas.

**favorìt|o,** **-a** **1.** *dkt v/m* favorítas -ë, numyl¸tinis -ë; **2.** *dkt v/m sport* favorítas -ë; **3.** *dkt v dgs*: ***~i*** þãndenos; **4.** *bdv* (*preferito*) mëgstamiãusias.

**fax** *dkt v nkt* **1.** (*l'apparecchio*) fåksas, fåkso aparåtas; **2.** (*il documento*) faksogramâ, fåksas *fam*; ***inviare*** ***un** **fax*** iðsiÿsti faksogråmà.

**faxâre** *vksm* (ið)siÿsti faksù.

**fazióne** *dkt m* fråkcija, grupuõtë.

**faziositâ** *dkt m* partiðkùmas; (*parzialitâ*) ðaliðkùmas.

**fazióso, -a** *bdv* **1.** partínis; (*parziale*) ðåliðkas; **2.** (*sovversivo*) maiðtíngas.

**fazzolettìno** *dkt m* nosinãitë; ***f. di*** ***carta*** popierí­në nósinë (*vienkartinë*).

**fazzolétto** *dkt v* **1.** nósinë; nosinãitë; **2.** (*foulard*) skarìlë; skepetãitë; **3.:** *fig **un f. di terra*** þìmës plotìlis.

**febbrâio** *dkt v* vasåris; ***a f. in f.*** vasårá, vasårio m¸nesá; ***il 26 f.*** vasårio 26-óji dienâ.

**fébbre** *dkt m* **1.** temperatûrâ, ka»ðtis; karðèiåvi­mas; *med* ka»ðtligë; *med* ***f. gialla*** geltonâsis drugýs; *med* ***f. da fieno*** ðiìnligë; ***misurare la f.*** pamatúoti temperat¾rà; ***ho*** ***la** **f.*** karðèiúoju, turiù temperat¾ros; ¨ ***f. da cavallo*** „ãrklinis ka»ðtis" (*apie labai aukðtà temperatûrà*); **2.** *fig* ka»ðtligë; ***la f. dell'oro*** ãukso ka»ðtligë.

**febbricitânte** *bdv, dlv* karðèiúojantis.

**febbricitâre** *vksm* [A] (*-br**ì**-*) karðèiúoti.

**febbrìle** *bdv* **1.:** ***accesso f.*** ka»ðtligës prîepuolis; ***stato f.*** karðèiåvimas; **2.** *fig* (*frenetico*) ka»ðt­ligiðkas; ***ricerca f.*** ka»ðtligiðka paieðkâ; **3.** *fig* (*inquieto*): ***êssere in attesa f.*** neramia¤ lãukti.

**febbrilménte** *prv* ka»ðtligiðkai.

**féccia** *dkt m* **1.** núosëdos *pl*; **2.** *fig*: ***la f. della societâ*** visúomenës pådugnës *pl*.

**féci** *dkt m dgs* *fisiol* íðmatos.

**fécola** *dkt m* krakmõlas; ***f. di patate*** bùlviø krak­mõlas.

**fecondâre** *vksm* (*-cón-*) **1.** apvaisínti; (*di fiori*) apdùlkinti; **2.** (*rendere fertile*) (pa)tr³ðti.

**fecondâto, -a** *bdv, prv* apvaisíntas.

**fecondazióne** *dkt m* apvaisínimas; (*di fiori*) ap­dùlki­ni­mas; ***f.*** ***artificiale (assistita)*** dirbtínis (pagãlbi­nis) apvaisínimas.

**fecóndo, -a** *bdv* **1.** derlíngas, naðùs; vaisíngas; **2.** *fig* (*creativo*) kû­rybíngas; kûrôbið­kas; (*che dâ frutto*) vaisíngas.

**féde** *dkt m* **1.** (*credo religioso*) tik¸jimas; ***mistero della f.*** tik¸jimo paslaptís; ***professare una f.*** iðpaþínti tik¸jimà; **2.** (*fiducia*) pasitik¸jimas; (*il credere*) tik¸jimas; ***avere f.*** (*in qc*) pasitik¸ti (*kuo*); tik¸ti; **3.** (*fedeltâ*) iðtikimôbë; lojalù­mas; ***êssere in buona f.*** sàþiníngai eµgtis (/ kalb¸ti *ir pan.*); ***tenere f. a una promessa*** iðtes¸ti påþadà; ¨ ***in f.*** (*nelle lettere*) tvírtinu; pagarbia¤; **4.** (*convinzioni*) ásitíkinimai *pl*; **5.:** **¨** ***fa f. il timbro postale*** galiójanèia la¤koma iðsiuntímo datâ; **6.** (*t.p. **f. nuziale***) vestùvinis þîedas.

**fedéle** **1.** *bdv* íðtikimas; (*devoto*) atsidåvæs; (*affidabi­le*) pâtikimas; (*leale*) lojalùs; ***un cane (cliente, marito) f.*** íðtiki­mas ðuõ (klie¹tas, vôras); **2.** *bdv* (*preciso*) tikslùs; (*attendibile*) pâtikimas; ***un ritratto f.*** tikslùs portrêtas; ***uno stòrico f.*** pâtikimas istòrikas; ***una traduzione f.*** tikslùs vertímas; **3.** *dkt v/m* (*credente*) tikin­týsis *masc*, tíkinèioji *femm*; **4.** *dkt v/m* (*seguace*) ðalini¹kas.

**fedelménte** *prv* **1.** iðtikima¤; atsidåvusiai; pati­kima¤; **2.** (*esattamente*) tikslia¤.

**fedeltâ** *dkt m* **1.** iðtikimôbë; atsidavímas; ***f. alla pâtria (alla mòglie)*** iðtikimôbë tëvýnei (þmó­nai); **2.** (*precisione*) tikslùmas; (*attendibilitâ*) patikimùmas; **3.:** *tecn* ***ad alta f.*** aukðèiãusios ga»so kokôbës.

**fédera** *dkt m* (pagãlvës) ùþvalkalas.

**federâle** **1.** *bdv* federålinis; federåcinis; (*di una federazione*) federåcijos; **2.** *dkt v/m* FTB ågentas.

**federalìsmo** *dkt v* federalízmas.

**federazióne** *dkt m* federåcija.

**fedìna** *dkt m*: *dir* ***f. (penale)*** þínios *pl* apiì teistùmà; nusikaltímø sçraðas; ***avere la f. penale*** ***pulita*** netur¸ti teistùmo.

**feeling** [&filin] *dkt v nkt* artimùmo i» patrauklù­mo ja÷smas; tarpùsavio supratímas.

**fégato** *dkt v* **1.** *anat* kìpenys *pl*; ¨ ***mangiarsi il f., ródersi il f.*** grãuþtis; **2.:** *fig* ***avere f.*** tur¸ti dràsõs; **3.** *gastr* (*t.p. dgs* ***fegatini***) kepenºlës *pl*.

**félce** *dkt m bot* papa»tis.

**felìce** *bdv* **1.** laimíngas; (*contento*) paténkintas; ***còppia f.*** laimínga porâ; ***êssere f.*** dþia÷gtis; ¨ ***f. di fare la Sua conoscenza*** malonù sù Jumís susipaþínti; **2.** (*gioioso*) dþiaugsmíngas; ¨ ***f. Anno nuovo!*** laimíngø Naujÿjø mìtø!; **3.** (*andato a buon fine*) sëkmíngas; (*riuscito*) výkæs; ***un paragone f.*** visãi výkæs palôginimas.

**feliceménte** *prv* **1.** laimíngai; **2.** (*ocn successo*) sëk­míngai; výkusiai.

**felicitâ** *dkt m* **1.** lãimë; ***fare la f.*** (*di qcn*) sute¤kti (*kam*) lãimës, pradþiùginti (*kà*); (*riuscire vantaggioso*) iðe¤ti (*kam*) ñ nãu­dà; ¨ ***il denaro non fa la f.*** nê piniguosê lãimë; **2.** (*gioia*) dþia÷gsmas.

**felicitârsi** *vksm* (*-l**ì**-*) (*con qc per qcs*) (pa)svéi­kinti (*kà su kuo / kokia proga / kà pada­rius*); ***f. con qc per la vittò­ria*** pasvéikinti k± sù pérgale.

**felicitazióni** *dkt m dgs* (*per qcs*) svéikinimai (*su kuo, kokia proga, kà padarius*); ***f. (per la lâurea)!*** svéikinu (apsigônus)!

**felìno, -a** **1.** *dkt v* *zool* katínis (*kaèiø ðeimos þind­uolis*); **2.** *bdv* kaèiÿ; **3.** *bdv fig* ka¤p katºs, kåtiðkas.

**félpa** *dkt m* **1.** (*tessuto*) pliùðas; **2.** (*maglia*) pliuðínis dþêmperis.

**felpâto, -a** *bdv fig*: ***passo f.*** atsa»gûs þi¹gsniai *pl*.

**feltrìno** *dkt v* apsaugínis lipdùkas (*ppr. veltinis*).

**féltro** *dkt v* fêtras; ***di f.*** fêtrinis *agg*, veltínis.

**fémmina** **1.** *dkt m* (*donna*) móteris -rs; *spreg* bóba; (*ragazza*) merginâ; (*bambina*) mergãite; ***due figli: un mâschio e una f.*** dù vaika¤: mergãitë i» ber­niùkas; **2.** *dkt m* (*di animale*) patìlë; **3.** *bdv* (*di sesso femminile*) móteriðkosios gimi­nºs. 

**femminìle** **1.** *bdv* móteriðkas; móterø, móters; ***bellezza f.*** móters grõþis; ***curiositâ f.*** móteri­ðkas smalsùmas; ***monastero f.*** móterø vie­nuolônas; ***rivista f.*** þurnålas móterims; *gram **génere f.*** móteriðkoji giminº; **2.** *dkt v gram* (*il genere*) móteriðkoji giminº; **3.** *dkt v gram* (*nome femminile*) móteriðkosios giminºs daiktåvardis; **4.** *dkt v sport* móterø tur­nýras.

**femminilitâ** *dkt m* moteriðkùmas.

**femminìsmo** *dkt v* feminízmas.

**femminìsta** *bdv* **1.** feminístinis; feminízmo; feminístø; **2.** *dkt v/m* feminístas -ë.

**femminùccia** *dkt m* **1.** mergãitë, mer­gôtë; **2.** *spreg* bóba (*apie vyrà*).

**fémore** *dkt v anat* ðlauníkaulis.

**fendént|e** *dkt v* **1.** (aðtrùs) ki»tis; ***tirare ~i con la spada*** ki»sti kalavijù; **2.** *sport* stiprùs sm¾gis ñ vartùs.

**féndere*** *vksm* **1.** (á)skélti; (*in due*) pérskelti; (*ta­gliare a mezzo*) pérkirsti; ***f. la terra*** (ið)vagóti di»và; ãrti þìmæ; **2.** *fig* raiþôti, skrósti; ***f. l'âria*** raiþôti órà.

    **> féndersi** *sngr* (á)skílti; (*in due*) pérskilti; (*fes­surar­si*) skéldëti.

**fendinébbia** *dkt v nkt* r¾ko þibi¹tas.

**fenditùra** *dkt m* tr¾kis, plyðýs; áskilímas.

**fenìce** *dkt m mit* fêniksas.

**fenìcio, -a** *stor* **1.** *bdv* Finíkijos, finikiìèiø; **2.** *dkt v/m* finikiìtis -ë; **3.** *dkt v* finikiìèiø kalbâ.

**fenicòttero** *dkt v zool* flamíngas.

**fenomenâle** *bdv* fenomenalùs; nepâprastas.

**fenòmeno** *dkt v* **1.** reiðkinýs, fenomênas; ***un f. raro (locale, strano)*** rìtas (viìtinis, ke¤stas) reiðkinýs; ***f. culturale*** kult¾rinis reiðkinýs; ***il f. (dell')emigra­zio­ne*** iðeivíjos fenomênas; **2.** (*in senso scientifico*) reiðkinýs; ***f. naturale*** gamtõs reiðkinýs **3.** *fig fam* fenomênas, åsas.

**féretro** *dkt v* ka»stas (*ppr. per laidotuves*).

**feriâl|e** *bdv*: ***(giorno)*** ***f.*** ðiokiådienis, dãrbo dienâ; ***orârio f.*** ðiokiådieniø dãrbo la¤kas; ***nei giorni ~i***  nê ðve¹èiø dienomís.

**fêrie** *dkt m dgs* atóstogos; ***f. estive*** våsaros ató­s­t­ogos; ***f. non godute*** nepanaudótos atóstogos; ***chiuso per f.*** uþdarôta dºl atóstogø; ***durante le f.*** pe» atóstogas; ***andare*** ***in** **f., prêndere le f.*** iðe¤ti atóstogø; ***êssere in f.*** atostogãuti.

**feriménto** *dkt v* suþeidímas.

**ferìre** *vksm* (*-**ì**sc-*) **1.** (su)þe¤sti; (*sparando*) paðãuti; (*con tagli, morsi*) áki»sti; ***f. alla testa*** suþe¤sti ñ gãlvà; ***f. a morte*** mirtina¤ suþalóti; ¨ ***senza colpo f.*** bê kovõs; **2.** (*moralmente*) áska÷dinti; uþgãuti, áþe¤sti; ***f. i sentimenti di qcn*** uþgãuti kienõ jausmùs.

**> ferìrsi** *sngr* susiþe¤sti; (*farsi male*) uþsigãuti; ***f. a una mano*** susiþe¤sti ra¹kà.

**ferìt|a** *dkt m* þaizdâ (*ir prk*); suþeidímas; ***una f. profonda (dolorosa)*** gilí (skaudí) þaizdâ (*ir prk*); ***f. d'arma da fuoco (d'arma da tâglio)*** ðautínë (kirstínë) þaizdâ; ***fasciare (lavare) una f.*** apríðti (iðplãuti) þa¤zdà; ***riportare gravi ~e*** patírti sunkiùs suþeidimùs; ***la f. ê guarita*** þaizdâ uþgíjo; ¨ ***riaprire una f.*** a¤trinti þa¤z­dà.

 **ferìto,** **-a** **1.** *bdv, dlv* sùþeistas; ***f. a una gamba*** sùþeistas ñ kójà; ***restare f.*** b¿ti suþeistãm; **2.** *dkt v/m* suþeistâsis -óji; ***un f. grave*** su¹kiai sù­þeistas þmogùs.

**feritóia** *dkt m* ambrazûrâ.

**fermacapêlli** *dkt v nkt* plaukÿ segtùkas.

**fermacârte** *dkt v nkt* prespapjº *inv*.

**fermacravâtta** *dkt v nkt* kaklåraiðèio segtùkas.

**fermâglio** *dkt v* **1.** sìgë; (*fibbia*) sa­gtís -iìs *femm*; **2.** (*fermacapelli*) segtùkas; **3.** (*graffetta*) sàvarþºlë.

**fermaménte** *prv* tvirta¤ (*ir prk*); *fig* ***f. deciso*** (*a far qcs*) tvirta¤ apsispréndæs (*kà daryti*).

**ferm|âre** *vksm* (*fêr-*) **1.** *tr* (su)stabdôti; (*trattenere*) sulaikôti; ***f. un passante (il gioco, il nemico)*** sustabdôti prae¤vá (þaidímà, prîeðà); ***f. la caduta dei capelli*** sustabdôti plaukÿ slinkí­mà; *dir **f. un sospetto*** sulaikôti ñtariamàjá; **2.** *tr* (*fissare*) pri­tvírtinti; (*con una graffetta*) prisêgti; (*con uno spillo*) prisme¤gti; (*cucendo*) prisi¿ti; ***f. i capelli con uno spillo*** susme¤gti plãukus; **3.** (*prenotare*) (uþ)rezer­vúoti ***f. una câmera*** uþrezervúoti ka§bará; **4.** *intr* [A] (su)stóti; ¨ ***~a!*** stók!; (*stai immobi­le!*) neju­d¸k!

**> ferm|ârsi** *sngr* **1.** (su)stóti; (*un attimo*) stâb­telëti; ***senza f.*** bê paliovõs; ***fêrmati!*** (su)stók!; (*smettila!*) liãukis!; ***il treno si ~a 5 minuti*** traukinýs stovºs penkiâs minutês, traukinýs sustõs penkióms minùtëms; **2.** (*trattenersi, restare*) (pa)­b¿ti; pasilíkti; ***f. in albergo*** apsistóti viìðbutyje; **3.** (*bloccarsi*) (su)­stóti; ***l'orològio si ê ~to*** la¤krodis sustójo.

**fermât|a** *dkt m* **1.** stotìlë; ***alla f. dell'âutobus*** autobùso stotìlëje; **2.** (*il fermarsi*) sustojímas; ***fare una f.*** sustóti; ***fare / effettuare due ~e*** sustóti dù kartùs.

**fermentâre** *vksm* [A] (*-mén-*) **1.** r¿gti, fermentúotis; ***far f.*** raugínti; ***méttere a f.*** (uþ)rãugti; **2.** *fig* vírti.

**fermentazióne** *dkt m* fermentåcija, rûgímas; (*il far fermentare*) raugínimas.

**ferménto** *dkt v* **1.** rãug(al)as, ferme¹tas; **2.** *fig fam* subruzdímas; ***êssere in f.*** kunkuliúoti.

**fermézza** *dkt m fig* (*determinazione*) rýþtas; ***con f.*** ryþtíngai.

**férm|o I,** **-a** *bdv* **1.** nêjudantis; (*che si ê ferma­to*) sustójæs; ***êssere*** ***f.*** stov¸­ti; ***stare f.*** nejud¸ti; ***non stare f. un minuto*** nº minùtës negal¸ti nustôgti; ***l'oro­lògio ê f.*** la¤krodis stóvi / sustójo; *sport* ***partenza da f.*** stãrtas íð viìtos; ¨ ***terra ~a*** þemônas; ***f. lì!, f. lâ!*** stók!; **2.** (*sospeso e sim.*) pristabdôtas; (*bloccato*) sustabdôtas; ***le fâbbriche sono ~e*** gamýklos neve¤kia; **3.** *fig* (*saldo*) tvírtas; (*irremovibile*) nepajùdinamas; ***con mano ~a, con polso f.*** tvirtâ rankâ; **4.:** *gram* ***punto f.*** tåðkas; **5.:** ¨ ***f. restando*** (*qcs*) nesike¤èiant (*kam*); nesigi¹èijant (*dël ko*).

**férmo II** *dkt v* **1.** (*per tener chiuso*) skl±stis; (*gancetto*) kabliùkas; **2.** *dir* (*t.p.* ***f. di polizia***) sula¤kymas; ***f. giudiziârio*** kãrdomoji prîemo­në; ***in stato di f.*** sula¤kytas *agg*.

**fermopòsta** *dkt v nkt* ikí pareikalåvimo.

**feróce** *bdv* **1.** þiaurùs, nuoþmùs; (*efferato*) þv¸riðkas; (*catti­vo*) píktas; ***bêstia f.*** laukínis þvërís; ***un cane f.*** píktas ðuõ; **2.** *fig* baisùs.

**feroceménte** *prv* nuõþmiai; ka¤p þvërís; laba¤ pikta¤.

**feròcia** *dkt m* þiaurùmas, nuoþmùmas; þvëriðkùmas.

**ferrâglia** *dkt m* gelþgalia¤ *pl*; geleþiìs lãuþas.

**ferragósto** *dkt v* rugpj¿èio 15-oji; Þõlinë.

**ferraménta** *dkt v/m* *nkt* geleþiìs dirbiniÿ parduotùvë; (*per duplicare chiavi*) råktø dirbtùvë.

**ferrâre** *vksm* (*fêr-*) (pa)kãustyti.

**ferrâto, -a** *bdv* **1.** pakãustytas; **2.** *fig* (*di qcn, in qcs*) pa­sikãustæs (*tam tikroje srityje*).

**fêrre|o,** **-a** *bdv* **1.** íð geleþiìs, geleþínis; **2.** *fig* ge­leþínis; ***volontâ ~a*** geleþínë valiâ.

**fêrr|o** *dkt v* **1.** geleþís -iìs *femm*; ***di** **f.*** geleþínis *agg* (*ir prk*); ***fil di f.*** vielâ; *stor* ***cortina di f.*** geleþínë ùþdanga; *fig* ***memòria di f.*** puikí atmintís; *fig* ***stòmaco di f.*** geleþínis skra¹dis; ¨ ***brâccio di f.*** ra¹kos lenkímas; *fig* konfrontåcija; ***bât­tere il f. finché ê caldo*** kãlti gìleþá, kõl karðtâ; ***êssere in una botte di f.*** „b¿ti geleþimí apkãu­stytoje statínëje" (*bûti visiðkai saugiam, nieko nebijoti*); ***toccare f.*** pastuksénti ñ mìdá; **2.** (*pezzo di ferro*) gelþgalýs; **3.** (*oggetto, attrezzo*): ***f.*** ***da** **mâglia*** vi»balas; ***f.*** ***da** **stiro*** lygintùvas; ***f.*** ***di** **cavallo*** pasagâ; ***bistecca ai ~i*** kepsnýs a¹t grotìliø; ***i ~i del mestiere*** dãrbo ïrankiai; ***méttere ai ~i*** sukãustyti pãnèiais; ¨ ***a f. di cavallo*** pasagínis *agg*, U pavídalo; ***méttere a f. e fuoco*** sia÷bti ugnimí i» kalavijù; ***venire ai ~i corti*** sma»kiai susiba»ti.

**ferróso, -a** *bdv* **1. g**eleþíngas; **2.** *chim* geleþiìs.

**ferrovêcchio** (*dgs* ferrivecchi) *dkt v fig* (*di qcs*) sìnas lãuþas.

**ferrovìa** *dkt m* geleþínkelis; ***f. suburbana*** priemiestínis geleþínkelis.

**ferroviâr|io,** **-a** *bdv* geleþínkelio; traukiniÿ; ***collegamento f.*** susisiekímas traukinia¤s; ***incidente f.*** geleþínkelio avårija; ***orârio f.*** traukiniÿ tvarkåraðtis; ***stazione*** ***~ia*** geleþínkelio stotís.

**ferroviére** *dkt v* geleþínkelininkas.

**fêrtile** *bdv* derlíngas; naðùs; (*anche di esseri uma­ni*) vaisíngas.

**fertilitâ** *dkt m* derlingùmas; naðùmas; (*anche di esseri uma­ni*) vaisingùmas.

**fertilizzânte** *dkt v* tràðâ, tr±ðos.

**fertilizzâre** *vksm* (pa)tr³ðti.

**fêrv|ere** *vksm* [-] vírti (*ppr. prk*); ***~ono i preparativi*** virtê vérda pasiruoðímo darba¤.

**fervénte** *bdv* (*di qcn*) uþsidìgæs; kãrðtas; ***f. patriota*** kãrðtas patriòtas.

**fêrvid|o, -a** *bdv* **1.** (*ardente*) kãrðtas; aistríngas; **2.** (*di qcn*) uþsidìgæs; **3.:** ***~a fantasia*** lakí vaizduõtë.

**fêrvido,** **-a** *bdv fig* **1.** kãrðtas; ***un f. patriota*** kãrðtas patriòtas; **2.** (*della fantasia*) lakùs.

**fervóre** *dkt v* **1.** (*ardore*) uþsidegímas, aistrâ; **2.** (*concitazione*) ïkarðtis.

**fésa** *dkt m tarm* (*di vitello*) ðlauniìs íðpjova; (*di tacchino*) kalakùto krûtinºlës filº.

**fesserì|a** *dkt m* nesçmonë; ***dire ~e*** svãièioti, niekùs ðnek¸ti, nusiðnek¸ti.

**fésso, -a** **1.** *bdv* kva¤ðas; ***fare f.*** apmu¤linti, ap­mùl­kinti; **2.** *dkt v/m* mùlkis -ë, kvãiða *com*.

**fessùra** *dkt m* plyðýs; (*crepa*) átrûkímas.

**fêsta** *dkt m* **1.** ðve¹të; (*anniversario*) íðkilmës *pl*; (*giorno festivo*) ðve¹èiø dienâ; iðeigínë dienâ; (*giorno di riposo*) nê dãrbo dienâ, póilsio die­nâ; ***f. nazionale*** valstôbi­në ðve¹të; ***la f. della mamma*** mamõs dienâ; *dgs* ***le ~e*** ðve¹tës; (*ppr.* Kalºdø) at­ósto­gos; ***du­rante le ~e*** pe» ðventês; ***per le f.*** ðve¹èiø próga; *fam* ***il vestito della f.*** iðeigínë suknìlë; *rel* ***f. di precetto*** privåloma ðve¹të; ***far f.*** (*non lavorare*) ðv³sti (*ne­dirbti*); (*far baldoria*) ¿þauti; ¨ ***a f.*** sve¹ti­ð­kai; ***da f.*** ðve¹tinis *agg*; ***conciare per le ~e*** sudau­þôti ka¤p óbuolá; ***fare la f. a qcn*** susidoróti (*su kuo*); *kaip jst* ***buone ~e!*** links­mÿ ðve¹èiø!, sù ðve¹­tëm(is)!; **2.** (*party e sim.*) vakarºlis, vakarõnë; póbûvis, pókylis; bålius; ***f. di addio al celibato*** bérnvakaris; ***f. di com­pleanno*** gimtådienio vakarºlis; *fam* ***la f. delle matrìcole*** (fùksø) krikðtýnos *pl*; ***organizzare una f.*** (su)­re¹gti vakarºlá; **3.** (*gioia*) linksmôbë, dþia÷gs­mas; ***êssere in f.*** dþi¿gauti; ***fare f. a qcn*** ðilta¤ (*kà*) pasitíkti / prii§ti; (*detto di cane*) gìrintis (*kam, prie ko*).

**festaiòlo, -a** *bdv* **1.** (*di qcn*) m¸gstantis ðv³sti; vaka­rºliø mëg¸jas; **2.** (*di qcs*) li¹ksmas.

**festânte** *bdv* dþi¿gaujantis; dþiugùs.

**festeggiamént|o** *dkt v* **1.** ðventímas; **2.:** *dgs **~i*** íðkilmës, ðve¹të *sg*.

**festeggiâre** *vksm* (*-stég-*) **1.** (at)ðv³sti; ***f. un anniversârio*** atðv³sti / pamin¸ti sùkaktá; ***f. il compleanno*** atðv³sti gimtådiená; **2.** (*accogliere in festa*) (pa)­svéikinti; dþiûgia¤ prii§ti; ***f. gli sposini*** svéikinti jaunúosius; **3.** *kaip intr* (*fare festa*) ðv³sti, línksmintis.

**festeggiâto**, **-a** *dkt v/m* jubiliåtas -ë.

**festìno** *dkt v* vakarºlis; bålius.

**fêstival** *dkt v nkt* festivålis.

**festivitâ** *dkt m nkt* (kalendõrinë) ðve¹të, ðve¹­èiø dienâ; íðkilmës *pl*; ***la f. di San Giovanni*** Jõninës *pl*; ***durante le f. natalìzie*** pe» Kalºdø ðventês.

**festìv|o,** **-a** *bdv* ðve¹tinis; ***(giorno)*** ***f.*** iðeigínë / ðve¹­èiø dienâ; ***orârio f.*** dãrbo la¤kas ðve¹èiø dienomís; ***nei giorni ~i*** ðve¹èiø dienomís.

**festóne** *dkt v* **1.** girliãnda; **2.** *archit* pynº, festònas.

**festós|o, -a** *bdv* dþiaugsmíngas; dþiugùs, li¹ks­mas; ***un'accoglienza ~a*** dþiugùs priëmímas.

**feténte** **1.** *bdv* dvõkiantis; smardùs, dvokùs; **2.** *dkt v fig spreg* bjaurôbë *com*, nenãudëlis.

**fetìccio** *dkt v* fetíðas (*ir prk*).

**fétido, -a** *bdv* ðle¤kðèiai dvõkiantis; smardùs.

**féto** *dkt v biol* va¤sius; gìmalas.

**fetóre** *dkt v* ðleikðtùs dvõkas, bjaurí smãrvë.

**fétt|a** *dkt m* (*di pane e sim.*) riekº; (*di salame e sim.*) grieþinýs; (*pezzettino*) gabaliùkas; (*spic­chio*) skíltis -ies; (*t.p.* ***fettina***) riekìlë, riekùtë; grieþinºlis; ***f.*** ***biscottata*** dþiû­vºsis (*saldus*); ***tagliare a ~e*** (su)raikô­ti; (su)pjãu­styti grieþinia¤s; ¨ ***fare a ~e, fare a fettine*** suriìsti ñ óþio rågà.

**fettùccia** *dkt m* juostìlë (*ppr. audinio*).

**fettuccìne** *dkt m dgs gastr* låkðtiniai.

**feudâle** *bdv stor* feodålinis.

**feudalésimo** *dkt v stor* feodalízmas.

**fêudo** *dkt v* **1.** *stor* lênas, feòdas; **2.** *fig* (*latifondo*) dvåras; **3.** *fig* (*proprietâ privata*) privatí valdâ.

**fiâba** *dkt m* påsaka.

**fiabésco, -a** *bdv* påsakiðkas; påsakos.

**fiâcca** *dkt m* núovargis; ¨ ***bâttere la f.*** dykin¸ti / tinginiãuti darbê; di¹derá mùðti.

**fiaccâre** *vksm* (nu)ålinti, (nu)vãrginti; ilsínti.

**fiacchézza** *dkt m* nuvargímas.

**fiâcco,** **-a** *bdv* nuva»gæs, nusiµpæs; nuvãrgintas.

**fiâccola** *dkt m* fåkelas, dìglas; ¨ ***f. olìmpica*** olím­pinis fåkelas.

**fiaccolâta** *dkt m* e¤sena sù fåkelais.

**fiâla** *dkt m* ãmpulë, buteliùkas.

**fiâmm|a** *dkt m* **1.** liepsnâ (*ir prk*); ugnís -iìs; *tecn* ***f. ossìdrica*** suvíri­nimo liepsnâ (*ppr. ið deguonies ir vandenilio*); ***andare in ~e*** uþsiliepsnóti; ***bruciare senza f.*** rusénti; smiµkti; ***cuòcere senza f.*** svílti; ***dare alle ~e*** sudìginti; ***êssere avvolto dalle f.*** pask³sti ugnyjê; ***êssere in ~e*** dêgti; ¨ ***far fuoco e ~e*** (ási)­lie­psnóti; **2.** *fig* liepsnâ; ***la f. dell'amore*** méilës liepsnâ; ¨ ***ritorno di f.*** senõs méilës sugráþí­mas; **3.: *Fiamme gialle*** „Geltonîeji a¹tsiu­vai" (*Italijos mokesèiø policija*).

**fiammânte** *bdv*: ¨ ***nuovo f.*** naujutëlãitis.

**fiammâta** *dkt m* (liepsnõs) pli¿psnis.

**fiammeggiâre** *vksm* [A] (*-még-*) liepsnóti (*ir prk*).

**fiammiferâi|o, -a** *dkt v/m* degtùkininkas -a; ¨ ***la pìccola ~a*** mergãitë sù degtùkais.

**fiammìfero** *dkt v* degtùkas; ***accéndere un f.*** uþ­(si)­br¸þti / uþ(si)dêgti degtùkà.

**fiammìngo, -a** **1.** *bdv* Flãndrijos, flamãndø; flamãn­diðkas; íð Flãndrijos; **2.** *dkt v/m* flamãn­das -ë; **3.** *dkt v* flamãndø kalbâ.

**fiancâta** *dkt m* ðónas; (*solo di nave*) bòrtas.

**fiancheggiâre** *vksm* (*-chég-*) **1.** b¿ti (/ stov¸ti *ir pan.*) (*kam*) íð ðónø, b¿ti (*ko*) ðónuose; e¤ti pålei (*kà*); **2.** *fig* (pa)re§ti.

**fiân|co** *dkt v* **1.** ðónas; *dgs* ***i ~chi*** klù­bai; ***dormire su un f.*** miegóti a¹t ðóno; ¨ ***f. a f.*** petýs ñ pìtá; ***prestare il f. alle crìtiche*** dúoti pågrindà prîekaiðtams, prisiða÷kti krítikos; ***restare al f.*** (*di qcn*) nepalíkti (*ko*); *kaip jst mil* ***f. destr (si­nistr)!*** de­ðinºn (kai­rºn) lygiúok!; **2.** *fig* (*lato*) ðónas; (*fiancata*) bòrtas; (*ala*) spa»­nas; *archit* flôgelis; *mil* flãngas; ¨ ***a f.*** ðaliâ; ***di f.*** (*a qcs*) ðaliâ (*ko*).

**fiâsco** *dkt v* **1.** apipíntas bùtelis, kreþínë; **2.** *fig* (*in­successo*) fiåsko; ***fare f.*** patírti fiåsko.

**fiatâre** *vksm* [A] (*aprir bocca*) prasiþióti, þõdá ið­ta»ti; ¨ ***senza f.*** nesigi¹èijant.

**fiâto** *dkt v* **1.** iðkvëpímas; (*alito*) kvåpas; ***senza f.*** pritr¿kæs *agg* óro; ***avere il f. corto / grosso*** pridùsti, uþdùsti; ***riprêndere*** ***f.*** atgãuti kvåpà, atsi­kvºpti; ¨ ***col f. in gola,*** ***col f. sospeso*** uþgniãuþus kvåpà; ***(tutto) d'un f.*** vîenu atsi­kvëpimù; ***tirare il f.*** lengvia÷ atsi­dùsti / atsi­kvºpti; ***ê f. sprecato*** kalb¸ti beviµ­tiðka; **2.** *mus* (*t.p.* ***strumento a f.***) puèiamâsis instrume¹tas.

**fiatóne** *dkt v*: ***ho il f., mi ê venuto il f.*** uþdusa÷.

**fìbbia** *dkt m* sagtís -iìs; sìgë.

**fìbr|a** *dkt m* **1.** (*t.p.* ***f. têssile***) plúoðtas, plauða¤ *pl*; ***f. sintética (vegetale)*** sintêtinis (augalínis) plúoðtas; **2.** *biol* skãi­dula; ***ricco di ~e*** skaidulíngas; **3.** (*costituzione fisica*) k¿no sudëjímas; **4.** *tecn*: ***f. di carbònio*** angliìs plúoðtas; ***f. di vetro*** stíklo plúoðtas; ***f. òttica*** ðviesólaidis.

**fibrillazióne** *dkt m* **1.** *med* fibriliåcija; **2.:** *fig* ***êssere in f.*** ba¤siai nekantrãuti; nenustôgti vietoje.

**fìca** *dkt m volg* Þ **fìga**.

**ficâta** *dkt m fam, volg* Þ **figâta**.

**ficcanâso** *dkt v/m nkt* land¾nas -ë (*kas smalsauja*). 

**ficcâre** *vksm* **1.** (*conficcare*) (á)sme¤gti, (á)kãlti; (*infilare*) ákíðti; (*spingere dentro*) (á)gr¿sti; ¨ ***f. il naso*** nósá kíðti, kíðtis; **2.** *fam* (*cacciare*) nukíðti.

**> ficcârsi** *sngr* **1.** (*conficcarsi*) ásmígti; **2.** (*qcs*) ásikíðti; *fig* ***f. in testa qcs*** ásikãlti k± ñ gãlvà; **3.** *fam* (*cacciarsi*) (á)lñsti; ***f. in un guâio*** ásivélti ñ bºdà.

**fiche** [fi^] *dkt m nkt* (loðímo) þetònas.

**fìco I** *dkt v* **1.** *bot* (*l'albero*) fígmedis; **2.** (*il frutto*) figâ; **3.** *fam* níèniekas; ¨ ***un f. secco!*** nº vélnio!; **4.** *bot*: ***f. d'Índia*** (*l'albero*) figva¤së opùncija; (*il frutto*) opùncija, kaktusínë figâ.

**fìco II, -a** *bdv fam* Þ **fìgo**.

**fiction** [&fik^on] *dkt m nkt* (*telefilm*) seriålas.

**fidanzaménto** *dkt v* susiþad¸jimas; (*il rito*) suþa­dëtù­vës *pl*, suþieduotùvës *pl*.

**fidanzârsi** *vksm* susiþad¸ti, susiþiedúoti.

**fidanzâta** *dkt m* **1.** suþad¸tinë; **2.** (*ragazza*) mer­ginâ.

**fidanzâto** *dkt v* **1.** suþad¸tinis; **2.** (*ragazzo*) vai­kí­nas.

**fidâre** *vksm* [A] (*in qc*) pasitik¸ti (*kuo*).

**> fid|ârsi** *sngr* **1.** (*di qc*) pasitik¸ti (*kuo*), pasikliãuti; ***non*** ***mi** **~o di te*** nepasítikiu tavimí; **2.** *fam* (*a far qcs*) drñsti (*kà daryti*).

**fidâto, -a** *bdv* pâtikimas; (*fedele*) íðtikimas.

**fideiussióne** *dkt m dir* garãntija; (*contratto*) garãnti­jos sutartís -iìs.

**fìdo, -a** *bdv* íðtikimas.

**fidùcia** *dkt m* (*in qc*) pasitik¸jimas (*kuo*); ***avere** **f.*** (*in qc*) pasitik¸ti (*kuo*); ***non ha f. in sé stesso*** nepasítiki savimí; *polit* ***votare la f.*** paréikðti pasitik¸jimà balsù; ¨ ***di f.*** pâtikimas *agg*; (*delicato*) delikatùs.

**fiducióso,** **-a** *bdv* optimístiðkai nusite¤kæs; ***êssere f.*** (*in qc*) tik¸ti (*kuo*).

**fiéle** *dkt v* tulþís -iìs *femm* (*ypaè gyvuliø*); ¨ ***amaro come il f.*** kartùs ka¤p pipíras.

**fienagióne** *dkt m* ðienapj¿të.

**fienìle** *dkt v* darþinº.

**fiéno** *dkt v* ðiìnas; *med* ***raffreddore da f.*** alêrgija þiedådulkëms.

**fiéra I** *dkt m* (*t.p.* ***f. campionâria***) mùgë; (*mo­stra*) parodâ; ***la f. del libro*** knýgø mùgë.

**fiéra II** *dkt m* (plëðrùs) þvërís -iìs *masc*.

**fierézza** *dkt m* iðdidùmas (*geràja prasme*).

**fiéro,** **-a** *bdv* **1.** (*altero*) iðpu¤kæs, iðdidùs; **2.** (*orgoglioso*) iðdidùs; ***êssere*** ***f., andare f.*** (*di qcs*) didþiúotis (*kuo*).

**fiévole** *bdv* siµpnas; (*solo di luce*) blausùs; (*solo di suo­no*) prislopíntas.

**fìfa** *dkt m fam* bãimë; ***hai f.?*** tãu kínkos drìba?

**fifóne, -a** *dkt v/m fam* skystabla÷zdis -ë, kinka­drebýs -º.

**fìga** *dkt m volg* **1.** (*vulva*) pùtë; **2.** (*ragazza*) pupôtë.

**figâta** *dkt m fam, volg* superínis dalýkas.

**fìglia** *dkt m* duktº, dukrâ; ***f.** **adottiva*** ïdukra; *fig* ***la criminalitâ ê f. della tolleranza*** nusikalstamùmas -- pakantùmo va¤sius; *kaip bdv* ***azien­da f.*** pavaldþióji / dukterínë *fam* ïmonë.

**figliâre** *vksm* at(si)vêsti, vêsti (jaunikliùs).

**figliâstra** *dkt m* pódukra.

**figliâstro** *dkt v* pósûnis.

**fìglio** *dkt v* **1.** sûnùs; (*bambino*) va¤kas; ***f.*** ***adottivo*** ïvaikis; ïsûnis; ***f. maggiore (minore)*** vyriãusias (jauniãusias) sûnùs; ***f. naturale*** nesantuokínis sûnùs / va¤kas; ***f. ùnico*** vien­tu»tis sûnùs / va¤kas; *fig* ***il razzismo ê f. della paura*** rasízmas -- bãimës va¤sius; ¨ ***f. d'arte*** mìni­ninkas (/ åktorius *ir pan.*) íð mìnininkø (/ åktoriø *ir pan.*) ðeimõs; ***f. di buona donna*** nenãudëlis; ***f. di nessu­no*** pamestinùkas; ***f. di papâ*** turtíngø tëvÿ vaikìlis; *volg* ***f. di puttana*** kalºs va¤kas; *flk* ***tale padre, tale f.*** kóks t¸vas, tóks i» va¤kas; **2.:** *rel **il F.*** ***(dell'Uomo)*** (Þmoga÷s) Sûnùs.

**figliòccia** *dkt m* kríkðto dukrâ / duktº, krikðtå­duktë.

**figliòccio** *dkt v* kríkðto sûnùs, krikðtå­sûnis; krikðtåvaikis.

**figliòla** *dkt m* **1.** dukrìlë; ¨ ***f.!*** vaikìli!; **2.** *fam* (*ragazza*) merginâ.

**figliolânza** *dkt m* åtþalos *pl* (*vaikai*).

**figliòlo** *dkt v* **1.** sûnìlis; (*figlio*) sûnùs; va¤kas; ¨ ***il figliol pròdigo*** sûnùs palaid¾nas; ***f.!*** vaikìli!; **2.** (*bambino*) berniùkas; va¤kas.

**fìgo, -a** *bdv fam* kîetas; ðùstras; superínis.

**figùra** *dkt m* **1.** (*forma*) pavídalas; (*linea del corpo*) figûrâ; (*aspetto*) ísvaizda; ***f. snella*** lieknâ figûrâ; **2.** (*persona, personaggio*) figûrâ; asmenôbë; ***una mezza f.*** vidutinôbë; **3.** (*illustrazione*) pavéikslas, paveiks­liùkas, paveikslºlis; iliustråcija; (*immagine*) åt­vai­zdas; **4.** (*mo­do di mostrarsi*): ***fare una bella f.*** gera¤ pasiródyti, padarôti (gìrà) ïspûdá; ***fare una brutta f.*** menka¤ pasiródy­ti, padarôti blõgà ïspûdá; ***fare la f. dello stùpido*** apsijuõkti, apsikva¤linti; ¨ ***fa la sua f.*** visãi nebloga¤ atródo; **5.** *geom* (*t.p.* ***f. geométrica***) (geomêtrinë) figûrâ; ***f. piana*** plokðèiâ figûrâ; ***f. sòlida*** geomêtrinis k¿nas, t¾rinë figûrâ; **6.** *lett* (*t.p.* ***f. retòrica***) retòrinë figûrâ; **7.** *sport* figûrâ; **8.** (*nelle carte*) figûrâ, põninë kortâ; **9.** (*negli scacchi*) figûrâ.

**figurâccia** *dkt m*: ***fare una f.*** apsikia÷linti.

**figur|âre** *vksm* (*-gù-*) b¿ti; figûrúoti; ***il suo nome non ~a nell'elenco*** saraðê jõ va»do nërâ; ***lui ~a come proprietârio*** jís nuródomas (/ áraðôtas *ir pan.*) ka¤p savini¹kas.

**> fig|urârsi** *sngr* **1.** ásivaizdúoti; **2.:** ¨ ***~ùrati*** ***se...*** ku»gi (tãu)... ; *kaip jst* ***~ùrati!*** (/ ***si ~uri!*** *ir pan.*) ku»gi!; (*immaginati!*) tík ásivaizdúok!; (*assolutamente no!*) visãi nê!; (*prego*) pråðom!; ***~uriãmoci!*** ku»gi nê!

**figuratìv|o,** **-a** *bdv art*: ***arte ~a*** vaizdúojamasis mì­nas; ***le arti ~e*** dailº *sg*.

**figurâto, -a** *bdv*: ***in senso f.*** pérkeltine prasmê.

**figurìna** *dkt m* lipdùkas (*klijuojamas paveikslëlis*).

**figurìno** *dkt v* 1. (*disegno*) mòdelio pieðinýs; **2.** (*damerino*) dabíðius, dabitâ *com*.

**figùro** *dkt v*  (*t.p.* ***losco f.***) áta»tinas típas.

**figuróne** *dkt v*: ***fare un f.*** áspûdíngai pasiródyti.

**fìl|a** *dkt m* **1.** eilº; vírtinë, vorâ; ***in prima f.*** pirmojê eilëjê; ***fare*** ***la** **f.*** stov¸ti eilëjê; ***méttere in f.*** (su)statôti ñ e¤læ, (ið)rikiúoti; ***méttersi in f.*** stóti ñ e¤læ; ***saltare la f.*** lñsti / prasmùkti bê eilºs; ¨ ***di f.*** íð eilºs; ***in f.*** (*uno alla volta*) paeiliu¤; (*in ordine*) eilºs tvarkâ; ***in f. indiana*** vorelê; ***in f. per due*** vorâ põ dù; **2.:** *dgs fig, mil* ***~a, ~e*** grìtos; *fig* ***nelle ~a del partito*** pãrti­jos gretosê.

**filaménto** *dkt v* **1.** (*di lampadina*) si¿las (*lempu­tës*), siûlìlis; **2.** (*fibra*) pla÷ðas; *anat* skãidula.

**filânda** *dkt m* verpyklâ.

**filânt|e** *bdv*: ***stelle ~i*** serpantínas *sg* (*spalvotos popieriaus juostelës*).

**filantropìa** *dkt m* filantròpija, labdarôbë.

**filântropo, -a** *dkt v/m* filantròpas -ë, labdarýs -º.

**fil|âre I** *vksm* **1.** *tr* ve»pti; ***f. il lino*** ve»pti linùs; **2.** *tr* (*del ragno e sim.*) mêgzti; **3.** *tr tarm* (*considerare qc*) skaitôtis (*su kuo*); **4.** *intr* [A] (*del formaggio e sim.*) tñsti; **5.** *intr* [E] (*andare veloce*) lºkti, dùmti, mãuti, skúosti; ¨ ***f. lìscio*** e¤ti ka¤p svîestu pâtepta; **6.** *intr* [E] (*scappare*) iðsinìðdinti, nìðdintis; ***~a (via)!*** nìðdinkis!; **7.** *intr* [E] (*di ragionamento*) b¿ti nuosekliãm; **8.** *intr fam* [E] (*amoreggiare*) flirtúoti.

**filâre II** *dkt v* eilº (*medþiø, augalø*).

**filarmònica** *dkt m* **1.** filharmònija; **2.** (*orchestra sinfonica*) simfòninis orkêstras.

**filastròcca** *dkt m* va¤kiðkas eilëraðtùkas; (*ninna nanna*) lopðínë.

**filatelìa** *dkt m* filatêlija.

**filatélico, -a** **1.** *bdv* filatêlijos; filatêli­nis; **2.** *dkt v/m*  filatelístas -ë.

**filatelìsta** *dkt v/m* filatelístas -ë.

**filât|o, -a 1.** *bdv*: ***zùcchero f.*** saldí va­tâ, cùkraus va­tâ; **2.** *bdv fig* (*continuo*): ***due ore ~e*** íðtisas dví vålandas; ¨ *kaip prv* ***di f.*** tuõj pât; **3.** *bdv fig* (*logico*) nuoseklùs; **4.:** *dkt v* (*ppr dgs* ***~i*****)** verpala¤ *pl*; ***f. di lana*** vilnõniai verpala¤.

**filatùra** *dkt m* **1.** (*l'azione*) verpímas; **2.** (*stabilimento e sim.*) verpyklâ.

**file** [&fail] *dkt v nkt inf* fãilas; rinkmenâ; ***aprire (chiùdere, zippare) un f.*** atdidarôti (uþdarôti, suspãusti) fãilà.

**filettatùra** *dkt m tecn* sriìgis, sriìgio víjos *pl*.

**filétto** *dkt v* filº *femm inv*; ***f. di manzo*** jãutienos filº.

**filiâle I** *bdv* s¿niðkas, sûna÷s.

**filiâle II** *dkt m* filiålas.

**filibustiére** *dkt v stor* flibustjêras. 

**filiéra** *dkt m tecn* **1.** (*di elementi metallici*) staklônas; **2.** (*tessile*) verptùvas.

**filigrâna** *dkt m* **1.** vandénþenklis; ¨ ***in f.*** subtilùs *agg* (*ið karto neáþûrimas*); **2.** (*in oreficeria*) filigrånas.

**filippìno, -a** **1.** *bdv* Filipínø, filipiniìèiø; íð Filipínø; **2.** *dkt v/m* filipiniìtis -ë; **3.** *dkt v* filipiniìèiø kalbâ.

**fìlm** *dkt v nkt* fílmas; ***f. so­no­ro (mu­to, a co­lo­ri, in bian­co e ne­ro)*** garsínis (ne­garsínis, spalvótas, ne­spal­vótas) fílmas; ***f. d'animazione (d'azio­ne, del­l'orrore)*** animåcinis (ve¤ksmo, sia÷bo) fílmas; ***f. giallo*** detektývas; ***f. a luci rosse*** pornogråfinis fílmas; ***girare un f.*** statôti / sùkti fílmà; ***proiettare / dare un f.*** demonstrúoti / ródyti fìlmà.

**filmâre** *vksm* (nu)filmúoti.

**filmâto** *dkt v* va¤zdo ïraðas; filmúota mìdþiaga.

**fìl|o** *dkt v* **1.** si¿las; gijâ; (*corda*) vi»vë; (*spago*) virvìlë, ðpagåtas; ***f. di lino*** linínis si¿las; ***un gomìtolo di f.*** si¿lø kamuolýs; ***f. interdentale*** ta»pdanèiø si¿­las; ***i ~i della ragnatela*** vorå­tinklio gíjos; *tecn* ***f. a piombo*** sva§balas; *fig* ***fare i ~i*** (*del formaggio*) tñsti; ¨ ***sul f. di lana*** paskutínæ akímirkà; ***dare f. da tòrcere*** apsu¹kinti gyvìnimà; ***êssere appeso a un f.*** kab¸ti a¹t si¿lo; *fam* ***fare il f.*** (*a qcn*) kabínti (*kà*), sùktis (*apie kà*); **2.** (*metallico e sim.; t.p. **fil di ferro***) vielâ; (*cavo*) la¤das, kåbelis; ***f. d'erba*** þolºs stiebìlis; ***f. (elêttrico)*** elêktros la¤das; ***f. spinato*** spygliúota vielâ; *tecn* ***senza ~i*** beviìlis *agg*; ¨ ***f. di perle*** pe»lø vërinýs; **3.** (*piccola quantitâ*) truputºlis; (*rivolo*) èiurkðlº; ***con un f. di voce*** uþkímusiu balsù; ***non c'ê un f. di vento*** v¸jo nº kvåpo; **4.** *fig* (*dgs* ***le ~a***): ***il f. del discorso*** mintiìs si¿las; ***pêrdere il f.*** pamêsti mí¹tá; ***tirare le ~a*** apibe¹drinti; ¨ ***f. conduttore, f. rosso*** „raudóna gijâ" (*pagrindinë idëja*), leitmotývas; ***per f. e per segno*** detalia¤, kruõpðèiai; **5.** (*di una lama*) åðme­nys *pl*; briaunâ; ¨ ***sul f. del rasòio*** skustùvo að­me­nimís; **6.:** ¨ ***a f.*** lygiagreèia¤; pålei (kråðtà *ir pan.*).

**fil(o)-** *prieðdëlis:* *pirmoji sudurtiniø þodþiø dalis, reiðkianti meilæ, pomëgá, pvz.,* filântropo, filoamericano, filosofia, filosoviêtico *ir t. t.*

**-filo** *priesaga:* *antroji sudurtiniø þodþiø dalis, reiðkianti meilæ, pomëgá, pvz.,* bibliòfilo, esteròfilo *ir t. t.*

**fìlobus** *dkt v* troleibùsas; ***in f.*** troleibusù; ***il f. per il centro*** troleibùsas ñ ce¹trà; ***préndere il f.*** va­þiúoti troleibusù; (*salire*) lípti ñ troleibùsà.

**filodiffusióne** *dkt m tecn* (*laidos*) transliåcija telefòno línijomis.

**filologìa** *dkt m* filològija; ***f. bâltica*** bãltø filolò­gija, baltístika; ***f. clãssica*** klasikínë filolò­gija.

**filològico,** **-a** *bdv* filològinis.

**filòlogo**, **-a** (*v dgs* -gi/-ghi) *dkt v/m* filològas -ë.

**filóne** *dkt v* **1.** (*vena*) gôsla; (*strato*) klõdas; ***f. au­rìfero*** ãukso gôsla; **2.** (*di pane*) pranc¾ziðkas batònas.

**filosofâre** *vksm* [A] (*-lò-*) Þ **filosofeggiâre**.

**filosofeggiâre** *vksm* [A] (*-fég­-*) (pa)filosofúoti (*ir prk*).

**filosofìa** *dkt m* **1.** filosòfija; **2.** (*materia di studio*) filosòfijos istòrija, filosòfija; **3.:** *fig* ***prênderla con f.*** nusite¤kti filosòfiðkai.

**filosòfico,** **-a** *bdv* **1.** filosòfinis; **2.** *fig* filosòfiðkas.

**filòsofo** *dkt v* filosòfas.

**filovìa** *dkt m* **1.** troleibùsø línija; **2.** Þ **fìlobus**.

**filtr|âre** *vksm* **1.** *tr* (nu)kóðti, pérkoðti; filtrúo­ti (*ir prk*); ***f. l'òlio*** kóðti aliìjø; **2.** *intr* [E] su¹k­tis, prasisu¹kti; skve»btis, prasiskve»bti; fil­trúo­tis; ***l'u­mi­ditâ ~a dalla parete*** pe» sienà su¹kiasi drëgmº; **3.** *intr* [E] *fig* (*trapelare*) kílti / iðe¤ti ñ viìðumà (*ppr. sunkiai*); **4.** *intr* [E] *sport* praslôsti (prõ gyn¸jus) (*apie kamuolá*).

**fìltro I** *dkt v* fíltras; ***sigaretta senza f.*** cigarêtë bê fíltro; ***f. solare*** sãulës fíltras; *tecn* ***f. dell'âria*** óro fíltras.

**fìltro II** *dkt v* mågiðkas g¸rimas; ***un f. d'amore*** méilës eliksýras.

**fìlza** *dkt m* vírtinë (*ppr. prk*).

**fin 1.** *prlk* Þ **fìno I**; **2.** *dkt v/m* Þ **fìne II**.

**finâle** *bdv* **1.** (*ultimo*) paskutínis; (*che sta alla fine*) galínis; ***(sìllaba) f.*** paskutínis skiemuõ; **2.** (*definitivo*) galutínis; (*conclusivo*) baigiamâsis, finålinis; ***esame f.*** baigiamâsis egzåminas; ***risultato f.*** galutínis rezultåtas; **3.** *dkt v* pabai­gâ; *mus* finålas; ***il f. di un romanzo*** ro­måno pabaigâ; **4.** *dkt m sport* finålas; ***quarto di f.*** ketvi»tfinalis; **5.** *dkt m gram* (*t.p.* ***propo­sizione f.***) tíkslo aplinkôbës sakinýs.

**finalìsta** *dkt v/m* finålininkas -ë, finalístas -ë.

**finalit|â** *dkt m nkt* tíkslas; ***le ~â di un'asso­cia­zione*** draugíjos tiksla¤.

**finalizzâto, -a** *bdv* (*volto a (far) qcs*) sîekiantis (*ko, kà daryti*), skírtas; ***un decreto f. alla riduzione della spesa*** dek­rêtas, skírtas íðlaidoms måþinti.

**finalménte** *prv* (*in senso esclamativo*) pagalia÷.

**finânz|a** *dkt m* **1.** (*t.p. dgs* ***~e***) finãnsai *pl* (*ir prk*); ***Ministero delle ~e*** Finãn­sø ministêrija; **2.** (*affari*) ve»­slas; finãnsinë vei­k­lâ; ***il mondo della f.*** ve»­slo pasãulis; **3.****:** ***la Guârdia di F.,*** *fam* ***la F.*** Mókesèiø polícija.

**finanziaménto** *dkt v* finansåvimas.

**finanziâre** *vksm* finansúoti; ***f. un progetto (la ricerca)*** finansúoti projêktà (tyrimùs).

**finanziâ|rio,** **-a** *bdv* **1.** finãnsinis; fi­nãnsø; ***analista f.*** finãnsø analítikas; ***situazio­ne ~ria*** finãnsinë b¿­klë; ***legge ~ria,*** *fam* ***la ~ria*** biudþêto áståtymas; *econ* ***anno f.*** biud­þêtiniai mìtai *pl*; **2.** (*degli affari*) ve»slo.

**finanziatóre, -trìce** *dkt v/m* finansúotojas -a.

**finanziére,** **-a** *dkt v/m* **1.** finãnsininkas -ë; **2.** *mil* Mókesèiø polícijos pareig¾nas -ë.

**finché** *jngt* (tõl,) kõl; líg(i); ***f. vivevamo in campagna, eravamo felici*** kõl gyvìnome kãime, bùvome laimíngi; ***non lo dimenticherò, f. vivrò!*** lígi gôvas tõ neuþmi»ðiu; ***aspetterò qui f. (non) torniate*** lãuksiu èiâ, kõl (ne)grñðite.

**fìn|e I** *bdv* **1.** (*sottile*) plónas; lãibas; (*di sab­bia, semi e sim.*) smùlkus; ***crosta f.*** plonâ plutâ; ***ne­ve f.*** birùs sniìgas; ***piòggia f.*** smùlkus lietùs; **2.** *fig* (*acuto*) skvarbùs; subtilùs; (*esperto*) pa­týræs; (*sensibile*) jautrùs; ***f. ironia*** subtilí irònija; ***udito f.*** gerâ klausâ; **3.** (*raffinato*) sko­níngas; ámantrùs; (*distinto*) rafinúotas; ***modi ~i*** gråþios maniêros; **4.** (*aggraziato*) dailùs; grakðtùs; **5.** (*di qualitâ*) kokôbiðkas; ***cioccolato finìssimo*** aukðèiãusios kokôbës ðokolådas; **6.** *kaip prv* plona¤; smùlkiai.

**fìn|e II** **1.** *dkt m* (*parte finale*) gålas (*ir prk*); pa­baigâ; ***la f. di una strada*** kìlio pabaigâ; ***saldi di f. sta­gio­ne*** sezòninis ið­par­davímas; ***alla*** ***f.*** (*di qcs*) (*ko*) pabaigojê; ***dal­l'inìzio al­la f.*** nuõ pra­dþiõs ikí gålo; ***fino*** ***alla** **f.*** ikí gålo; *fig* ***la f. ê vicina*** (*la morte, il crollo*) art¸ja gålas; ¨ *ppr. vyr* ***il f.*** ***settimana*** savãi­t­galis; ***al­la fin f.,*** ***in fin dei conti*** galÿ galê, galiãusiai; ***in fin di vita*** léisgyvis *agg*; mírðtantis, artí mirtiìs; ***avere f.*** ba¤gtis; ***méttere f., porre f.*** uþba¤gti; (*stroncare*) padarôti gålà; ***non ê la f. del mon­do*** ta¤ dãr nê pasãulio pabaigâ; ***ê la f. del mon­do!*** (*ê meraviglioso!*) ta¤ nuostabù!; *kaip jst* ***buo­na f. e buon princìpio!*** laimíngø Naujÿjø mìtø!; **2.** *dkt m* (*esito*) baigtís; pabaigâ; ***an­da­to a buon f.*** výkæs; sëkmíngas; ¨ ***fare una brutta f.*** bloga¤ ba¤g­tis; ***che f. ha fatto?*** ku» di¹go?; kãs nutíko (*kam*)?; **3.** *dkt v* (*sco­po*) tíkslas; (*obiettivo*) siìkis; ***secondi ~i*** slaptí tiksla¤; ***f. a sé stes­so*** savítiks­lis *agg*; *gram* ***comple­mento di f.*** tíkslo aplinkôbë; ¨ ***al f.*** (*di far qc*) ida¹t (*kà darytum*), tãm, kâd; sîekiant (*kà daryti*); ***a fin di bene*** kâd gera¤ b¿tø; (*kieno*) låbui; ***il f. giustìfica i mezzi*** tíkslas patéisina prîemones.

**finêstra** *dkt m* **1.** lãngas; ***al­la f.*** priì lãngo; ***apri­re (chiùdere) la f.*** atidarôti (uþdarôti) lãngà;  ***gu­ar­da­re dal­la f.*** þiûr¸ti prõ lãngà; ***la f. dâ sul cor­ti­le*** lãngas iðe¤na ñ kiìmà; **2.** *inf* lãngas.

**finestrìno** *dkt v* langìlis; langiùkas; lãngas; ***un posto vicino al f.*** vietâ priì lãngo.

**finézza** *dkt m* **1.** (*sottigliezza*) plonùmas; smulkù­mas; **2.** *fig* (*acutezza*) skvarbùmas; subtilù­mas; ***f. d'inge­gno*** prõto áþvalgùmas; **3.** (*raffi­natezza*) skõnis, sko­ningùmas; rafi­nuo­tùmas; **4.** (*grazia*) grakðtùmas; dailùmas.

**fìng|ere*** *vksm* **1.** (*qcs, di far qcs*) apsimêsti (*kà da­rant*), d¸tis; ***f. stupore*** apsimêsti nustìbusiam; ***~e di dormire*** apsímeta miìgantis; ***~e di êssere malato*** apsímeta li­góniu; apsímeta, kãd se»ga; ***ha finto di êssere un giornalista*** apsímetë es±s þurnalístas; **2.** (*immaginare*) ásivaizdúoti; (*supporre*) ta»ti.

    **> fìng|ersi** *sngr* (*qc*) apsimêsti (*kuo*), d¸tis; ***f. un altro*** d¸tis kuõ nórs kitù; ***f. un esperto*** apsi­mêsti þino­vù; ***si ~e malato*** apsímeta, kãd se»ga; apsí­meta se»gantis.

**finiménti** *dkt v dgs* paki¹ktai.

**finimóndo** *dkt v* chaòsas; ***ê successo il f.*** kílo (tí­kras) chaòsas.

**finì|re** *vksm* (*-**ì**sc-*) **1.** *tr* (pa)ba¤gti, uþba¤g­ti; ***f. un lavoro*** ba¤gti dãrbà; ***f. di lavora­re*** ba¤gti dírbti; ***f. gli stu­di (l'universitâ)*** ba¤gti mókslà (universitêtà); ***ho ~to di mangia­re*** pâbaigiau vãlgyti, pavãlgiau; ***con me hai ~to*** sù manimí ba¤gta; ¨ ***a non f.*** bê gålo (bê krå­ðto); **2.** *tr* (*consumare*) suvartóti; (pa)ba¤gti; ***f. l'âcqua*** ba¤gti vãndená; ***f. una medicina*** suvartóti vaistùs; ***f. una sigaretta*** surûkôti cigarêtæ; **3.** *tr* (*smettere di far qcs*) liãutis (*kà dary­ti, kà pa­da­rius*); ***~sci di lamentarti!*** ganâ sk¼stis!, liãukis skùndæ­sis!; *beasm* ***ha ~to di piòvere*** lióvësi lôti; **4.** *tr* (*uccidere*) priba¤gti; **5.** *intr* [A]: ***f. per far qcs, f. con il far qcs*** galÿ galê (*kà da­ryti*); ***ha ~to con l'accetta­re*** galÿ galê sutíko; **6.** *intr* [E] ba¤gtis, pasiba¤gti; ***la strada ~sce qui*** kìlias ba¤giasi èiâ; ***la*** ***lezione** **ê ~ta** **alle** **tre*** pamokâ pasíbaigë trìèià; ***l'es­ta­te sta fi­nen­do*** ba¤gia­si våsa­ra; ¨ *kaip dkt v **sul f.*** (*di qcs*) (*ko*) pabaigojê, (*kam*) besiba¤giant; *beasm* ***ê ~ta!*** vískas ba¤gta!; ***(e) non ~sce qui, (e) non ê ~ta qui*** i» ta¤ dãr nê vískas; *flk* ***tutto ê bene quel che ~sce bene*** vískas gera¤, kâs gera¤ ba¤giasi; **7.** *intr* [E] (*consu­marsi*) ba¤gtis, pasiba¤gti; iðsiba¤gti; ***ê ~­to l'in­chiost­ro*** råða­las pasíbai­gë; **8.** *intr* [E] (*in un luogo, anche fig; t.p. **andare a f.***) patêkti; atsidùrti; pakli¿­ti; ***f. sotto una mâcchina*** pa­kli¿ti põ råtais; ***f. in un fosso*** áva­þiúoti ñ griõvá; ***f. in un vìcolo cieco*** patêkti ñ aklåvietæ (*ir prk*); ***f. nei gu­ai*** patêkti ñ bºdà; ¨ ***dov'ê ~to?*** ku» pasid¸jo?; **9.** *intr* [E] (*aver esito*) ba¤gtis; ***f. bene*** gera¤ ba¤gtis; iðe¤ti ñ gìrà; ***f. male*** þlùg­ti; praþ¿ti; **10.** *intr* [E] (*sfociare*) iðtêkti.

  **> finì|rla** *ávdþ fam* ba¤gti; ***ê ora di f. con queste assurditâ!*** la¤kas ba¤gti sù tõm nesçmonëm!

**finìt|o,** **-a** *bdv* **1.** pâbaigtas, ba¤gtas; pasiba¤gæs; ***pro­dotto f.*** gaminýs; ***a lavoro f.*** paba¤gus dãrbà; ***a festa ~a*** pasiba¤gus vakarºliui; *t.p.* Þ **finìre**; ¨ *fam **farla ~a*** liãutis; (*con qcn*) nutrãukti (visùs ryðiùs) (*su kuo*); (*suicidarsi*) pasi­darôti (sãu) gålà; **2.** (*senza futuro*) þlùgæs; **3.:** *gram* ***modo f.*** núosaka.

**finitùr|a** *dkt m* apdailõs darba¤ *pl*, apdailâ; *dgs* ***~e*** apdailõs detålës, apdailâ *sg*, (*ornamenti*) papuoðí­mai.

**finlandése** **1.** *bdv* Súomijos, súomiø; súomiðkas; íð Súomijos; ***sâuna f.*** sãuna, súomiðka pirtís; **2.** *dkt v/m* súomis -ë; **3.** *dkt v* súomiø kalbâ.

**fìno I** *prlk* **1.** (*a qc*) ikí (*ko*); líg(i); ***f. all'alba*** ikí auðrõs; ***f.*** ***alla** **fine*** ikí gålo; ***f. a*** ***domani*** ikí rytójaus; ***f. ad ora*** ikí ðiõl; ***contare f. a dieci*** skaièiúoti ikí dìðimt; ***dormire f. a tardi*** ilga¤ miegóti; ¨ *kaip jngt* ***f. a quando*** (ikí) kõl; tõl, kõl; **2.:** ***fin*** (*da qcs*) nuõ (*ko*); ***fin*** ***dalla** **nâscita*** nuõ pât gimímo; ***fin da pìccolo*** (ja÷) nuõ maþe¹s; **3.** *kaip prv*: ***fin*** ***troppo*** nêt pe» da÷g.

**fìno II, -a** *bdv* **1.** smùlkus; ***sale f.*** smùlki druskâ; **2.** *fig* (*acuto*) aðtrùs; ***cervello f.*** guvùs prõtas; **3.** (*puro*) grônas; ***oro f.*** grônas ãuksas; **4.** *fam* rafinúotas; ¨ ***avere il palato f.*** b¿ti iðrankiãm.

**finòcchio** *dkt v* **1.** *bot* pa¹kolis; **2.** *volg* pederåstas.

**finóra** *prv* ikí ðiõl, lígi ðiõl; kõl kâs.

**fìnta** *dkt m* **1.** apsimetímas; ***fare*** ***f.*** (*di far qcs*) apsimêsti (*kà darant*); ***fare*** ***f.** **di non sentire*** nuléisti negirdomís; ***ê tutta una f.*** tík apsime­ti­n¸ja; ***fa f. di non capire*** apsí­me­ta nesupran­t±s; apsí­me­ta, kâd nesupra¹ta; ¨ ***per f.*** juoka¤s; **2.** *sport* apgaulíngas / klaidínantis judesýs, fíntas.

**fìnt|o, -a** *bdv* **1.** netíkras; (*artificiale*) dirbtínis; (*simulato*) apsimestínis; ***nome f.*** netíkras va»­das; ***pelle ~a*** dirbtínë óda; **2.:** ¨ ***fare il f. ton­to*** d¸tis kvailiù.

**finzióne** *dkt m* **1.** apsimetin¸jimas; fíkcija; **2.: *f. poêtica*** poêtinë íðmonë; **3.:** ***f. ci­nematogrâfica*** kíno / fílmo fíkcija.

**fìo** *dkt v*: ¨ ***pagare il f.*** (*esser punito*) gãuti åtpildà.

**fiocc|âre** *vksm* [E] (*fiòc-*) **1.** *tarm* snígti (kçsniais); **2.** *fig* pasipílti; ***~ano gli applâusi*** pasípila plojímai.

**fiòc|co** *dkt v* **1.** kåspinas (*perriðtas*); ¨ ***con i ~chi*** puikùs *agg*; **2.:** ***f.** **di cotone*** vatõs kuokðtìlis; ***f. di** **neve*** sna¤gë; **3.** (*di cereali*) dríbs­nis; ***~chi di granturco*** kukur¾zø dríbsniai.

**fiòcina** *dkt m* þebérklas.

**fiòco, -a** *bdv* (*di luce*) blankùs, blau­sùs; (*di suono*) duslùs; ki­mùs.

**fiónda** *dkt m* **1.** laidýnë; **2.** (*l'arma*) svaidýklë.

**fiorâio, -a** *dkt v/m* gºlininkas -ë (*par­da­vëjas*), gëliÿ pardav¸jas -a

**fiordalìso** *dkt v bot* rùgiagëlë, rugiågëlë.

**fiordilâtte** *dkt v* (*gelato*) plombýras.

**fiórdo** *dkt v geogr* fiòrdas.

**fiór|e** *dkt v* **1.** gëlº; þîedas; ***mazzo di ~i*** gëliÿ púokðtë; ***méttere i ~i nell'âcqua*** (pa)me»kti gëlês; ¨ ***a*** ***~i*** gël¸tas *agg*; ***f. all'occhiello*** paþibâ, pasidi­dþiå­vimas; ***(tutto) rose e ~i*** rõþëmis klótas *agg*; ***êssere in f.*** þyd¸ti; ***méttere i ~i*** praþôsti; **2.** *fig* (*t.p.* ***il fior f.***) elítas; grietinºlë; þîedas; ***il fior f. degli atleti*** pajëgiãusi atlêtai; ¨ ***fior*** (*di qcs*) (*gran quantitâ*) (*ko*) galôbës; ***nel f. degli anni*** paèiamê þyd¸jime; ***un fior*** (*di qcs*) (*un autentico*) tíkras *agg*; **3.:** ***a fior di qcs*** kõ pavi»ðiuje; ¨ ***ho i nervi a fior di pelle*** âð ba¤­siai susinêrvinusi; **4.:** *dgs **~i*** (*nelle carte*) krýþiai (*kortu spalva*).

**fiorellìno** *dkt v dimin* gëlôtë (*ir prk*).

**fiorénte** *bdv fig* klìstintis.

**fiorentìna** *dkt m gastr* (*t.p.* ***bistecca alla f.***) jãu­tie­nos kepsnýs (sù T fòrmos kãulu).

**fiorentìno,** **-a** **1.** *bdv* Florêncijos, florentiìèiø; florentiìtiðkas; íð Florêncijos; **2.** *dkt v/m* florentiìtis -ë; **3.** *dkt v* florentiìèiø tarmº.

**fiorétto** *dkt v* *sport* rapyrâ.

**fioricoltóre, -trìce** *dkt v/m* gºlininkas -ë.

**fioricoltùra** *dkt m* gëlininkýstë.

**fioriéra** *dkt m* vazònas, gëliåpuodis.

**fiorìno** *dkt v* (*moneta*) florínas.

**fior|ìre** *vksm* [E] (*-**ì**sc-*) **1.** (su)þyd¸ti, praþôsti; ¨ *flk* ***se son rose, ~iranno*** jéi ta¤p lémta, ta¤p i» bùs (*„jei roþës, tai praþys"*); **2.** *fig* (su)­klest¸ti; veð¸ti; **3.** *fig* (*avere macchie*) þyd¸ti; (*della pelle*) (ið)be»ti.

**fiorìsta** *dkt v/m* gºlininkas -ë.

**fioritùra** *dkt m* **1.** þyd¸jimas; **2.** *fig* (su)­klest¸ji­mas; **3.** *fig* (*comparsa di macchie*) (ið)bërímas.

**fiòtt|o** *dkt v* èiurkðlº; ***uscire a ~i*** trôkðti; pl¿sti.

**fìrma** *dkt m* **1.** påraðas; ***una f. falsa (illeggìbile)*** padírbtas (neáska¤tomas) påra­ðas; ***apporre / méttere una f.*** d¸ti påra­ðà, pasiraðôti; ¨ ***farci la f., métterci la f.*** pasiraðôti (*sutikti ne­dve­jojus*); **2.** (*il firmare*) pasiråðymas.

**firmaménto** *dkt v* danga÷s sklia÷tas.

**firmâre** *vksm* pasiraðôti; ***f. un accordo interna­zionale (un contratto)*** pasiraðôti tarptautíná su­sita­rímà (sùtartá); ***f. un autògrafo*** pasiraðôti autogråfà.

**firmatârio, -a** *dkt v/m* (*dokumentà ir pan.*) pasiråðæs asmuõ, pasiråðantysis -èioji; signatåras -ë.

**firmâto, -a** *bdv*: ***âbito f.*** garsiõs fírmos þénklu paþym¸tas kostiùmas; garsa÷s dizãinero (/ garsiõs dizãinerës) suknìlë.

**fisarmònica** *dkt m mus* akordeònas.

**fiscâle** *bdv* **1.** mókesèiø; fiskålinis; ***condono f.*** mókesèiø amnêstija; ***evasione f.*** mókesèiø vengímas; ***ricevuta f., scontrino f.*** èêkis, kvítas; ¨ ***còdice f.*** mókesèiø mok¸tojo kòdas; @ asme¹s kòdas; **2.** *fig* grîeþtas; pedãntiðkas.

**fischiâre** *vksm* (*f**ì**-*) **1.** *intr* [A] (su)ðviµpti; (*di sirena e sim.*) (su)ka÷kti; (*cupamente*) ¾kti; (*sibilare*) ðnýpðti; (*passar sibilando*) praðviµpti; ***mi fìschiano le orécchie*** mãn ausysê zvi§bia (*ir prk*); **2.** *intr* [A] (*pronunciare male*) ðveplúoti; **3.** *tr* (*fischiettare*) ðvílpauti; **4.** *tr* (*disapprovare*) nuðviµpti; **5.** *tr* *sport* paskírti (*ðvilpiant*); ***f. la fine della partita*** skélbti rungtýniø påbaigà; ***f. un fallo*** uþfiksúoti pråþangà.

**fischiettâre** *vksm* (*-schiét-*) ðvílpauti.

**fischiétto** *dkt v* ðvilpùkas.

**fì|schio** *dkt v* ðvilpímas; (*sibilo*) ðvil­pesýs; ***coperto dai ~schi*** nùðvilptas; ***fare un f.*** paðviµpti; ¨ ***capire ~schi per fiaschi*** visãi nê ta¤p suprâsti; ***fammi un f.*** dúok þinóti / þínià.

**fìsco** *dkt v* íþdas; (*il sistema tributario*) mókesèiø siste­mâ; (*l'ispettorato*) mókesèiø inspêkcija.

**fìsica** *dkt m* **1.** fízika; ***le leggi della f.*** fízikos d¸sniai; ***manuale di f.*** fízikos vado­vºlis; *fam **iscrìversi a f.*** ástóti ñ fízikos fa­kultêtà; **2.** *fam* (*ginnastica*) k¿no kultûrâ.

**fisicaménte** *prv* **1.** fíziðkai; **2.:** ***com'ê f.?*** ka¤p jí atródo?

**fìsi|co, -a 1.** *bdv* fízinis; (*della fisica*) fízikos; *fis* fizikínis; ***geo­grafia ~ca*** fízinë geogråfija; ***leggi ~che*** fízikos d¸sniai; *fis* ***corpo f.*** fizikínis k¿nas; **2.** *bdv* (*del corpo umano*) fízinis; ***attivitâ ~ca*** fízinë veiklâ; ***attrazione ~ca*** fízinis pó­traukis; ***difetto f.*** fízinis tr¿kumas; ***educazione ~ca*** k¿no kultûrâ; ***piacere f.*** fízinis malonùmas; **3.** *dkt v* (*corpo*) k¿­nas; k¿­no sudë­jímas; ***fa bene al f.*** ta¤ sve¤ka k¿­nui; *fam* ***avere il f.*** b¿ti tvírto sudëjímo; b¿ti atlê­tiðkam; **4.** *dkt v/m* fízikas -ë.

**fìsima** *dkt m* (keistâ) månija; ùþgaida.

**fisiologìa** *dkt m* fiziològija.

**fisiològico,** **-a** *bdv* **1.** *med* fiziològinis; **2.** *fig* (*nella nor­ma*) ñprastas; natûralùs.

**fisionomìa** *dkt m* fizionòmija; véido brúoþai *pl*.

**fisionomìsta** *dkt v/m*: ***êssere un buon f.*** gera¤ ásimi¹­ti þmónes (/ véidus *ir pan.*).

**fisioterapìsta** *dkt v/m* fizioterapêutas -ë.

**fisioterapìa** *dkt m med* fizioteråpija.

**fissâre** *vksm* **1.** (pri)tvírtinti; (uþ)fiksúoti; (*cucendo*) prisi¿ti; (*legando*) priríðti; (*con chiodi*) prikãlti; (*con ganci*) prikabínti; (*con fermagli*) prisêgti; (*con spilli*) prisme¤gti; ***f. i capelli con la lacca*** uþfiksúoti plãukus lakù; *fig* ***f. il pen­siero, f. la mente*** (*su qcs*) nukre¤pti mi¹tá (*á kà*); **2.** (*qc*) (*t.p. **f. gli occhi, f. lo sguardo*** (*su qc*)) ásiþiûr¸ti (*á kà*); sme¤gti þviµgs­ná, ábêsti akís; ***f. il cielo*** ádëmia¤ þiûr¸ti ñ da¹gø; **3.** (*stabilire*) nustatôti; (pa)skírti; fiksúoti; ***f. un appunta­mento*** paskírti pasimåtymà; (*per una visita e sim.*) uþ(si)registrúoti, uþ(si)raðôti; ***f. la pròpria residenza*** nustatôti gyvìnamàjà viìtà; **4.** (*registrare*) (uþ)fiksúoti; **5.** *fot* (uþ)fiksúoti.

    **> fissârsi** *sngr* **1.** (*nella mente*) ásimi¹ti; (*dello sguardo su qc*) uþkli¿ti (*uþ ko*); **2.** (*stabilirsi in un luogo*) apsigyvénti; **3.** *fig* (*ostinarsi*) ásikãlti ñ gãlvà; uþsispírti; **4.** (*l'un l'altro*) ádëmia¤ þiû­r¸ti ñ akís (*vienam kitam*).

**fissâto,** **-a** **1.** *bdv, dlv* pritvírtintas; *t.p.* Þ **fissâ­re**; **2.** *bdv, dlv* (*stabilito*) nustatôtas; pa­skír­tas; **3.** *bdv* (*ossessionato*) (*ákyrios minties*) aps¸stas.

**fissazióne** *dkt m* (*ossessione*) ákyrí mintís -iìs; apsë­dímas.

**fissióne** *dkt m chim, fis* skilímas.

**fìss|o,** **-a** *bdv* **1.** pritvírtintas; nêjudantis; *fig* ***con gli occhi ~i*** (*su qc*) ábìdæs akís (*á kà*); ¨ ***chiodo f., idea ~a*** ákyrí mintís; apsëdímas; *t.p.* Þ **fissâre**; **2.** (*stabilito*) nustatôtas; tãm tíkras; ***ricevere (il pùbblico) a ore ~e*** prii§ti interesãntus tãm tikromís valandomís; ***senza ~a dimora*** bê nuolatínë gyvìnamosios viìtos; **3.** (*stabile*) pastovùs; ***lavoro f.*** pastovùs dãrbas; ***rêddito f.*** pastõvios påjamos; **4.** *kaip prv*: ***guardare f.*** (*qc; qcn negli occhi*) ádëmia¤ þiûr¸ti (*á kà; kam á akis*).

**fìtta** *dkt m*  dieglýs; ***sento una f. al fianco*** (mãn) dîegia ðónà; (*a intervalli*) dílgèioja ðónà.

**fittìzio,** **-a** *bdv* fiktyvùs, netíkras; ***ma­trimònio f.*** fiktyví sãntuoka.

**fìtt|o I,** **-a** **1.** *bdv* tãnkus; ***erba ~a*** tãnki þolº; ***tessuto f. / a trama ~a*** tãnkus audinýs; *fig* ***colpi ~i*** daþní sm¾giai; **2.** *bdv* (*denso*) ti»ðtas; ***bùio f.*** tamsù, nórs ñ åká dùrk; gûdí tamsã (*ir prk*); ***nébbia ~a*** ti»ðtas r¾kas; *fig* ***una giornata ~a di impegni*** dienâ pilnâ darbÿ i» susitikímø; **3.** *dkt v* tankmº, tankýnë; **4.** *kaip prv*: ***piove f.*** sma»kiai lýja; **5.** *bdv* (*confitto*) ásme¤gtas; ásmígæs; ¨ ***a capo f.*** staèiâ gãlva.

**fìtto II** *dkt v* Þ **affìtto**.

**fiumâna** *dkt m* **1.** galínga srovº; **2.** *fig* (*di gente*) didþiùlis þmoniÿ sra÷tas.

**fiùm|e** *dkt v* **1.** ùpë; ***f. navigabile*** laivúojama ùpë; ***il f. ê straripato*** ùpë iðsilîejo (íð krantÿ); **2.** *fig* sra÷­tas; ùpë; ***un f. di lâcrime (di parole)*** åðarø (þõ­dþiø) sra÷tas; ***~i di sângue*** kra÷jo ùpës; ***sono stati scritti ~i di inchiostro*** tîek råðalo iðlîeta; ¨ ***scòrrere a ~i*** tek¸ti ùpëmis; **3.** *fig* (*di gente*) Þ **fiumâna 2.**; **4.** *kaip bdv nkt*: ***un romanzo f.*** romånas, kuriõ pabaigõs nematôti.

**fiutâre** *vksm* **1.** (uþ)úosti; suúosti; (*con insi­sten­za*) uostin¸ti; ðniukðtin¸ti; (*annusare*) (pa)úo­styti; ***f. la preda*** uþúosti grõbio kvåpà; **2.** (*aspirare*) (pa)­úostyti; trãukti; ***f. cocaina*** úostyti kokaínà; **3.** *fig* (*intuire*) uþúosti, suúostyti *fam*; *fam **f. l'affare*** su­úostyti bízná.

**fiùto** *dkt v* **1.** uoslº (*ir prk*); ***dal f.*** ***buono*** uoslín­gas *agg*; *fig* ***avere f.*** (*per qcs*) tur¸ti uõslæ (*kam*); **2.:** ***tabacco da f.*** úostomasis tabåkas.

**flâccido, -a** *bdv* suglìbæs, glebùs.

**flacóne** *dkt v* flakònas, buteliùkas.

**flagellâre** *vksm* (*-gêl-*) **1.** (nu)plãkti rykðtê, (nu)plîe­k­ti; **2.** (*del vento e sim.*) (nu)gãirinti, (nu)èãiþy­ti; **3.** *fig* (*criticare*) (ið)plîekti.

**flagêllo** *dkt v* rôkðtë (*ir prk*); *fig* ***il f. divino*** Diìvo rôkðtë.

**flagrânte** *bdv* *dir* akivaizdùs (*ir prk*); ***f. delitto*** a­ki­vai­zdùs nusikaltímas; ***cògliere in f.*** nutvérti (*nusikalstant*).

**flaménco** *dkt v* flame¹kas.

**flanêlla** *dkt m* flanêlë; ***di f.*** flanelínis *agg*.

**flash** [fl0^] *dkt v nkt* **1.** *fot* blôkstë, fotoblôkstë; **2.** *fig* (*t.p.* ***f. d'agenzia***) skubùs (agent¾ros) prane­ðímas; *kaip bdv nkt* ***telegiornale f.*** trumpâ þiniÿ laidâ; **3.** *fig fam*: ***mi ê venuto un f.*** mãn tóptelëjo.

**flautìsta** *dkt v/m* fle¤tininkas -ë.

**flâuto** *dkt v* fleitâ; *mus* ***f. diritto / dolce (traverso)*** iðil­gínë (skersínë) fleitâ; ***suonare il f.*** grîeþti fle¤tà, gróti fleitâ.

**flébile** *bdv* võs gi»dimas, siµpnas.

**flébo** *dkt m nkt fam* Þ **fleboclìsi**.

**fleboclìsi** *dkt m nkt med* laðelínë.

**flêmma** *dkt m fig* flegmâ; ***con un po' troppa f.*** ganâ flegmåtiðkai.

**flemmâtico,** **-a** *bdv* flegmåtiðkas.

**flessìbile** *bdv* lankstùs (*ir prk*); ***tubo f.*** lankstùs va§z­dis; þarnâ; *fig* ***orãrio f.*** la¤s­vas dãrbo gråfikas.

**flessibilitâ** *dkt m* lankstùmas (*ir prk*).

**flessióne** *dkt m* **1.** (su)lenkímas; (*incurvamento*) iðlenkímas; **2.** (*l'esercizio ginnico*) atsispaudí­mas; **3.** *fig* núosmukis; (*calo*) sumaþ¸jimas; **4.** *ling* ka¤ty­mas, flêksija.

**flessuóso, -a** *bdv* **1.** lankstùs; **2.** (*con anse*) vingrùs.

**flêttere** *vksm* **1.** (su)le¹kti; **2.** *ling* kaitôti.

**flìpper** *dkt v nkt* kíniðkasis biliãrdas.

**flirt** [flY:t, flert] *dkt v nkt* flírtas.

**flirtâre** *vksm* [A] flirtúoti; tampôtis.

**flop** [flEp] *dkt v nkt* nenusisìkæs dalýkas.

**floppy disk** [flEppi&disk] *dkt v nkt inf* diskìlis.

**flòra** *dkt m* **1.** *bot* florâ, augmenijâ, augalijâ; **2.:** *biol* ***f. in­tes­ti­na­le*** vírðki­ni­mo florâ.

**floreâle** *bdv* **1.** gëliÿ; ***omâggio f.*** gëliÿ púokðtë, gºlës *pl*; **2.****:** ***stile f.*** „*art nouveau*".

**floricoltùra** *dkt m* Þ **fioricoltùra**.

**floricultóre, -trìce** *dkt v/m* Þ **fioricoltóre**.

**floricultùra** *dkt m* Þ **fioricoltùra**.

**flòrido, -a** *bdv* **1.** sódrus, veðlùs; **2.** *fig* (*fiorente*) klìstintis; **3.** *fig* (*sano*) þôdintis.

**flòscio, -a** *bdv* iðglìbæs, suglìbæs; nestangrùs, nestandùs.

**flòtta** *dkt m* laivônas; ***f. aerêa*** óro laivônas.

**flottìglia** *dkt m* flotílë.

**fluénte** *bdv* **1.** tìkantis; tekùs; **2.** *fig* (*di scritto e sim.*) sklandùs; **3.** *fig* (*di barba, capelli*) veðlùs.

**flùido,** **-a** **1.** *bdv* skôstas; takùs; **2.** *bdv* *fig* (*scorre­vole*) sklandùs; **3.** *bdv* *fig* (*mutevole*) nenusisto­v¸­jæs -usi; **4.** *dkt v fis* skýstis; **5.** *dkt v* (*fascino*) þavesýs.

**fluìre** *vksm* [E] (*-**ì**sc-*) **1.** sruvénti, tek¸ti; **2.** *fig* (*del tempo*) sli¹kti, tek¸ti.

**fluorescénte** *bdv* fluoresce¹cinis.

**fluòro** *dkt v chim* flúoras.

**flùsso** *m* **1.** tek¸jimas; sra÷tas; srovº; ***f. di âria cal­da*** ðiµto óro sra÷tas; **2.** *fig* sra÷tas; ***f. del trâffico*** e¤s­mo sra÷tas; *fis **f. di elet­tro­ni*** elektrònø sra÷tas; *inf **f. di da­ti*** duo­menÿ sra÷tas; *lit **f. di cos­cien­za*** sçmo­nës sra÷tas; **3.:** ***il f. del tempo*** la¤ko tëkmº; **4.** *geogr* pótvynis.

**flùtto** *dkt v* (j¿ros) bangâ, vilnís -iìs *femm*.

**fluttuânte** *bdv econ, fin, fig* svyrúojantis.

**fluttuâre** *vksm* [A] (*flùt-*) **1.** plãukioti; pl¾duriuoti; (*on­­deggiare*) liû­liúoti; (*per aria*) draikôtis orê; **2.** (*oscillare, anche econ*) svyrúoti.

**fluttuazióne** *dkt m econ* svyråvimas.

**fluviâle** *bdv* ùpiø, ùpës; ùpinis; ***battello f.*** ùpiø / ùpinis kéltas; ***porto f.*** ùpës úostas.

**fobìa** *dkt m med* fòbija; *fig* ***avere una f.*** (*per qcs*)***, avere la f.*** (*di qcs*) nek³sti (*ko*).

**-fobìa** *priesaga:* *antrasis sudurtiniø þodþiø dëmuo, reið­kianti ko nors baimæ, vengimà, prieðiðkumà, neprisitaikymà su kuo nors, pvz.,* agorafobia, idrofobia, xenofobia *ir t. t.*

**-fobo** *priesaga:* *antrasis sudurtiniø þodþiø dëmuo, rodantis, kad kas bijo, vengia ko, neprisitaikæs prie ko ar prie­ðiðkas kam, pvz.,* claustròfobo, xenòfobo *ir t. t.*

**fòca** *dkt m zool* rúonis.

**focâccia** (*dgs* -ce) *dkt m* paplotºlis; ¨ ***réndere pan per f.*** atsilôginti tuõ paèiù (*uþ blogá*).

**focâle** *bdv* **1.** *spec* þídinio; þidiníns; *fis* ***distanza f.*** þídinio notolis; **2.** *fig* pagrindínis.

**focalizzâre** *vksm* **1.** *fot* (su)fokusúoti; **2.:** *fig* ***f. l'at­tenzione*** (*su qc*) suteµkti dºmesá (*á kà*).

**fóce** *dkt m* þiótys *pl*; *geogr* ***f. a delta*** dêlta.

**focolâio** *dkt v med, fig* þidinýs.

**focolâre** *dkt v* **1.** þidinýs; ***al f.*** priì þídinio; **2.:­** *fig* ***il f. domêstico*** namÿ / ðeimõs þidinýs.

**focóso, -a** *bdv* ugníngas; aistríngas, ûmùs.

**fòdera** *dkt m* (*interna*) påmuðalas; (*esterna*) åp­muða­las; (*di cuscino, piumino*) ùþvalkalas.

**foderâre** *vksm* (*fò-*) (*qcs*) (á)d¸ti / (á)si¿ti påmuðalà (*kam*); ***f. una giacca*** d¸ti ðva»kui påmuðalà.

**fòdero** *dkt v* makðtís -iìs *femm*; (*custodia*) dºklas.

**fóga** *dkt v* uþsidëgímas; ïkarðtis; ***con f.*** ánirtíngai; sù ïkarðèiu; ***preso*** ***dalla** **f.*** impùlso / ïkarðèio pagãutas; ¨ ***per la f.*** (*fretta*) dºl skub¸jimo.

**fòggia** *dkt m* mòdelis; (*forma*) pavídalas; (*taglio di un abito*) fasònas, sukirpímas; ¨ ***a f.*** (*di qcs*) (*ko*) pavídalo.

**foggiâre** *vksm* (*fòg-*) sute¤kti (*kam*) (*ko*) pavídalo; (su)modeliúoti (*kà*).

**fò|glia** *dkt m* **1.** låpas (*augalo*), lapìlis; ***~glie secche*** sausí låpai; ***~glie di tê*** arbåtlapiai; ***le ~glie câdono*** kri¹ta låpai; ***méttere le ~glie*** (su)lapóti; ¨ ***mangiar la f.*** pérmanyti (*kà ne­gera*), nuja÷sti; ***trema come una f.*** vírpa ka¤p epuðºs låpas; **2.** (*lamina*) låkðtas.

**fogliâme** *dkt v* lapijâ, låpai *pl*.

**fogliétto** *dkt v* lapìlis; (*bigliettino scritto*) raðtìlis.

**fòglio** (*dgs* -gli) *dkt v* **1.** (põpieriaus) låpas, låk­ðtas; (*pagina*) pùslapis; ***f. a righe (a quadretti)*** liniúotas (langúotas) låpas; *inf* ***f. di câlcolo*** skaièiuõklë; **¨** ***f. protocollo*** *toks* liniúotas põpieriaus låpas (*nestandartinio 31x42 cm formato, ppr. dokumentams*); ***f. volante*** ádëtínis låpas; **2.** (*modulo*) blãnkas; (*documento*) dokume¹tas; ¨ ***f. di congedo*** paleidþiamâ­sis paþym¸jimas; ***f. rosa*** leidímas vairúoti (*besi­ruoðiantiems vairavimo egzaminui*); ***f. di via (ob­bligatòrio)*** iðsiuntímo íð ðaliìs òrderis; **3.** (*lamina*) låkðtas, låpas.

**fógn|a** *dkt m* **1.** (kanalizåcijos) kolêktorius; *dgs* ***~e*** kanalizåcija *sg*; **2.** *fig* (*luogo sozzo*) kloakâ; **3.** *fig fam* (*ingordo*) ëdr¾nas -ë, rij¾nas -ë.

**fognatùra** *dkt m* kanalizåcija.

**folâta** *dkt m* (*t.p.* ***f. di vento***) v¸jo g¾sis, ðúoras.

**folclóre** *dkt v* folklòras, liãudies kûrôba; (*fiabe e leggende*) tautósaka.

**folclorìstico, -a** *bdv* folklòrinis; folklòro.

**folgorâre** *vksm* (*fól-*) **1.** (nu)tre¹kti; **2.** *fig* apåkinti; ***f. con lo sguardo*** pérverti þvilgsniù.

**folgorazióne** *dkt m* **1.** nutrenkímas elektrâ; **2.** *fig* staigùs prareg¸jimas.

**fólgore** *dkt m* þa¤bas.

**folklóre** *dkt v* Þ **folclóre**.

**folklorìstico, -a** *bdv* Þ **folclorìstico**.

**fòlla** *dkt m* **1.** miniâ; ***pieno di f.*** pílnas þmoniÿ; ***una f. inferocita*** át¿þusi miniâ; ***farsi largo tra la f.*** skve»btis / brãutis prõ mínià; ***mescolarsi alla folla, confóndersi tra la folla*** ásimaiðôti / ásitrínti ñ mínià; **2.** *fig* daugôbë, bìgalë.

**fòll|e** **1.** *bdv* (*di qcs*) beprõtiðkas (*ir prk*); (*insen­sato*) bepråsmis; ***un gesto f.*** beprõtiðkas póelgis; *fig* ***prezzi ~i*** beprõtiðkos kãinos; **2.** *bdv* (*di qcn*) iðprot¸jæs, pamíðæs; ***f. d'a­more*** beprõtiðkai ásimyl¸­jæs; pakva¤ðæs íð méilës; **3.:** *bdv tecn*: ***êssere in f.*** ve¤kti tuðèiçja eigâ; (*andare senza marcia*) vaþiúoti laisvâ påvara; ***méttere in f.*** ájùngti la¤svà påvarà; **4.** *dkt v/m* beprõtis -ë (*ir prk*), pamíðëlis -ë.

**folleggiâre** *vksm* [A] (*-lêg-*) d¾kti; siãutëti, ðºlti.

**folleménte** *prv* beprõtiðkai; bê prõto.

**follétto** *dkt v mit* ãitvaras (*ir prk*); ka÷kas.

**follì|a** *dkt m* **1.** beprotôbë, pamiðímas; ***f. collet­tiva*** visúotinis pamiðímas; ¨ ***alla f.*** ikí pami­ðí­mo, beprõtiðkai; ***fare ~e*** beprõtiðkai eµgtis; (*divertirsi*) d¾kti; **2.** (*azione folle*) beprotýstë; ***ê una f.!*** ta¤  beprotýstë!; **3.:** *fig* ***spéndere una f.*** iðléisti beprõtiðkus pínigus.

**fólt|o,** **-a** **1.** *bdv* tãnkus; veðlùs; ***capelli ~i*** tãnkûs plauka¤; **2.** *bdv* (*numeroso*) gausùs; (*di qc*) apstùs (*ko*); ***~e schiere di ammiratori*** ga÷­sios gerb¸jø grìtos; *fig* ***un testo f. di citazioni*** têkstas, ku» apstù citåtø; **3.** *dkt v* tankmº.

**fomentâre** *vksm* (*-mén-*) (su)kùrstyti.

**fon** [fEn] *dkt v nkt* plaukÿ dþiovintùvas.

**fónda** *dkt m*: ***alla f.*** (stóvintis) nuléidæs i¹karà.

**fondâle** *dkt v* **1.** (*eþero, jûros*) dùgnas; **2.** (*sfondo*) fònas (*ir prk*); *teatr* atpakalýs.

**fondamentâl|e** *bdv* **1.** *bdv* (*di base*) fundame¹ti­nis; pamatínis; ***un princìpio f.*** fundame¹ti­nis príncipas; *geogr* ***meridiano f.*** nùlinis dienóvidinis; **2.** *bdv* (*essenziale*) fundamentalùs; (*il più importante*) svar­biãusias; ***un giocatore f.*** nepake¤èia­mas þaid¸jas; ***un'òpera f.*** fundamentalùs ve¤kalas; **3.:** *dkt v dgs sport **i** **~i*** pagrinda¤.

**fondamentalìsta** *dkt v/m* fundamentalístas -ë.

**fondamént|o** *dkt v* **1.** (*dgs* ***~a***, *m*) påmatas (*ir prk*); ***gettare / porre le*** ***~a*** d¸ti påmatus (*ir prk*); *fig* ***abbâttere dalle ~a*** sugriãuti ikí pamatÿ; **2.** *fig* (*dgs* ***~i***) (*principio*) pågrindas; ***i ~i della fìsica*** fízikos pagrinda¤; **3.** (*dgs* ***~i***) (*ragione fondata*) påmatas, pågrindas; ***accuse senza alcun f. / prive di f.*** kãltinimai bê jókio påmato; **4.:** *dgs m* ***~a*** (*a Venezia*) (kanålo) krantínë *sg* (*Ve­necijoje*).

**fondâre** *vksm* (*fón-*) **1.** (á)kùrti; (á)ste¤gti; ***f. una cittâ (un partito, una scuola)*** ákùrti miì­stà (pãrtijà, mokýklà); **2.** *fig* (*basare*) (*su qcs*) (pa)grñ­sti (*kuo*); (*motivare*) motyvúoti; ***f. l'ac­cusa su indizi*** pagrñsti kãltinimà netiesióginiais áródymais.

    **> fondârsi** *sngr* (*su qcs*) re§tis (*kuo*); b¿ti pa­grá­stãm.

**fondatézza** *dkt m* pagrástùmas.

**fondât|o,** **-a** **1.** *bdv* pâgrástas; pa­matúotas; ***ave­re ~i sospetti*** pagrásta¤ áta»ti; ***avere ~i motivi*** (*per far qcs*) tur¸ti pågrindà (*kà daryti*); ***rive­lar­si f.*** pasitvírtinti; **2.** *bdv, dlv* ákùrtas, ñstei­g­tas; **3.** *bdv, dlv* (*basato*) (*su qc*) pâgrástas (*kuo*).

**fondatóre,** **-trìce** *dkt m* ákûr¸jas -a, steig¸jas -a.

**fondazióne** *dkt m* **1.** ákûrímas*;* (á)steigímas; ***la f. di Roma*** Ròmos ákûrímas; **2.** (*organizzazione*) fòndas.

**fondêll|o** *dkt v*: ¨ *fam* ***prêndere per i ~i*** mùlkinti.

**fondénte** *bdv*: ***cioccolato f.*** juodâsis ðokolådas.

**fóndere** *vksm* **1.** *tr* (ið)lôdyti; (*sciogliere*) (ið)tirp­dôti; ***f. la cera (un metallo)*** (ið)lôdyti våðkà (metålà); **2.** *tr* (*una statua e sim.*) (ið)lîeti; **3.** *tr*: ***f. il motore*** sudìginti varíklá; **4.** *tr fig* (*accorpare*) (su)­jùngti; ***f. due societâ*** su­jùngti dví bendróves; **5.** *intr* [A] Þ **fóndersi 1.**

**> fóndersi** *sngr* **1.** lôdytis, iðsilôdyti; (*sciogliersi*) (ið)­­ti»pti; **2.** *fig* (*unirsi*) susijùngti.

**fonderìa** *dkt m* liejyklâ; lydyklâ.

**fondiâ|rio, -a** *bdv* þìmës; ***diritto f.*** þìmës téisë; ***proprietâ ~ria*** þìmës nuosavôbë; þìmës sklýpas.

**fondìna** *dkt m* pistolêto dºklas.

**fondìsta** *dkt v/m* slidin¸tojas -a (*ne kalnø*).

**fónd|o I** *dkt v* **1.** dùgnas; apaèiâ; ***senza f.*** bedùgnis *agg* (*ir prk*); ***sul f.*** a¹t dùgno; dugnê; ***dóppio f.*** dví­gubas dùgnas; ***f. sabbioso*** smël¸tas dùgnas; ***andare a f.*** (nu)gri§zti, nusk³sti; *fig* gílintis; ¨ ***f. di bicchiere*** netíkras brãngakmenis (*ppr. deimantas*); ***f. stradale*** kìlio dangâ; ***f. di vernice*** gru¹tas; ***toccare il f.*** nusigyvénti; **2.** (*estremitâ*) gålas; ***in f.*** (*a qcs*) (*ko*) galê; ***in f. alla pâgina*** pùslapio apaèiojê; ***il f. dei pantaloni*** kélniø ùþpakalis; *sport* ***lìnea di f.*** galínë línija; *sport* ***rinvio dal f.*** sm¾gis nuõ va»tø; ¨ ***a f.*** íð panagiÿ; nuodugnia¤; ***da cima a f.*** nuõ pradþiõs ikí gålo; ***fino in f.*** ikí gålo; ***in f. (in f.)*** apskrita¤; íð esmºs; palôginti; ***f. di magazzino*** uþsigul¸jusi prìkë; ***dare f. ai risparmi*** gerókai apmåþinti sãntaupas; ***lavare a f.*** ðvaria¤ iðplãuti; **3.** (*sedimenti*) dru§zlës *pl*; pådugnës *pl*; ***~i di caffê*** kavõs ti»ðèiai; *fig* ***un f. di speranza*** viltiìs kruopìlë; **4.** (*sfondo*) fònas, dùgnas; ***su f. blu*** m¸lyname fonê; **5.** (*terreno*) (þìmës) sklýpas; **6.** (*essenza*) esmº; ¨ ***di f.*** esmínis *agg*; pagrindínis *agg*; ***(artìcolo di) f.*** vedamâsis (strãipsnis); **7:** *econ, fin dgs **~i*** l¸ðos; ***~i neri*** nelegålios l¸ðos; ***~i*** ***strutturali UE*** ES strukt¾riniai fòndai; ¨ *vns* ***prêstito a f. perduto*** negràþínama paskolâ; **8.:** *fin* (*titoli*) fòndas; ***f. d'investimento*** investícinis fòndas; **9.** *dir* (*patrimonio; ente*) fòndas; ***f. pensioni*** pe¹sijø fòndas; **10.** *sport*: ***gara di f.*** ílgo núotolio lenktýnës; ***sci di f.*** slidin¸jimas (*ne kalnø*).

**fónd|o II,** **-a** *bdv* gilùs; ***piatto f.*** gilí lëkðtº; *fig* ***a notte ~a*** vëla¤ nåktá, g¾dþià nåktá.

**fondoschiéna** *dkt v nkt fam* pastùrgalis.

**fondotìnta** *dkt v nkt* makiåþo pågrindas.

**fondovâlle** *dkt m nkt* slºnio gilumâ.

**fondùta** *dkt m gastr* (s¿rio) fondiù.

**fonéma** *dkt m ling* fonemâ.

**fonética** *dkt m* fonêtika.

**fonético,** **-a** *bdv* fonêtinis; fonêtikos.

**fonògrafo** *dkt v* fonogråfas.

**fonogrâmma** *dkt v* telefonogramâ.

**font** [fEnt] *dkt v/m nkt inf* ðríftas.

**fontâna** *dkt m* fontånas.

**fónte** **1.** *dkt m* ðaltínis, versmº; (*di fiume*) íðtaka; **2.** *dkt m fig* ðaltínis; íðtakos *pl*; ***una f. attendìbile*** pâtikimas ðaltínis; ***f. di calore*** ðilumõs ðaltínis; ***la f. di tutti i mali*** víso blõgio ðaltínis; **3.** *dkt v*: ***f. battesimale*** krikðtyklâ.

**fontìna** *dkt m* „fontinâ" (*toks Aostos slënio sûris*).

**football** [&fut­bol] *dkt v nkt sport*: ***f. americano*** ameri­kiìtiðkasis fùtbolas.

**footing** [&fut­i)(g)] *dkt v nkt* bëgiójimas ristelê.

**forâggio** *dkt v* påðaras.

**for|âre** *vksm* (*fó-*) **1.** padarôti skýlæ (*kur*); pradùrti (*kà*); (*col trapano*) gr³þti (*kà*); ið­gr³þti skýlæ (*kur*); (*da parte a parte*) pragr³þti (*kà*); pragr³þti (*kà*); ***f. una parete*** ið­gr³þti skýlæ sîenoje; ***f. il biglietto*** pramùðti bílietà; **2.** (*t.p.* ***f. una gomma***) pradùrti pådangà; ***ho ~ato*** prakiùro padangâ.

**> forârsi** *sngr* **1.** (*qcs per sé*) prasidùrti; **2.** (*per usura*) prakiùrti.

**foratùra** *dkt m* **1.** (skylºs) (pra)græþímas; græþímas; (*con aghi e sim.*) pradûrímas; (*di biglietto*) pramuðí­mas; **2.** (*foro*) skylùtë, skylº.

**fòrbic|e** *dkt m* **1.** (*ppr. dgs* ***~i***) þírklës *pl*; ***tagliare con le ~i*** ki»pti þírklëmis; **2.** *econ* þírklës *pl*; ***la f. dei prezzi*** kãinø þírklës; **3.** *zool* auslindâ.

**fòrbici** *dkt m dgs* Þ **fòrbice 1.**

**forbicìn|a** *dkt m* **1.** *dimin* (*ppr. dgs* ***~e***) þirklùtës *pl*; **2.** *zool* auslindâ.

**forbìto, -a** *bdv* nuðlifúotas (*apie stiliø, kalbà*); rafinúo­tas.

**fórca** *dkt m* **1.** (*attrezzo agricolo*) ðåkës *pl*, ðiìnða­kë; **2.** (*capestro*) kãrtuvës *pl*; ***condannare alla f.*** nute¤sti pakãrti; ¨ ***pendâglio da f.*** nevidõ­nas; **3.:** ¨ ***passare sotto le Forche Caudine*** patírti ska÷dø paþìminimà.

**forcêlla** *dkt m* **1.** ðakìlës *pl*; **2.** (*di un ramo e sim.*) iðsiðakójimas; **3.** Þ **forcìna**.

**forchétta** *dkt m* ðakùtë; ¨ ***una buona f.*** (*buon­gu­staio*) gurmånas.

**forcìna** *dkt m* (*t.p. **f. per capelli***) plaukÿ segtùkas.

**fòrcipe** *dkt v med* chirùrginës þnôplës *pl*.

**forcóne** *dkt v* ðåkës *pl*.

**forêsta** *dkt m* míðkas (*ir prk*); giriâ; *geogr* ***f. pluviale*** drëgnâsis tròpinis míðkas; *fig* ***una f. di capelli*** gau­ra¤ *pl*; ¨ ***f. vêrgine*** þmoga÷s nêlie­stas míðkas.

**forestâle** **1.** *bdv* míðko, miðkÿ; miðkÿ apsaugõs; ¨ ***Corpo f.*** Miðkÿ apsaugõs tarnôba; **2.** *dkt v/m* míðkininkas -ë; eigulýs -º.

**foresterìa** *dkt m* forestêrija (*vienuolyno dalis skirta sve­èiams priimti*).

**forestiéro, -a** **1.** *bdv* uþsieniìèiø; uþsieniìtiðkas; ùþ­sienio; **2.** *dkt v/m* atvýkëlis -ë; uþsieniìtis -ë.

**forfait I** [for&f0] *dkt v nkt* *sport* neatvykímas ñ run­gty­nês (/ ñ varþôbas *ir pan.*); ***dichiarare f., dare f.*** pasitrãukti (*ir prk*), pasidúoti.

**forfait II** [for&f0] *dkt v nkt comm*: ¨ ***a f.*** íð a¹ks­to sùtarta kãina; pagaµ fiksúotà dýdá.

**forfettârio, -a** *bdv* fiksúoto dýdþio.

**fórfora** *dkt m* pléiskanos; ***avere la f.*** tur¸ti pléiskanø.

**fòrgia** *dkt m* kãlvës þa¤zdras.

**forgiâre** *vksm* (*fòr-*) **1.** (nu)kãlti; (nu)­kãl­d­inti; **2.** *fig* (su)formúoti.

**forgiatùra** *dkt m* kalôba.

**foriéro, -a** *bdv* (*di qcs*) pranaðãujantis (*kà*).

**fórm|a** *dkt m* **1.** fòrma, pavídalas; (*aspetto*) íðvaizda; ***senza f.*** befòrmis *agg*; ***dare f.*** formúoti; sute¤kti fòrmà; (*plasmare*) sulipdôti; ***prênde­re f.*** ágãuti pavídalà / fòrmà; formúotis; ¨ ***a f.*** (*di qcs*) (*ko*) pavídalo, (*ko*) fòrmos; ***di f. quadrata*** kvadråto fòrmos, kvadråtinis *agg*; ***sotto f.*** (*di qcs*) ka¤p (*kas*); *t.p. þymi bûdà*; **2.** (*ppr. dgs* ***~e***) k¿no fòrmos, figûrâ *sg*; **3.** (*tipo*) fòrma; r¿ðis -ies; (*modo*) b¾das; ***f. di go­verno*** sãntvarka, vaµdymo fòrma; ***f. grave di una malattia*** rimtâ ligõs fòrma; ***f. mentale*** galvósena; ***in f. privata*** privatùs *agg*; *biol* ***f. di vita*** bûtôbë; *mus* ***f. sonata*** sonåtos fòrma; **4*.*** *(espressione*) fòrma; ***f. e contenuto*** tu­rinýs i» fòrma; **5.** (*stampo e sim.*) (kepímo / liejímo *ir pan.*) fòrma; *fig* ***una f. di parmigiano*** parmidþåno ritinýs; **6.** (*condizione fisica*) fòrma; (fízinis) pajëgùmas; ***êssere*** ***in** **f.*** b¿ti gerõs fòrmos; ***non êssere in f., êssere fuori f.*** b¿ti prastõs / blogõs fòrmos; ¨ *kaip* *bdv nkt* ***peso f.*** idealùs svõris; **7.** (*formalitâ*) formalùmai *pl*; etikêto taisýklës *pl*; ***per (la) f.*** formalia¤; dºl akiÿ; ¨ ***salvare la f.*** laikôtis manda­gù­mo; **8.** *gram* fòrma; ***f. attiva (passiva)*** (ne)veikiamóji r¿ðis; ***f. di cortesia*** mandagùmo fòrma; **9.** *gram* (*variante*) fòrma; þõdis.

**formagg(i)éra** *dkt m* s¿rinë (*indas sûriui*).

**formaggìno** *dkt v* sûrìlis; ***f.*** ***fuso*** lôdytas sûrìlis.

**formâggio** *dkt v* s¿ris; ***f. con i buchi*** a­k­ô­tas s¿ris; ***f. caprino / di capra*** oþkõs s¿ris; ***f. olandese (svìzzero)*** olãndiðkasis (ðveicåriðka­sis) s¿ris; ***f. stagionato*** brandíntas s¿ris.

**formâl|e** *bdv* **1.** formalùs; oficialùs; ***protesta f.*** oficialùs protêstas; ***dei rap­por­ti ~i*** formålûs sãnty­kiai; ***êssere molto f.*** smùlkmeniðkai lai­kôtis fòrmos; **2.** (*esteriore*) iðorínis.

**formalìsmo** *dkt v* formalízmas.

**formalit|â** *dkt m nkt* formalùmas; proced¾ros *pl*; ***~a doganali*** mu¤tinës formalùmai; ***sbri­ga­re al­cu­ne ~â*** su­tvarkôti / atlíkti kelís formalumùs; ¨ ***ê solo una f.*** èiâ tík formalùmas.

**formalizzârsi** *vksm* **1.** (*indignarsi*) pasipíkinti, píktintis; **2.** (*far complimenti*) kùklintis.

**formalménte** *prv* formalia¤.

**formâ|re** *vksm* (*fór-*) **1.** (su)formúoti, su­da­rôti; ***f. una fila*** rikiúotis (ñ e¤læ); ***f. una frase*** sudarôti såkiná; ***f. il go­ver­no*** formúoti / sudarôti vy­riausôbæ; ***il consìglio ê ~to da sei membri*** tarôba susídeda íð ðeðiÿ nariÿ, tarôbà sudåro ðeðí naria¤; **2.** (*creare*) sukùrti; (*fondare*) ákùrti; **3.** (*modellare*) (nu)lipdôti; (*dar forma a qcs*) sute¤kti (*kam*) fòrmà; **4.** *fig* (su)­formúoti; (*educare*) (ið)ugdôti; (*addestrare*) apmókyti; ***f. il carâtte­re*** formúoti charåkte­rá; ***f. il personale*** apmókyti personålà.

**> form|ârsi** *sngr* formúotis, susiformúoti, susidarôti, darôtis; (*comparire*) atsirâsti; (*su una superficie*) d¸tis, uþsid¸ti; (*dei frutti*) mêgztis;  ***si ê ~ata la cros­ta sul­la fe­ri­ta*** ðåðas a¹t þaizdõs uþsid¸jo, þaizdâ uþsitrãukë; ***sull'âcqua si ~a il ghiâccio*** a¹t vand­e¹s dìdasi lìdas; ***nel terreno si ê ~ato un avval­lamento*** þìmë á­dùbo; ***sull'intònaco si ê ~ata la muffa*** apipe­líjo ti¹kas.

**formâto 1.** *dkt v* formåtas; dýdis; ***f. tascâbile*** kiðenínis *agg*; ***fotografia f. têssera*** dokume¹tinë núotrauka; ¨ ***f. famìglia*** ðeimôninis *agg* (*apie porcijà, pakuotæ*); **2.** *bdv* (*maturo e sim.*) subréndæs; **3.** *bdv, dlv*: ***êssere f.*** (*da qc*) b¿ti sudarôtam (*ið ko*), susi­d¸ti (*ið ko*); *t.p.* Þ **formâre**.

**formattâre** *vksm inf* **1.** (*dischi*) þénklinti (*diskus*); **2.** (*testo e sim.*) (su)form(at)úoti.

**formattazióne** *dkt m inf* **1.** (*di testo e sim.*) formuõtë; (*l'azione*) (su)form(at)åvimas; **2.** (*di dischi*) þénkli­nimas.

**formazióne** *dkt m* **1.** formåvimas(is)*,* su(si)då­rymas; (*comparsa*) atsiradímas; ***la f. di un ascesso*** p¿linio atsiradímas; ***la f. della persona­litâ*** asmenôbës formåvimasis; *ling* ***f. delle parole*** þõdþiø darôba; ***êssere in (via di) f.*** darôtis, formúotis; **2.** (*creazione*) kûrímas; **3.** *fig* (*educazione*) ùgdymas; (*addestramento*) apmó­kymas; parengímas; instruktåþas; ***corso di f.*** parengia­mâ­sis kùrsas; ***f. culturale*** iðsimóks­li­ni­mas; ***la f. dei giòvani*** jauní­mo mókymas; **4.** *anat, geol* formåcija, darinýs; **5.** *mil* rikiuõtë; ið(si)d¸stymas; ¨ ***in f.*** rikiuõ­tëje; iðsid¸stæ *pl*; **6.** *sport* sudëtís; (*schieramento*) (þaid¸jø) ið(si)d¸sty­mas.

**form|ìca** *dkt m* **1.** *zool* skruzdëlº, skruzdº; ***labo­rioso come una f.*** darbðtùs ka¤p bítë; **2.** *fam*: ***ho le ~che a un piede*** (mãn) nuti»po kója.

**formicâio** *dkt v* skruzdëlônas.

**formichiére** *dkt v zool* skruzd¸da.

**formicolâre** *vksm* (*-m**ì**-*) **1.** [A] (*brulicare*) knibþd¸ti; **2.** [E] (*di parti del corpo*) ti»pti (*apie kûno dalá*).

**formicolìo** *dkt v* **1.** (*brulichio*) knibþd¸­jimas; **2.** (*intorpidimento*) nutirpímas; tirpu­lýs; (*doloroso*) dilgs¸ji­mas.

**formidâbile** *bdv* **1.** (*potente*) galíngas; **2.** (*ecce­zionale*) nuostabùs; áspûdíngas; fantåstiðkas; ***memòria f.*** nuostabí atmintís.

**formìna** *dkt m dimin* formìlë.

**formós|o, -a** *bdv* apvalùs (*apie moters kûno formas*); ***ragazza ~a*** merginâ sù apvalùmais.

**fòrmula** *dkt m* **1.** fòrmulë; þõdþiai *pl*; ***f. augura­le*** link¸jimas; ***f. mâgi­ca*** mågið­ka fòrmulë; ***f. di giuramento*** prîesaikos þõdþiai; ***f. di saluto*** svéikinimosi þõdþiai; *dir **assòlvere con f. piena*** vísiðkai iðtéisinti; **2.** *chim, fis, mat* fòrmulë; ***f. chì­mi­ca*** chêminë fòrmulë; **3.** *fig* (*insieme di criteri e sim.*) fòrma; (*metodo*) metòdas; ***la f. di un programma televisivo*** televízijos laidõs fòrma; **4.** (*motto*) ð¾kis; **5.:** *sport* ***F. uno, F. 1*** Fòrmulë-1.

**formulâre** *vksm* (*fòr-*) **1.** (su)formulúoti; (*esprimere*) iðréikðti; ***f. un augùrio*** palink¸ti, pasakôti lin­k¸jimà; *dir* ***f. un capo d'impu­ta­zione*** patéikti kãltinimà; **2.** (*inventare e sim.*) (su)kùrti; sugalvóti; ***f. un'ipòte­si*** sukùrti hi­potêzæ.

**formulazióne** *dkt m* formuluõtë, apibrëþímas; (*il formulare*) formulåvimas.

**fornâce** *dkt m* krósnis -ies; (*fabbrica*) plýtø gamyklâ.

**fornâio**, **-a** *dkt v/m* kep¸jas -a; duonkepýs -º.

**fornêll|o** *dkt v* **1.** (*t.p. dgs* ***~i***) virýklë; viryklºlë; (*bru­ciatore*) (virýklës) degíklis; ***f. a cherosene*** prímusas; ***f. a gas*** dùjinë virýklë; ***f. elêttrico*** ka¤tvietë, elektrínë plytìlë; ***méttere la péntola sul f.*** uþka¤sti púodà; ¨ ***stare tra i ~i*** sùktis põ virtùvæ; **2.: *il f. della pipa*** pôpkës galvùtë; **3.** (*di stufa e sim.*) pakurâ.

**fornicâre** *vksm* [A] (*fór-*) paleistuvãuti; svetimãuti.

**forn|ìre** *vksm* (*-**ì**sc-*) **1.** (*procurare*) (pa)tiìkti; (*dare*) dúo­ti; (pa)te¤kti; (*rifornire*) (*qc di qcs*) apr¾pinti (*kà kuo*); ***f. un consìglio*** dúoti pata­rímà; ***f. energia elêttrica*** tiìkti elêktros enêr­gijà; ***f. informazioni*** te¤kti infor­måcijà; ***f. un servìzio*** tiìkti påslaugà; ***le pécore ~ìscono lana*** åvys dúoda vílnà; **2.** (*presentare*) pate¤k­ti; ***f. prove schiaccianti*** pate¤k­ti negi¹­èijamus áródymus.

**> fornìrsi** *sngr* (*di qcs*) apsir¾pinti (*kuo*); (*essere cliente*) b¿ti (*ko*) klientù, nuõlat pi»kti (*tam ti­kroje parduotuvëje*).

**fornitóre,** **-trìce** *dkt v/m* tiek¸jas -a.

**fornitùra** *dkt m* tiekímas.

**fórno** *dkt v* **1.** krósnis -ies *femm*; (*di cucina*) órkaitë; ***f. a legna*** mãlkinë krósnis; ***f. a microonde*** mikro­bangÿ krosnìlë; ***al*** ***f.*** kêptas órkaitëje; ***prodotto da f.*** kepinýs; ***méttere in f.*** paðãuti ñ krósná; ád¸ti ñ órkai­tæ; **2.** (*panificio*) kepyklâ; **3.** *tecn* krósnis -ies *femm*; ***f. crematòrio*** krematò­riu­mas; **4.** *fig* (*luogo cal­do*) pirtís -iìs *femm* (*karðta vieta*), keptùvë.

**fóro I** *dkt v* skylº, skylùtë; (*apertura*) angâ.

**fòro II** *dkt v* **1.** *stor* fòrumas; **2.** *dir* te¤smas.

**fórse** *prv* **1.** gãl; galb¿t; ***f. ver­so se­ra verrâ a piò­ve­re*** ñ våka­rà gãl pradºs lôti; **2.** (*in domande*) a»gi; nêgi; benê; (*per caso*) ka»tais; ***sei f. cie­co?*** a»gi tù åklas?; ***hai f. paura?*** nêgi bija¤?; **3.** *kaip* *dkt v*: ¨ ***méttere in f.*** kélti abejõniø; ve»sti suabejóti; ***la riunione ê in f.*** neãiðku, a» pósëdis ávýks; ***sono in f. se...*** nesù tíkras, a»...

**forsennâto, -a** *dkt v/m* pasiùtæs; pamíðæs.

**fòrt|e** **1.** *bdv* stiprùs (*ir prk*); (*potente*) galíngas; pajëgùs; ***carâttere f.*** stiprùs charåkteris; ***un colpo f.*** stiprùs sm¾gis; *fig* ***un f. bevitore*** díde­lis gëríkas; *fig* ***f. in matemâtica*** gera¤ iðmånantis matemåtikà; ¨ ***f.*** (*di qcs*) ásitíkinæs (*kuo*); ***piatto f.*** pagrindínis påtiekalas; *fig* akce¹tas; ***il sesso f.*** stiprióji lytís; ***maniere ~i*** grîeþtos prîemo­nës; ***dare man f.*** (*a qcn*) pare§ti (*kà*); ***farsi f.*** (*di qcs*) pasinaudóti (*kuo*); ***ê più f. di me*** ne­galiù susilaikôti; *kaip jst* ***f.!, che f.!*** jëgâ!; **2.** *bdv* (*resistente*) tvi»tas, stiprùs; patvarùs; ***colla f.*** stíprûs klija¤; ***economia f.*** tvirtâ ekonòmi­ka; ***una stoffa f.*** patvarùs audinýs; **3.** *bdv* (*in­tenso*) stiprùs; smarkùs; ***caffê f.*** stiprí kavâ; ***dolore f.*** stiprùs ska÷smas; ***luce f.*** ryðkí ðvie­sâ; ***~i pêrdite*** didelí núosto­liai; ***rumore f.*** garsùs triùkðmas; ***salsa f.*** aðt­rùs pådaþas; ***~i sospetti*** su¹kûs átarímai; ***un f. vento*** smarkùs / stiprùs v¸jas; ¨ ***parole ~i*** ðiu»­kðtûs þõdþiai; **4.** *bdv* (*grande*) dídelis; (*importante*) svarbùs; ***una f. somma (di denaro)*** dídelë sumâ; ***tâglia f.*** dídelis dýdis; (*di qcn*) drûtuõlis; **5.** *prv* (*con forza*) stipria¤; sma»kiai; ***abbracciare f.*** stip­ria¤ apkabínti; ***piòvere f.*** sma»kiai lôti; **6.** *prv* (*saldamente*) tvirta¤; ***tenersi f.*** tvirta¤ laikôtis; **7.** *prv* (*velocemente*) gre¤tai; ¨ ***andare f.*** b¿ti laba¤ populia­riãm; b¿ti a¹t bangõs; **8.** *prv* (*non piano*) ga»siai; ***parlare f.*** ga»siai kalb¸ti; ¨ ***puoi dirlo f.!*** galí nepåsakoti!; i» dãr ka¤p!; **9.** *prv* (*posposto: assai*) laba¤; ***dubitare f.*** laba¤ abejóti; ***brutto f.*** ganâ (*labai*) negraþùs; **10.** *dkt v nkt* stiprióji pùsë; ***non ê il mio f.*** ta¤ nê mâno sritís; nelaba¤ mãn sìkasi (*tam tikroje srityje*); **11.** *dkt v* (*fortezza e sim.*) fòrtas; **12.** *dkt v/m* (*persona forte*) stipruõlis -ë.

**forteménte** *prv* (*molto*) laba¤; *t.p.* Þ **fòrte 9.**

**fortézza** *dkt m* **1.** (*roccaforte*) tvirtóvë; **2.** (*d'ani­mo*) tvirtùmas.

**fortificâre** *vksm* (*-t**ì**-*) **1.** (su)stíprinti; (su)­tvírtinti; (uþ)gr¿dinti; **2.** *mil* átvírtinti.

**fortificazióne** *dkt m* átvírtinimas; fortifikåcija.

**fortilìzio** (*dgs* -zi) nedídelë tvirtóvë.

**fortìno** *dkt v* nedídelis fòrtas; redùtas.

**fortùito,** **-a** *bdv* atsitiktínis; ***un incontro f.*** atsitiktínis susitikímas.

**fortùna** *dkt m* **1.** (*la dea bendata*) Fortûnâ; ***la f. ci arride*** Fortûnâ mùms ðýpsosi; ¨ ***tentare la f.*** mëgínti lãimæ; *kaip jst* ***buona f.!*** sëkmºs!; gerõs  klotiìs!, **2.** (*buona sorte*) sëkmº; lãi­më; (*successo*) pasisekímas; ***colpo di f.*** netik¸­ta sëkmº; ***hanno avuto f.*** jîems pasísekë; ***la f. ê dalla mia*** mãn sìkasi; ¨ ***per*** ***f., f. che...*** lãi­mei... ; (visâ) lãimë (, kâd...); ***cercar f.*** ieðkóti lãimës; ***fare f.*** pralõbti; ***non ho la f. di conó­scerlo*** dëjâ, jõ nepaþïstu; netur¸jau lãimës sù juõ susipaþínti; *flk* ***cias­cu­­no ê artêfi­ce del­la pròpria f.*** kiek­vîenas þmo­gùs sâvo lãimës kãlvis; *kaip jst* ***che f.!*** nâ, i» sìkasi!; **3.** tu»tai *pl*; ¨ ***costare una f.*** kainúoti milijonùs; **4.:** ¨ ***atterrâggio di f.*** pri­verstínis nutûpímas; ***con mezzi di f.*** iðsive»èiant (*nesant reikiamø priemoniø*); ka¤p nórs; ***fare una riparazione di f.*** ka¤p nórs aptaisôti.

**fortunâle** *dkt v* ðtòrmas, smarkí audrâ j¿roje.

**fortunataménte** *prv* lãimei; visâ lãimë; ***f. per te*** tâvo lãimei / lãimë.

**fortunâto,** **-a** *bdv* **1.** (*di qcn*) tùrintis lãimës; laimíngas; ***sei*** ***stato** **f.*** tãu pasísekë; **2.** (*di qcs*) (*fausto; propizio*) laimíngas;  ***il mio nùmero f.*** laimíngas ska¤èius; **3.** (*di qcs*) (*che riesce bene*) sëkmíngas; ***tentativo f.*** sëkmíngas ba¹dymas.

**fortunóso, -a** *bdv* nus¸tas nelaimíngø atisiti­kímø; pérmainingas.

**forùncolo** *dkt v med* furùnkulas, ðùnvotë.

**fòrz|a** *dkt m* **1.** jëgâ; stiprôbë; (*vigore*) pajëgù­m­as; (*intensitâ*) stiprùmas; (*potenza*) galiâ; ***f. fìsica*** fízinë jëgâ; ***f. di volontâ*** vålios galiâ; ***f. della natura*** stíchija, ga¤valas; ***la f. dell'abitù­dine*** ïproèio jëgâ; ***la f. di un'esplosione*** sprogímo jëgâ; ***con tutte le pròprie ~e, a tutta f.*** íð visÿ jëgÿ; visomís íðgalëmis; ***êssere senza ~e*** netur¸ti jë­gÿ; ***ricórrere alla f.*** griìbtis jëgõs; ***riméttersi in ~e*** atgãuti jëgâs; ***usare la f.*** naudóti jºgà; *mil* ***êssere in f.*** (*a qc*) b¿ti paskirtãm (*á kà*); *dir* ***per câuse di f. maggiore*** dºl nenugalimõs jëgõs aplinkôbiø; ¨ ***a f.*** (*di (far) qcs*) *þymi priemonæ*; nuõlat (*kà darant*); ***con f.*** stipria¤; ***a f., di f., con la f.*** jëgâ, pe» jºgà; pe» prîevartà; (*contro voglia*) nenóromis; *dir* ***in f.*** (*di qcs*) pagaµ (*kà*); rìmiantis (*kuo*); ***per amore o per f.*** nórom nenórom; ***per f. (di cose)*** þínoma; (*non sorprende*) nenuostabù; (*inevitabilmente*) neiðvéngiamai; ***fare f.*** stipria¤ spãusti (/ trãukti *ir pan.*); *flk* ***l'unione fa la f.*** vienôbë -- stiprôbë; *kaip jst* ***bella f.!, per f.!*** dídelis èíâ dãiktas!; *kaip jst* ***che f.!*** jëgâ!; *kaip jst* ***f. Inter!*** pirmýn, Ínterai!; **2.** (*t.p.* ***f. d'ânimo***) (dvåsios) stiprôbë; (*coraggio*) dràsâ; (*tenacia*) atkaklùmas; tvirtùmas; ***con f.*** tvirta¤; ***contare sulle pròprie ~e*** kliãutis sâvo jëgomís; ¨ ***fare f.*** (*a qcn*) padrçsinti (*kà*); ***farsi f.*** pasidrçsinti; *kaip jst* ***f.!*** nâgi!; dràsia÷!; **3.** *econ*: ***f. lavoro*** dãrbo jëgâ; ***~e produttive*** gamôbinës jºgos; **4.** *fis*: ***f. centrìfuga*** iðcentrínë jëgâ; ***f. di gravitâ*** su¹kis, su¹kio jëgâ; ***f. di inerzia*** inêrcijos jëgâ; ***f. motrice*** våromoji jëgâ; **5.:** *dgs **~e*** påjëgos; ***~e*** ***armate*** ginklúotosios påjëgos; ***~e dell'òrdine*** (*t.p. vns* ***la f. pùbblica***) polícija (*policininkai ir karabinieriai*); ***~e polìtiche*** polítinës jºgos.

**forzâre** *vksm* (*fòr-*) **1.** iðlãuþti; jëgâ atidarôti; ***f. una porta (la serratura)*** iðlãuþti durís (spý­nà); **2.** (*sforzare*) pe» jºgà sùkti (/ kíðti / trãukti / ve»sti *ir pan.*); (*accelerare*) spa»tinti; ***f. il motore*** spãusti varíklá; ***f. un tappo*** kíðti ka§ðtá pe» jºgà; *fig **f. gli eventi*** forsúoti ïvykius; *fig* ***f. il senso di un discorso*** iðkre¤pti þõdþiø pråsmæ; *fig* ***f. i tempi*** paspa»tinti te§pà; **3.** *mil* forsúoti; (*sfondare*) pralãuþti; ***f. un assédio*** prasilãuþti / prasimùðti íð apsuptiìs; **4.** (*obbligare*) (*a far qcs*) (pri)ve»sti (*kà daryti*); spãusti; ***f. il fìglio a studiare*** spãusti va¤kà mókytis; *fig* ***f. la pròpria natura*** e¤ti priìð prígimtá.

**forzât|o,** **-a** **1.** *bdv* priverstínis; ***lavori*** ***~i*** priverèiamîeji darba¤; kåtorga; ***sosta ~a*** priverstínis susto­­­jímas; **2.** *bdv* (*innaturale*) dirbtínis, nenatûralùs; ***sorriso f.*** priverstínë / dirbtínë ðýpsena; **3.** *bdv fig* prítemptas; **4.** *dkt v* kåtorgininkas.

**forziére** *dkt v* skryniâ (*vertybëms*).

**forzóso, -a** *bdv dir, econ* priverstínis.

**forzùto, -a** *bdv* jëgíngas; stiprùs.

**foschìa** *dkt m* ¿kana, r¾kana.

**fó|sco,** **-a** *bdv* **1.** gûdùs; tamsùs; (*di cielo e sim.*) niûrùs; ¨ ***dipíngere a tinte ~sche*** nupiìðti li¾dnà va¤­zdà; **2.** *fig* niûrùs, tamsùs; ***pensieri ~schi*** ta§sios mi¹tys.

**fosfâto** *dkt v chim* fosfåtas.

**fosforescénte** *bdv* ðvýtintis (*dël fosforescenci­jos*); ðviì­èiantis tamsojê; fosforínis.

**fòsforo** *dkt v chim* fòsforas.

**fòss|a** *dkt m* **1.** duobº; (*fosso*) griovýs; ***la f. dei leoni*** li¿tø duobº; ***f. dell'orchestra*** orkêstro duobº; *anat **~e nasali*** nósies ïdubos; *tecn* ***f. biològica*** núotekø biològinio vålymo árenginýs; **2.** (*tomba*) kåpas; ***f. comune*** be¹dras kåpas; ***colmare una f.*** supílti kåpà; ***scavare la f.*** (ið)kâsti duõbæ (*ir prk*); ¨ ***ave­re un pie­de nel­la f., êssere con un piede nella f.*** b¿ti vîena kója karstê; *flk* ***del senno di poi son piene le ~e*** kiekvîenas gudrùs põ la¤ko; **3.** *geol, geogr* (*t.p. **fossa oceânica***) povandenínis lovýs, povandenínë ïduba.

**fossâto** *dkt v* (*anche di castello*) griovýs (*t.p. ap­link pilá*).

**fossétta** *dkt m* duobùtë, duobùkë.

**fòssile 1.** *bdv* iðkastínis, fosílinis; ***carbone f.*** ak­me¹s a¹glys *pl*; ***combustìbile f.*** iðkastínis kùras; **2.** *bdv fig* prieðtvanínis; **3.** *dkt v* (*t.p. **reperto f.***) fosílija, (suak­men¸jusi) íðkasena; **4.** *dkt v fig* (*di qcn*) prieðtva­níniø pa­þiûrÿ þmogùs.

**fòsso** *dkt v* griovýs, griovìlis; ***fi­ni­re in un f.*** (*con l'auto e sim.*) áva­þiúoti ñ griõvá.

**fòto** *dkt m nkt* Þ **fotografìa 1.**

**foto-** *prieðdëlis:* *pirmasis sudurtiniø þodþiø dëmuo, rodantis sàsajà su ðviesa arba su fotografija, pvz.,* fotoservìzio, fotostâtico, fototeca *ir t. t..*

**fotocâmera** *dkt m* (*t.p. **f. digitale***) skaitmenínis fotoaparåtas.

**fotocêllula** *dkt m tecn* fotoeleme¹tas.

**fotocòpia** *dkt m* kòpija (*ir prk*), fotokòpija; ***fare una f.*** (*di qcs*) padarôti (*ko*) kòpijà.

**fotocopiâre** *vksm* (*-cò-*) (*qcs*) kopijúoti (*kà*), padarôti (*ko*) kòpijà / kòpijas.

**fotocopiatrìce** *dkt m* kopijåvimo aparåtas.

**fotofinish** [-&fini^] *dkt v nkt* fotofíniðas; ¨ ***al f.*** pa­skutínæ akímirkà.

**fotogénico,** **-a** *bdv* fotogêniðkas.

**fotografâre** *vksm* (*-tò-*) **(**nu)fotografúo­ti; padarôti (*kam*) núotraukà / núotraukas.

**fotografìa** *dkt m* **1.** núotrauka, fotogråfija; ***fare una f.*** padarôti núotraukà; ***la f. ê (venuta) mossa*** núotrauka iðºjo neryðkí; susilîejo va¤zdas; **2.** (*tecnica*) foto­gråfija; ***direttore della f.*** operåtorius statôto­jas; **3.** *fig* (*rappresentazione di qcs*) pavéiks­las; **4.** *fig* (*di qcn*) kòpija.

**fotogrâfic|o,** **-a** *bdv* fo­togråfinis; fotogråfijos; fotogråfiðkas; ***filtro f.*** fo­togråfinis fíltras; ***mâcchina*** ***~a*** fotoaparåtas; ***mostra ~a*** fotogråfijos parodâ; ***servìzio f.*** fotoreportåþas; *fig* ***memòria ~a*** fotogråfiðkai tikslí atmintís, fotogråfiðka atmintís.

**fotògrafo,** **-a** *dkt v/m* fotogråfas -ë.

**fotogrâmma** *dkt v* (*di pellicola*) kådras.

**fotomodêllo, -a** *dkt v/m* fotomòdelis *masc*.

**fotomontâggio** *dkt v* fotomontåþas.

**fotosìntesi** *dkt m biol* fotosi¹tezë.

**fóttere** *vksm volg* **1.** *tr* písti; ¨ ***vai a farti f.!*** uþsi­písk!; **2.** *intr* [A] krùðtis, pístis; **3.** *tr fig fam* ap­mãuti; **4.** *tr* *fig fam* (*rubare*) nuknísti, nudþiãuti.

**fottùto, -a** *bdv volg* **1.** (*dannato*) sùpistas; **2.** (*finito*) (pra)þùvæs; ***sono f.*** mãn gålas.

**foulard** [fu&l2_] *dkt v nkt* skarìlë, skepetãitë; ka­klåskarë.

**fra I** *prlk* **1.** (*di luogo, anche fig*) (*qcs*) ta»p (*ko*) (*ir prk*); ***fra i boschi*** ta»p miðkÿ; *fig* ***la pace fra i pòpoli*** taikâ ta»p tautÿ; **2.** (*indicando distanza, intervallo*) ta»p (*ko*); (*dopo*) põ (*ko*), ùþ; ***fra le due e le tre*** ta»p antrõs i» treèiõs valandõs; ***fra*** ***un** **minuto*** põ / ùþ minùtës; **3.** (*di relazione*) ta»p (*ko*); ***fra*** *(**di)** **noi*** ta»p m¿sø; ***chi fra*** ***di** **voi?*** kâs íð j¿sø?; **4.** (*partitivo*) íð (*ko*); ta»p; ***fra tutti il migliore*** (íð) visÿ geriãusias; **5.** (*con valore cumulativo*): ***fra una cosa e l'altra ho perso la mattinata*** pe» kelís dãrbus su­gaiða÷ vísà rôtà; *t.p.* Þ **tra**.

**fra II** *dkt v nkt* brólis (*apie vienuolá*).

**frac** [frak] *dkt v nkt* fråkas.

**fracassâre** *vksm* (su)dauþôti, sukúlti; sulãuþyti; suknìþinti; ***f. una finestra*** iðda÷þti lãngà; ***f. la mâcchi­na*** sudauþôti maðínà; ***f. la testa a qcn*** suknìþinti kãm gãlvà.

    **> fracassârsi** *sngr* **1.** sudùþti; **2.** (*qcs*) susilãu­þyti.

**fracâsso** *dkt v* **1.** triùkðmas; bildes­ýs; tre¹ks­m­as; **2.** *fam* galôbë; ***un f. di soldi*** krûvâ pinigÿ.

**frâdicio, -a** *bdv* paþliùgæs; pér­mir­kæs, pérðlapæs; ***ba­gnato f.*** kiaura¤ pérmirkæs; ***ubriaco f.*** gírtas ka¤p p¸das.

**frâgile** *bdv* **1.** trapùs (*ir prk*); duþùs; ***êssere molto f.*** gre¤tai / lengva¤ d¾þti; **2.** *fig* (*delicato*) glìþ­nas, trapùs; (*debole*) siµpnas.

**fragilitâ** *dkt m* **1.** trapùmas (*ir prk*); duþùmas; **2.** *fig* (*delicatezza*) gleþnùmas, trapùmas; (*debo­lezza*) silpnù­m­as; ***la f. del­la natura umana*** þmoga÷s prigimtiìs trapùmas.

**frâgol|a** *dkt m* bråðkë; ***gelato alla f.*** bråðkiniai leda¤; ***marmellata di ~e*** bråð­kiø uogiìnë; *bot* ***f.*** ***di** **bosco*** þémuogë.

**fragóre** *dkt v* gria÷smas; tre¹ksmas; dundesýs; ***il f. del tuono*** perk¿no gria÷smas.

**fragorós|o, -a** *bdv* griausmíngas, audríngas; ***u­na cascata ~a*** ðniõkðèiantis krioklýs.

**fragrânte** *bdv* kvapnùs; (*solo del cibo*) gardþia¤ kvìpiantis, aromatíngas.

**fragrânza** *dkt m* kvapnùmas; (*aroma*) aromåtas.

**frainté|ndere*** *vksm* nê ta¤p suprâsti, klaidíngai su­prâsti; ***sono stato ~so*** manê nê ta¤p supråto; ***non ~ndermi*** nesuprâsk man³s klaidíngai.

**frammentârio,** **-a** *bdv* fragme¹tiðkas; nevísas.

**framménto** *dkt v* **1.** núolauþa; (*scheggia*) skevél­dra; (*coccio*) ðùkë; **2.** *fig* fragme¹tas.

**frammìsto, -a** *bdv* (*a qcs*) sumaiðôtas (*su kuo*).

**frâna** *dkt m* **1.** griûtís -iìs; núoðliauþa; **2.** *fig* (*di qcn*) nevýkëlis -ë, nìtikða *com*.

**franâre** *vksm* [E] (su)gri¿ti; (*all'interno*) ágri¿ti.

**francaménte** *prv* **1.** atvira¤; **2.** atvira¤ kaµbant.

**francescâno, -a** **1.** *bdv* pranciðkõnø; **2.** *dkt v/m* pranciðkõnas -ë.

**francése** **1.** *bdv* Prancûzíjos, pranc¾zø, pranc¾­zið­kas; íð Prancûzíjos; *stor **Rivoluzione f.*** Prancûzíjos revoliù­ci­ja; **2.** *dkt v/m* pranc¾zas -ë; **3.** *dkt v* pran­c¾zø kal­bâ; ***in f.*** pranc¾ziðkai.

**franchézza** *dkt m* atvirùmas; nuoðirdùmas.

**frân|co I,** **-a** *bdv* **1.** nuoðirdùs, åt­vi­ras; ¨ ***farla ~ca*** iðsisùkti; **2.** *comm* neapmu¤­tinamas; ***f. di dogana / di porto*** bemu¤tis *agg*; *kaip prv* frãnko; ***porto f.*** laisvâsis úostas; **3.:** *mil* ***f. tiratore*** snãiperis; *fig polit* pérsimetëlis -ë (*kas slaptai balsuoja prieð savo partijos nurodymus*).

**frânco II** *dkt v* (*la valuta*) frãnkas; ***f. svìzzero*** Ðveicå­rijos frãnkas.

**francobóll|o** *dkt v* påðto þénklas; ***f. da un êuro*** vîeno e÷ro vertºs påðto þénklas; ***collezionare ~i*** ri¹kti påðto þénklus; ***in******collare un f., attaccare un f.*** prikli­júoti påðto þénklà.

**frangénte** *dkt v* **1.** l¿þtanti bangâ; **2.** *fig* keblí padëtís -iìs.

**frangétta** *dkt m* kirpèiùkai *pl*.

**frângia** *dkt m* **1.** kuta¤ *pl*; **2.** (*di capelli*) ki»pèiai *pl*; ***portare la f.*** neðióti kirpèiùs; **3.** *polit* fråkcija; gru­pìlë.

**frangiflùtti** *dkt v nkt* bangólauþis.

**franóso, -a** *bdv*  nepatvarùs (*apie uolienà, þemës sluok­sná*); slankùs.

**frantòio** *dkt v* (*l'edificio*) aliìjaus spau­dyklâ; (*la macchina*) alývuogiø spaustùvas.

**frantumâre** *vksm* (*-tù-*) (su)dauþôti, su­kùlti; ið­dau­þôti; ***f. una brocca*** su­kùlti àsõtá; *fig* ***f. i sogni di qcn*** sugriãuti kienõ svajonês.

**> frantumârsi** *sngr* (su)dùþti (ñ ðukês); (*sbri­ciolar­si*) (su)­byr¸ti.

**frantùmi** *dkt v dgs* ðùkës, ðipulia¤; ***in f.*** sudùþæs -usi; ***mandare in f.*** iðdauþôti, iðda÷þti; sudau­þôti ñ ðukês / ñ ðípulius (*ir prk*); ***il vaso ê in f.*** vazâ sudùþusi / sudauþôta.

**frappê** *dkt v nkt* (pîeno i» lìdo) kokte¤lis.

**frappórre*** *vksm* *fig*: ***f. ostâcoli*** su­da­rôti kli¿èiø.

> **frappórsi** *sngr* ásikíðti; ***f. tra due litiganti*** ásikíðti ta»p dviejÿ besigi¹èijanèiø.

**frasârio** *dkt v* **1.** (profêsinë) kalbâ; **2.** (*manualetto*) pasikalb¸jimø knygìlë.

**frâsca** *dkt m* (lapúota) ðakâ, lapínë; ¨ ***saltare di pa­lo in f.*** ðokin¸ti nuõ vienõs têmos priì kitõs.

**frâse** *dkt m* **1.** sakinýs; fråzë; *gram* ***f. sémplice*** vientisínis sakinýs; **2.** (*locuzione*) pósakis; ¨ ***f. fatta*** sustabar¸jæs pósakis, ðta§pas; **3.** *mus* (*t.p.* ***f. musicale***) fråzë.

**fraseologìa** *dkt m* frazeològija.

**frâssino** *dkt v bot* úosis.

**frastagliâto, -a** *bdv* dantôtas, raiþôtas.

**frastornâre** *vksm* (*-stór-*) apku»tinti, pritre¹kti.

**frastornâto, -a** *bdv* apspa¹gæs -usi.

**frastuòno** *dkt v* klegesýs; ga÷smas.

**frâte** *dkt v* vienuõlis; ***f. domenicano*** dominikõ­nas; ***f. francescano*** pranciðkõnas; ***farsi f.*** ástó­ti ñ vienuolônà.

**fratellânza** *dkt m* brolôbë.

**fratellâstro** *dkt v* ïbrolis.

**fratêll|o** *dkt v* **1.** brólis (*ir prk*); ***mio f.*** mâno bró­lis; ***f. maggiore (minore)*** vyrêsnis (jaunêsnis) brólis; ***il f.*** ***maggiore (minore)*** vyresnýsis (jau­nesnýsis) brólis; ***~i gemelli*** bróliai dvynia¤; *med* ***~i siamesi*** Siåmo dvynia¤; ***hai ~i o so­relle?*** a» turí bróliø a» seserÿ?; ***Nino e Ada sono ~i*** Nínas i» Adâ yrâ brólis i» sesuõ; **2.** *rel* (*t.p.* ***f. in Cristo***) brólis (Krístuje).

**fraternitâ** *dkt m* Þ **fratellânza**.

**fraternizzâre** *vksm* [A] broliãutis; (*farsi amico*) susibièiuliãuti. 

**fratêrno,** **-a** *bdv* bróliðkas (*ir prk*); ***amore f.*** bró­liðka méilë; ***legame f.*** brolýstës ryðýs.

**fratricìda** *dkt v/m* brolþudýs -º.

**frâtta** *dkt m* tankmº, tankumônas.

**frattâglie** *dkt m dgs* pla÷èke­pe­niai.

**frattânto** *prv* tuõ metù, tuõ tãrpu.

**frattémpo** *dkt v*: ¨ ***nel** **f.*** tuõ tãrpu; pe» t± la¤kà; ***tu chiami l'a­scensore e nel f. io chiudo la porta*** âð uþra­kínsiu, õ tù iðkviìsk líftà; kõl âð rakinù, tù iðkviìsk líftà.

**frâtto, -a** *bdv mat*: ***sei f. due*** ðeðí padalínti íð dviejÿ; ***venti d. dieci*** dvídeðimt padalínta íð dìðimt.

**frattùra** *dkt m* **1.** *med* l¾þis; lûþímas; ***f. esposta (mùltipla)*** åtviras (dau­gínis) l¾þis; **2.** *fig* skilímas; l¾þis; ***c'ê stata una f. all'interno del partito*** pãrtija suskílo.

**fratturâre** *vksm* (*-tù-*) *med* sulãuþyti.

**> fratturârsi** *sngr med* **1.** *intr* (su)l¾þti; **2.** *tr* su­si­lãuþyti.

**fraudolént|o, -a** *bdv* **1.** (*di qcs*) apgavíkiðkas; *dir* ***ban­­carotta ~a*** tôèinis bankròtas; **2.** (*di qcn*) sùktas.

**frazionâre** *vksm* (*-zió-*) (su)skírstyti, (pa)­dalôti; (*un terreno*) (ið)parceliúoti.

**frazióne** *dkt m* **1.** *mat* trùpmena; ***f. decimale*** de­ðimta¤në trùpmena; **2.** (*parte*) dalís -iìs; dalìlë; ***una f. dell'elettorato*** elektoråto dalís; ***per una f. di secondo*** akímirkà; **3.** (*centro abitato*) „fråkcija" (*Italijoje: gyvenvietë, neturinti savivaldos*); @ seniûnijâ; **4.** *polit* fråkcija; grupuõtë; **5.** *sport* estafêtës etåpas.

**freâtico, -a** *bdv geol* gruntínis.

**fréc|cia** *dkt m* **1.** strëlº; ***scoccare una f***. paléisti strº­læ; **2.** (*segno grafico*) rodýklë; ***fare una f.*** nupiìðti rodýklæ; **3.** (*cartello stradale*) (kìlio) rodýklë; **4.** (*di auto*) pósûkio lem­pù­të; ***méttere la f.*** ájùngti pósûkio lempùtæ; **5.:** *mil **~ce tricolori*** „trispaµvës strºlës" (*Italijos karinës aeronautikos akroba­tinis pa­da­linys*).

**frecciâta** *dkt m* irònijos strëlº; kandí pastabâ.

**freddaménte** *prv fig* ðalta¤.

**freddâre** *vksm* (*fréd-*) **1.** atðãldyti (*ir prk*); atvësínti; **2.** (*uccidere*) ðaltakra÷jiðkai nuþudôti; **3.** *fig* (*con lo sguardo*) pérverti akimís.

**> freddârsi** *sngr* (at)ðãlti (*ir prk*).

**freddézza** *dkt m* **1.** ðaltùmas; **2.** *fig* ðaltùmas; a­bejin­gùmas; ***con f.*** ðalta¤; **3.** *fig* (*sangue freddo*) ðalta­kraujiðkùmas.

**frédd|o I, -a** **1.** *bdv* ðãltas; ***il mese più f.*** ðalèiãusias m¸nuo; ***piatti ~i*** ðaltí patiekala¤; *stor **guerra ~a*** ðaltâsis kåras; ***diven­tare f.*** (at)ðãlti; ***ê f.*** ðãlta; ***hai le mani ~e*** tâvo ðãltos ra¹kos; ¨ ***colori ~i, tinte ~e*** ðãltos spaµvos; ***dóccia ~a*** ðãltas dùðas; ***a sân­gue f.*** (*spietatamente*) ðaltakra÷jiðkai; *kaip prv* ***sudo f.*** píla ðãltas pråkaitas; **2.** *bdv sport* dãr neapðílæs; ¨ ***a f.*** dãr nesp¸jus apðílti; **3.** *bdv fig* ðãltas; (*arido*) beja÷smis; (*indifferente*) abejíngas; ***un'acco­glienza ~a*** ðãltas priëmímas; ***sguardo f.*** ðãltas þviµgsnis.

**frédd|o II** *dkt v* **1.** ðaµtis; (*gelo*) spéigas; ***i primi ~i*** pirmîeji ðaµèiai; ***che f.!*** ka¤p ðãlta!; ***dor­mire al f.*** miegóti ðaµtyje; ***prêndere*** ***f.*** suðãlti; pérsiðaldyti; ***tremare dal f.*** dreb¸ti íð ðaµèio; ***fa*** ***f.*** ðãlta; *fam* ***fa*** ***un f. cane*** pasiùtusiai ðãlta; ***fa f. fuori?*** a» ðãlta laukê?; ***ho f., sento f.*** mãn ðãlta; ¨ ***a f.*** ðaµtai (*ir prk*); *fig* (*all'istante*) nepasiruõðus; ***non mi fa né caldo né f.*** mãn ne¤ ðiµta, ne¤ ðãlta; ***mi viene f. solo a pensarci*** ma­nê ðiu»pina vîen pagalvójus; **2.** *fig* ðaµtis, ðaltí sãntykiai *pl*.

**freddolóso, -a** *bdv* (laba¤) jautrùs ðaµèiui; ðaltmirýs.

**freddùra** *dkt m* sçmojis (*nebûtinai vykæs*).

**freezer** [&fridzer] *dkt v nkt* refriþeråtorius; ðãl­dymo kåmera; (*congelatore*) ðaldíklis.

**fre|gâre** *vksm* (*fré-*) **1.** (pa)trínti; (*per pulire*) (nu)­ðve¤­sti; ***f. un fiammìfero*** br¸þti / bra÷kti degtùkà; **2.** *fam* (*ingan­nare*) apmãuti; ***non mi ~ghi!*** neap­mãusi!; **3.** *fam* (*rubare*) (nu)knia÷kti, nuðviµpti.

**> freg|ârsene** *ávdþ fam*: ***non me ne ~a nien­te di nien­te*** mãn vískas nusispjãuti; ***non me ne ~a nien­te di lei*** nusispjãuti mãn ñ j±; ***e chi se ne ~a?*** kãm r¾pi?; ***tu ~atene dei suoi consi­gli!*** spjãuk tù ñ jõ patarimùs!

  **>** **fregârsi** *sngr* **1.:** ***f. le ma­ni*** patrínti rankâs (*ir prk*); ***f. gli occhi*** trínti akís; **2.** *fam* pirðtùs nu­dêgti (*apsigauti*).

**fregâta I** *dkt m mar* fregatâ.

**fregâta II** *dkt m* **1.: *dare una f.*** (*a qcs*) patrínti (*kà*); paðve¤sti; **2.** *fam* Þ **fregatùra**.

**fregatùra** *dkt m fam*: ***dare una f.*** apmãuti; ***hai preso una f.*** tavê apmóvë; ¨ *kaip jst* ***che f.!*** ka¤p apmóvë!; sùkèiai!

**fré|gio** *dkt v* **1.** dekoratývinis åpvadas; papuoðímas; **2.** *archit* frízas.

**freménte** *bdv* vírpantis, drìbantis.

**frémere** *vksm* [A] (*fré-*) virp¸ti, dreb¸ti; ***f. di gelosia*** nesitvérti pavydù; ***f. di râbbia*** virp¸ti pykèiù;  ***f. di sdegno*** dreb¸ti íð pasipíktinimo.

**frémito** *dkt v* virpulýs, jaudulýs; virpesýs.

**fren|âre** *vksm* (*fré-*) **1.** *intr* [A] (su)­­stab­dôti; pristabdôti; ***f. bruscamente*** staigâ ­­stab­dôti; ***la mâcchina non ~a*** maðínos stabdþia¤ bloga¤ ve¤kia; *fig* ***il ministro ~a sulla possibilitâ di ridurre le tasse*** minístras nêtiki, kâd galimôbë sumåþinti mókes­èius realí; **2.** *tr* (su)­sta­b­dôti (*ir prk*); (*rallentare*) pristab­dôti (*ir prk*); *fig* ***f. l'inflazio­ne*** pristabdôti infliåcijà; **3.** *tr* *fig* (*tratte­ne­re*) sulaikôti; (*contenere*) (nu)slopín­ti; ***f. le lâ­crime*** sulaikôti åðaras; ***f. la língua*** suvaldôti lieþùvá; ***f. le passioni*** slopínti aistrâs.

**> frenârsi** *sngr* tvãrdytis, susitvãrdyti; susilai­kôti.

**frenât|a** *dkt m* (pri)ståbdymas; ***spâzio di f.*** stå­bdy­mo kìlias; ***evitare brusche ~e*** nestabdôti sta¤giai; ***fare una f.*** paspãusti ståbdá.

**frenesìa** *dkt m* **1.** (*pazzia*) ásiutímas; **2.** (*smania*) aistrâ; månija; (*al gioco*) azãrtas.

**frenéti|co,** **-a** *bdv* pad¾kæs; (*febbri­le*) ka»ðtligiðkas; (*entusiastico*) entuziastíng­as; ***applâusi ~ci*** entuzia­stíngi plojímai.

**frén|o** *dkt v* **1.** stabdýs; (*il pedale*) stabdþiÿ pe­då­­las; ***f. a mano*** ra¹kinis stabdýs; **2.** (*del cavallo*) þçslai *pl*; ***mòrdere il f.*** þçslus kramtôti; *fig* nerímti; ¨ ***senza f., senza ~i*** bê sa¤ko; nesuvãldomas *agg*; ***tenere a f.*** paþabóti, sulaikôti.

**frequentâre** *vksm* (*-quén-*) lankôti(s); ***f. gli amici*** ma­tôtis sù drauga¤s; ***f. un bar*** nuõlat lankôtis barê; ***f.*** ***la** **scuola*** lankôti mokýklà, mókytis mokýkloje.

    **> frequentârsi** *sngr* bendrãuti, matôtis.

**frequentâto,** **-a** *bdv* la¹komas; pílnas þmoniÿ; ***mal f.*** átartinÿ típø la¹komas.

**frequént|e** *bdv* dåþnas; ***piogge ~i*** daþní lîetûs; ***ê (una cosa) abbastanza f.*** ta¤ pasitãiko ganâ daþna¤; ¨ ***di f.*** daþna¤.

**frequenteménte** *prv* daþna¤.

**frequénza** *dkt m* **1.** daþnùmas; ¨ ***con f.*** daþna¤; **2.** (*partecipazione*) lankomùmas; la¹kymas; ***la f. alle urne*** rink¸jø aktyvùmas; ***òbbligo di f.*** privålomas la¹kymas; ***c'ê grande f. dei turisti sulle spiagge*** paplûdimiuosê didelí poilsiãu­tojø srauta¤; **3.** *fis, med* dåþnis; *fis* ***alta (bassa) f.*** (aukðtâsis) þemâsis dåþnis; *med **f. del polso*** pùlso dåþnis.

**frésa** *dkt v tecn* frezâ; (*disco*) pjovímo dískas.

**fresâre** *vksm* (*fré-*) frezúoti.

**fresatóre** *dkt v* frezúotojas.

**fresatrìce** *dkt m tecn* (elektrínë) frezâ.

**freschézza** *dkt m* **1.** ðvieþùmas; ***la f. dei prodotti*** prodùktø ðvieþùmas; **2.** (*dell'aria sim.*) vësù­mas; vësâ, vësumâ; **3.** *fig* (*floridezza*) skaistù­mas; (*vitalitâ*) gai­vùmas.

**frés|co I,** **-a** *bdv* **1.** vësùs; (*rinfrescante*) gaivùs; ***âlito f.*** gaivùs kvåpas; ***âria ~sca*** grônas / gaivùs óras; ***notti ~che*** vºsios nåktys; ***stanza ~ca*** vësùs kam­barýs; ¨ *fam **stai f.!*** nesulãuksi!; **2.** (*di cibi e sim.*) ðviìþias; ***fiori ~chi*** k± tík nuskíntos gºlës; ***for­mâggio (latte) f.*** ðviìþias s¿ris (pîenas); ***frutta ~ca*** ðvieþí va¤siai *pl*; ***pesce f.*** ðviìþios þùvys *pl*; **3.** (*riposato*) pails¸jæs, þvalùs; (*florido*) skaistùs; ***un colorito f.*** skaistí véido óda; ***truppe ~che*** na÷jos påjëgos; ¨ ***f. come una rosa*** vísiðkai þvalùs; ***a mente ~ca*** blaiviù protù; **4.:** ***notìzie ~che*** naujãusios þínios, naujîenos; ***ricordo f.*** dãr gôvas pri-siminí­mas; ***sposi ~chi*** jaunavedþia¤; ***vernice ~ca*** dãr neiðdþi¿væ daþa¤ *pl*; ¨ ***di f.*** nesenia¤; nauja¤; ðvie­þia¤; **5.:** ***f. di lâurea (di studi)*** k± tík ba¤gæs univer­sitêtà (mókslus); ***f. di nòmina*** k± tík íðrinktas.

**frésco II** *dkt v* vësumâ, vësâ; ***il f. della sera*** vå­karo vësâ; ***fa*** ***f.*** vësù; *fam* ***fa freschino*** vësóka; ***conservare al f., tenere in f.*** laikôti vësiojê viì­toje; ***godersi il f.*** m¸gautis vºsuma; ¨ ***con il f., col f.*** ka¤ vësù; ***sbâttere al f.*** ákíðti ñ cýpæ.

**frescùra** *dkt m* vësâ.

**frésia** *dkt m bot* frêzija.

**frétta** *dkt m* skub¸jimas, skubâ; ***senza f.*** bê sku­bõs; nêskubant; ***aver*** ***f.*** skub¸ti; ***fare f.*** (*a qcn*) (pa)skù­binti (*kà*); ***non c'ê f.*** nërâ kõ skub¸ti; ***fai in f.!*** paskub¸k!; ¨ ***di f., in*** ***f.*** skubia¤; pa­skubomís, pasku­bõm; ***in f. e fùria*** pasiùtusiai skubia¤; ***per la f., nella f.*** pe» skùbà; ***che f.!*** kãm ta¤p skub¸ti?

**frettolosaménte** *prv* skubótai.

**frettolós|o,** **-a** *bdv* **1.** (*di qcs*) skubótas; ***conclusioni ~e*** skubótos íðvados; ***saluto f.*** gre¤tas atsisvéikinimas; **2.** (*di qcn*) skùbantis; ***êssere troppo f.*** pe» da÷g skub¸ti.

**friâbile** *bdv* trapùs; (*del terreno*) birùs.

**friggere*** *vksm* **1.** *tr* (ið)kêpti (*riebaluose*); (pa)kì­pin­ti; ¨ ***andare a farsi f.*** niìkais iðe¤ti; ***vai a farti f.!*** e¤k velnióp!; **2.** *intr* [A] (ið)kêpti; (*sfri­golare*) èi»k­ðti, spírgti; **3.** *intr* [A]: *fig* ***f. di râb­bia*** kunkuliúoti pykèiù.

**friggitrìce** *dkt m* gruzdintùvë.

**frigiditâ** *dkt m* frigidiðùmas, lytínis ðaltùmas.

**frìgido, -a** *bdv* frigídiðkas.

**frignâre** *vksm* [A] bliãuti; verkðlénti, zýzti; zirzénti.

**frignâta** *dkt m* zyzímas.

**frignìo** *dkt v* zirzìnimas, zirzesýs.

**frigobar** [-&bar] *dkt v nkt* båras (*spintelë gërimamas*); måþas ðaldytùvas (*ppr. gërimams laikyti*).

**frìgo** *dkt v nkt* Þ **frigorìfero**.

**frigorìfer|o, -a** **1.** *dkt v* ðaldytùvas; ***conservare in f.*** laikôti ðaldytuvê; **2.** *bdv*: ***borsa ~a*** ðãltkrepðis; ***cella ~a*** ðãldymo kåmera; ***vagone f.*** refriþeråtorius.

**fringuéllo** *dkt v zool* kikílis.

**frinìre** *vksm* [A] (*-**ì**sc-*) svi»pti.

**frittâta** *dkt m* kiauðiniìnë; omlêtas; ¨ ***rivoltare / ri­girare la f.*** iðve»sti ñ kítà pùsæ; (*mutare opinione*) iðve»sti kãilá; (*sostenere l'assurdo*) árodin¸ti, kãd júo­da yrâ bãlta; ***la f. ê fatta*** þalâ ja÷ padarôta.

**frittêlla** *dkt m* blýnas, skli¹dis; blynìlis.

**frìtt|o,** **-a** **1.** *bdv, dlv* kìptas (*riebaluose*), íðkep­t­as; ***patate ~e*** grùzdintos bùlvës; ***patatine ~e*** bùlviø traðkùèiai; ¨ ***âria ~a*** tuðèiaþodþiåvi­mas; **2.** *bdv fig* þùvæs; ***siamo ~i*** mìs þùvæ; **3.** *dkt v gastr* kìptas påtiekalas; ***f. misto (di pe­sce)*** keptÿ j¿ros gërôbiø rinkinýs; ***f. di zuc­chine*** kìptos cukínijos.

**frittùra** *dkt m* **1.** kepímas; **2.** Þ **frìtto 3.**

**friulâno, -a** **1.** *bdv* Friùlio; friuliìèiø; íð Friùlio; **2.** *dkt v/m* friuliìtis -ë; **3.** *dkt v* friuliìèiø ðnektâ.

**frivolézza** *dkt m* lengvabûdiðkùmas.

**frìvol|o,** **-a** *bdv* lengvab¾diðkas; nerímtas, le¹­gvas; ***discorsi ~i*** nerímtos kaµbos; ***un tipo*** ***f.*** lengvab¾­dis.

**frizionâre** *vksm* (*-zió-*) (pa)trínti; (*qcs con qcs*) átrínti (*kà kuo*).  

**frizióne** *dkt m* **1.** (pa)trynímas; átrynímas; **2.** *tecn* sãnkaba; (*il pedale*) sãnkabos pedålas / pami­nâ; **3.** *fis* trintís -iìs; **4.** *fig* trintís -iìs, nesutarímas.

**frizzânte** *bdv* **1.** (*dell'aria e sim.*) ga¤viai ðãltas; **2.** *fig* (*vivace*) trôkðtantis enêrgija; pad¾kæs; **3.** (*gasa­to*) gazúotas; (*che fa la schiuma*) putójantis; ***âcqua f.*** gazúotas vanduõ.

**frìzzo** *dkt v* kandùs sçmojis; (*di scherno*) paðaipâ.

**fròcio** *dkt v volg* píderas, hòmikas.

**frodâre** *vksm* (*frò-*) apgãuti; (*più volte*) apgaudin¸ti; átrãukti ñ afêrà.

**fròde** *dkt m* sukèiåvimas; apgãule; aferâ; ***con la f.*** apgãule; *dir* ***f. in commêrcio*** prekôbinis sukèiåvimas; *dir* ***f. fiscale*** mokestínis sukèiå­vimas.

**fròdo** *dkt v*: ***cacciatore di f.*** brakoniìrius; ***câccia (pe­sca) di f.*** brakonieriåvimas.

**frollâre** *vksm* (*frò-*) (su)mínkðtinti (*ppr. mësà*).

**fròll|o, -a** *bdv*: ***pasta ~a*** smëlínë teðlâ.

**frónd|a** *dkt m* (lapúota) ðakâ, lapínë; *dgs* ***le ~e*** låpai, lapijâ *sg*; lajâ *sg*.

**frondóso, -a** *bdv* lapúotas; skarótas.

**frontâle** *bdv* **1.** prieðakínis, prîekinis; frontalùs; íð prîekio; ***attacco f.*** frontalí atakâ; ***parte f.*** prîekinë dalís; ***vista f.*** va¤zdas íð prîekio; ***(scontro) f.*** susi­dûrímas kaktómuða; **2.:** *anat* ***osso f.*** kaktíkaulis.

**frontalìno** *dkt v* (*di autoradio*) automagnetòlos sky­dìlis, automobílinës magnetòlos sky­dìlis.

**frónte 1.** *dkt m* kaktâ; ***corrugare la f.*** sura÷kti kåktà; ¨ ***di** **f.*** prîeðais; (*a qc*) priìð (*kà*), prîeðais; *kaip bdv* prîeðais ìsantis, prîeðingas; ***scrit­to / stam­pa­to in f.*** a¹t kaktõs uþ­raðôta; ***trovarsi di f.*** (*a qcs*) atsidùrti (*kur*); susidùrti (*su kuo*); **2.** *dkt v mil* fròntas; ***al f., sul f.*** frònte; ¨ ***far f.*** (*a qcs*) dorótis (*su kuo*), su­sidoróti; ***far f. agli impegni*** (á)vôkdyti ásipareigó­jimus; ***far f. alle spese*** tur¸ti íðlaidø.

**fronteggiâre** *vksm* (*-tég-*) **1.** prîeðintis (*kam*), pasi­prîeðinti; dr±siai sutíkti (*kà*); *fig* ***f. delle difficoltâ*** dorótis sù sunkùmais; **2.** (*essere di fronte*) (*qc*) b¿ti (/ stov¸ti) prîeðais (*kà*).

**frontespìzio** *dkt v* antraðtínis / titulínis pùslapis.

**frontiér|a** *dkt m* **1.** pasîenis; (*confine*) sîena; ***f. di stato*** valstôbës sîena; ***pas­sare / varcare la f.*** péreiti / ki»sti sîenà; **2.** *fig* (*ppr. dgs **~e***) ríbos.

**frontóne** *dkt v arch* frontònas.

**frónzol|o** *dkt v* (*ppr. dgs **~i***) nereikalínga puoð­me­nâ, niekùtis; *fig* ***senza (troppi) ~i*** tiìsiai ðviìsiai.

**fròtt|a** *dkt m* bûrýs; puµkas; ¨ ***in f., a ~e*** bûria¤s.

**fròttol|a** *dkt m* påsaka; pråmanas; ***(rac)contare ~e*** påsakoti neb¿tus dalykùs.

**frugâle** *bdv* kuklùs; bê prabangõs; pâprastas; ***pasto f.*** kuklùs / pâprastas vaµgis.

**frugâre** *vksm* **1.** *intr* [A] ra÷stis; narðôti; ***f. negli archivi*** ra÷stis põ archyvùs; **2.** *tr* apieðkóti; iðkratôti; ***f. i passeggeri*** apieðkóti keleiviùs; ***f. (in) tutta la casa*** iðkratôti vísà nåmà.

**fruìbile** *bdv* (*da parte di qc*) (*kieno*) naudójamas; gålimas naudóti; ***bene f. da tutti*** tu»tas, kuriuõ visí gåli naudótis.

**fru|ìre** *vksm* [A] (*-**ì**sc-*) (*di qcs*) (gal¸ti) nau­dótis (*kuo*); tur¸ti (*teisæ, pajamas ir pan.*); ***f. di una pensione*** gãuti / tur¸ti pe¹sijà; ***gli studenti ~íscono di sconti*** stude¹tams tãikomos núolaidos.

**frullâre** *vksm* **1.** *tr* (ið)plâkti; **2.** *intr* [A] plasnóti; **3.** *intr* [A] *fig*: ***f. per la testa*** kirb¸ti galvojê.

**frullâto** *dkt v* (pîeno i» va¤siø) kokte¤lis.

**frullatóre** *dkt v* plakíklis.

**frùllo** *dkt v* tãnkus sparnÿ plazdìnimas.

**fruménto** *dkt v* kvieèia¤ *pl*; *bot* kvietýs; ***farina di f.*** kvietíniai míltai *pl*.

**frusciâre** *vksm* [A] (*frù-*) ðlam¸ti; èeþ¸ti; zirz¸ti.

**fruscìo** *dkt v* ðlamesýs; ðlam¸jimas; èeþ¸jimas.

**frùsta** *dkt m* botågas; rôkðtë; ***schioc­ca­re la f.*** pliauk­ð¸ti botagù.

**frustâre** *vksm* (ið)plâkti (sù botagù), (ið)pliìkti (sù rôkðte); èãiþyti, kapóti, lùpti.

**frustât|a** *dkt m* ki»tis (sù) botagù (/ rôkðte), botågo (/ rôkðtës) ki»tis; *dgs* ***~e*** plakímas *sg*.

**frustìno** *dkt v* ðma¤kðtis, ri§bas.

**frùsto, -a** *bdv* nudëv¸tas, nuvãlkiotas (*ir prk*).

**frustrâto, -a 1.** *bdv* (laba¤) nusivôlæs; âpimtas fru­s­tråcijos, frustrúotas; **2.** *dkt v/m* nusivôlëlis -ë.

**frustrazióne** *dkt m* gilùs nusivylímas; fru­s­tråcija.

**frùtta** *dkt m* va¤siai *pl*; ***f. secca*** dþiovínti va¤siai; (*noci e sim.*) rîeðutai *pl*; ***macedònia di f.*** va¤siø salõ­tos; ¨ ***êssere alla f.*** b¿ti prisiba¤gusiam.

**fruttâre** *vksm* **1.** *intr* [A] vêsti vaisiùs / va¤siø (*ir prk*), der¸ti; dúoti de»liø; **2.** *intr* [A] (*dare un utile*) dúoti peµno; atsipi»kti; **3.** *tr* dúoti (*der­liø, pelno, rezultatà*); pelnôti; ***f. qualche milione*** atnêðti kelís milijo­nùs pajamÿ; ***f. il 20******%*** dúoti 20 proc. peµno; *fig **f. elogi*** pelnôti pagyrímà; *fig* ***f. l'invìdia di molti*** sukélti da÷gelio pavýdà.

**fruttéto** *dkt v* va¤smedþiø sõdas.

**frutticoltóre, -trìce** *dkt v/m* va¤siø augíntojas -a, va¤­sininkas -ë; sõdininkas -ë.

**fruttìfero, -a** *bdv* vaisíngas (*ir prk*).

**fruttivéndolo,** **-a** *dkt v/m* va¤siø i» darþóviø pardav¸­jas -a; darþóvininkas -ë.

**frùtt|o** *dkt v* **1.** va¤sius (*ir prk*); ***~i ma­tu­ri (acer­bi)*** prinókæ (þalí) va¤siai; ***âlbero da f.*** va¤sinis mìdis, va¤smedis; ***dare ~i, portare f.*** vêsti vaisiùs; *fig* dúoti va¤siø / rezultåtø; *fig* ***i ~i del pròprio lavoro*** sâvo dãrbo va¤siai; ¨ ***il f. proibito*** úþdraustas va¤­sius; ***~i*** ***di** **bosco*** úogos; ***~i*** ***di** **mare*** j¿ros gërôbës; *fig **méttere a f.*** (*qcs*) sëk­míngai naudótis (*kuo*); *t.p.* Þ **frùtta**; **2.** *fig* (*conseguenza, prodotto*) padarinýs; va¤sius; ***f. del­la fan­ta­sia*** vaizduõtës pa­da­rinýs; ***il f. di una buona edu­cazione*** gìro ãuklëjimo va¤sius; **3.** *fig*: ***dare buoni ~i*** (*rendere*) dúoti peµno.

**fruttòsio** *dkt v chim* fruktòzë, va¤siø cùkrus.

**fruttuóso,** **-a** *bdv* **1.** *fig* vaisíngas; naðùs; **2.** *fig* (*econo­mi­ca­mente*) pelníngas.

**fu** *bdv nkt* veliónis; ***Ivo Rossi fu Gino*** Ivo Rossi, veliónio Gino sûnùs.

**fucilâre** *vksm* (*-c**ì**-*) (su)ðãudyti.

**fucilâta** *dkt m* ðãutuvo ð¾vis.

**fucilazióne** *dkt m* (su)ðãudymas; ***condannare al­la f.*** nute¤sti suðãudyti.

**fucìle** *dkt v* ðãutuvas; ***f. automâtico*** automåtinis ðãutuvas; ***f. a pallettoni*** ðratínis ðãutuvas; ***f. subâcqueo*** povandenínis ðãutuvas.

**fucìna** *dkt m* **1.** þa¤zdras; (*officina*) kãlvë; **2.** *fig* kãlvë, formåvimosi térpë.

**fucinatùra** *dkt m* kalôba; (*l'azione*) kalímas.

**fùco** *dkt v* trånas.

**fùcsia** **1.** *dkt m nkt* *bot* fùksija; **2.** *bdv nkt* ra÷s­vas sù violêtiniu åtspalviu, fùksijos spalvâ..

**fùga** *dkt m* **1.** (pa)bëgímas (*ir prk*); ***f. dal cârcere*** pabë­gímas íð kal¸jimo; *fig* ***f. dalla realtâ*** pa­bëgí­mas nuõ tikróvës; ***darsi alla f.*** pasiléisti b¸gti; ***méttere in f.*** prive»sti b¸gti, vôti; **2.** *fig* (*perdita*) núotëkis; nutek¸jimas; ***f. di cervelli (di notìzie)*** smege­nÿ (informåcijos) nutek¸­ji­mas; ***f. di gas*** dùjø núotëkis; *econ* ***f. di capitali*** kapitålo bëgímas; kapitålo iðveþímas; ***c'ê sta­ta u­na f. di gas*** nu­te­­k¸jo dùjos; **3.** *mus* fugâ; **4.** *sport* spùrtas.

**fugâce** *bdv* trumpala¤kis; gre¤tai prab¸gantis.

**fugâre** *vksm* (ið)sklaidôti; iðblaðkôti; *fig* ***f. i dubbi*** iðsklaidô­ti abejonês.

**fuggévole** *bdv* gre¤tai prae¤nantis, trumpala¤kis; la¤­kinas.

**fuggiâsco, -a** *dkt v/m* bëglýs -º.

**fuggifùggi** *dkt v nkt* (*t.p.* ***f.*** ***generale***) påniðkas bëgí­mas.

**fuggìre** *vksm* **1.** *intr* [E] (pa)b¸gti; (pa)sprùkti, iðtr¿kti; ***f. di casa (di prigione)*** pab¸gti íð na­mÿ (íð kal¸jimo); ***f. all'êstero (davanti al ne­mico)*** pab¸gti ñ ùþsiená (nuõ prîeðo); **2.** *intr* [E] (*del tempo*) (pra)­b¸gti; **3.** *tr* (*evitare*) (*qcs*) véngti (*ko*); ***f. ogni re­sponsabilitâ*** véngti bêt kokiõs atsakomôbës.

**fuggitìvo**, **-a** *dkt v/m* bëglýs -ë.

**fùlcro** *dkt v* atramõs tåðkas (*ir prk*).

**fùlgido, -a** *bdv* **1.** skaistùs, spi¹dintis; **2.** *fig* ryðkùs.

**fulgóre** *dkt v* åkinamas spindesýs; spind¸jimas.

**fulìggine** *dkt m* súodþiai *pl*; ***sporco di f.*** súodinas.

**fulmin|âre** *vksm* (*fùl-*) **1.** *tr* (nu)tre¹kti; ***ê stato ~to dalla corrente*** jñ nùtrenkë elektrâ; *fig* ***f.*** (*qcn*) ***con gli occhi*** pérsmeigti (*kà*) þvilg­s­niù; akimís þaibúoti; **2.** *fig* (*uccidere*) paki»­sti; ***l'ha ~to un infarto*** jñ paki»to infãrktas; **3.:** *intr* [E, A] (su)þaibúoti, paþaibúoti.

**> fulminârsi** *sngr* (*di lampadina*) pérdegti.

**fùlmine** *dkt v* þa¤bas; ***come un f.*** þa¤biðkai; ka¤p þa¤­bas; ¨ ***f. a ciel sereno*** perk¿nas íð giìdro danga÷s;  ***colpo di f.*** méilë íð pírmo þviµgsnio.

**fulmìneo,** **-a** *bdv* þa¤biðkas.

**fùlvo, -a** *bdv* gelsva¤ rùdas; rùsvas.

**fumaiòlo** *dkt v* kåminas (*ppr. laivo, fabriko*).

**fum|âre** *vksm* **1.** *tr* (pa)rûkôti; ***sméttere di f.*** mês­ti rûkôti; ***f. la pipa*** rûkôti pôpkæ; ***f. una siga­retta*** (su)rûkôti cigarêtæ; parûkôti; ***lei ~a?*** j¾s r¾kote?; ¨ ***vietato f.*** r¿kyti dra÷dþiama; **2.** *intr* [A] r¾kti; smiµkti; (*emettere vapore*) ga­rúoti; ***la minestra ~a*** sriubâ garúoja.

**fumâ|rio, -a** *bdv*: ***canna ~ria*** d¿mtraukis, kåminas (*vamzdis*).

**fumâta** *dkt m* **1.** (*vienu kartu*) iðleidþiamí d¿mai *pl*; d¿mas; ¨ ***f. nera*** juodí d¿mai (*iðleidþiami nepavy­kus iðrinkti popieþiaus; t.p. prk, þymi nepasisekimà*); **2.: *fare / farsi una f.*** parûkôti.

**fumatór|e,** **-trìce** *dkt v/m* rûkålius -ë; rûkôtojas -a; *kaip* *bdv nkt* ***(per) non ~i*** ner¾kantiems.

**fumettìsta** *dkt v/m* kòmiksø kûr¸jas -a / pieð¸jas -a.

**fumétt|o** *dkt v* (*ppr. dgs* ***~i***) kòmiksas.

**fùm|o** *dkt v* **1.** d¿mai *pl*; ***odore di f.*** d¾mø kvå­pas; ***il f. esce dal camino*** d¿mai r¾ksta íð kå­mino; ***puz­zo di f.*** smírdu d¾mais, nuõ man³s d¿­mais nìða; ¨ ***andare in f.*** þlùgti; subli¿kðti; ***mandare in f.*** (su)þlugdôti; ***véndere f.*** p¾sti míglà ñ akís; *flk* ***non c'ê f. sen­za ar­ros­to*** nërâ d¿mø bê ug­niìs; **2.** (*il fumare*) r¾kymas; ***tabacco da f.*** r¾komasis tabåkas; ***il f. fa male alla salute*** r¾kymas ke¹kia sveikåtai; ¨ ***f. passivo*** pasyvùs r¾kymas; **3.** *fig*: ***i ~i dell'alcol*** apsvaigímas, svaigulýs; ***i ~i dell'ira*** prõto apte­mímas (supýkus); **4.** *fam* þolº (*haðiðas ir pan.*), þolôtë.

**fumògeno** *dkt v* d¿minis fåkelas.

**fumóso, -a** *bdv* **1.** pílnas d¿mø; prirûkôtas; dûmúo­tas; **2.** *fig* mi­glótas.

**funâmbolo** *dkt v* lôno akrobåtas.

**fùne** *dkt m* vi»vë; lônas, tròsas; ***tiro alla f.*** vi»vës traukímas.

**fùnebr|e** *bdv* **1.** gedulíngas; lãidotuviø; ***corona f.*** lãidotuviø vainíkas; ***mârcia f.*** gedulíngas mãrðas; ***pompe ~i*** (*la ditta*) lãidojimo biùras; ***véglia f.*** ðe»­menys *pl*; **2.** (*lugubre*) graudùs; gûdùs.

**funerâl|e** *dkt v* (*t.p. dgs **~i***) lãidotuvës *pl*; ¨ ***da f.*** niûrùs; lãidotuviðkas; ***f. di Stato*** valstôbinës lãidotuvës; ***êssere un f., avere una fâccia da f.*** b¿ti ka¤p pe» lãidotuves; b¿ti ka¤p þìmæ pardåvus.

**funerâri|o, -a** *bdv* a¹tkapio; kåpo; lãidojimo; ***iscri­zione ~a*** a¹tkapio ùþraðas; ***urna ~a*** lãidojimo ùrna.

**funêreo, -a** *bdv* gedulíngas; gûdùs.

**funestâre** *vksm* (*-nê-*) aptémdyti; (*solo qcn*) sute¤kti ska÷smo (*kam*).

**funêsto, -a** *bdv* **1.** (*rovinoso*) praþûtíngas; **2.** (*luttuoso*) gedulíngas; niûrùs; (*doloroso*) sielvartíngas.

**fungâia** *dkt m* grybåvietë.

**fùng|ere*** *vksm* [A] **1.** (*da qcn*) e¤ti (*kito þmogaus*) påreigas; pavadúoti (*kà*); **2.** (*da qcs*) b¿ti var­tó­jamam (*vietoj ko kito*), b¿ti naudójamam; (*anda­re bene*) tíkti; ***la cucina ~e anche da sala da pranzo*** virtùvë ti¹ka i» ka¤p vãlgomasis; ***questa parola ~e da soggetto*** ðís þõdis e¤na veiksniù; **3.** (*sostituire*) atstóti; (*in un incarico*) (*da qc*) pavadúoti (*kà*).

**fùn|go** *dkt v* **1.** grýbas; ***f. com­mestìbi­le (ve­le­no­so)*** vãlgo­ma­s (nuodíngas) grýbas; ***ri­sotto coi /ai ~ghi*** rýþiai *pl* / rizòtas sù grýbais; ***andare*** ***a** **/ per** **~ghi, cercare** **~ghi*** grybãuti; ¨ ***a f.*** grýbo pavídalo; ***spùnta­no co­me ~ghi*** dôgsta ka¤p grýbai põ lie­ta÷s; **2.:** *fig* ***f. atòmico*** atòminis grýbas; **3.** *med* (ódos) grybìlis.

**funicolâre** *dkt m* funikuliìrius.

**funivìa** *dkt m* (kalnÿ) (lônø) kéltuvas.

**funzionâle** *bdv* **1.** fùnkcinis; *med* ***disturbo f.*** fùn­kcinis sutrikímas; **2.** (*pratico*) funk­cionalùs; pråk­tiðkas; (*comodo*) patogùs.

**funzionalitâ** *dkt m nkt* **1.** funk­cionalùmas; praktiðkù­mas; (*comoditâ*) patogùmas; **2.** Þ **funzióne 1.**

**funzionaménto** *dkt v* veikímas; funkcionåvi­m­as; *tecn* eigâ; ***difetto di f.*** veikímo sutrikímas (/ defêktas); ***spiegare il f. di qcs*** paãiðkinti, ka¤p kâs ve¤kia.

**funzion|âre** *vksm* [A] (*-zió-*) **1.** (*tinkamai*) ve¤kti; fun­kcionúoti; ***f. a elettricitâ*** naudóti e­lêktrà; ***non ~a*** neve¤kia, sugì­do; **2.** (*dare risultati*) dúoti va¤siø; b¿ti naudíngam; ***con me le lâcrime non ~ano*** åðaromis man³s nepave¤ksi.

**funzionârio,** **-a** *dkt v/m* valdini¹kas -ë, valstôbës tar­nãutojas -a; (*pubblio ufficiale*) pareig¾nas -ë; ***f. di banca*** bãnko tarnãutojas.

**funzión|e** *dkt m* **1.** fùnkcija; (*finalitâ*) paskirtís -iìs; ***~i vi­ta­li*** gyvôbi­nës fùnkcijos; ***svòlgere una f.*** atlíkti fùnkcijà; **2.** (*attivitâ*) veiklâ; veikímas; (*ruolo*) vaidmuõ; ¨ ***in f.*** (*in qualitâ di*) ka¤p; ***entrare in f.*** suve¤kti; ásijùngti; ***êssere in f.*** ve¤kti; (*di qcs*) priklausôti (*nuo ko*); ***méttere in f.*** ájùngti; **3.** (*rito sacro*) påmaldos *pl*; **4.** (*mansione*) (*ppr. dgs **~i***) påreigos *pl*; ***eser­ci­ta­re le ~i di pre­si­den­te*** e¤ti pre­zi­de¹to påreigas; ¨ ***facente f.*** laikina¤ e¤nantis påreigas; **5.** *mat* fùnkcija.

**fuò|co** *dkt v* **1.** ugnís -iìs *femm*; (*fiamma*) liepsnâ; ***~chi*** ***d'artifìcio, ~chi*** ***artificiali*** fejervêr­kai; ***dare*** ***f., appiccare il f.*** (*a qc*) padêgti (*kà*); ***accéndere (spégnere) il f.*** uþdêgti ((uþ)ge­sínti) ùgná; ¨ ***di f.*** ugníngas *agg*; ***f. fâtuo*** þaltvý­kslë; ***f. di pâglia*** trum­pala¤kë sensåcija; atsitiktínis laim¸jimas; ***Vìgile del F.*** ga¤srininkas -ë; ugniagesýs; ***a f. lento (vivo)*** a¹t silpnõs (stipriõs) ugniìs; ***andare a f.*** (su)­dêgti; ***prêndere*** ***f.*** uþsidêgti; ***métterci la mano sul f.*** dúoti ra¹kà nuki»sti; ***gettare òlio sul f., soffiare sul f.*** pílti aliìjø ñ ùgná; ***giocare / scherzare col f.*** þa¤sti sù ugnimí; ***al f.!*** ugnís!; *kaip jst* ***f. f.*** (*nei giochi*) ðiµta, ðilèia÷; **2.** (*fornello*) degíklis; ka¤tvietë; ***méttere l'âcqua sul f.*** (uþ)ka¤sti vãndená; ***méttere la péntola sul f.*** (uþ)d¸ti púodà a¹t ugniìs; **3.** (*focolare*) þidinýs; ***accén­dere il f.*** (su)kùrti ùgná; **4.** *fig* (*ardore*) ugnís -iìs *femm*; uþsidegímas; **5.** *geom, fis* þidinýs; **6.:** *fot*: ***a f.*** sufokusúotas *agg*; ***méttere a f.*** (su)fokusúoti; **7.** *mil* ugnís -iìs *femm*; ***sotto il f. nemico*** prîeðo ugnyjê; ***f. incrociato*** kryþmínë ugnís; ***f. di sbarramento*** ugniìs rúoþas; ***cessate il f.*** ugniìs nutraukímas; *fig* paliãubos *pl*; ***scontro a f.*** susiðãudymas; ¨ ***f. amico*** dra÷giðka kulkâ; ***arma da f.*** ðaunamâsis gi¹klas; ***bocca da f.*** pab¾klas; ***aprire il f.,*** ***fare f.*** ðãuti; paléisti ùgná; (*su qc*) apðãudyti (*kà*); ***êssere tra due ~chi*** ta»p dviejÿ ugniÿ; ***rispòndere al f.*** atsiðãudyti; *kaip jst* ***f.!*** ugnís!

**fuorché** **1.** *prlk* iðskôrus (*kà*); ***tutti f. lui*** visí iðskôrus jñ; **2.** *jngt* tík nê; ***sono pronto a tutto, f. scusarmi*** âð vískam pasiruõðæs, tík neatsipraðôsiu.

**fuòri** **1.** *prlk* (*qcs, da qcs*) ùþ (*ko*); ***f.*** ***cittâ*** ùþ miìsto, ùþmiestyje; (*moto a luogo*) ñ ùþmiestá; ***f. Milano*** ùþ Milåno; ***aspettare f. dalla porta*** lãukti ùþ dùrø; ***guardare f. dalla finestra*** þiûr¸ti prõ lãngà; ***méttere qcs f. posto*** pad¸ti k± nê ñ viìtà; *sport* ***f. casa*** íðvykoje, sveèiuosê, nê namiì; ¨ ***al di f.*** (*di qcn*) iðskôrus (*kà*); ***f. (dai giochi)*** ùþ bòrto; ***f. corso*** nebegaliójantis *agg*; ***f. luogo*** (nê laikù i») nê viìtoje; ***f. mano*** nuoðalùs *agg*; nuoðalia¤; ***f.*** ***moda*** ne(be)ma­díngas *agg*; pasìnæs *agg*; *fam* ***f. porta*** ùþmiestyje; ***f. di sé*** nesåvas *agg*; ***f. tempo*** nê ñ tåktà; ***f. di testa*** patråkæs *agg*, iðsikrãustæs *agg* ís galvõs; ***f. uso, f. servìzio*** sugìdæs *agg*; ***dire f. dai denti*** iðr¸þti, iðdróþti; ***non métte­re pie­de f. di ca­sa*** neiðkélti nº kójos íð namÿ; **2.** *prv* laukê; (*moto a luogo*) ñ la÷kà; la÷k; ***da f.*** íð la÷ko; ***andare*** ***fuori*** iðe¤ti, e¤ti ñ la÷kà; ***buttare f.*** iðmêsti; ***f. fa freddo*** ðãlta laukê; *sport* ***tirare f.*** mùðti ñ ùþribá; ¨ ***di f.*** íð íðorës; iðorínis *agg*, la÷ko; (*di gente*) nê viìtinis *agg*; ***in f.*** ñ íðoræ; (*in avanti*) ñ prîeká; ***tagliato f.*** ùþ bòrto (*iðmestas, neátrauktas*); ***far f.*** (*qcn*) nugalåbyti (*kà*); ***veni­re f., saltare f.*** (*diventare noto*) iðaiðk¸ti; iðkílti aikðtºn; (*comparire*) atsirâsti; ***tirare f.*** iðtrãuk­ti; iði§ti; ***venire f., uscire f.*** iðlñsti; iðbrísti (*ir prk*); ***ê f. perìcolo*** gyvôbei nebêgrìsia pavõjus; ***f. i soldi!*** atidúok pínigus!; *kaip jst* ***f. (di qui)!*** la÷k (íð èiâ)!; e¤k íð èiâ!; *kaip dkt*: ***il di f.*** íðorë; **3.** *prv* (*non a casa*): ***cenare f.*** vakarieniãuti nê namiì (/ restoranê *ir pan.*); ***stare f. a lungo*** ilga¤ nepare¤ti; **4.** *prv* (*non al lavoro*): nê darbê; ***il direttore ê f.*** vadõvas iðvýkæs; **5.** *prv* *fam* (*non in carcere*) lãisvë­je; paléistas *agg*; **6.** *prv fig* (*all'apparenza*) íð íðorës; **7.** *bdv nkt fam* tre¹ktas; ***sei f.?*** a» iðprot¸­jai?; gãl pakvaiða¤?

**fuoriclâsse** *dkt v/m nkt* åsas -ë.

**fuorigiòco** *dkt v nkt sport* núoðalë.

**fuorilégge** **1.** *bdv* *nkt* ùþ áståtymo ribÿ; **2.** *dkt v/m nkt* nusikaµtëlis -ë.

**fuorisérie** *dkt m nkt* nestandãrtinis automobílis (*pra­bangus, pagamintas pagal uþsakymà*).

**fuoristrâda** *dkt v nkt* visure¤gis; dþípas.

**fuor(i)uscìre*** *vksm* [E] iðsipílti; ið­silîeti; (*fluire*) (ið)­tek¸ti; (*erompere*) iðsive»þti, ve»þtis.

**fuor(i)uscìta** *dkt m* ið­siliejímas: (*perdita*) núotëkis.

**fuor(i)uscìto, -a** *dkt v/m* diside¹tas -ë; atskal¾nas -ë.

**fuorviâre** *vksm* (*-v**ì**-*) (su)klaidínti.

**furbacchióne, -a** *dkt v/m* gudrõèius -ë.

**furberìa** *dkt m* **1.** Þ **furbìzia 1.**; **2.** (*inganno*) suktôbë.

**furbìzia** *dkt m* **1.** gudrùmas; ***con f.*** gudria¤; **2.** (*azione furba*) gudrôbë; gudrùs triùkas.

**fùrbo, -a** **1.** *bdv* gudrùs; ***fatti f.!*** nebùk kva¤las!; **2.** *dkt v/m* gudruõlis -ë; ***non fare il f.!*** tík negudrãuk!

**furénte** *bdv* ásiùtæs, át¿þæs.

**furétto** *dkt v zool* (laukínis) ðìðkas.

**furfânte** *dkt v* nevidõnas, nenãudëlis.

**furgoncìno** *dkt v* furgonºlis; pikåpas.

**furgóne** *dkt v* furgònas; ***f. del latte*** pîenvëþis.

**fùr|ia** *dkt m* **1.** ïnirðis; ïtûþis; (á)siutímas; ¨ ***an­dare / montare su tutte le ~ie*** áni»ðti, ásiùsti; **2.** *fig* (*im­peto*) siãutëjimas; ðºlsmas; ïnirðis; ¨ ***a f.*** (*di (far) qcs*) *þymi bûdà, priemonæ*; nuõlat (*kà darant*); **3.** *fig, mit* fùrija; **4.** Þ **frétta**; ***in fretta e f.*** paðºlusiai skubia¤.

**furibóndo,** **-a** *bdv* **1.** (*di qcn*) ásiùtæs, áni»ðæs; **2.** (*di qcs*) audríngas, paðºlæs.

**furióso,** **-a** *bdv* **1.** (*di qcn*) (vísiðkai) ásiùtæs, áði»dæs; ***êssere f.*** ði»sti, siùsti, laba¤ pýkti; ¨ ***pazzo f.*** vísiðkas beprõtis; **2.** (*di qcs*) ánirtíngas; (*del mare, del vento e sim.*) siautulíngas.

**furóre** *dkt v* **1.** ïnirðis; pasiutímas; ***preso dal f.*** ïnirðio âpimtas; ¨ ***a furor di pòpolo*** (*visiems*) audríngai prítariant; ***fare f.*** su­kélti furòrà; **2.** *fig* (*impeto*) ðºlsmas, siau­tulýs; (*slancio*) pólëkis; ***f. giovanile*** jaunåtviðkas ïkarðtis.

**furoreggiâre** *vksm* [A] (*-rég-*) b¿ti a¹t bangõs.

**furtìvo, -a** *bdv* slåptas; vogèiomís dåromas; ***con pas­so f.*** vogèiâ / slaptâ sle¹kant; sºlinant.

**furtivaménte** *prv* vogèiâ, vogèiomís; paslapèio­mís.

**fùrto** *dkt v* vagýstë; (*l'azione*) pavogímas; ***accusare di f.*** apkãltinti vagystê; ***comméttere un f.*** pavõgti; ávôkdyti vagýstæ; ***condannare per f.*** nute¤sti ùþ va­gýstæ; *fig* ***ê un f.!*** èiâ grynâ vagýstë!, ta¤ apiplëðí­mas!

**fùsa** *dkt m dgs*: ***fare le f.*** mu»kti.

**fuscêllo** *dkt v* (*pagliuzza*) ðiãudas; (*ramoscello*) vi»bas.

**fusìbile** *dkt v tecn* (lydùsis) saugíklis; ***ê saltato il f.*** pérdegë saugíklis.

**fusìllo** *dkt v* (*ppr. dgs* ***~i***) spirålinis makarõnas, srai­gtìlis.

**fusióne** *dkt m* **1.** lôdymas(is); (*scioglimento*) (ið)­ti»p­dy­mas; (ið)tirpímas; *fis* ***temperatura di f.*** lô­dy­mosi tem­pera­tûrâ; *fis* ***f. nucleare*** (termo)bran­duo­línë si¹tezë; **2.** (*colata*) liejímas; **3.** *fig* su(si)jungí­mas; su(si)­viìni­ji­mas.

**fùso I** *dkt v* **1.** ve»pstas, ve»pstë; ***drit­to co­me un f.*** ít miìtà praríjæs; **2.:** ***f.*** ***orârio*** la¤ko júosta.

**fùso, -a II** *bdv, dlv* **1.** (ið)lôdytas; (*scioltosi*) susilôdæs; ***formâggio f.*** lôdytas s¿ris; **2.** (*colato*) lietínis; nu­lîetas; **3.** *bdv fam* nuva»gæs, iðsìkæs.

**fusoliéra** *dkt m* fiuzeliåþas, órlaivio liemuõ.

**fustâgno** *dkt v* mùltinas.

**fustigâre** *vksm* (*fù-*) (nu)plâkti (sù) rôkðte.

**fustìno** *dkt v* dëþùtë (*ppr. cilindrinë, biralams ir pan.*).

**fùsto** *dkt v* **1.** kamîenas; liemuõ; *bot* (*caule*) stîebas; virkðèiâ; **2.** (*bidone*) sta­tínë; bidònas; **3.** *archit* fù­stas; (kolònos) liemuõ; **4.** *fam* (*di qcn*) graþuõlis; r¸mas *fam*.

**fùtil|e** *bdv* nereikðmíngas; nerímtas; (*vano*) tùðèias; ***discorsi ~i*** tùðèios kaµbos; ***per ~i motivi*** dºl niì­kø, bê rimtõs prieþastiìs.

**futurìsmo** *dkt v* futurízmas.

**futurìsta 1.** *bdv* futurízmo; futurístinis; futurístø; **2.** *dkt v/m* futurístas -ë.

**futurìstico, -a** *bdv* futurístinis.

**futùr|o, -a** **1.** *bdv* b¿simas; (*del futuro*) ateitiìs; ***~a mamma*** bûsimâ mamâ; ***le generazioni ~e*** b¿si­mosios ka»tos; **2.** *dkt v* ateitís -iìs *femm*; ***pensare al f.*** galvóti apiì åteitá; ***prevedere il f.*** numatôti / sp¸ti åteitá; ¨ ***in*** ***f.*** ateityjê; ***senza f.*** bê ateitiìs; **3.** *dkt v gram* bûsimâsis la¤kas; ***f. anteriore, f. secondo, f. composto*** sudëtínis bûsimâsis la¤kas.
  `;
