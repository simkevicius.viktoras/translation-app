import { useState } from "react";
import { italianDB } from "../data/italianDB";
import { lithuanianDB } from "../data/lithuanianDB";

export default function TextConverter() {
  const [dataToSend, setDataToSend] = useState([]);

  function formatInput(db) {
    const array = db.split("\n");
    const array2 = array.map((ele) => ele.trim()); // išskirstytas pagal paragrafus ir pašalinti tušti tarpai nuo priekio
    const array3 = array2.filter((ele) => ele !== ""); // markdowne yra tuščių eilučių - pašalinam

    // Pašalinam pirmus du **
    const array4 = array3.map((ele) => {
      if (ele.startsWith("**")) {
        return ele.split(/\*\*(.*)/)[1];
      }
      return ele;
    });

    // Atskiriam pirma žodi nuo likusio teksto
    const array5 = array4.map((ele) => {
      return ele
        .split(/\*\*(.*)/)
        .filter((ele) => ele !== "")
        .map((ele) => ele.trim());
    });

    // Pasidarome objekta
    const arrayOfObjects1 = array5.map((ele) => {
      return {
        word: ele[0],
        rest: ele[1],
      };
    });

    //tikrinam ar prasideda * nes gali būti dar teksto iki pirmo *   \s(?=\*)   /\*(.*)/
    // const arrayOfObjects2 = arrayOfObjects1.map((ele) => {
    //   if (!ele.rest.startsWith("*")) {
    //     const arr = ele.rest.split(/\s(?=(\*(.*)))/); // find space before first * and select everything after
    //     return {
    //       ...ele,
    //       extra1: arr[0],
    //       rest: arr[1],
    //     };
    //   }
    //   return {
    //     ...ele,
    //     extra1: "",
    //   };
    // });

    //bandome išgauti tipa, tarp pakreipto teksto - ar tai daiktavardis būdvardis ...
    // const arrayOfObjects3 = arrayOfObjects2.map((ele) => {
    //   if (ele.rest.startsWith("*")) {
    //     const arr = ele.rest.split(/\*(.*)/)[1]; // panaikinam pirma *
    //     const arr2 = arr.split(/\*(.*)/); // splitinam per antra *
    //     return {
    //       ...ele,
    //       type: arr2[0],
    //       rest: arr2[1].trim(),
    //     };
    //   } else {
    //     return "error in arrayOfObjects3";
    //   }
    // });
    setDataToSend(arrayOfObjects1);
    // console.log(arrayOfObjects1);
  }

  console.log("dataToSend", dataToSend);

  // const backEndUrl = "http://localhost:5000/api/lt/postall";
  const backEndUrl = "http://localhost:5000/api/";

  async function postToLt() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(dataToSend),
    };
    try {
      let res = await fetch(`${backEndUrl}lt/postall`, requestOptions);
      if (!res.ok) {
        alert("We have problems with the connection :(");
        throw new Error(res.statusText);
      }
      const data = await res.json();
      console.log(data.message);
    } catch (err) {
      console.error("Error:", err);
    }
  }

  async function postToIt() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(dataToSend),
    };
    try {
      let res = await fetch(`${backEndUrl}it/postall`, requestOptions);
      if (!res.ok) {
        alert("We have problems with the connection :(");
        throw new Error(res.statusText);
      }
      const data = await res.json();
      console.log(data.message);
    } catch (err) {
      console.error("Error:", err);
    }
  }

  return (
    // <div>
    //   {array.map((ele, index) => (
    //     <div key={index}>{ele}</div>
    //   ))}
    // </div>
    <div className="main-container">
      <div className="post-btn-container">
        <button className="run" onClick={() => formatInput(lithuanianDB)}>
          Convert Lithuanian
        </button>
        <button className="run" onClick={() => formatInput(italianDB)}>
          Convert Italian
        </button>
      </div>
      <div className="post-btn-container">
        <button className="post-lt-btn" onClick={postToLt}>
          Post to LT
        </button>
        <button className="post-it-btn" onClick={postToIt}>
          Post to IT
        </button>
      </div>
    </div>
  );
}
