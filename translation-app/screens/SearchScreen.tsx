import { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Pressable,
  ScrollView,
} from "react-native";
import { Searchbar } from "react-native-paper";
import { backEndUrlConstant } from "../constants/constants";

import { useDispatch } from "react-redux";
import { addRecent } from "../store/slices/recentSlice";

import Animated, { FadeInUp, FadeOutDown } from "react-native-reanimated";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

export default function Search({ route, navigation }) {
  const lang = route.params.lang;
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const dispatch = useDispatch();
  let backEndUrl = `${backEndUrlConstant}/api/${lang}`;
  const screenToNavigateTo = `Translation${
    lang.toLowerCase().charAt(0).toUpperCase() + lang.slice(1).toLowerCase()
  }`;
  const [styles, theme] = useTheme(stylesFunction);
  const onChangeSearch = (query) => setSearchQuery(query);

  useEffect(() => {
    if (!searchQuery) return;
    const getSearchResults = setTimeout(async () => {
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ word: searchQuery }),
      };
      try {
        let res = await fetch(backEndUrl, requestOptions);
        if (!res.ok) {
          Alert.alert("Error", "We have problems with the connection :(", [
            { text: "OK", style: "destructive" },
          ]);
          throw new Error(res.statusText);
        }
        const data = await res.json();
        setSearchResults(data);
      } catch (err) {
        console.error("Error:", err);
      }
    }, 500);

    return () => {
      clearTimeout(getSearchResults);
    };
  }, [searchQuery, backEndUrl]);

  useEffect(() => {
    if (searchQuery.length === 0) {
      setSearchResults([]);
    }
  }, [searchQuery]);

  function handleSearchResultPress(ele) {
    dispatch(
      addRecent({
        id: ele._id,
        word: ele.word,
        lang,
      })
    );

    navigation.navigate(screenToNavigateTo, {
      id: ele._id,
      word: ele.word,
      lang,
    });
  }

  return (
    <>
      <View style={styles.mainContainer}>
        <View style={styles.textContainer}>
          {lang === "LT" ? (
            <Text style={styles.text}>Lithuanian - Italian translation</Text>
          ) : (
            <Text style={styles.text}>Italian - Lithuanian translation</Text>
          )}
        </View>

        <Animated.View
          style={styles.searchBarContainer}
          entering={FadeInUp}
          exiting={FadeOutDown}
        >
          <Searchbar
            placeholder="Search"
            onChangeText={onChangeSearch}
            value={searchQuery}
          />
        </Animated.View>

        {searchResults.length > 0 && searchQuery && (
          <Animated.View
            style={styles.searchResultsContainer}
            entering={FadeInUp}
            // exiting={FadeOutDown}
          >
            <ScrollView>
              {searchResults.map((ele) => {
                return (
                  <Animated.View
                    key={ele._id}
                    entering={FadeInUp}
                    // exiting={FadeOutDown}
                  >
                    <Pressable
                      android_ripple={{ color: "#c2c2c2" }}
                      onPress={() => handleSearchResultPress(ele)}
                    >
                      <View>
                        <Text style={styles.resultText}>{ele.word}</Text>
                      </View>
                    </Pressable>
                  </Animated.View>
                );
              })}
            </ScrollView>
          </Animated.View>
        )}
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    mainContainer: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      paddingVertical: 80,
    },
    searchBarContainer: {
      width: "80%",
      borderRadius: 50,
      overflow: "hidden",
      elevation: 5,
      zIndex: 10,
    },
    searchResultsContainer: {
      backgroundColor: "#fff",
      width: "80%",
      marginTop: -30,
      paddingTop: 40,
      elevation: 1,
      borderBottomEndRadius: 20,
      borderBottomStartRadius: 20,
      overflow: "hidden",
    },
    resultText: {
      padding: 15,
    },
    textContainer: {
      marginBottom: 20,
    },
    text: {
      color: Colors[theme].text,
    },
  });
};
