import { StyleSheet, Text, View, Pressable, Image } from "react-native";
import { Entypo, AntDesign } from "@expo/vector-icons";
import { useAppSelector } from "../store/hooks";

import Colors from "../global_styles/styles";
let colors;

export default function Home({ navigation }) {
  const darkMode = useAppSelector((state) => state.darkMode);

  darkMode ? (colors = Colors.dark) : (colors = Colors.light);

  function searchScreenButtonHandler(lang: string) {
    lang === "IT"
      ? navigation.navigate("Tabs", { screen: "SearchIt", lang })
      : navigation.navigate("Tabs", { screen: "SearchLt", lang });
  }

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.heading}>Italian-Lithuanian dictionary</Text>
        <View style={styles.imagesRow}>
          <Pressable
            style={styles.imagePressable}
            onPress={() => searchScreenButtonHandler("IT")}
          >
            <View style={styles.imageContainer}>
              <Image
                source={require("../assets/images/Italy-flag-icon.png")}
                style={styles.image}
              />
            </View>
          </Pressable>
          <Pressable
            style={styles.imagePressable}
            onPress={() => searchScreenButtonHandler("LT")}
          >
            <View style={styles.imageContainer}>
              <Image
                source={require("../assets/images/lt-flag-icon.png")}
                style={styles.image}
              />
            </View>
          </Pressable>
        </View>
        <View style={styles.featuresContainer}>
          <Entypo name="open-book" size={30} color={colors.primary200} />
          <Text style={styles.featuresText}>
            60 000 ++ Italian - Lithuanian
          </Text>
        </View>
        <View style={styles.featuresContainer}>
          <Entypo name="open-book" size={30} color={colors.primary200} />
          <Text style={styles.featuresText}>
            140 000 ++ Lithuanian - Italian
          </Text>
        </View>
        <View style={styles.featuresContainer}>
          <AntDesign name="sound" size={30} color={colors.primary500} />
          <Text style={styles.featuresText2}>
            Full pronunciation support for both languages
          </Text>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  heading: {
    fontSize: 40,
    textAlign: "center",
    fontWeight: "800",
    color: colors.primary500,
  },
  imagesRow: {
    flexDirection: "row",
    width: "100%",
    height: 100,
    marginVertical: 30,
    justifyContent: "center",
  },
  imagePressable: {
    width: "45%",
    height: "100%",
  },
  imageContainer: {
    width: "100%",
    height: "100%",
  },
  image: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
  featuresContainer: {
    flexDirection: "row",
    marginBottom: 5,
    alignItems: "center",
  },
  featuresText: {
    marginLeft: 10,
    fontSize: 15,
    color: colors.primary200,
    fontWeight: "700",
  },
  featuresText2: {
    marginLeft: 10,
    fontSize: 15,
    color: colors.primary500,
    fontWeight: "700",
  },
});
