import { useLayoutEffect, useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  useWindowDimensions,
} from "react-native";
import { useIsFocused } from "@react-navigation/native";
import RenderHtml from "react-native-render-html";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { backEndUrlConstant } from "../constants/constants";
import IconButton from "../components/UI/IconButton";
import FavoritesModal from "../components/FavoritesModal";

import { useAppSelector, useAppDispatch } from "../store/hooks";
import {
  removeFavorite,
  favoritesSliceType,
} from "../store/slices/favoritesSlice";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

type translationDataTypes = {
  _id: string;
  word: string;
  description?: string;
  examples?: string[][];
  translateToIt?: {
    _id: string;
    word: string;
    description?: string;
    examples?: string[][];
  };
  translateToLt?: {
    _id: string;
    word: string;
    description?: string;
    examples?: string[][];
  };
};

export default function Translation({ route, navigation }) {
  const wordId = route.params.id;
  const translationWord = route.params.word;
  const lang = route.params.lang;
  const dataUrl = `${backEndUrlConstant}/api/${lang}/${wordId}`;
  const [translationData, setTranslationData] = useState<translationDataTypes>(
    {} as translationDataTypes
  );
  const dispatch = useAppDispatch();
  const isFocused = useIsFocused();
  const favorites = useAppSelector((state) => state.favorites);
  const recents = useAppSelector((state) => state.recent);
  const [isInFavorites, setIsInFavorites] = useState(false);
  const [styles, theme] = useTheme(stylesFunction);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { width } = useWindowDimensions();

  //Patikrinam ar yra favorites ir kokia icona naudoti favorites mygtukui
  useEffect(() => {
    const checkFavorites =
      favorites?.find((category: favoritesSliceType) =>
        category.wordList?.find((ele) => ele._id === translationData._id)
      ) !== undefined
        ? true
        : false;
    setIsInFavorites(checkFavorites);
  }, [favorites, isFocused, route, translationData]);

  function onAddToFavoritesPress() {
    setIsModalVisible(true);
  }

  function onRemoveFromFavoritesPress() {
    dispatch(removeFavorite(translationData._id));
    // setIsInFavorites(false);
  }

  function onSettingsPress() {
    navigation.getParent("Drawer").openDrawer();
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      title: translationWord,
      headerRight: () => {
        return (
          <>
            {isInFavorites ? (
              <IconButton
                icon={"star"}
                size={30}
                onPress={onRemoveFromFavoritesPress}
                color="orange"
              />
            ) : (
              <IconButton
                icon={"star-outline"}
                size={30}
                onPress={onAddToFavoritesPress}
                color={Colors[theme].text}
              />
            )}

            <IconButton
              icon="settings-outline"
              size={30}
              onPress={onSettingsPress}
              color={Colors[theme].text}
            />
          </>
        );
      },
    });
  }, [
    isInFavorites,
    translationWord,
    onRemoveFromFavoritesPress,
    onAddToFavoritesPress,
    onSettingsPress,
    theme,
  ]);

  // Function to replace *** to bold and * to italic
  function boldBetweenStars(inputString: string) {
    const formattedString = inputString
      .replace(/\*\*\*([\w\s\W]+?)\*\*\*/g, "<b>$1</b>")
      .replace(/\*\*([\w\s\W]+?)\*\*/g, "<b>$1</b>")
      .replace(/\*([\w\s\W]+?)\*/g, "<i>$1</i>");

    return formattedString;
  }

  //Get translation data by ID
  useEffect(() => {
    async function getData() {
      try {
        const res = await fetch(dataUrl);
        if (!res.ok) {
          Alert.alert("Error", "We have problems with the connection :(", [
            { text: "OK", style: "destructive" },
          ]);
          throw new Error(res.statusText);
        }
        const data = await res.json();
        data.description = boldBetweenStars(data.description);
        setTranslationData(data);
      } catch (err) {
        console.error("Error:", err);
      }
    }
    getData();
  }, [dataUrl]);

  // source to HTML renderer package
  const source = {
    html: `${translationData?.description}`,
  };

  //Set favorites in storage
  useEffect(() => {
    async function setFavoritesInStorage() {
      await AsyncStorage.setItem("favorites", JSON.stringify(favorites));
    }
    setFavoritesInStorage();
  }, [favorites]);

  //Set recents in storage
  useEffect(() => {
    async function setFavoritesInStorage() {
      await AsyncStorage.setItem("recents", JSON.stringify(recents));
    }
    setFavoritesInStorage();
  }, [recents]);

  return (
    <>
      <View style={styles.mainContainer}>
        {isModalVisible && (
          <FavoritesModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            translationData={{
              _id: translationData._id,
              word: translationData.word,
              lang,
            }}
          />
        )}
        {translationData && (
          <>
            <View>
              <Text style={styles.word}>{translationData?.word}</Text>
              <RenderHtml
                contentWidth={width}
                source={source}
                baseStyle={{ fontSize: 20, color: Colors[theme].text }}
              />
              {/* <Text style={styles.description}>
                {translationData?.description}
              </Text> */}
              {/* {translationData?.examples?.map((ele, index) => {
                return (
                  <View key={index} style={styles.exampleLine}>
                    <Text style={styles.exampleTitle}>
                      {ele[0]} {" - "}
                      <Text style={styles.exampleDescription}>{ele[1]}</Text>
                    </Text>
                  </View>
                );
              })} */}
            </View>
            {/* <View style={styles.divider}>
              <View style={styles.dividerLine}></View>
            </View>
            <View>
              <Text style={styles.word}>
                {translationData.translateToIt?.word}
              </Text>
              <Text style={styles.description}>
                {translationData.translateToIt?.description}
              </Text>
              {translationData?.translateToIt?.examples?.map((ele, index) => {
                return (
                  <View key={index}>
                    <Text style={styles.exampleTitle}>
                      {ele[0]} {" - "}
                      <Text style={styles.exampleDescription}>{ele[1]}</Text>
                    </Text>
                  </View>
                );
              })}
            </View> */}
          </>
        )}
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    mainContainer: {
      padding: 30,
      flex: 1,
    },
    divider: {
      paddingVertical: 20,
    },
    dividerLine: {
      borderBottomWidth: 2,
      borderColor: Colors[theme].text,
    },
    exampleLine: {
      flexDirection: "row",
    },
    word: {
      fontSize: 25,
      color: Colors[theme].text,
      fontWeight: "600",
      marginBottom: 20,
    },
    description: {
      color: Colors[theme].text,
      marginTop: 10,
      marginBottom: 10,
    },
    exampleTitle: {
      color: Colors[theme].text,
      fontWeight: "600",
    },
    exampleDescription: {
      fontWeight: "300",
    },
  });
};
