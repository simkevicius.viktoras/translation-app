import { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Pressable,
  ScrollView,
  Alert,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { useAppSelector, useAppDispatch } from "../store/hooks";
import { favoritesSliceType } from "../store/slices/favoritesSlice";
import { deleteCategory } from "../store/slices/favoritesSlice";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

import IconButton from "../components/UI/IconButton";

export default function FavoritesScreen({ navigation }) {
  const favorites = useAppSelector((state) => state.favorites);
  const [styles, theme] = useTheme(stylesFunction);
  const dispatch = useAppDispatch();

  function handleSearchResultPress(ele) {
    if (ele.lang == "LT") {
      navigation.navigate("SearchLt", {
        screen: "TranslationLt",
        params: {
          id: ele._id,
          word: ele.word,
          lang: ele.lang,
        },
      });
    } else {
      navigation.navigate("SearchIt", {
        screen: "TranslationIt",
        params: {
          id: ele._id,
          word: ele.word,
          lang: ele.lang,
        },
      });
    }
  }

  function categoryDeleteHandler(name: string) {
    dispatch(deleteCategory(name));
  }

  function categoryDeleteButtonHandler(name: string) {
    Alert.alert(
      "Delete category",
      "Are you sure that you want to delete this category and all the words assigned to this category?",
      [
        {
          text: "Delete",
          style: "destructive",
          onPress: () => categoryDeleteHandler(name),
        },
        {
          text: "Cancel",
          style: "cancel",
        },
      ]
    );
  }

  //Set favorites in storage
  useEffect(() => {
    async function setFavoritesInStorage() {
      await AsyncStorage.setItem("favorites", JSON.stringify(favorites));
    }
    setFavoritesInStorage();
  }, [favorites]);

  return (
    <>
      <View style={styles.mainContainer}>
        <Text style={styles.heading}>Favorites</Text>
        <View>
          <ScrollView>
            {favorites.length > 0 ? (
              favorites.map((category: favoritesSliceType, index: number) => {
                return (
                  <View key={category.name}>
                    <View style={styles.categoryHeadingContainer}>
                      <Text style={styles.categoryHeading}>
                        {category.name}
                      </Text>
                      <IconButton
                        icon="close"
                        onPress={categoryDeleteButtonHandler.bind(
                          this,
                          category.name
                        )}
                        color={Colors[theme].text}
                        size={20}
                      />
                    </View>
                    {category.wordList.map((ele) => (
                      <View
                        style={styles.wordContainer}
                        key={category.name + ele._id}
                      >
                        <Pressable
                          android_ripple={{ color: "#c2c2c2" }}
                          onPress={() => handleSearchResultPress(ele)}
                        >
                          <Text style={styles.word}>{ele.word}</Text>
                        </Pressable>
                      </View>
                    ))}
                  </View>
                );
              })
            ) : (
              <Text style={styles.noFavsText}>No favorites added yet.</Text>
            )}
          </ScrollView>
        </View>
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    mainContainer: {
      padding: 50,
      flex: 1,
      backgroundColor: Colors[theme].bg,
    },
    heading: {
      fontSize: 30,
      fontWeight: "700",
      color: Colors[theme].text,
    },
    categoryHeadingContainer: {
      flexDirection: "row",
      marginTop: 30,
      justifyContent: "space-between",
      alignItems: "center",
    },
    categoryHeading: {
      color: Colors[theme].text,
      fontSize: 20,
    },
    wordContainer: {},
    word: {
      padding: 10,
      borderBottomWidth: 1,
      color: Colors[theme].text,
      borderColor: Colors[theme].text,
    },
    noFavsText: {
      fontSize: 20,
      color: Colors[theme].text,
    },
  });
};
