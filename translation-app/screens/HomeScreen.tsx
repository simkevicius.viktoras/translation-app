import {
  StyleSheet,
  Text,
  View,
  Pressable,
  Image,
  Dimensions,
} from "react-native";
import { Entypo, AntDesign } from "@expo/vector-icons";
import { useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";
import { Ionicons } from "@expo/vector-icons";

import { useAppDispatch } from "../store/hooks";
import { setFavoritesFromStorage } from "../store/slices/favoritesSlice";
import { setRecentsFromStorage } from "../store/slices/recentSlice";

export default function Home({ navigation }) {
  const [styles, theme] = useTheme(stylesFunction);
  const dispatch = useAppDispatch();
  const deviceWidth = Dimensions.get("window").width;

  function searchScreenButtonHandler(lang: string) {
    lang === "IT"
      ? navigation.navigate("Tabs", { screen: "SearchIt", lang })
      : navigation.navigate("Tabs", { screen: "SearchLt", lang });
  }

  //Get favorites from storage
  useEffect(() => {
    async function getFavoritesFromStorage() {
      const favoritesInStorage = await AsyncStorage.getItem("favorites");
      if (favoritesInStorage) {
        const convertedFavoritesInStorage = await JSON.parse(
          favoritesInStorage
        );
        dispatch(setFavoritesFromStorage(convertedFavoritesInStorage));
      }
    }
    getFavoritesFromStorage();
  }, []);

  //Get recents from storage
  useEffect(() => {
    async function getRecentsFromStorage() {
      const recentsInStorage = await AsyncStorage.getItem("recents");
      if (recentsInStorage) {
        const convertedRecentsInStorage = await JSON.parse(recentsInStorage);
        dispatch(setRecentsFromStorage(convertedRecentsInStorage));
      }
    }
    getRecentsFromStorage();
  }, []);

  return (
    <>
      <View style={styles.container}>
        <Text
          style={[styles.heading, { fontSize: deviceWidth < 370 ? 35 : 40 }]}
        >
          Italian-Lithuanian dictionary
        </Text>
        <View style={styles.imagesRow}>
          <Pressable
            style={styles.imagePressable}
            onPress={() => searchScreenButtonHandler("IT")}
          >
            <View style={styles.imageContainer}>
              <Image
                source={require("../assets/images/Italy-flag-icon.png")}
                style={styles.image}
              />
            </View>
          </Pressable>
          <Pressable
            style={styles.imagePressable}
            onPress={() => searchScreenButtonHandler("LT")}
          >
            <View style={styles.imageContainer}>
              <Image
                source={require("../assets/images/lt-flag-icon.png")}
                style={styles.image}
              />
            </View>
          </Pressable>
        </View>
        <View style={styles.featuresContainer}>
          <Entypo name="open-book" size={30} color={Colors[theme].heading} />
          <Text style={styles.featuresText}>
            Italian - Lithuanian Translation
          </Text>
        </View>
        <View style={styles.featuresContainer}>
          <Entypo name="open-book" size={30} color={Colors[theme].heading} />
          <Text style={styles.featuresText}>
            Lithuanian - Italian Translation
          </Text>
        </View>
        <Pressable
          style={({ pressed }) => [styles.button, pressed && styles.pressed]}
          onPress={() => navigation.getParent("Drawer").openDrawer()}
        >
          <View style={styles.settingsContainer}>
            <Text style={styles.featuresText}>Settings</Text>
            <Ionicons
              name="settings-outline"
              size={24}
              color={Colors[theme].heading}
            />
          </View>
        </Pressable>
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    heading: {
      fontSize: 40,
      textAlign: "center",
      fontWeight: "800",
      color: Colors[theme].heading,
    },
    imagesRow: {
      flexDirection: "row",
      width: "100%",
      height: 100,
      marginVertical: 30,
      justifyContent: "center",
    },
    imagePressable: {
      width: "45%",
      height: "100%",
    },
    imageContainer: {
      width: "100%",
      height: "100%",
    },
    image: {
      width: "100%",
      height: "100%",
      objectFit: "contain",
    },
    featuresContainer: {
      flexDirection: "row",
      marginBottom: 5,
      alignItems: "center",
    },
    featuresText: {
      marginLeft: 10,
      fontSize: 15,
      color: Colors[theme].text,
      fontWeight: "700",
    },
    featuresText2: {
      marginLeft: 10,
      fontSize: 15,
      color: Colors[theme].text,
      fontWeight: "700",
    },
    settingsContainer: {
      flexDirection: "row",
      marginBottom: 5,
      marginTop: 30,
      alignItems: "center",
      justifyContent: "center",
      gap: 10,
    },
  });
};
