import { StyleSheet, Text, View, Pressable, ScrollView } from "react-native";
import { useAppSelector, useAppDispatch } from "../store/hooks";
import {
  addRecent,
  removeRecent,
  recentSliceType,
} from "../store/slices/recentSlice";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

export default function Recents({ navigation }) {
  const recent = useAppSelector((state) => state.recent);
  const dispatch = useAppDispatch();
  const [styles, theme] = useTheme(stylesFunction);

  function handleSearchResultPress(ele: recentSliceType) {
    dispatch(
      addRecent({
        id: ele.id,
        word: ele.word,
        lang: ele.lang,
      })
    );
    if (ele.lang == "LT") {
      navigation.navigate("SearchLt", {
        screen: "TranslationLt",
        params: {
          id: ele.id,
          word: ele.word,
          lang: ele.lang,
        },
      });
    } else {
      navigation.navigate("SearchIt", {
        screen: "TranslationIt",
        params: {
          id: ele.id,
          word: ele.word,
          lang: ele.lang,
        },
      });
    }
  }

  return (
    <>
      <View style={styles.mainContainer}>
        <Text style={styles.heading}>Recents</Text>
        <ScrollView>
          {recent.length > 0 ? (
            recent.map((ele: recentSliceType, index: number) => {
              return (
                <View key={index} style={styles.wordContainer}>
                  <Pressable
                    android_ripple={{ color: "#c2c2c2" }}
                    onPress={() => handleSearchResultPress(ele)}
                  >
                    <Text style={styles.word}>{ele.word}</Text>
                  </Pressable>
                </View>
              );
            })
          ) : (
            <Text style={styles.noRecentsText}>No recent searches yet.</Text>
          )}
        </ScrollView>
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    mainContainer: {
      padding: 50,
      flex: 1,
      backgroundColor: Colors[theme].bg,
    },
    heading: {
      fontSize: 30,
      fontWeight: "700",
      marginBottom: 20,
      color: Colors[theme].text,
    },
    wordContainer: {},
    word: {
      padding: 10,
      borderBottomWidth: 1,
      color: Colors[theme].text,
      borderColor: Colors[theme].text,
    },
    noRecentsText: {
      fontSize: 20,
      color: Colors[theme].text,
    },
  });
};
