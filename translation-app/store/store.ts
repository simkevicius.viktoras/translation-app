import { configureStore } from "@reduxjs/toolkit";
import favoritesSlice from "./slices/favoritesSlice";
import recentSlice from "./slices/recentSlice";
import darkModeSlice from "./slices/darkModeSlice";

export const store = configureStore({
  reducer: {
    favorites: favoritesSlice,
    recent: recentSlice,
    darkMode: darkModeSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
