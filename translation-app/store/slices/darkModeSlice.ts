import { createSlice } from "@reduxjs/toolkit";

export interface darkModeType {
  isDarkMode: boolean;
}

const initialState: darkModeType = {
  isDarkMode: false,
};

export const darkModeSlice = createSlice({
  name: "darkMode",
  initialState,
  reducers: {
    toggle: (state) => {
      return {
        isDarkMode: !state.isDarkMode,
      };
    },
  },
});

export const { toggle } = darkModeSlice.actions;

export default darkModeSlice.reducer;
