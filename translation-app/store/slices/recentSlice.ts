import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface recentSliceType {
  id: string;
  word: string;
  lang: string;
}

const initialState: recentSliceType[] | [] = [];

export const recentSlice = createSlice({
  name: "recent",
  initialState,
  reducers: {
    addRecent: (state, { payload }: PayloadAction<recentSliceType>) => {
      if (state.length > 12) {
        const newRecent = [...state];
        newRecent.pop();
        newRecent.unshift(payload);
        return newRecent;
      }
      const newRecent = [payload, ...state];
      return newRecent;
    },
    removeRecent: (state) => {
      state = initialState;
    },
    setRecentsFromStorage: (
      state,
      { payload }: PayloadAction<recentSliceType[]>
    ) => {
      const newState = [...payload];
      return newState;
    },
  },
});

export const { addRecent, removeRecent, setRecentsFromStorage } =
  recentSlice.actions;

export default recentSlice.reducer;
