import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface favoritesSliceType {
  name: string;
  wordList: {
    _id: string;
    word: string;
    lang: string;
  }[];
}
export interface addFavoritePayloadType {
  name: string;
  favoriteWord: {
    _id: string;
    word: string;
    lang: string;
  };
}

const initialState: favoritesSliceType[] | [] = [];

export const favoritesSlice = createSlice({
  name: "favorites",
  initialState,
  reducers: {
    addCategory: (state, { payload }: PayloadAction<favoritesSliceType>) => {
      const newCategory = [payload, ...state];
      return newCategory;
    },
    addFavorite: (
      state,
      { payload }: PayloadAction<addFavoritePayloadType>
    ) => {
      const index = state.findIndex(
        (category: favoritesSliceType) => category.name === payload.name
      );
      state[index].wordList.push(payload.favoriteWord);
    },
    removeFavorite: (state, { payload }: PayloadAction<string>) => {
      const newState = [...state];
      const filteredFavorites = newState.forEach(
        (category: favoritesSliceType) =>
          (category.wordList = category.wordList.filter((ele) => {
            return ele._id !== payload;
          }))
      );

      return filteredFavorites;
    },
    setFavoritesFromStorage: (
      state,
      { payload }: PayloadAction<favoritesSliceType[]>
    ) => {
      const newState = [...payload];
      return newState;
    },
    deleteCategory: (
      state: favoritesSliceType[],
      { payload }: PayloadAction<string>
    ) => {
      return state.filter((category) => category.name !== payload);
    },
  },
});

export const {
  addCategory,
  removeFavorite,
  addFavorite,
  setFavoritesFromStorage,
  deleteCategory,
} = favoritesSlice.actions;

export default favoritesSlice.reducer;
