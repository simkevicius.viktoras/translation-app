import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import HomeScreen from "./screens/HomeScreen";
import FavoritesScreen from "./screens/FavoritesScreen";
import RecentsScreen from "./screens/RecentsScreen";
import SearchScreen from "./screens/SearchScreen";
import TranslationScreen from "./screens/TranslationScreen";
import Colors from "./global_styles/styles";
import DrawerComponent from "./components/Drawer";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import useTheme from "./hooks/useTheme";

const Stack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();

function InnerLtScreen() {
  const [styles, theme] = useTheme(stylesFunction);
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: Colors[theme].header },
        headerTintColor: Colors[theme].text,
        contentStyle: { backgroundColor: Colors[theme].bg },
      }}
    >
      <Stack.Screen
        name="SearchLtInner"
        component={SearchScreen}
        initialParams={{ lang: "LT" }}
        options={{ title: "Search" }}
      />
      <Stack.Screen name="TranslationLt" component={TranslationScreen} />
    </Stack.Navigator>
  );
}

function InnerItScreen() {
  const [styles, theme] = useTheme(stylesFunction);
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: Colors[theme].header },
        headerTintColor: Colors[theme].text,
        contentStyle: { backgroundColor: Colors[theme].bg },
      }}
    >
      <Stack.Screen
        name="SearchItInner"
        component={SearchScreen}
        initialParams={{ lang: "IT" }}
        options={{ title: "Search" }}
      />
      <Stack.Screen name="TranslationIt" component={TranslationScreen} />
    </Stack.Navigator>
  );
}

function MyTabs() {
  const [styles, theme] = useTheme(stylesFunction);
  return (
    <Tab.Navigator
      activeColor={Colors[theme].text}
      barStyle={{ backgroundColor: Colors[theme].header }}
      theme={{ colors: { secondaryContainer: "#fff" } }}
      screenOptions={{
        tabBarColor: Colors[theme].bg,
      }}
    >
      <Tab.Screen
        name="SearchIt"
        component={InnerItScreen}
        initialParams={{ lang: "IT" }}
        options={{
          title: "Italian",
          tabBarIcon: ({ color }) => (
            <View>
              <Image
                source={require("./assets/images/Italy-flag-icon.png")}
                style={{ width: 30, height: 20 }}
              ></Image>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="SearchLt"
        component={InnerLtScreen}
        initialParams={{ lang: "LT" }}
        options={{
          title: "Lithuanian",
          tabBarIcon: ({ color }) => (
            <View>
              <Image
                source={require("./assets/images/lt-flag-icon.png")}
                style={{ width: 30, height: 20 }}
              ></Image>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Recents"
        component={RecentsScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="time" size={24} color={Colors[theme].text} />
          ),
        }}
      />
      <Tab.Screen
        name="Favorites"
        component={FavoritesScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="star" size={24} color="orange" />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

function TopStacks() {
  const [styles, theme] = useTheme(stylesFunction);
  return (
    <Stack.Navigator
      screenOptions={{ contentStyle: { backgroundColor: Colors[theme].bg } }}
    >
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Tabs"
        component={MyTabs}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

export default function Layout() {
  const [styles, theme] = useTheme(stylesFunction);
  const statusBarTheme = theme === "dark" ? "light" : "dark";
  return (
    <>
      {/* <StatusBar style="light" /> */}
      <StatusBar style={statusBarTheme} animated={true} />

      <NavigationContainer>
        <Drawer.Navigator drawerContent={() => <DrawerComponent />} id="Drawer">
          <Drawer.Screen
            name="Homedrawer"
            component={TopStacks}
            options={{ headerShown: false }}
          />
        </Drawer.Navigator>
      </NavigationContainer>
    </>
  );
}

const stylesFunction = (theme: string) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
    },
  });
