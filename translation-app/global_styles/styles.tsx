const Colors = {
  dark: {
    primary500: "#000000",
    bg: "#141c30",
    text: "#ffffff",
    header: "#202a40",
    heading: "#ffffff",
    grey: "#DBDBDB",
  },
  light: {
    primary200: "#07e6dc",
    primary500: "#090979",
    bg: "#f2f2f2",
    text: "#141c30",
    header: "#ffffff",
    heading: "#202a40",
    grey: "#DBDBDB",
  },
};

export default Colors;
