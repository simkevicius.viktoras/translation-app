import { Drawer } from "react-native-paper";
import { StyleSheet, Text, View, Pressable, Image } from "react-native";
import { Switch } from "react-native-paper";

import { useAppSelector, useAppDispatch } from "../store/hooks";
import { toggle } from "../store/slices/darkModeSlice";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

export default function DrawerComponent() {
  const [styles, theme] = useTheme(stylesFunction);

  const dispatch = useAppDispatch();
  const isSwitchOn = useAppSelector((state) => state.darkMode.isDarkMode);

  function onToggleSwitch() {
    dispatch(toggle());
  }

  return (
    <>
      <View style={styles.container}>
        <View>
          <Text style={styles.heading}>Settings</Text>
          <View style={styles.darkModeContainer}>
            <Text style={styles.text}>Enable Dark Mode</Text>
            <Switch value={isSwitchOn} onValueChange={onToggleSwitch} />
          </View>
        </View>
      </View>
    </>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 100,
      paddingLeft: 50,
      backgroundColor: Colors[theme].bg,
    },
    heading: {
      fontSize: 20,
      fontWeight: "700",
      color: Colors[theme].text,
    },
    darkModeContainer: {
      flexDirection: "row",
      alignItems: "center",
      gap: 20,
    },
    text: {
      color: Colors[theme].text,
    },
  });
};
