import {
  Pressable,
  StyleSheet,
  View,
  StyleProp,
  ViewStyle,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

type IconButtonProps = {
  icon: keyof typeof Ionicons.glyphMap;
  color?: string;
  size?: number;
  onPress: () => void;
  containerStyles?: StyleProp<ViewStyle>;
  pressableStyles?: StyleProp<ViewStyle>;
};

export default function IconButton({
  icon,
  color,
  size,
  onPress,
  containerStyles,
  pressableStyles,
}: IconButtonProps) {
  return (
    <View style={containerStyles}>
      <Pressable
        onPress={onPress}
        style={({ pressed }) => [
          styles.button,
          pressed && styles.pressed,
          pressableStyles,
        ]}
      >
        <Ionicons name={icon} color={color} size={size} />
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    margin: 8,
    borderRadius: 20,
  },
  pressed: {
    opacity: 0.7,
  },
});
