import { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Pressable,
  Modal,
  Button,
  TextInput,
  ScrollView,
} from "react-native";

import { useAppSelector, useAppDispatch } from "../store/hooks";
import {
  addCategory,
  addFavorite,
  favoritesSliceType,
} from "../store/slices/favoritesSlice";

import Colors from "../global_styles/styles";
import useTheme from "../hooks/useTheme";

import IconButton from "./UI/IconButton";
import { Ionicons } from "@expo/vector-icons";

type ModalType = {
  isModalVisible: boolean;
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  translationData: {
    _id: string;
    word: string;
    lang: string;
  };
};

export default function FavoritesModal({
  isModalVisible,
  setIsModalVisible,
  translationData,
}: ModalType) {
  const dispatch = useAppDispatch();
  const favorites = useAppSelector((state) => state.favorites);
  const [styles, theme] = useTheme(stylesFunction);
  const [newCategoryName, setNewCategoryName] = useState("");

  function submitHandler() {
    if (
      favorites.find(
        (category: favoritesSliceType) => category.name === newCategoryName
      )
    ) {
      Alert.alert("Category with this name already exists");
      return;
    }
    if (newCategoryName === "") {
      Alert.alert("Please enter a category name first.");
      return;
    }

    dispatch(
      addCategory({
        name: newCategoryName,
        wordList: [
          {
            _id: translationData._id,
            word: translationData.word,
            lang: translationData.lang,
          },
        ],
      })
    );
    setNewCategoryName("");
    setIsModalVisible(false);
  }

  function categoryPressHandler(name: string) {
    if (
      favorites.find((category: favoritesSliceType) =>
        category.wordList.find((ele) => ele._id === translationData._id)
      )
    ) {
      Alert.alert("This word already exists in this category");
      return;
    }

    dispatch(
      addFavorite({
        name,
        favoriteWord: {
          _id: translationData._id,
          word: translationData.word,
          lang: translationData.lang,
        },
      })
    );
    setIsModalVisible(false);
  }

  function closeModal() {
    setIsModalVisible(false);
  }

  return (
    <Modal visible={isModalVisible} transparent={true} animationType="slide">
      <View style={styles.mainContainer}>
        {/* Close button */}
        <View style={styles.closeContainerStyles}>
          <Pressable onPress={closeModal} style={styles.closePressableStyles}>
            <Ionicons name="close" color={Colors[theme].text} size={40} />
          </Pressable>
        </View>

        <View style={styles.modalContainer}>
          <ScrollView style={styles.modalContainerScrollView}>
            {favorites.length > 0 && (
              <View style={styles.categoryListContainerToCenter}>
                <View style={styles.categoryListContainer}>
                  <Text style={styles.listHeading}>Choose a category</Text>
                  <View style={styles.scrollViewContainer}>
                    <ScrollView>
                      {favorites?.map((category) => (
                        <Pressable
                          key={category.name}
                          onPress={categoryPressHandler.bind(
                            this,
                            category.name
                          )}
                          style={styles.listPressable}
                        >
                          <Text style={styles.word}>{category.name}</Text>
                        </Pressable>
                      ))}
                    </ScrollView>
                  </View>
                </View>
              </View>
            )}
            <View style={styles.newCategoryContainer}>
              <Text style={styles.addNewHeading}>Add new Category</Text>
              <View style={styles.formContainer}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Enter new category name"
                  value={newCategoryName}
                  onChangeText={(value) => setNewCategoryName(value)}
                  placeholderTextColor={Colors[theme].text}
                  onSubmitEditing={submitHandler}
                />
                <Button
                  title="Add"
                  onPress={submitHandler}
                  // color={Colors[theme].text}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}

const stylesFunction = (theme: string) => {
  return StyleSheet.create({
    mainContainer: {
      margin: 30,
      flex: 1,
      backgroundColor: Colors[theme].header,
      justifyContent: "center",
      alignItems: "center",
      elevation: 5,
      borderRadius: 20,
      position: "relative",
    },
    modalContainer: {
      justifyContent: "center",
      alignItems: "center",
      width: "90%",
      paddingVertical: 30,
      // backgroundColor: "blue",
    },
    modalContainerScrollView: {
      // justifyContent: "center",
      // alignItems: "center",
      width: "100%",
    },
    categoryListContainerToCenter: {
      width: "100%",
      alignItems: "center",
      justifyContent: "center",
    },
    categoryListContainer: {
      width: "80%",
      alignItems: "center",
      justifyContent: "center",
    },
    scrollViewContainer: {
      width: "100%",
      maxHeight: "90%",
    },
    listHeading: {
      fontSize: 20,
      color: Colors[theme].text,
      fontWeight: "600",
      paddingBottom: 20,
    },
    addNewHeading: {
      fontSize: 20,
      color: Colors[theme].text,
      fontWeight: "600",
    },
    listPressable: {
      width: "100%",
      alignItems: "center",
    },
    word: {
      padding: 10,
      borderBottomWidth: 1,
      color: Colors[theme].text,
      borderColor: Colors[theme].text,
      width: "100%",
    },
    newCategoryContainer: {
      alignItems: "center",
      paddingTop: 50,
    },
    formContainer: {
      flexDirection: "row",
      marginTop: 15,
    },
    textInput: {
      paddingVertical: 5,
      paddingHorizontal: 20,
      borderWidth: 1,
      borderColor: Colors[theme].grey,
      color: Colors[theme].text,
    },
    closeContainerStyles: {
      position: "absolute",
      top: 10,
      right: 10,
      zIndex: 100,
    },
    closePressableStyles: {
      position: "absolute",
      top: 0,
      right: 0,
      zIndex: 100,
    },
  });
};
