import { useAppSelector } from "../store/hooks";

export default function useTheme(stylesFunction) {
  const darkMode = useAppSelector((state) => state.darkMode);
  let styles;
  let theme;
  darkMode.isDarkMode
    ? (styles = stylesFunction("dark"))
    : (styles = stylesFunction("light"));
  darkMode.isDarkMode ? (theme = "dark") : (theme = "light");
  return [styles, theme];
}
